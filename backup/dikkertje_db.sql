-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2020 at 07:18 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `jw_dikkertje`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `admin_menu_id` int(1) UNSIGNED NOT NULL,
  `admin_menu_title` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_menu_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_menu_method` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_menu_icon` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_menu_order` int(1) DEFAULT NULL,
  `admin_menu_importance` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`admin_menu_id`, `admin_menu_title`, `admin_menu_link`, `admin_menu_method`, `admin_menu_icon`, `admin_menu_order`, `admin_menu_importance`) VALUES
(1, 'Pages Management', 'pages_management', 'pages_management', 'fa fa-info', 1, 0),
(2, 'Menu Management', 'menu_management', 'menu_management', 'fa fa-bars', 2, 0),
(3, 'Settings', 'settings_management', 'settings_management', 'fa fa-wrench', 2, 0),
(5, 'Posts Categories', 'management/posts_categories', 'management/posts_categories', 'fa fa-cogs', 3, 0),
(6, 'Newsletter Subscribers', 'management/newsletter_subscribers', 'newsletter_subscribers', 'fa fa-envelope-o', 2, 0),
(8, 'Blogs Categories', 'blogs_categories_management', 'blogs_categories_management', 'fa fa-book', 3, 0),
(9, 'Page Content Management', 'page_content_management', 'page_content_management', 'fa fa-edit', 4, 0),
(14, 'Meta Contents', 'meta_content_management', 'meta_content_management', 'fa fa-edit', 8, 1),
(11, 'Users', 'users_management', 'users_management', 'fa fa-user', 1, 0),
(13, 'Portfolio', 'portfolio_management', 'portfolio_management', 'fa fa-edit', 4, 1),
(22, 'Site Gallery', 'site_gallery_management', 'site_gallery_management', 'fa fa-image', 5, 2),
(18, 'Admin Menu Management', 'admin_menu_management', 'admin_menu_management', 'fa fa-bars', 2, 0),
(19, 'Main Slider', 'slider_management', 'slider_management', 'fa fa-edit', 2, 1),
(20, 'Product Categories', 'products_categories_management', 'products_categories_management', 'fa fa-edit', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banner_id` int(1) NOT NULL,
  `banner_name` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_description` text COLLATE utf8mb4_unicode_ci,
  `banner_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_small_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_large_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner_id`, `banner_name`, `banner_description`, `banner_slug`, `banner_small_image`, `banner_large_image`) VALUES
(1, 'Casual Style', 'Smart-casual, μία ορολογία που έχεις ακουστέι και γραφτέι πολλές φορές, αλλά σε τι αναφέρεται στην πραγματικότητα; Εδω στην POSH BOUTIQUE υπάρχουν τα ρούχα που ανήκουν σε αυτή την κατηγορία και αφορούν κομμάτια με τα οποία αισθάνεσαι άνετα, αλλά παράλληλα είσαι και προσεγμένος, χωρίς να είσαι επίσημα ντυμένος. Μπορείς να φορέσεις τέτοια ρούχα παντού, στη δουλειά ή ακόμη και για να πας να δεις έναν αγώνα με την αγαπημένη σου ομάδα.Μπορεί να έχεις διάφορα ρούχα μέσα στη ντουλάπα σου, τα οποία είναι διαχρονικά, αλλά πολλά από αυτά δεν ανήκουν στην κατηγορία των smart-casual ρούχων. Το bomber jacket είναι μία από τις τελευταίες τάσεις της μόδας, και φοριέται ολοένα και περισσότερο από τους άντρες. Η αλήθεια είναι πως είναι δύσκολο να συνδυάσεις ένα τέτοιο σακάκι με τα ρούχα σου γιατί κάνει το ντύσιμο σου πιο επίσημο. Ευτυχώς, όμως, είναι η ιδανική λύση για να έχεις μια smart-casual εμφάνιση. Επέλεξε ένα δερμάτινο, suede ή μάλλινο μπουφάν και συνδύασε το με ένα μονόχρωμο παντελόνι κι ένα ζευγάρι brogues παπούτσια. Ένα statement σακάκι μπορεί να προκαλέσει διάφορες αντιδράσεις στους γύρω σου. Παρόλα αυτά είναι ένα ρούχο που ολοκληρώνει το σύνολο σου. Προτίμησε να φορέσεις ένα απλό λευκό T-shirt και ένα μαύρο τζιν παντελόνι, και συνδύασε το με ένα statement σακάκι. Αμέσως, η εμφάνιση σου γίνεται πιο εκλεπτυσμένη.Αυτό που πρέπει να θυμάσαι είναι να διατηρήσεις τα υπόλοιπα ρούχα απλά για να ξεχωρίσει το σακάκι σου. Μπορείς να επιλέξεις ένα σακάκι με κάποιο σχέδιο ή ακόμη και ένα σακάκι σε διαφορετική απόχρωση ή με φόδρα που έχει επίσης κάποιο σχέδιο. Το εμπριμέ πουκάμισο είναι ένα ρούχο που πιθανόν να έχεις μέσα στη ντουλάπα σου. Αν θέλεις να έχει μια απλή αλλά κολακευτική εμφάνιση, τότε θα σου πρότεινα να αγοράσεις ένα πουκάμισο με λεπτές βούλες ή διακριτικό καρό σχέδιο. Από την άλλη, όμως, τα floral σχέδια και τα λαχούρια κάνουν τους ώμους και τα μπράτσα σου να φαίνονται πιο πλατιά. Ένα πουλόβερ δίνει άλλο στυλ στην εμφάνιση σου και θα έπρεπε να υπάρχει στη ντουλάπα σου όλο το χρόνο και για κάθε περίσταση.Δοκίμασε να φορέσεις ένα πουλόβερ με ένα Oxford πουκάμισο και ένα μάλλινο παντελόνι. Για να αποφύγεις τη μονοτονία, επέλεξε σχέδια σε χειμωνιάτικα χρώματα και απλά σχέδια. Το τζιν είναι ένα παντελόνι που μπορείς να το φορέσεις όλη μέρα. Το σκούρο, όμως, τζιν μπορεί να «ανεβάσει» την εμφάνιση σου σε άλλο επίπεδο. Συνδύασε το με ένα πουκάμισο κι ένα σακάκι και θα έχεις ένα ντύσιμο που θα ζηλεύουν όλοι. Ένα ακόμη πράγμα που πρέπει να έχεις είναι μερικά αξεσουάρ. Τα αξεσουάρ είναι ένα σημαντικό κομμάτι της εμφάνισης σου και μπορούν να συμπληρώσουν το σύνολο σου. Ένα κασκόλ, μια τσάντα, ένα ζευγάρι κατάλληλα παπούτσια, ή ένα ρολόι αποτελούν την κατάλληλη επιλογή για εσένα.\n\n', NULL, NULL, '74e80-casual.jpg'),
(2, 'Street Style', 'Μία απο τις κατηγορίες στο ντύσιμο που προσφέρει η Posh Boutique ειναι και το Street Style. Πρωτοεμφανίστηκε το 1978 απο τον Bill Cunningham,  φωτογράφος των New York Times, ο οποίος κυκλοφορεί στη Νέα Υόρκη με το ποδηλατάκι του, καταγράφει τους πιο stylish ανθρώπους της πόλης του και δημιουργεί κάθε Κυριακή τη διάσημη στήλη του On The Street. Γι’ αυτό και η ιδιαίτερη οπτική του πάνω στο θέμα γέννησε ουσιαστικά τη σχολή του street-style φωτογράφου, κάπου στα μέσα της δεκαετίας του 2000. Εκεί περίπου θεωρούμε ότι τοποθετείται χρονικά το σημείο όπου υπήρξε ένας κορεσμός σε σχέση με την κουλτούρα των celebrities και το celebrity style ως κυρίαρχο σημείο αναφοράς στη μόδα και το στυλ. Εξάλλου, όλες και όλοι οι σταρ φορούσαν τα ρούχα, τα αξεσουάρ και τα κοσμήματα όχι επειδή τα είχαν πληρώσει, αλλά ως κινούμενα billboards μέσα στο παιχνίδι του product placement. Την ίδια στιγμή, η ίδια η μόδα και το styling άρχισαν να είναι λιγότερο αυστηρές. Τα trends δεν έμοιαζαν πλέον ασφυκτικές επιταγές από τις οποίες δεν μπορούσες να ξεφύγεις, αλλά άρχισαν να θυμίζουν περισσότερο απλές συστάσεις. Κι όλα αυτά γιατί η κάνουλα με το χρήμα άρχισε σιγά σιγά να στρίβει και να σφίγγει. Δεν μπορούσες πλέον να ταυτιστείς με τους σταρ, ήταν αδύνατον πλέον όλα τα ρούχα και τα αξεσουάρ να κρατούν μόνο μία σεζόν. Ολοι και όλες άρχισαν να στρέφονται στο «προσωπικό στυλ». Πραγματα που ήδη υπήρχαν μέσα στην ντουλάπα σε καινούργιους συνδυασμούς, μεταποιήσεις για να αλλάξει πρόσωπο ένα πολυφορεμένο κομμάτι, έξυπνο accessorizing, όλα αυτά τα πραγματάκια άρχισαν να γεννούν τους νέους style stars της Μητέρας Ανάγκης: Ανθρώπους με ιδιαίτερο, εντελώς δικό τους στυλ, οι οποίοι ξαφνικά άρχισαν να κυκλοφορούν μέσα στην πόλη ομορφαίνοντάς την, ή έστω κάνοντάς την να δείχνει διαφορετική\n\n', 'cccaaa', NULL, '42a38-streetstyletop3.jpg'),
(3, 'Office Style', '<p>\r\n	&Kappa;ά&theta;&epsilon; &mu;έ&rho;&alpha; &kappa;&iota; έ&nu;&alpha; &sigma;&tau;&upsilon;&lambda;&iota;&sigma;&tau;&iota;&kappa;ό &delta;ί&lambda;&eta;&mu;&mu;&alpha;: &laquo;&Tau;&iota; &nu;&alpha; &phi;&omicron;&rho;έ&sigma;&omega; &sigma;ή&mu;&epsilon;&rho;&alpha; &sigma;&tau;&eta; &delta;&omicron;&upsilon;&lambda;&epsilon;&iota;ά;&raquo;. &Alpha;&nu; &alpha;&nu;&omicron;ί&xi;&epsilon;&iota;&sigmaf; &tau;&eta; &nu;&tau;&omicron;&upsilon;&lambda;ά&pi;&alpha; &sigma;&omicron;&upsilon; &kappa;&alpha;&iota; &alpha;&rho;&chi;ί&sigma;&epsilon;&iota;&sigmaf; &nu;&alpha; &pi;&alpha;&rho;&alpha;&tau;&eta;&rho;&epsilon;ί&sigmaf; &tau;&alpha; &rho;&omicron;ύ&chi;&alpha; &sigma;&omicron;&upsilon;, &theta;&alpha; &delta;&epsilon;&iota;&sigmaf; &pi;&omega;&sigmaf; &pi;ά&nu;&tau;&alpha; &upsilon;&pi;ά&rho;&chi;&omicron;&upsilon;&nu; &epsilon;&pi;&iota;&lambda;&omicron;&gamma;έ&sigmaf;, &alpha;&kappa;ό&mu;&eta; &kappa;&iota; &alpha;&nu; &epsilon;&sigma;ύ &kappa;ά&pi;&omicron;&iota;&epsilon;&sigmaf; &phi;&omicron;&rho;έ&sigmaf; &tau;&iota;&sigmaf; &alpha;&gamma;&nu;&omicron;&epsilon;ί&sigmaf;. &Pi;ώ&sigmaf;, &lambda;&omicron;&iota;&pi;ό&nu;, &mu;&pi;&omicron;&rho;&epsilon;ί&sigmaf; &nu;&alpha; &alpha;&nu;&alpha;&beta;&alpha;&theta;&mu;ί&sigma;&epsilon;&iota;&sigmaf; &tau;&omicron; office style &sigma;&omicron;&upsilon; &kappa;&alpha;&iota; &nu;&alpha; &delta;&iota;&alpha;&lambda;έ&gamma;&epsilon;&iota;&sigmaf; &pi;&iota;&omicron; chic &sigma;ύ&nu;&omicron;&lambda;&alpha; &gamma;&iota;&alpha; &tau;&omicron; &gamma;&rho;&alpha;&phi;&epsilon;ί&omicron;; &Omicron;&iota; &pi;&alpha;&rho;&alpha;&kappa;ά&tau;&omega; 3 &alpha;&pi;&lambda;&omicron;ί &kappa;&alpha;&nu;ό&nu;&epsilon;&sigmaf;, &theta;&alpha; &sigma;&epsilon; &beta;&omicron;&eta;&theta;ή&sigma;&omicron;&upsilon;&nu; &nu;&alpha; &tau;&omicron; &pi;&epsilon;&tau;ύ&chi;&epsilon;&iota;&sigmaf;. &Kappa;&Alpha;&Nu;&Omicron;&Nu;&Alpha;&Sigma; 1: Έ&chi;&epsilon; &pi;ά&nu;&tau;&alpha; &phi;&rho;&epsilon;&sigma;&kappa;&omicron;&sigma;&iota;&delta;&epsilon;&rho;&omega;&mu;έ&nu;&omicron; έ&nu;&alpha; &mu;&omicron;&nu;ό&chi;&rho;&omega;&mu;&omicron; &kappa;&omicron;&sigma;&tau;&omicron;ύ&mu;&iota;. &Mu;&pi;&omicron;&rho;&epsilon;ί&sigmaf; &nu;&alpha; &tau;&omicron; &phi;&omicron;&rho;έ&sigma;&epsilon;&iota;&sigmaf; &omega;&sigmaf; deux pieces, ή &nu;&alpha; &delta;&iota;&alpha;&lambda;έ&xi;&epsilon;&iota;&sigmaf; &tau;&omicron; &sigma;&alpha;&kappa;ά&kappa;&iota;, ή &tau;&omicron; &pi;&alpha;&nu;&tau;&epsilon;&lambda;ό&nu;&iota;, &omega;&sigmaf; &beta;ά&sigma;&epsilon;&iota;&sigmaf; &gamma;&iota;&alpha; &nu;&alpha; &delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;ή&sigma;&epsilon;&iota;&sigmaf; &epsilon;&upsilon;&phi;ά&nu;&tau;&alpha;&sigma;&tau;&alpha; looks. &Kappa;&Alpha;&Nu;&Omicron;&Nu;&Alpha;&Sigma; 2: &Mu;&eta; &phi;&omicron;&beta;ά&sigma;&alpha;&iota; &tau;&omicron; mix n&#39; match. Έ&nu;&alpha;&sigmaf; &alpha;&pi;ό &tau;&omicron;&upsilon;&sigmaf; &beta;&alpha;&sigma;&iota;&kappa;&omicron;ύ&sigmaf; &lambda;ό&gamma;&omicron;&upsilon;&sigmaf; &pi;&omicron;&upsilon; &sigma;&tau;&epsilon;&rho;&epsilon;ύ&epsilon;&iota;&sigmaf; &alpha;&pi;ό &iota;&delta;έ&epsilon;&sigmaf;, &kappa;ά&theta;&epsilon; &phi;&omicron;&rho;ά &pi;&omicron;&upsilon; &pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &delta;&iota;&alpha;&lambda;έ&xi;&epsilon;&iota;&sigmaf; &tau;&iota; &theta;&alpha; &phi;&omicron;&rho;έ&sigma;&epsilon;&iota;&sigmaf; &sigma;&tau;&omicron; &gamma;&rho;&alpha;&phi;&epsilon;ί&omicron;, &epsilon;ί&nu;&alpha;&iota; &tau;&omicron; &gamma;&epsilon;&gamma;&omicron;&nu;ό&sigmaf; &pi;&omega;&sigmaf; &delta;&epsilon; &sigma;&kappa;έ&phi;&tau;&epsilon;&sigma;&alpha;&iota; &delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;&iota;&kappa;ά. &Tau;ό&lambda;&mu;&eta;&sigma;έ &tau;&omicron; &kappa;&alpha;&iota; &pi;&alpha;ί&xi;&epsilon; &mu;&epsilon; &delta;&iota;&alpha;&phi;&omicron;&rho;&epsilon;&tau;&iota;&kappa;έ&sigmaf; &tau;ά&sigma;&epsilon;&iota;&sigmaf;, &phi;ό&rho;&epsilon;&sigma;&epsilon; έ&nu;&tau;&omicron;&nu;&alpha; &chi;&rho;ώ&mu;&alpha;&tau;&alpha; &mu;&alpha;&zeta;ί, &kappa;ά&nu;&omicron;&nu;&tau;&alpha;&sigmaf; color-blocking, ή &alpha;&kappa;ό&mu;&eta; &kappa;&alpha;&iota; prints &mu;&epsilon; &delta;&iota;&alpha;&phi;&omicron;&rho;&epsilon;&tau;&iota;&kappa;ά &mu;&omicron;&tau;ί&beta;&alpha; - &alpha;&rho;&kappa;&epsilon;ί, &tau;&omicron; &alpha;&pi;&omicron;&tau;έ&lambda;&epsilon;&sigma;&mu;&alpha; &nu;&alpha; έ&chi;&epsilon;&iota; &alpha;&rho;&mu;&omicron;&nu;ί&alpha;. &Kappa;&Alpha;&Nu;&Omicron;&Nu;&Alpha;&Sigma; 3: &Chi;&rho;&epsilon;&iota;ά&zeta;&epsilon;&sigma;&alpha;&iota; &tau;&rho;ί&alpha; &zeta;&epsilon;&upsilon;&gamma;ά&rho;&iota;&alpha; &pi;&alpha;&pi;&omicron;ύ&tau;&sigma;&iota;&alpha;. &Gamma;&iota;&alpha; &tau;&omicron; &phi;&theta;&iota;&nu;ό&pi;&omega;&rho;&omicron; &kappa;&alpha;&iota; &tau;&omicron; &chi;&epsilon;&iota;&mu;ώ&nu;&alpha;, &chi;&rho;&epsilon;&iota;ά&zeta;&epsilon;&sigma;&alpha;&iota; &omicron;&pi;&omega;&sigma;&delta;ή&pi;&omicron;&tau;&epsilon; έ&nu;&alpha; &zeta;&epsilon;&upsilon;&gamma;ά&rho;&iota; &mu;&alpha;ύ&rho;&alpha;, έ&nu;&alpha; &zeta;&epsilon;&upsilon;&gamma;ά&rho;&iota; &kappa;&alpha;&phi;έ &kappa;&alpha;&iota; έ&nu;&alpha; &zeta;&epsilon;&upsilon;&gamma;ά&rho;&iota; &mu;&pi;&lambda;έ &pi;&alpha;&pi;&omicron;ύ&tau;&sigma;&iota;&alpha;! &Upsilon;&pi;ά&rho;&chi;&epsilon;&iota; &phi;&upsilon;&sigma;&iota;&kappa;ά &kappa;&alpha;&iota; &omicron; &Kappa;&Alpha;&Nu;&Omicron;&Nu;&Alpha;&Sigma; 4 &omicron; &omicron;&pi;&omicron;ί&omicron;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &kappa;&alpha;&iota; &omicron; &pi;&iota;&omicron; &sigma;&eta;&mu;&alpha;&nu;&tau;&iota;&kappa;ό&sigmaf; &kappa;&alpha;&iota; &delta;&epsilon;&nu; &alpha;&nu;&alpha;&phi;έ&rho;&theta;&eta;&kappa;&epsilon; &pi;&iota;&omicron; &pi;ά&nu;&omega;, &pi;&omicron;&upsilon; &delta;&epsilon;&nu; &epsilon;ί&nu;&alpha;&iota; ά&lambda;&lambda;&omicron;&sigmaf; &alpha;&pi;&omicron; &tau;&omicron; &nu;&alpha; &epsilon;&pi;&iota;&sigma;&kappa;&epsilon;&phi;&tau;&epsilon;ί &kappa;&alpha;&nu;έ&iota;&sigmaf; &tau;&eta;&nu; POSH BOUTIQUE &gamma;&iota;&alpha; &nu;&alpha; &beta;&rho;&epsilon;&iota; ό&chi;&iota; &mu;ό&nu;&omicron; &tau;&alpha; &pi;&iota;&omicron; &pi;ά&nu;&omega; &alpha;&lambda;&lambda;ά &kappa;&alpha;&iota; &tau;&iota;&sigmaf; &iota;&delta;έ&epsilon;&sigmaf; &sigma;&tau;&alpha; &sigma;ύ&nu;&omicron;&lambda;&alpha; &kappa;&alpha;&iota; &sigma;&upsilon;&nu;&delta;&upsilon;&alpha;&sigma;&mu;&omicron;ύ&sigmaf;!</p>\r\n', NULL, NULL, '9a258-men-shirts-office-styles-natty-shirts-shop.jpg'),
(4, 'Wedding', 'Είτε πρόκειται να παντρευτείς είτε να παραστείς σε γάμο, πρέπει να διαλέξεις το σύνολο που σε αντιπροσωπεύει! Στην POSH Boutique μπορείς να βρεις και τις δύο αυτές επιλογές. Μια τεράστια γκάμα από γαμπριάτικα κοστούμια για την σημαντική αυτή μέρα της ζωής ενός άντρα που νυμφεύεται καθώς επίσης μια τεράστια συλλογή από κοστούμια για κάθε σημαντική δεξίωση.\n\nΚοστούμια όμως και προτάσεις υπάρχουν ακόμη και για το γραφείο, αφού δεν είναι λίγοι οι άντρες αυτοί που επιλέγουν καθημερινά στο γραφείο το κοστούμι, είτε γιατί το επιβάλει η εργασία είτε γιατί αυτό είναι το στυλ που τον χαρακτηρίζει.', 'wedding', NULL, '1a570-wedding_style_posh_boutique_cyprus.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `blog_id` int(1) NOT NULL,
  `blog_related_category_id` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `blog_related_user_id` int(11) UNSIGNED NOT NULL,
  `blog_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_description` text COLLATE utf8mb4_unicode_ci,
  `blog_image` varchar(455) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_created_at` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`blog_id`, `blog_related_category_id`, `blog_related_user_id`, `blog_title`, `blog_description`, `blog_image`, `blog_created_at`) VALUES
(25, 1, 1, 'A Gastronomical experience on board ', '<p>\r\n	Feel the joys of the mediterranean sea in a private boat,</p>\r\n<p>\r\n	filled with taste.</p>\r\n<p>\r\n	The trip includes lisence captain , cyprus culinary delights , drinks , fishing and photoshot.</p>\r\n<p>\r\n	Starting point : Limassol Marina</p>\r\n<p>\r\n	Ending point&nbsp; : Limassol Marina</p>\r\n<p>\r\n	Please contact us for further information.</p>\r\n', '4103d-side-1-00000003-.jpg', '1495996622');

-- --------------------------------------------------------

--
-- Table structure for table `blogs_categories`
--

CREATE TABLE `blogs_categories` (
  `blogs_category_id` int(1) UNSIGNED NOT NULL,
  `blogs_category_name` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blogs_category_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blogs_category_slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blogs_category_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs_categories`
--

INSERT INTO `blogs_categories` (`blogs_category_id`, `blogs_category_name`, `blogs_category_description`, `blogs_category_slug`, `blogs_category_image`) VALUES
(1, 'News', 'News', 'news', NULL),
(2, 'Tip of the Day', 'Tip of the Day', 'tip-of-the-day', NULL),
(3, 'Upcoming Events', 'Upcoming Events', 'upcoming-events', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blogs_tags`
--

CREATE TABLE `blogs_tags` (
  `blog_tag_id` int(1) UNSIGNED NOT NULL,
  `blog_tag_title` int(11) NOT NULL,
  `blog_tag_language_id` int(1) UNSIGNED NOT NULL,
  `blog_tag_blog_id` int(1) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `collection_id` int(1) NOT NULL,
  `collection_main_category_related_id` int(1) NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_description` text COLLATE utf8mb4_unicode_ci,
  `collection_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_gallery_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_order` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `collection_visible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`collection_id`, `collection_main_category_related_id`, `collection_name`, `collection_description`, `collection_image`, `collection_gallery_image`, `collection_link`, `collection_order`, `collection_visible`) VALUES
(3, 1, 'Berna Italia', NULL, '28644-berna_posh_boutique.jpg', 'acc43-berna_collection.jpg', NULL, 2, 1),
(5, 1, 'Takeshy Kurosawa', NULL, 'c981b-takeshi_kurosawa_posh_boutique.jpg', '9e696-takeshy_collection.jpg', NULL, 7, 1),
(6, 1, 'Imperial', NULL, 'd90c2-imperial_posh_boutique.jpg', 'b19fc-imperial_collection.jpg', NULL, 8, 1),
(7, 1, 'Me&My', NULL, '040f2-me_my_posh_boutique.jpg', '136e2-me-and-my-ss17-1.jpg', NULL, 3, 1),
(8, 1, 'Makis Tselios', NULL, 'e6ef8-makis_tselios_posh_boutique.jpg', '99f8f-makis_collection.png', NULL, 5, 1),
(10, 1, 'Ralf New York', NULL, 'f0b17-ralf_new_york_posh_boutique.jpg', 'c090b-ralf.png', NULL, 5, 1),
(11, 1, 'Project X Paris', NULL, '69ecb-project_x_posh_boutique.jpg', 'd4af7-projectparis_collection.jpg', NULL, 4, 1),
(12, 1, 'Absolut Joy', NULL, NULL, 'b0277-absolut_joy_posh_cyprus_collection_summer_2017.jpg', NULL, 4, 1),
(14, 1, 'Below', NULL, NULL, '6062c-below_posh_cyprus.jpg', NULL, 4, 1),
(15, 1, 'Suits', NULL, NULL, '47367-16175825_1435673173123512_965765431350525952_n.jpg', NULL, 3, 1),
(16, 1, 'Santa Monica Polo Club', NULL, NULL, 'd8c83-santa_monica_collection.jpg', NULL, 4, 1),
(17, 1, 'Oliver Queen', NULL, NULL, '4be14-oliver_queen.jpg', NULL, 4, 1),
(18, 1, 'Posh', '<p>\r\n	Posh</p>\r\n', NULL, '429e7-posh_boutique_collection_cyprus.jpg', NULL, 0, 1),
(19, 2, 'G.IOS', NULL, NULL, 'c7725-14572199_1140911842661550_5655978477295153233_n.jpg', NULL, 0, 1),
(20, 2, 'Sorbino', NULL, NULL, 'ac407-14681700_1307809955910150_6941688823718511444_n.jpg', NULL, 0, 1),
(21, 2, 'Project X Paris', NULL, NULL, 'ada7e-15107491_1228611217199294_2829564140384497767_n.jpg', NULL, 0, 1),
(22, 2, 'IMPERIAL', '<p>\r\n	IMPERIAL COLLECTION WINTER 2016 /17</p>\r\n', NULL, 'e6e9b-imperial-man20521-1.jpg', NULL, 0, 1),
(23, 2, 'Berna Italia', NULL, NULL, 'aca66-bernauomo_fw2016-17-8.jpg', NULL, 0, 1),
(24, 2, 'Makis Tselios', NULL, NULL, '840d2-tselios_winter_posh.png', NULL, 0, 1),
(25, 2, 'Takeshy Kurosawa', NULL, NULL, '24479-lookbookfw1617art-page-002.jpg', NULL, 0, 1),
(26, 2, 'Me&My', NULL, NULL, '19ff5-me_my_posh_cyprus_winter.jpg', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `collections_main_categories`
--

CREATE TABLE `collections_main_categories` (
  `collections_main_category_id` int(1) NOT NULL,
  `collections_main_category_title` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collections_main_category_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collections_main_category_slug` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collections_main_category_is_current` tinyint(1) DEFAULT '1',
  `collections_main_category_is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `collections_main_category_order` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `collections_main_categories`
--

INSERT INTO `collections_main_categories` (`collections_main_category_id`, `collections_main_category_title`, `collections_main_category_description`, `collections_main_category_slug`, `collections_main_category_is_current`, `collections_main_category_is_visible`, `collections_main_category_order`) VALUES
(1, 'Spring/Summer 2017', '', 'ss-17', 1, 1, 1),
(2, 'Winter 2016', '', 'winter-16', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `color_id` int(11) NOT NULL,
  `color_hex` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_title` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`color_id`, `color_hex`, `color_title`) VALUES
(1, '#DFDFDF', 'White'),
(2, '#000000', 'Black'),
(3, '#AAA', 'Grey'),
(4, '#333', 'Green'),
(5, '#FF3333', 'Red'),
(6, '#1A2AEA', 'Blue'),
(7, '#873600', 'Brown'),
(8, '#F1C40F', 'Yellow');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `language_id` int(1) UNSIGNED NOT NULL,
  `language_settings_id` int(1) UNSIGNED NOT NULL,
  `language_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_slug` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_code` int(50) DEFAULT NULL,
  `language_order` int(1) DEFAULT NULL,
  `language_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`language_id`, `language_settings_id`, `language_name`, `language_slug`, `language_code`, `language_order`, `language_image`, `is_default`, `is_active`) VALUES
(1, 1, 'Dutch', 'nl', NULL, 1, '', 0, 1),
(2, 1, 'Greek', 'ru', NULL, 2, '', 0, 1),
(3, 0, 'German', 'de', 0, 3, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `language_vars`
--

CREATE TABLE `language_vars` (
  `lang_var_id` int(10) NOT NULL,
  `lang_var_related_settings_id` int(1) NOT NULL DEFAULT '1',
  `lang_var_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang_var_text_en` longtext COLLATE utf8_unicode_ci,
  `lang_var_text_gr` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 CHECKSUM=1 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language_vars`
--

INSERT INTO `language_vars` (`lang_var_id`, `lang_var_related_settings_id`, `lang_var_key`, `lang_var_text_en`, `lang_var_text_gr`) VALUES
(1, 1, 'text_11', 'asdas', 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `main_gallery`
--

CREATE TABLE `main_gallery` (
  `main_gallery_id` int(1) NOT NULL,
  `main_gallery_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_gallery_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_gallery_priority` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_gallery`
--

INSERT INTO `main_gallery` (`main_gallery_id`, `main_gallery_title`, `main_gallery_url`, `main_gallery_priority`) VALUES
(6, NULL, '856f5-27199769-cyprus-wallpapers.jpg', 1),
(7, NULL, 'a1f29-38709716-cyprus-wallpapers.jpg', 2),
(8, NULL, 'e36b2-27351961-cyprus-wallpapers.jpg', 3),
(12, NULL, '0b36a-38840281-cyprus-wallpapers.jpg', 4),
(13, NULL, '1eae3-39195195-cyprus-wallpapers.jpg', 5),
(14, NULL, '61db5-39986073-cyprus-wallpapers.jpg', 6),
(15, NULL, '7b58b-protaras-cyprus_537206696.jpg', 7);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_slug` varchar(155) NOT NULL,
  `menu_language_id` int(1) UNSIGNED NOT NULL,
  `menu_template` varchar(255) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL,
  `menu_link` varchar(255) DEFAULT NULL,
  `sub_menu` int(10) UNSIGNED DEFAULT '0',
  `page_id` int(10) UNSIGNED DEFAULT NULL,
  `is_parent` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `has_sub_menu` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_header` tinyint(4) NOT NULL DEFAULT '1',
  `show_in_footer` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(10) UNSIGNED DEFAULT NULL,
  `menu_order` int(10) UNSIGNED DEFAULT NULL,
  `public` int(1) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_slug`, `menu_language_id`, `menu_template`, `menu_icon`, `menu_link`, `sub_menu`, `page_id`, `is_parent`, `parent_id`, `has_sub_menu`, `show_in_header`, `show_in_footer`, `created_date`, `menu_order`, `public`) VALUES
(1, 'Het Restaurant', 'het-restaurant', 1, NULL, NULL, NULL, 0, 2, 1, 1, 0, 1, 1, 1407879004, 1, 1),
(2, 'Reserveren', 'reserveren', 1, NULL, NULL, 'services', NULL, NULL, 1, 1, 0, 1, 1, NULL, 2, 1),
(12, 'УСЛУГИ', 'services', 2, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 1, 1, NULL, 4, 1),
(24, 'дом', '', 2, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 0, 1),
(26, 'О КОМПАНИИ', 'about', 2, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 2, NULL),
(27, 'Menukaart', 'menukaart', 1, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 6, 1),
(28, 'Contact Us', 'contact', 1, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 8, 1),
(29, 'Home', '', 1, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 0, 1),
(34, 'карьера', 'career', 2, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 1, NULL, 5, 1),
(33, 'ФЛОТ', 'fleet', 2, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 5, 1),
(35, 'КОНТАКТЫ', 'contact', 2, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 6, 1),
(37, 'Startseite', '', 3, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 0, 1),
(38, 'Über uns', 'about', 3, NULL, NULL, NULL, 0, 2, 1, 1, 0, 1, 1, 1407879004, 1, 1),
(39, 'Dienstleistungen', 'services', 3, NULL, NULL, 'services', NULL, NULL, 1, 1, 0, 1, 1, NULL, 2, 1),
(40, 'Flotte', 'fleet', 3, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 4, 1),
(41, 'Karriere', 'career', 3, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 6, 1),
(42, 'Kontaktiere uns', 'contact', 3, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 8, 1),
(44, 'Recensies', 'recensies', 1, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 1, NULL, 6, 1),
(45, 'Gastenboek', 'gastenboek', 1, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 1, 1, NULL, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `meta_content`
--

CREATE TABLE `meta_content` (
  `meta_content_id` int(1) UNSIGNED NOT NULL,
  `meta_content_related_page_id` int(11) NOT NULL,
  `meta_content_meta_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_content_meta_secondary_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_content_meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_content_meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_content_meta_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_content`
--

INSERT INTO `meta_content` (`meta_content_id`, `meta_content_related_page_id`, `meta_content_meta_title`, `meta_content_meta_secondary_title`, `meta_content_meta_description`, `meta_content_meta_keywords`, `meta_content_meta_image`) VALUES
(1, 1, 'Home', NULL, 'We are a family owned business based in Limassol, Cyprus. We have created many great projects and homes for the people giving them quality and comfort. Our 30 years experience', 'construction,limassol,cyprus', NULL),
(2, 6, 'Our portfolio', NULL, 'View our projects. Our 30year experience has let us to many great projects, creating sweet homes for the people giving them quality and comfort. Our passion and extensive experience will lead us to many more.', 'portfolio,projects,limassol,cyprus', NULL),
(3, 5, 'Our Services', 'Our Services', 'D&A homes are built for life and the way you live it. You’ll go over every step together and address your questions before construction begins.', 'services,christodoulou,homes,limassol,cyprus', NULL),
(4, 2, 'About Us', 'About Us', 'We are a family owned business based in Limassol, Cyprus.The company handles new projects and refurbishments. The company also collaborates with Architects, Civil Engineers', NULL, NULL),
(5, 3, 'Contact Us', 'Contact Us', 'Contact Us. The company handles new projects and refurbishments. The company also collaborates with Architects, Civil Engineers, and Electrical & Mechanical Engineers along with a large list of Sub-contractors.', 'contact', NULL),
(6, 43, 'Terms & Conditions', NULL, 'The Terms and Conditions apply to all visitors and users of the Website. Please thus read these Terms and Conditions carefully before accessing, browsing and/or using the Website.', 'Terms, visitors, Website, da,christodoulou, limassol,cyprus', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_subscribers`
--

CREATE TABLE `newsletter_subscribers` (
  `subscriber_id` int(10) UNSIGNED NOT NULL,
  `subscriber_name` int(155) DEFAULT NULL,
  `subscriber_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscriber_created_at` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletter_subscribers`
--

INSERT INTO `newsletter_subscribers` (`subscriber_id`, `subscriber_name`, `subscriber_email`, `subscriber_created_at`) VALUES
(7, NULL, 'aasd@dsa.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(1) UNSIGNED NOT NULL,
  `page_type` int(1) NOT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_slug` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_template` text COLLATE utf8mb4_unicode_ci,
  `page_content` text COLLATE utf8mb4_unicode_ci,
  `is_public` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_order` int(1) NOT NULL,
  `page_language_id` int(1) UNSIGNED NOT NULL,
  `page_meta_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_meta_secondary_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_type`, `page_name`, `page_slug`, `page_template`, `page_content`, `is_public`, `page_order`, `page_language_id`, `page_meta_title`, `page_meta_secondary_title`, `page_meta_description`, `page_meta_keywords`) VALUES
(1, 1, 'Welcome', 'welcome', 'main_slider', NULL, '1', 1, 1, 'Home', 'Home', 'Lazarou', 'Lazarou'),
(2, 0, 'About', 'het-restaurant', '_about', '<div class=\"row\">\n<div class=\"col-md-12\">\n<h2>Heading</h2>\n<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p>\n<p><a class=\"btn btn-default\" href=\"#\" role=\"button\">View details »</a></p>\n</div>\n</div>\n<p><br /><br /><br /></p>', '1', 0, 1, 'About Us', 'About Us', 'LCLC Properties Cyprus', 'About Us'),
(3, 0, 'contact', 'contact', '_contact', '<form name=\"sentMessage\" id=\"contactForm\" novalidate=\"\">\n<div class=\"row\">\n<div class=\"col-md-6\">\n<div class=\"form-group\"></div>\n</div>\n<div class=\"col-md-6\"></div>\n</div>\n</form>', '1', 1, 1, 'Contact Us', 'Contact Us', 'LCLC Properties Cyprus', 'Contact Us'),
(5, 0, 'menukaart', 'menukaart', '_menukart', NULL, '1', 1, 1, 'menukaart', 'menukaart', 'menukaart', 'menukaart'),
(6, 0, 'Reserveren', 'reserveren', '_reservation', NULL, '1', 1, 1, 'Reserveren', 'Reserveren', 'Reserveren', 'Reserveren'),
(41, 0, 'Fly now DE', 'fly-now', NULL, NULL, '', 0, 0, 'Fliege jetzt', 'Fliege jetzt', 'Fliege jetzt', 'Fliege jetzt'),
(43, 0, 'terms', 'terms', '_terms', NULL, '1', 0, 1, 'Terms & Conditions', NULL, 'Terms & Conditions D.A Christodoulou Limassol Cyprus ', 'terms,christodoulou');

-- --------------------------------------------------------

--
-- Table structure for table `page_content`
--

CREATE TABLE `page_content` (
  `page_content_id` int(1) UNSIGNED NOT NULL,
  `page_content_type_related_id` int(1) UNSIGNED NOT NULL,
  `page_content_related_id` int(1) UNSIGNED NOT NULL,
  `page_content_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_content_data` text COLLATE utf8mb4_unicode_ci,
  `page_content_template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `page_content_order` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_content`
--

INSERT INTO `page_content` (`page_content_id`, `page_content_type_related_id`, `page_content_related_id`, `page_content_name`, `page_content_data`, `page_content_template`, `is_public`, `page_content_order`) VALUES
(3, 2, 2, 'about1', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/msaviation_bg_about.jpg\'); background-size: cover;\"></div>\r\n<!-- Start Supertabs -->\r\n<p></p>\r\n<!-- Start About -->\r\n<section class=\"about section_padding\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>About MS Aviation</h2>\r\n<p></p>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble -->\r\n<div class=\"row\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<div class=\"funfact-contant\">\r\n<div class=\"sectn-title\">\r\n<h2>We are a group of industry professionals in business aviation</h2>\r\n</div>\r\n<!-- /.section-titel -->\r\n<p class=\"mb-40\">The Austrian company MS Aviation was founded by a group of industry professionals in business aviation. Every member of our team realizes that the standards for our area of work are very high. And we are ready to exceed all the quality expectations of our individual and corporative customers.</p>\r\n<p class=\"mb-40\">MS Aviation focuses on aviation asset management – from technical support and solving operational issues to the financial enhancement of business jet ownership. Our company renders services that cover the entire scope of aircraft management.</p>\r\n</div>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<div class=\"funfact-img\"><img class=\"block\" src=\"{base_url()}assets/images/about/msaviation_about_1.jpg\" alt=\"MSaviation About Image1\" /></div>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble --> <!-- End Header-Preamble -->\r\n<div class=\"row padding-top-40\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<div class=\"funfact-img\"><img class=\"block\" src=\"{base_url()}assets/images/about/msaviation_about_2.jpg\" alt=\"MSaviation About Image2\" /></div>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<div class=\"funfact-contant\">\r\n<div class=\"sectn-title\">\r\n<h2>We stay in line with all the European standards of safety and security</h2>\r\n</div>\r\n<!-- /.section-titel -->\r\n<p class=\"mb-40\">Our team works to protect and secure the financial interests of aircraft owners. That’s why our main principal is to make all aircraft management processes clear and fully transparent. MS Aviation optimizes the costs of our clients and stays in line with all the European standards of safety and security.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble --></div>\r\n</section>\r\n<!-- End About -->\r\n<p></p>\r\n<!-- Start Supertabs -->\r\n<section class=\"supertabs no-top\">\r\n<div class=\"container\"><!-- Start Header-Preamble -->\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>Our Team</h2>\r\n<p>Specialists of the MS Aviation company are devoted to the work. Every member of our team has significant experience in private jet management. Working together we can meet any complex challenge.</p>\r\n<p>EVERY MEMBER OF OUR TEAM HAS SIGNIFICANT EXPERIENCE IN PRIVATE JET MANAGEMENT.</p>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble -->\r\n<div class=\"row\">\r\n<div class=\"team\">\r\n<div class=\"col-lg-4 col-md-4 col-sm-6 member text-center\">\r\n<div class=\"member-inner\"><img src=\"{base_url()}assets/images/team/591_maikl_maier__1.jpg\" alt=\"maikl maier\" /></div>\r\n<h5>Michael Mayer</h5>\r\n<p><span>Co-Founder, CEO and Accountable Manager</span></p>\r\n</div>\r\n<div class=\"col-lg-4 col-md-4 col-sm-6 member text-center\">\r\n<div class=\"member-inner\"><img src=\"{base_url()}assets/images/team/591_torsten_kincl_0.jpg\" alt=\"torsten kincl\" /></div>\r\n<h5>Thorsten Kinzl</h5>\r\n<p><span>Nominated Person continuing airworthiness</span></p>\r\n</div>\r\n<div class=\"col-lg-4 col-md-4 col-sm-6 member text-center\">\r\n<div class=\"member-inner\"><img src=\"{base_url()}assets/images/team/591_roland_malek_1.jpg\" alt=\" roland malek \" /></div>\r\n<h5>Roland Malek</h5>\r\n<p><span>Co - Founder and Managin Director of Operations</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<!-- End Supertabs -->\r\n<p></p>\r\n<!-- Start About -->\r\n<p></p>', '_about', 1, 1),
(5, 2, 1, 'slider template file', '<p><span style=\"font-size: 36pt;\">sfadas ds</span></p>', 'main_slider', 1, 1),
(36, 1, 8, 'financial-consulting 1', '<div class=\"clearfix\"></div>\r\n<div class=\"titlebar two\">\r\n<div class=\"container\">\r\n<div class=\"breadcrumb\">\r\n<div class=\"row\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-6\">\r\n<h1>Property</h1>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6\">\r\n<div class=\"pagenation\"><a href=\"{site_url()}\">Home</a> <i class=\"fa fa-angle-right\"></i> Property</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"clearfix\"></div>\r\n<section class=\"section_category4\">\r\n<div class=\"container\">\r\n<h4 class=\"center\">Christofi Group has initiated the construction of some of the biggest ever Residential and Commercial Property Developments in Cyprus creating local landmarks.</h4>\r\n<div class=\"margin-top4\"></div>\r\n</div>\r\n</section>\r\n<section class=\"section_category4\">\r\n<div class=\"container\">\r\n<div class=\"clearfix\"></div>\r\n<div class=\"row\">\r\n<div class=\"col-lg-7 col-md-6 col-sm-12 col-xs-12 \">\r\n<div class=\"sec_title\">\r\n<h1 style=\"font-weight: 500;\">Riviera Cyprus</h1>\r\n</div>\r\n<div class=\"clearfix margin-top1\"></div>\r\n<p>We have initiated the design of some of the biggest ever Residential and Commercial Property Developments in Cyprus creating local landmarks. Combining luxury Apartments, elegant Residences, exclusive Villas and a commercial vibrant Shopping Centre with enticing mix of Recreational, Retail and Office Area to create a unique experience through architectural masterpieces. Cooperating with some of the most professional and internationally known consultants like ESA Architecture, RAMBOLL Engineers, Chris Blandford Associates, Bouygues Batiment International and more.</p>\r\n<p>Riviera is by far the most exiting new development in Nicosia. We are owners of the largest plot of land in Nicosia 176.000 sqm by the river which can be developed for residential and commercial purpose.</p>\r\n<p>Situated in an undisturbed area, only a few minutes from the center of Nicosia and with easy access to the Limassol and Larnaca highway as well as Troodos mountain highway, Riviera is designed in meticulous detail to match international standards of high-end residential and commercial development.</p>\r\n<p>Riviera has been designed to provide a new residential community of:</p>\r\n<ul>\r\n<li>apartments buildings,</li>\r\n<li>courtyard houses,</li>\r\n<li>detached and semi-detached houses,</li>\r\n<li>villas and exclusive villas with extensive gardens</li>\r\n</ul>\r\n<p>but also includes a commercial center with a variety of buildings to accommodate offices, shops, restaurants, cafeterias and extensive social and recreational facilities.</p>\r\n</div>\r\n<div class=\"col-lg-5 col-md-6 col-sm-12 col-xs-12 \" data-anim-type=\"fade-in-right\" data-anim-delay=\"100\">\r\n<div class=\"feature-box3 text-center\">\r\n<div class=\"iconbox-large round dark-outline\"><img src=\"{base_url()}assets/images/logo/logo_w.png\" alt=\"riviera cyprus\" /></div>\r\n<a href=\"http://www.rivieracyprus.com.cy\" class=\"btn btn-default\">View Website</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<section class=\"section_category4\">\r\n<div class=\"container\">\r\n<div class=\"margin-top4\"></div>\r\n<div class=\"row\">\r\n<div class=\"col-lg-7 col-md-6 col-sm-12 col-xs-12 \">\r\n<div class=\"sec_title\">\r\n<h1 style=\"font-weight: 500;\">Epias22</h1>\r\n</div>\r\n<div class=\"clearfix margin-top1\"></div>\r\n<p>Construction of a four-storey apartment building with semi- basement and parking spaces. Building consist of 16 apartments, 8 No. 2 bedrooms and 8 No. 3 bedrooms. Each apartment is entitled to a storage area and 2 parking spots in the semi-basement area.</p>\r\n<div class=\"margin-top2\"></div>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"col-lg-5 col-md-6 col-sm-12 col-xs-12 \" data-anim-type=\"fade-in-right\" data-anim-delay=\"100\">\r\n<div class=\"feature-box3 text-center\">\r\n<div class=\"iconbox-large round dark-outline\"><img src=\"{base_url()}assets/images/logo/epias22_f.png\" alt=\"epias22\" /></div>\r\n<a href=\"http://epias22.com/\" target=\"_blank\" class=\"btn btn-default\">View Website</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<section class=\"section_category4\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-lg-7 col-md-6 col-sm-12 col-xs-12 \">\r\n<div class=\"sec_title\">\r\n<h1 style=\"font-weight: 500;\">Empire</h1>\r\n</div>\r\n<p>Waterfront Condominiums is a unique development, on the beachfront of Limassol, Cyprus, to cater to the needs of affluent international investors.</p>\r\n<p>It consists of a 30 storey tower with luxurious condominiums with unique and privileged access to facilities on the beach (across the street) including, yacht club, restaurant, bar, night club, spa and its own private beach.</p>\r\n<p>With state of the art architectural design and building materials, wonderfully landscaped gardens, parking and all the amenities of a modern residence.</p>\r\n<p>The condominiums have uninterrupted view of the beautiful south western Mediterranean sea, all-year-round bright blue sky and spectacular sunsets.</p>\r\n<div class=\"sec_title\">\r\n<h1 style=\"font-weight: 500;\">Whitemoon Building</h1>\r\n</div>\r\n<p>A landmark building situated in the centre of Engomi Municipality since 2005 with a total area of 12,600 m2 of business headquarters and commercial retail hotspots with on-level and underground controlled parking. The premises that are being fully renovated offer State-of-the-art facilities and services, including manned reception, 24-hour security and controlled access to parking &amp; building areas, property management Retail and F&amp;B venues.</p>\r\n<div class=\"margin-top2\"></div>\r\n<div class=\"margin-top2\"></div>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"col-lg-5 col-md-6 col-sm-12 col-xs-12 \" data-anim-type=\"fade-in-right\" data-anim-delay=\"100\"></div>\r\n</div>\r\n</div>\r\n</section>', NULL, 1, 1),
(10, 2, 3, 'Contact English', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/contacts-build.jpg\'); background-size: cover;\"></div>\r\n<section class=\"supertabs no-top\">\r\n<div class=\"container\">\r\n<div class=\"col-md-12\"><!-- End Map Locations --></div>\r\n</div>\r\n</section>', '_contact', 1, 1),
(54, 0, 0, '', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/contacts-build.jpg\'); background-size: cover;\"></div>\r\n<!-- Start Contact -->\r\n<section class=\"contact-map style-2 section_padding\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header \">\r\n<h2>КОНТАКТЫ</h2>\r\n<p>Вы можете написать нам и мы с вами свяжемся в течение 2х часов.</p>\r\n</div>\r\n</div>\r\n<!-- End Map Contact -->\r\n<div class=\"container\"><!-- Start Contact-Form -->\r\n<div class=\"col-md-8 col-xs-offset-2\">\r\n<div class=\"contact-form\">\r\n<h5 class=\"contact-form-title\">Send us a message</h5>\r\n<img src=\"img/divisor2.png\" alt=\"\" class=\"divisor\" /><form action=\"index.html\" class=\"default-form\">\r\n<p class=\"alert-message warning\"><i class=\"ico fa fa-exclamation-circle\"></i> All fields are required!</p>\r\n<div class=\"row\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" placeholder=\"Name\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" placeholder=\"Phone\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" placeholder=\"Email\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" placeholder=\"Topic\" /></p>\r\n</div>\r\n</div>\r\n<p class=\"form-row\"><textarea name=\"message\" placeholder=\"How we can help?\"></textarea></p>\r\n<button class=\"btn light\">Send Message</button></form></div>\r\n</div>\r\n<!-- End Contact-Form --></div>\r\n</section>\r\n<!-- End Contact -->\r\n<p></p>\r\n<!-- Start Locations -->\r\n<section class=\"locations\">\r\n<div class=\"container\">\r\n<div class=\"row\"><!-- Start Location-Agency -->\r\n<div class=\"location-agency\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"location-agency-title\">MS Aviation Австрия</h3>\r\n<h5 class=\"location-agency-subtitle\">Office</h5>\r\n<ul class=\"custom-list location-agency-contact col-lg-6 col-md-6\">\r\n<li>IZ NÖ-Süd, Strasse 3, Objekt 1, Top 1, Wiener</li>\r\n<li>Neudorf, Austria 2355</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h3 class=\"location-agency-title\">MS Аviation Россия Представительство в Москве:</h3>\r\n<h5 class=\"location-agency-subtitle\">Office</h5>\r\n<ul class=\"custom-list location-agency-contact col-lg-6 col-md-6\">\r\n<li>Глинищевский переулок дом 3, офис 203-204, Москва, Россия 125009</li>\r\n<li>Tel: +7 495 295 00 30 e-mail: aa@lfs.group | ad@lfs.group</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h3 class=\"location-agency-title\">MS Аviation Представительство в Украине:</h3>\r\n<h5 class=\"location-agency-subtitle\">Office</h5>\r\n<ul class=\"custom-list location-agency-contact col-lg-6 col-md-6\">\r\n<li>ул. Пироговского, 18, 3 этаж, Киев, Украина 03110</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<!-- End Location-Agency --></div>\r\n</div>\r\n</section>\r\n<!-- End Locations -->\r\n<section class=\"supertabs no-top\">\r\n<div class=\"container\"><!-- Start Header-Preamble -->\r\n<div class=\"col-md-12\"><!-- Start Map Locations -->\r\n<div id=\"map-locations\"></div>\r\n<!-- End Map Locations --></div>\r\n</div>\r\n</section>', NULL, 1, 1),
(45, 2, 5, 'Services', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/msaviation_img_2.jpg\'); background-size: cover;\"></div>\n<!-- Start Supertabs -->\n<section class=\"supertabs section_padding\">\n<div class=\"container\"><!-- Start Header-Preamble -->\n<div class=\"row\">\n<div class=\"col-sm-12 section-header\">\n<h2>Our Services</h2>\n<p>Learn more about the services we provide in private jet management.</p>\n</div>\n</div>\n<!-- End Header-Preamble -->\n<div class=\"row\"><!-- Start Tabs-Navigation -->\n<div class=\"col-lg-4 col-md-4 tabs-navigation\">\n<ul class=\"custom-list tab-title-list\">\n<li data-href=\"#yacht\">\n<h5>Operational Management</h5>\n<p>Design,control,maintenance</p>\n</li>\n<li data-href=\"#fleet\">\n<h5>TECHNICAL MANAGEMENT</h5>\n<p>best specialists and maintenance</p>\n</li>\n<li data-href=\"#drivers\">\n<h5>Financial Management</h5>\n<p>monitoring of expenditures</p>\n</li>\n<li data-href=\"#ourlocs\">\n<h5>Crewing Service</h5>\n<p>We Are Available</p>\n</li>\n</ul>\n</div>\n<!-- End Tabs-Navigation -->\n<div class=\"clearfix visible-sm visible-xs\"></div>\n<!-- Start Tabs-Content -->\n<div class=\"col-lg-8 col-md-8 tabs-content\">\n<ul class=\"custom-list tab-content-list\"><!-- Start Yacht -->\n<li id=\"yacht\" data-bgimage=\"{base_url()}assets/images/services/img_9407_1.jpg\">\n<div class=\"heading\">\n<h5>OPERATIONAL MANAGEMENT</h5>\n<p>design and control</p>\n</div>\n<p class=\"bb\">Our company takes full responsibility for the design and control of all operations and maintenance of the airworthiness of your aircraft. We manage all the costs, including tax and customs duties, and build optimal logistic chains for every operation. Our clients always get cost-saving plans and regular reports on actual expenditures. Due to this scheme, our performance can always be monitored.</p>\n</li>\n<!-- End Yacht --> <!-- Start Fleet -->\n<li id=\"fleet\" data-bgimage=\"{base_url()}assets/images/services/ying_5_0_1.jpg\">\n<div class=\"heading\">\n<h5>TECHNICAL MANAGEMENT</h5>\n<p>best specialists and maintenance</p>\n</div>\n<p class=\"bb\">MS Aviation manages aircraft maintenance checks according to approved programs and recommendations made by aircraft manufacturers and aviation authorities. We can find the best technical specialists and maintenance companies for our customers.</p>\n</li>\n<!-- End Fleet --> <!-- Start Drivers -->\n<li id=\"drivers\" data-bgimage=\"{base_url()}assets/images/services/unbenannt.jpg\">\n<div class=\"heading\">\n<h5>FINANCIAL MANAGEMENT</h5>\n<p>monitoring of expenditures</p>\n</div>\n<p class=\"bb\">Rigorous monitoring of expenditures is an integral part of our approach. Our clients always get clear, full and transparent information on actual costs. We are ready to provide guidance on financial optimization in order to reduce permanent and operational costs by significant amounts.</p>\n</li>\n<!-- End Drivers --> <!-- Start Ourlocs -->\n<li id=\"ourlocs\" data-bgimage=\"{base_url()}assets/images/services/msaviation_crewing_service.jpeg\">\n<div class=\"heading\">\n<h5>CREWING SERVICE</h5>\n<p>Best specialists</p>\n</div>\n<p class=\"bb\">MS Aviation has its own database on aviation technical personnel. Due to this, we have the capacity to offer our clients the best specialist to cover any needs.</p>\n</li>\n<!-- End Ourlocs --></ul>\n</div>\n<!-- End Tabs-Content --></div>\n</div>\n</section>\n<!-- End Supertabs -->\n<p></p>\n<!-- Start About -->\n<p></p>\n<!-- End About -->', '_services', 1, 1),
(53, 1, 23, 'Career RU', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/msaviation_def_img.jpg\'); background-size: cover;\"></div>\r\n<!-- Start Contact -->\r\n<section class=\"contact-map style-2 section_padding\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header \">\r\n<h2>карьера</h2>\r\n<p>Заполните форму заявления о карьере, и мы свяжемся с вами как можно скорее.</p>\r\n</div>\r\n</div>\r\n<!-- End Map Contact -->\r\n<div class=\"container\"><!-- Start Contact-Form -->\r\n<div class=\"col-md-8 col-xs-offset-2\">\r\n<div class=\"contact-form\"><img src=\"img/divisor2.png\" alt=\"\" class=\"divisor\" /><form action=\"{site_url(submitCareer)}\" id=\"careerForm\" method=\"POST\" class=\"default-form\">\r\n<p class=\"alert-message warning\"><i class=\"ico fa fa-exclamation-circle\"></i> Все поля обязательны для заполнения!</p>\r\n<div class=\"row\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" name=\"fullname\" placeholder=\"имя\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" name=\"phone\" placeholder=\"Телефон\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" name=\"email\" placeholder=\"Email\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" name=\"subject\" placeholder=\"тема\" /></p>\r\n</div>\r\n</div>\r\n<div class=\"row hidden\">\r\n<div class=\"col-xs-12\"><label style=\"color: #fff;\">Загрузите свое резюме</label>\r\n<p class=\"form-row\"><input name=\"myFile\" type=\"file\" style=\"width: 100%;\" /></p>\r\n</div>\r\n</div>\r\n<p class=\"form-row\"><textarea name=\"message\" placeholder=\"Дополнительные комментарии\"></textarea></p>\r\n<button class=\"btn light\">Подать заявку</button></form></div>\r\n</div>\r\n<!-- End Contact-Form --></div>\r\n</section>\r\n<!-- End Contact -->\r\n<p></p>', NULL, 1, 1),
(50, 1, 1, 'Landing Logos template', NULL, 'logos', 1, 3),
(51, 2, 5, 'menukaart', NULL, '_menukart', 1, 1),
(59, 1, 26, 'Terms English', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/msaviation_bg_about.jpg\'); background-size: cover;\"></div>\r\n<!-- Start Supertabs -->\r\n<p></p>\r\n<!-- Start About -->\r\n<section class=\"about section_padding\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>Terms &amp; Conditions</h2>\r\n<p></p>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble -->\r\n<div class=\"row\">\r\n<p></p>\r\n<p>Your use and access of this website indicates you accept these Terms and Conditions.</p>\r\n<h4>1. <strong>General</strong></h4>\r\n<ol>\r\n<li>These General Terms and Conditions for Charter and any additional terms set out in any relevant quotation and/or confirmation (collectively hereinafter the “Terms”) form the contractual basis for the provision of Flight Services arranged by MS Aviation GmbH (“MS Aviation”).</li>\r\n<li>The Terms are applicable for commercial transport of Passengers and/or any permitted goods from an agreed point of departure to an agreed point of destination as more particularly detailed in the quotation and/or confirmation (“Flight Services”).</li>\r\n<li>The contract may‐be concluded with (i) Agents only or (ii) with Passenger directly however in any case Agents and Passengers (“Client”) are jointly bound by these Terms. It is the responsibility of the Client to ensure that each Passenger abides by the Terms.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>2. <strong>Conclusion of Contract</strong></h4>\r\n<ol>\r\n<li>The quotation issued by MS Aviation constitutes a non‐binding offer. Only the issuance of a Confirmation by MS Aviation constitutes a binding offer which requires acceptance within the acceptance period. If such acceptance period is lapsed, MS Aviation shall not be bound by its Confirmation.</li>\r\n<li>The return of the Confirmation duly signed by an Officer of Client constitutes a binding contract of carriage between Client and MS Aviation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>3. <b>Performance of Flight Services</b></h4>\r\n<ol>\r\n<li>MS Aviation shall not act as a common carrier or any other type of carrier in respect of any of its obligations under the relevant agreement with such third party and MS Aviation acts solely as agent for Client and operator. Therefore, MS Aviation subcontracts its contractual obligations in part or in whole to a third party or to third parties including any actual carrier subject to clause 12. In such cases, the terms of business of third party operators shall apply to Client, which are hereby incorporated by reference and a copy shall be furnished to Client upon request.</li>\r\n<li>The Flight Services will be performed pursuant to and in accordance with the actual Terms as applicable at the date of Flight Services and the operating procedures approved by the competent authority of the contractual or the actual carrier as the case may‐</li>\r\n<li>MS Aviation and/or the actual operator expressively reserve the right to utilize on its own account any lay‐over period or empty capacity the aircraft may have, including any empty legs related to the Flight Services, before, during or after the period in which the aircraft is available to Client.</li>\r\n<li>Flight Services are planned with a set of one (1) crew (Pilot in Command, Copilot) subject to crew duty time and rest period restrictions by applicable duty limitation regulations.</li>\r\n<li>Cabin Service by one (1) Cabin Hostess is included for all flights on Heavy Jets. Additional Cabin Hostess may be supplied upon request and subject to additional charge at the sole discretion of MS Aviation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>4. <b>Included and Excluded Costs</b></h4>\r\n<ol>\r\n<li>The Price does include aircraft costs including crew, fuel, maintenance, air navigation, airport and handling charges, standard use of a General Aviation Terminal or similar facilities at destinations by passengers (e.g. FBOs, VIP Halls, VIP areas), inflight MS Aviation standard catering (depending on flight time and time of day), Passenger and baggage insurance as provided by the actual carrier.</li>\r\n<li>The following costs are not included and shall be charged separately to Client at cost:</li>\r\n<li>\r\n<ol></ol>\r\n<p>            - De- or Anti-Icings;</p>\r\n<p>            ‐ Insurance surcharges;</p>\r\n<p>            ‐ SATCOM services;</p>\r\n<p>            ‐ Special catering requests such as but not limited to caviar and special wines or spirits;</p>\r\n<p>            - VIP lounges or meeting rooms for exclusive use at the destinations</p>\r\n<p>            ‐ special handling, helicopter and/or limousine services;</p>\r\n<p>            ‐ any other concierge services rendered by MS Aviation upon request by Client;</p>\r\n<p>‐ additional or enlarged crew or Cabin Hostess as result of a request by the Client and/or any  Passenger. In such event, Client acknowledges and agrees that if the actual carrier has to use an enlarged or second crew, this may necessitate crew being in the cabin during the flight.</p>\r\n</li>\r\n<li>Costs for schedule changes and re-routings upon Client’s request, costs generated by passenger delays, costs for flight diversions and the extension of airport operating hours are excluded and shall be charged to Client.</li>\r\n<li>\r\n<ul></ul>\r\n<p> </p>\r\n</li>\r\n<li>MS Aviation offers are net and do not include any commission, unless requested by Client.</li>\r\n<li>Taxes if applicable will be posted separately on the quotation/invoice.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>5. <b>Payment</b></h4>\r\n<ol>\r\n<li>All payments due to MS Aviation shall be made upon receipt of invoice and no later than one (1) business day prior flight date (the “Due Date”), without setoff or deduction. Time for payment shall be of the essence, if the Due Date is not a business day (bank holiday, Saturday or Sunday) the due and payable amount shall be received and credited to MS Aviation account on the last preceding business day. Payments are to be made at costs of the sender in Euro currency to the bank account stated below:</li>\r\n<li>\r\n<ol></ol>\r\n<p> </p>\r\n<p>Beneficiary MS Aviation GmbH</p>\r\n<p>Bank: Raiffeisen Regionalbank Mödling, Am Kirchanger 3A, 2353 Guntramsdorf</p>\r\n<p>BIC: RLNWATWWGTD</p>\r\n<p>IBAN: AT35 3225 0000 0101 8449</p>\r\n<p>           </p>\r\n</li>\r\n<li>Late payments shall be subject to interest at 10% of the outstanding sum per annum from the date due until MS Aviation receipt and MS Aviation shall not be in breach of contract if it suspends Flight Services or additional services until receipt of funds.</li>\r\n<li>Incoming payments shall first be offset against the oldest debt. Payment which is not sufficient to cover the entire debt will first be offset against the interest and finally against the principal debt.</li>\r\n<li>If the payment has still not been made after issuance of a reminder and the setting of a deadline for payment, MS Aviation shall be entitled to withdraw from the contract and cancel the booking, subject to cancellation charges as set out in Section 12. MS Aviation may refuse to set a payment deadline if the imminence of the departure date makes it unfeasible to stipulate a period for payment prior to departure. In such event, MS Aviation may withdraw from the contract and refuse performance of Flight Services subject to cancellation charges as set out in Section 12.</li>\r\n<li>Major credit cards will be accepted for payment subject to any surcharges that may apply and a handling charge of 5%. If a credit card institute or a bank refuses to honor the payment required under the contract, MS Aviation shall levy Client with an administration charge of EUR 500, in addition to any charges made by the credit card institute or the bank.</li>\r\n<li>Client and Passenger shall be jointly and severally liable for the payment of the Flight Services and any additional costs set out in the quotation and/or Terms as well the cost of any damages or losses caused as a result of the conduct of the Client and/or any Passenger.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>6. <b>Flight Changes and Delays</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual carrier shall endeavor to the best of their ability to ensure punctual carriage of passengers and baggage. However, the announced flight times are subject to reasonable changes owing to operational and technical circumstances beyond MS Aviation and/or the actual operator’s control.</li>\r\n<li>The Client is responsible to ensure that passengers arrive adequately in advance of the scheduled departure time. MS Aviation and/or the actual operator’s ability to satisfy any variation in the Flight Services shall always be subject to crew duty times and rest periods and the availability of additional crew.</li>\r\n<li>Client may request a departure delay of up to a maximum of 60 minutes beyond any confirmed departure time. MS Aviation and/or the actual operator shall agree to such delay if it is compatible with crew duty time restrictions, applicable aviation regulations and air traffic control requirements. If Client delays a flight in excess of 60 minutes beyond the confirmed departure time for any reason that is not the fault of MS Aviation and/or the actual operator, the Flight Services shall be deemed to be cancelled by Client.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>7. <b>Substitution or Subcharter</b></h4>\r\n<ol>\r\n<li>Flight Services are aircraft type specific. MS Aviation reserves the right to provide the Client at MS Aviation sole discretion with an equivalent or superior aircraft type from any operator at no additional cost to Client (Substitution Aircraft). Substitution or Subcharter may occur en‐route during the Flight Services.</li>\r\n<li>In the event that an equivalent or superior Substitution Aircraft is not available for the Flight Services, MS Aviation shall advise Client without delay and provide a revised quotation with revised pricing to reflect the provision for an inferior Substitution Aircraft. In the event Client does not agree to the provision of an inferior Substitution Aircraft, Client shall be entitled to terminate Flight Services at the point of substitution subject to informing MS Aviation promptly of such cancellation and MS Aviation shall refund the amount paid on a pro‐rata basis less costs for positioning the aircraft back to point of departure for the remaining part of the trip affected by the substitution event. Should Client fail to advise MS Aviation of such cancellation promptly after being informed of such planned substitution by MS Aviation (which shall be reasonably dictated by the circumstances) then MS Aviation shall be entitled to deduct all pre-positioning costs from any applicable refund.</li>\r\n<li>Where due to a substitution event a Substitution or Subcharter Aircraft is supplied, Client’s liability shall always be to pay the costs and sums set out in the quotation plus excess costs if any for the Subcharter Aircraft.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>8. <b>Compliance with entry and exit requirements; required documents</b></h4>\r\n<ol>\r\n<li>Passengers are responsible for obtaining, and presenting at check‐in/immigration, the necessary travel documents, visas and doctor\'s certificates, certificates of vaccination and the like which are required – for themselves, or for children or animals travelling with them –  under the passport, visa and health regulations of the countries in question. In particular, MS Aviation would like to draw Passengers’ attention to visa requirements for foreign nationals.</li>\r\n<li>The contracted carrier is obliged by law to refuse carriage if the entry and exit requirements for the country of departure or destination are not met, or if the required documentation / certification is not presented.</li>\r\n<li>MS Aviation and/or the actual operator take no responsibility with the regard to entry or exit requirements of Passengers. Any costs or disadvantages arising from the failure to observe these requirements shall be incurred jointly and severally by the agent and by the Passenger such as but not limited to fines and cost of repatriation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>9. <b>Safety and Security</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual operator are entitled to change the route, flight schedule, seating capacity and maximum take‐off weight if these are required under certain operational circumstances not caused by MS Aviation and/or the actual operator.</li>\r\n<li>Captain’s Decision: The pilot in command shall at all times be entitled to take all necessary measures for safety reasons. The pilot has the authority to decide with regard to Passenger\'s seating as well as baggage loading, allocation/placement and unloading. The pilot decides whether or not and how the flight is operated. The same applies if the behavior or the physical or mental condition of a Passenger requires extraordinary assistance on behalf of the actual operator’s crew. Passenger hereby accepts all such decisions. Passenger agrees that when, in the reasonable view of the actual operator or the pilot in command, safety or security may be compromised, the actual operator or the pilot in command may decide to refuse to start or commence a flight, divert a flight or take other action necessitated by such safety considerations without liability for loss, injury, damage or delay.</li>\r\n<li>Carriage of infants, children and adolescents:</li>\r\n<li>            Infants travel according to the safety regulations of the actual operator.</li>\r\n<li>Carriage of pets:</li>\r\n<li>Owing to safety reasons and because of the limited space available, Passengers are entitled to demand the transport of pets only if MS Aviation and/or the actual operator have been notified at      the time of booking and have           confirmed carriage of the pet. Passenger is responsible that the pet complies with the requirements in the        country of destination.</li>\r\n<li>Carriage of baggage:</li>\r\n<li>            (a) Excess and general baggage</li>\r\n<li>     Passenger baggage weight is limited for flight safety reasons and varies according to aircraft type. Items determined by the crew to be of excessive weight or size will not be permitted on the aircraft. Passengers are obliged to notify MS Aviation and/or the actual operator of all excess     and general baggage, stating the dimensions and weight of the items such as but not limited         to sports equipment, pushchair/buggy and child’s car seat. The carriage of excess and general baggage shall be decided on the basis of the available hold capacity and security regulations for each flight at the sole discretion of the pilot in command. Accordingly, MS Aviation and/or the actual operator reserve the right to accept only a limited quantity or refuse the carriage of excess or general baggage entirely.</li>\r\n<li>            (b) Generally prohibited baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>            (c) Prohibited items in checked baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>            (d) Prohibited items in hand baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>Electronic equipment</li>\r\n<li>For safety reasons, the use of all personal electronic devices is strictly prohibited during take‐off and landing. The use of mobile phones is not permitted throughout the entire flight. The use of other electronic devices is permitted only with the consent of the pilot in command.</li>\r\n<li> </li>\r\n<li>Smoking</li>\r\n<li>  Smoking may be prohibited on some flights depending on the individual aircraft. Additional Costs for fines, fees and/or cabin cleaning will be charged to Passenger.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>10. <b>Force Majeure</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual carrier reserve the right to at any time during the carriage to suspend or redirect the flight in question and/or provide the Passenger with another similar aircraft or cancel the flight without further liability to the Passenger in the event that the carriage cannot be completed in accordance with Passenger’s requirements due to war, warlike events, infringements of a country’s neutrality, insurrection, civil war, civil unrest, riots, sabotage, strikes, blockades, lockouts, quarantine, hijacking, terrorist actions, requisition, confiscation, expropriation, seizure, adverse weather conditions or other force majeure of any nature, technical reasons, detention or similar measures, accidents with aircraft, or due no other factors over which MS AVIATION and/or the actual operator have no control, or when the safety of the Passengers or the crew from the aircraft can reasonably be assessed to be in danger, at the discretion of the pilot in command or of the actual operator’s personnel (“Force Majeure Event”). Where MS Aviation and/or the actual operator cancel the contract having commenced but not completed the carriage due to the Force Majeure Event, Passenger shall only be charged on a pro rata basis for the portion of the carriage performed and any balance shall be refunded to Passenger.</li>\r\n<li>In the event that a Force Majeure Event occurs prior to the commencement of the carriage and no suitable solution can be found in the reasonable opinion of MS Aviation and/or the actual operator, MS Aviation and/or the actual operator reserve the right to cancel the contract of carriage without liability to Passenger. In this case, MS Aviation shall credit the Passenger with an amount corresponding to the flight in question minus all expenses already incurred.</li>\r\n<li>Unless stated otherwise in mandatory (indispensable) legislation, MS Aviation shall not be responsible for damage or loss as a result of or arising, directly or indirectly, in connection with the abovementioned circumstances.</li>\r\n<li>MS Aviation shall not be liable for any damage or loss of any nature whatsoever to Passengers arising from any delay arising as a result of a Force Majeure Event.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>11. <b>General Liability Provisions</b></h4>\r\n<ol>\r\n<li>\r\n<p>(a) Liability in connection with the carriage of Passengers, freight and baggage is subject to the individual terms and conditions of the actual operator and can be provided to Client upon request. MS Aviation makes no         representation or warranty with regard to the third party operator and will not be liable in any way whatsoever        for any loss, damage, injury or expense suffered or incurred by the Client, the Passenger(s) or any third party   howsoever.</p>\r\n<p>(b) Client shall indemnify and hold harmless MS Aviation against all liabilities, claims and expenses (including legal costs and fees) in respect of any liability of MS Aviation towards such third party operators for any loss or damage whatsoever (including costs and expenses on a full indemnity basis) arising out of any act or omission of Client, its servants or agents or any Passenger(s) carried by authority of Client.</p>\r\n<p>(c) MS Aviation and/or the actual carrier shall not be liable for damage caused in fulfilment of      government regulations or because the Passenger fails to satisfy his/her obligations pursuant to these regulations.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>12. <b>Cancellation</b></h4>\r\n<ol>\r\n<li>Flight Services shall be deemed cancelled by Client in the event of (i) cancellation of any booked flight communicated by the Client to MS Aviation in advance of the commencement of Flight Services, (ii) a delay of any passenger and/or Client in excess of 60 minutes to the scheduled time of departure unless specifically agreed by MS Aviation in advance in writing (iii) a no‐show of either the Client and/or any Passenger, or (iv) any refusal of the Client and/or any of its passengers to comply with the reasonable instructions of MS Aviation or the actual operator’s pilot‐in command for flight safety and/or security reasons leading to the pilot‐in‐command to reasonably deem it necessary to cancel or terminate a planned flight and (v) if Client fails to make the payment prior the Due Date.</li>\r\n<li>In such circumstances the Cancellation Fees set out below shall be applicable and payable by Client:</li>\r\n<li>\r\n<ul></ul>\r\n<p> <strong>Up to 15 days before the 1st scheduled departure: 10% of the charter price;</strong></p>\r\n<p><strong></strong><strong>less than 15 days but at least 7 days before the 1st scheduled departure: 20% of the charter price;</strong></p>\r\n<p><strong>less than 7 days but at least 48 hours before the 1st scheduled departure: 35% of the charter price</strong></p>\r\n</li>\r\n<li>\r\n<p><strong></strong><strong>less than 48 hours but at least 24 hours before the 1st scheduled departure or in case of a no show: 50% of the charter price.</strong></p>\r\n<p><strong></strong><strong> less than 24 hours before the 1st scheduled departure or in case of a no show: 100% of the charter price.</strong></p>\r\n</li>\r\n<li>All Cancellation Fees are subject to a minimum payment of Euro 1,000 which is a reasonable pre‐estimate of the minimum cost to MS Aviation where a booked flight is cancelled and takes account, by way of example only, costs associated with the arrangement and movement of flight crew, permissions and associated administration and the logistics involved in organizing the flight and any extra services.</li>\r\n<li>In the event a flight is cancelled not as a result of actions of MS Aviation, the costs of any additional goods and/or services arranged by MS Aviation at the Client’s request through third party supplier(s) ancillary to the actual booked flight shall also remain the responsibility of the    Client and shall be charged to the Client at the cost.</li>\r\n<li>Any reimbursement of flight charges and any other amounts paid by the Client in advance of the booked flight shall be subject to the deduction of any amounts outstanding, Cancellation Fees and the balance of the sum paid by the Client, shall be repaid to the Client within 7 days of the date of receipt of cleared funds for the original booking. In any other circumstances (for example if the funds paid by Client are not sufficient to cover the Cancellation Fees), Client undertakes to make payment of any sums to cover the Cancellation Fees within 7 days of the date of issue by MS Aviation of an invoice to the Client for such sums.</li>\r\n<li>MS Aviation will not be liable to the Client for any loss or expense incurred by the Client or any Passenger in the event of cancellation due to their failure to comply with the provisions set out in these Terms.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>13. <b>Amendments</b></h4>\r\n<ol>\r\n<li>MS Aviation reserves the right to amend these Terms at any time with effect for the future without obligation to notify Client. On continuing to use MS Aviation services after amendment of the Terms, Client declares the consent to the amendments.</li>\r\n<li>No agency, employee or any other third party is entitled to make any amendments and/or addenda to these Terms or to waive their applicability.</li>\r\n<li>These Terms contain the entire provisions of the contract between the Passenger and MS Aviation and supersede all previous agreements, regardless of whether such agreements were made verbally, by electronic means or in writing. In case of conflict between these Terms and any Special Terms, the Special Terms shall take precedence.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>14. <b>Privacy and data protection</b></h4>\r\n<ol>\r\n<li>\r\n<p>(a) Personal data (such as name, address, telephone number and credit card details) are essential for booking. The protection of Passengers’ personal data is very important to MS Aviation. Data will be stored and protected at MS Aviation files.</p>\r\n<p>(b)MS Aviation and/or the actual carrier are explicitly entitled to transmit data obtained from official photo identification documents and other personal data processed or used in connection with   the carriage to public authorities, provided that the authority\'s request for disclosure is based on mandatory legal regulations and is necessary for performance of the contract.</p>\r\n<p>(c)MS Aviation and/or actual carrier are entitled to capture, process and use personal data within the scope of performance of the contract. The data is processed or used for the following purposes: making reservations, purchase of a ticket, purchase of additional services and handling payment transactions; development and provision of services, facilitating entry and customs clearance procedures.</p>\r\n<p>(d) The Passenger authorizes MS Aviation and/or actual carrier to capture, save, modify, block, delete and use the        data for that purpose and to transmit it to their own branch offices, authorized representatives and to the        parties who provide the above services on behalf of MS Aviation and/or actual carrier.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>15. <b>Severability</b></h4>\r\n<ol>\r\n<li>\r\n<p>Should any one or more clauses of these terms be found to be illegal or unenforceable in any respect, the validity, legality and enforceability of the remaining clauses shall not in any way be affected or impaired thereby.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>16. <b>Applicable Law</b></h4>\r\n<ol>\r\n<li>\r\n<p>The contractual relationship between the Passenger and MS Aviation shall be governed by the laws of the Republic of Austria, irrespective of the Passenger’s nationality.</p>\r\n<p><strong>Please countersign this Charter Confirmation and return to us by fax or    e-mail. By countersigning and returning this Confirmation to us, you acknowledge having read, you confirm you understood and hereby agree to our General Terms and Conditions.</strong></p>\r\n<span style=\"font-weight: 400;\">.</span></li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH --></div>\r\n</div>\r\n</section>', NULL, 1, 1);
INSERT INTO `page_content` (`page_content_id`, `page_content_type_related_id`, `page_content_related_id`, `page_content_name`, `page_content_data`, `page_content_template`, `is_public`, `page_content_order`) VALUES
(61, 2, 28, 'Fly Now ENG', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/contacts-build.jpg\'); background-size: cover;\"></div>\r\n<!-- Start Contact -->\r\n<section class=\"contact-map style-2 section_padding\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header \">\r\n<h2>Fly Now</h2>\r\n<p>Request a quote to fly with us below and we\'ll contact you shortly.</p>\r\n</div>\r\n</div>\r\n<!-- End Map Contact -->\r\n<div class=\"container\">\r\n<div class=\"content-area\">\r\n<div class=\"post-wrap\">\r\n<article class=\"about-post style4\">\r\n<div class=\"entry-content\"><form action=\"#\" method=\"get\" accept-charset=\"utf-8\">\r\n<div class=\"flat-form-quote style1\">\r\n<div class=\"title-quote\">Quotation request</div>\r\n<p><label> Your name * <span> <input type=\"text\" name=\"your name\" placeholder=\"You Name\" /> </span> </label> <label> Company name * <span> <input type=\"text\" name=\"Company name\" placeholder=\"Company name\" /> </span> </label> <label> Phone number * <span> <input type=\"text\" name=\"phone number\" placeholder=\"123-456-7890\" /> </span> </label> <label> Your email * <span> <input type=\"text\" name=\"your name\" placeholder=\"You email\" /> </span> </label> <label> Pick-up address information <span>Country, City, Postal code *</span> <span> <input type=\"text\" name=\"phone number\" placeholder=\"Country\" /> </span> </label> <label> Delivery address information <span>Country, City, Postal code *</span> <span> <input type=\"text\" name=\"your name\" placeholder=\"Country\" /> </span> </label> <label> What mode of transport do you require? <span><select name=\"select quote\">\r\n<option value=\"Air Freight\">Air Freight</option>\r\n<option value=\"Road and Rail Freight\">Road and Rail Freight</option>\r\n<option value=\"Ocean Freight\">Ocean Freight</option>\r\n</select></span> </label></p>\r\n<div class=\"title-form\">Description of the goods</div>\r\n<p>Please supply us with information about the goods you want to ship. Number of pallets / packages, dimensions, weight. Also if possible information on the desired loading and unloading date and time or any other requests which we need to take into account.</p>\r\n<div class=\"title-form\">Goods information *</div>\r\n<p><span> <textarea name=\"your-message\" cols=\"40\" rows=\"10\"></textarea> </span> <button type=\"submit\" class=\"button-form-quote\">Send Reuqest</button></p>\r\n</div>\r\n<!-- /.flat-form-qoute style1 --></form><!-- /form --></div>\r\n<!-- /.entry-content --></article>\r\n<!-- /.about-post style4 --></div>\r\n<!-- /.post-wrap --></div>\r\n<!-- Start Contact-Form -->\r\n<div class=\"col-md-8 col-xs-offset-2\">\r\n<div class=\"contact-form\"><img src=\"img/divisor2.png\" alt=\"\" class=\"divisor\" /><form action=\"{site_url()}\" method=\"POST\" id=\"contactForm\" class=\"default-form\">\r\n<p class=\"alert-message warning\"><i class=\"ico fa fa-exclamation-circle\"></i> All fields are required!</p>\r\n<div class=\"row\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" name=\"fullname\" placeholder=\"Full Name\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" name=\"phone\" placeholder=\"Phone Number\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" name=\"email\" placeholder=\"Email Address\" /></p>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n<p class=\"form-row\"><input class=\"required\" type=\"text\" name=\"subject\" placeholder=\"Предмет\" /></p>\r\n<div class=\"form-group registration-date\"><label class=\"control-label col-sm-3\" for=\"registration-date\">Date:</label>\r\n<div class=\"input-group registration-date-time\"><span class=\"input-group-addon\" id=\"basic-addon1\"><span class=\"glyphicon glyphicon-calendar\" aria-hidden=\"true\"></span> </span> <input class=\"form-control\" name=\"registration_date\" id=\"registration-date\" type=\"date\" /> <span class=\"input-group-addon\" id=\"basic-addon1\"><span class=\"glyphicon glyphicon-time\" aria-hidden=\"true\"></span> </span> <input class=\"form-control\" name=\"registration_time\" id=\"registration-time\" type=\"time\" /> <span class=\"input-group-btn\"> <button class=\"btn btn-default\" type=\"button\" onclick=\"addNow()\"><span class=\"glyphicon glyphicon-map-marker\" aria-hidden=\"true\"></span> Now </button> <button class=\"btn btn-default\" type=\"button\" onclick=\"stopNow()\"><span class=\"glyphicon glyphicon-minus-sign\" aria-hidden=\"true\"></span> Stop</button> </span></div>\r\n</div>\r\n</div>\r\n</div>\r\n<p class=\"form-row\"><textarea name=\"message\" placeholder=\"Как мы можем помочь?\"></textarea></p>\r\n<button class=\"btn light\">Отправить сообщение</button></form></div>\r\n</div>\r\n<!-- End Contact-Form --></div>\r\n</section>\r\n<!-- End Contact -->\r\n<p></p>\r\n<!-- Start Locations -->\r\n<section class=\"locations\">\r\n<div class=\"container\">\r\n<div class=\"row\"><!-- Start Location-Agency -->\r\n<div class=\"location-agency\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"location-agency-title\">MS Aviation Австрия</h3>\r\n<ul class=\"custom-list location-agency-contact col-lg-6 col-md-6\">\r\n<li>IZ NÖ-Süd, Strasse 3, Objekt 1,</li>\r\n<li>Top1 2355 Wiener Neudorf Austria</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h3 class=\"location-agency-title\">MS Аviation Россия Представительство в Москве:</h3>\r\n<ul class=\"custom-list location-agency-contact col-lg-6 col-md-6\">\r\n<li>Глинищевский переулок дом 3, офис 203-204, Москва, Россия 125009</li>\r\n<li>Tel: +7 495 295 00 30 e-mail: aa@lfs.group | ad@lfs.group</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h3 class=\"location-agency-title\">MS Аviation Представительство в Украине:</h3>\r\n<ul class=\"custom-list location-agency-contact col-lg-6 col-md-6\">\r\n<li>ул. Пироговского, 18, 3 этаж, Киев, Украина 03110</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<!-- End Location-Agency --></div>\r\n</div>\r\n</section>\r\n<!-- End Locations -->\r\n<section class=\"supertabs no-top\">\r\n<div class=\"container\"><!-- Start Header-Preamble -->\r\n<div class=\"col-md-12\"><!-- Start Map Locations -->\r\n<div id=\"map-locations\"></div>\r\n<!-- End Map Locations --></div>\r\n</div>\r\n</section>', '_fly_now_form', 1, 1),
(60, 1, 27, 'Legal Notice English', '<section class=\"fleets section_padding padding-top-190\">\r\n<div class=\"container-fluid\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>lEGAL NOTICE</h2>\r\n<!-- <p>The Austrian company MS Aviation was founded by a group of industry professionals in business aviation.</p> --></div>\r\n</div>\r\n<div class=\"container\">\r\n<p class=\"x_MsoNormal\"><span>Impressum</span></p>\r\n<p class=\"x_MsoNormal\"><span>Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz</span></p>\r\n<p class=\"x_MsoNormal\"> </p>\r\n<p class=\"x_MsoNormal\"><span>MS Aviation GmbH</span></p>\r\n<p class=\"x_MsoNormal\"><span>Unternehmensgegenstand: Luftfahrtunternehmen</span></p>\r\n<p class=\"x_MsoNormal\"><span>UID-Nummer: ATU71491245</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmenbuchnummer: FN459795 w</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmenbuchgericht: Landesgericht Wr. Neustadt</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmensitz: 2355 Wiener Neudorf</span></p>\r\n<p class=\"x_MsoNormal\"><span>IZ NÖ-SÜD, Strasse 3 Objekt 1, Top 1,Austria</span></p>\r\n<p class=\"x_MsoNormal\"><span>Tel.: <a href=\"tel:06645020462\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">06645020462</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>E-Mail: <a href=\"mailto:ms@msaviation.at\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">ms@msaviation.at</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>Berufsrecht: <a href=\"http://www.ris.bka.gv.at/\" target=\"_blank\" rel=\"noopener noreferrer\">www.ris.bka.gv.at</a></span></p>\r\n<p class=\"x_MsoNormal\"> </p>\r\n<p class=\"x_MsoNormal\"><span>Aufsichtsbehörde/Gewerbebehörde: Austro Control Österreichische Gesellschaft für Zivilluftfahrt mit beschränkter Haftung Wagramer Straße 19, 1220 Vienna <a href=\"http://www.austrocontrol.co.at/\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">www.austrocontrol.co.at</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>Berufsbezeichnung: Luftfahrtunternehmen</span></p>\r\n<p class=\"x_MsoNormal\"><span>Verleihungsstaat: Österreich</span></p>\r\n<p class=\"x_MsoNormal\"><span>Angaben zur Online-Streitbeilegung: Verbraucher haben die Möglichkeit, Beschwerden an die OnlineStreitbeilegungsplattform der EU zu richten: <a href=\"http://ec.europa.eu/odr\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">http://ec.europa.eu/odr</a>. Sie können allfällige Beschwerde auch an die oben angegebene E-Mail-Adresse richten.</span></p>\r\n<!-- <p>The Austrian company MS Aviation was founded by a group of industry professionals in business aviation.</p> --></div>\r\n</div>\r\n</section>', NULL, 1, 1),
(62, 1, 29, 'about1', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/msaviation_bg_about.jpg\'); background-size: cover;\"></div>\r\n<!-- Start Supertabs -->\r\n<p></p>\r\n<!-- Start About -->\r\n<section class=\"about section_padding\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>ÜBER MS AVIATION</h2>\r\n<p></p>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble -->\r\n<div class=\"row\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<div class=\"funfact-contant\">\r\n<div class=\"sectn-title\">\r\n<h2>Wir sind eine Gruppe von Branchenexperten in der Geschäftsluftfahrt</h2>\r\n</div>\r\n<!-- /.section-titel -->\r\n<p class=\"mb-40\">Das österreichische Unternehmen MS Aviation wurde von einer Gruppe von Branchenexperten in der Geschäftsluftfahrt gegründet. Jedes Mitglied unseres Teams erkennt, dass die Standards für unseren Arbeitsbereich sehr hoch sind. Und wir sind bereit, alle Qualitätserwartungen unserer individuellen und korporativen Kunden zu übertreffen.</p>\r\n<p class=\"mb-40\">MS Aviation konzentriert sich auf das Asset Management der Luftfahrt - vom technischen Support und der Lösung betrieblicher Probleme bis hin zur finanziellen Verbesserung des Geschäftsjet-Eigentums. Unsere Firma erbringt Services, die den gesamten Umfang des Flugzeugmanagements abdecken.</p>\r\n</div>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<div class=\"funfact-img\"><img class=\"block\" src=\"{base_url()}assets/images/about/msaviation_about_1.jpg\" alt=\"MSaviation About Image1\" /></div>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble --> <!-- End Header-Preamble -->\r\n<div class=\"row padding-top-40\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<div class=\"funfact-img\"><img class=\"block\" src=\"{base_url()}assets/images/about/msaviation_about_2.jpg\" alt=\"MSaviation About Image2\" /></div>\r\n</div>\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<div class=\"funfact-contant\">\r\n<div class=\"sectn-title\">\r\n<h2>Wir halten uns an alle europäischen Sicherheitsstandards</h2>\r\n</div>\r\n<!-- /.section-titel -->\r\n<p class=\"mb-40\">Unser Team arbeitet, um die finanziellen Interessen von Flugzeugen zu schützen und zu sichern Besitzer. Deshalb ist unser Hauptprinzip, das gesamte Flugzeugmanagement zu machen Prozesse sind klar und vollständig transparent. MS Aviation optimiert die Kosten von unseren Kunden und bleibt in Übereinstimmung mit allen europäischen Standards der Sicherheit und Sicherheit.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble --></div>\r\n</section>\r\n<!-- End About -->\r\n<p></p>\r\n<!-- Start Supertabs -->\r\n<section class=\"supertabs no-top\">\r\n<div class=\"container\"><!-- Start Header-Preamble -->\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>Unser Team</h2>\r\n<p>Spezialisten der Firma MS Aviation widmen sich der Arbeit. Jedes Mitglied unseres Teams verfügt über umfangreiche Erfahrung im Privatjet-Management. Gemeinsam können wir jede komplexe Herausforderung meistern.</p>\r\n<p>JEDES MITGLIED UNSERES TEAMS HAT WICHTIGE ERFAHRUNGEN IM PRIVATEN JET-MANAGEMENT.</p>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble -->\r\n<div class=\"row\">\r\n<div class=\"team\">\r\n<div class=\"col-lg-4 col-md-4 col-sm-6 member text-center\">\r\n<div class=\"member-inner\"><img src=\"{base_url()}assets/images/team/591_maikl_maier__1.jpg\" alt=\"maikl maier\" /></div>\r\n<h5>Michael Mayer</h5>\r\n<p><span>Mitbegründer, CEO und verantwortlicher Manager</span></p>\r\n</div>\r\n<div class=\"col-lg-4 col-md-4 col-sm-6 member text-center\">\r\n<div class=\"member-inner\"><img src=\"{base_url()}assets/images/team/591_torsten_kincl_0.jpg\" alt=\"torsten kincl\" /></div>\r\n<h5>Thorsten Kinzl</h5>\r\n<p><span>Nominierte Person, die die Lufttüchtigkeit fortsetzt</span></p>\r\n</div>\r\n<div class=\"col-lg-4 col-md-4 col-sm-6 member text-center\">\r\n<div class=\"member-inner\"><img src=\"{base_url()}assets/images/team/591_roland_malek_1.jpg\" alt=\" roland malek \" /></div>\r\n<h5>Roland Malek</h5>\r\n<p><span>Mitbegründer und Geschäftsführer von Operations</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<!-- End Supertabs -->\r\n<p></p>\r\n<!-- Start About -->\r\n<p></p>', NULL, 1, 1),
(63, 2, 31, 'WELCOME DE', NULL, 'main_slider', 1, 1),
(68, 1, 36, 'TERMS DE', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/msaviation_bg_about.jpg\'); background-size: cover;\"></div>\r\n<!-- Start Supertabs -->\r\n<p></p>\r\n<!-- Start About -->\r\n<section class=\"about section_padding\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>Geschäftsbedingungen</h2>\r\n<p></p>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble -->\r\n<div class=\"row\">\r\n<p></p>\r\n<p>Your use and access of this website indicates you accept these Terms and Conditions.</p>\r\n<h4>1. <strong>General</strong></h4>\r\n<ol>\r\n<li>These General Terms and Conditions for Charter and any additional terms set out in any relevant quotation and/or confirmation (collectively hereinafter the “Terms”) form the contractual basis for the provision of Flight Services arranged by MS Aviation GmbH (“MS Aviation”).</li>\r\n<li>The Terms are applicable for commercial transport of Passengers and/or any permitted goods from an agreed point of departure to an agreed point of destination as more particularly detailed in the quotation and/or confirmation (“Flight Services”).</li>\r\n<li>The contract may‐be concluded with (i) Agents only or (ii) with Passenger directly however in any case Agents and Passengers (“Client”) are jointly bound by these Terms. It is the responsibility of the Client to ensure that each Passenger abides by the Terms.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>2. <strong>Conclusion of Contract</strong></h4>\r\n<ol>\r\n<li>The quotation issued by MS Aviation constitutes a non‐binding offer. Only the issuance of a Confirmation by MS Aviation constitutes a binding offer which requires acceptance within the acceptance period. If such acceptance period is lapsed, MS Aviation shall not be bound by its Confirmation.</li>\r\n<li>The return of the Confirmation duly signed by an Officer of Client constitutes a binding contract of carriage between Client and MS Aviation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>3. <b>Performance of Flight Services</b></h4>\r\n<ol>\r\n<li>MS Aviation shall not act as a common carrier or any other type of carrier in respect of any of its obligations under the relevant agreement with such third party and MS Aviation acts solely as agent for Client and operator. Therefore, MS Aviation subcontracts its contractual obligations in part or in whole to a third party or to third parties including any actual carrier subject to clause 12. In such cases, the terms of business of third party operators shall apply to Client, which are hereby incorporated by reference and a copy shall be furnished to Client upon request.</li>\r\n<li>The Flight Services will be performed pursuant to and in accordance with the actual Terms as applicable at the date of Flight Services and the operating procedures approved by the competent authority of the contractual or the actual carrier as the case may‐</li>\r\n<li>MS Aviation and/or the actual operator expressively reserve the right to utilize on its own account any lay‐over period or empty capacity the aircraft may have, including any empty legs related to the Flight Services, before, during or after the period in which the aircraft is available to Client.</li>\r\n<li>Flight Services are planned with a set of one (1) crew (Pilot in Command, Copilot) subject to crew duty time and rest period restrictions by applicable duty limitation regulations.</li>\r\n<li>Cabin Service by one (1) Cabin Hostess is included for all flights on Heavy Jets. Additional Cabin Hostess may be supplied upon request and subject to additional charge at the sole discretion of MS Aviation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>4. <b>Included and Excluded Costs</b></h4>\r\n<ol>\r\n<li>The Price does include aircraft costs including crew, fuel, maintenance, air navigation, airport and handling charges, standard use of a General Aviation Terminal or similar facilities at destinations by passengers (e.g. FBOs, VIP Halls, VIP areas), inflight MS Aviation standard catering (depending on flight time and time of day), Passenger and baggage insurance as provided by the actual carrier.</li>\r\n<li>The following costs are not included and shall be charged separately to Client at cost:</li>\r\n<li>\r\n<ol></ol>\r\n<p>            - De- or Anti-Icings;</p>\r\n<p>            ‐ Insurance surcharges;</p>\r\n<p>            ‐ SATCOM services;</p>\r\n<p>            ‐ Special catering requests such as but not limited to caviar and special wines or spirits;</p>\r\n<p>            - VIP lounges or meeting rooms for exclusive use at the destinations</p>\r\n<p>            ‐ special handling, helicopter and/or limousine services;</p>\r\n<p>            ‐ any other concierge services rendered by MS Aviation upon request by Client;</p>\r\n<p>‐ additional or enlarged crew or Cabin Hostess as result of a request by the Client and/or any  Passenger. In such event, Client acknowledges and agrees that if the actual carrier has to use an enlarged or second crew, this may necessitate crew being in the cabin during the flight.</p>\r\n</li>\r\n<li>Costs for schedule changes and re-routings upon Client’s request, costs generated by passenger delays, costs for flight diversions and the extension of airport operating hours are excluded and shall be charged to Client.</li>\r\n<li>\r\n<ul></ul>\r\n<p> </p>\r\n</li>\r\n<li>MS Aviation offers are net and do not include any commission, unless requested by Client.</li>\r\n<li>Taxes if applicable will be posted separately on the quotation/invoice.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>5. <b>Payment</b></h4>\r\n<ol>\r\n<li>All payments due to MS Aviation shall be made upon receipt of invoice and no later than one (1) business day prior flight date (the “Due Date”), without setoff or deduction. Time for payment shall be of the essence, if the Due Date is not a business day (bank holiday, Saturday or Sunday) the due and payable amount shall be received and credited to MS Aviation account on the last preceding business day. Payments are to be made at costs of the sender in Euro currency to the bank account stated below:</li>\r\n<li>\r\n<ol></ol>\r\n<p> </p>\r\n<p>Beneficiary MS Aviation GmbH</p>\r\n<p>Bank: Raiffeisen Regionalbank Mödling, Am Kirchanger 3A, 2353 Guntramsdorf</p>\r\n<p>BIC: RLNWATWWGTD</p>\r\n<p>IBAN: AT35 3225 0000 0101 8449</p>\r\n<p>           </p>\r\n</li>\r\n<li>Late payments shall be subject to interest at 10% of the outstanding sum per annum from the date due until MS Aviation receipt and MS Aviation shall not be in breach of contract if it suspends Flight Services or additional services until receipt of funds.</li>\r\n<li>Incoming payments shall first be offset against the oldest debt. Payment which is not sufficient to cover the entire debt will first be offset against the interest and finally against the principal debt.</li>\r\n<li>If the payment has still not been made after issuance of a reminder and the setting of a deadline for payment, MS Aviation shall be entitled to withdraw from the contract and cancel the booking, subject to cancellation charges as set out in Section 12. MS Aviation may refuse to set a payment deadline if the imminence of the departure date makes it unfeasible to stipulate a period for payment prior to departure. In such event, MS Aviation may withdraw from the contract and refuse performance of Flight Services subject to cancellation charges as set out in Section 12.</li>\r\n<li>Major credit cards will be accepted for payment subject to any surcharges that may apply and a handling charge of 5%. If a credit card institute or a bank refuses to honor the payment required under the contract, MS Aviation shall levy Client with an administration charge of EUR 500, in addition to any charges made by the credit card institute or the bank.</li>\r\n<li>Client and Passenger shall be jointly and severally liable for the payment of the Flight Services and any additional costs set out in the quotation and/or Terms as well the cost of any damages or losses caused as a result of the conduct of the Client and/or any Passenger.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>6. <b>Flight Changes and Delays</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual carrier shall endeavor to the best of their ability to ensure punctual carriage of passengers and baggage. However, the announced flight times are subject to reasonable changes owing to operational and technical circumstances beyond MS Aviation and/or the actual operator’s control.</li>\r\n<li>The Client is responsible to ensure that passengers arrive adequately in advance of the scheduled departure time. MS Aviation and/or the actual operator’s ability to satisfy any variation in the Flight Services shall always be subject to crew duty times and rest periods and the availability of additional crew.</li>\r\n<li>Client may request a departure delay of up to a maximum of 60 minutes beyond any confirmed departure time. MS Aviation and/or the actual operator shall agree to such delay if it is compatible with crew duty time restrictions, applicable aviation regulations and air traffic control requirements. If Client delays a flight in excess of 60 minutes beyond the confirmed departure time for any reason that is not the fault of MS Aviation and/or the actual operator, the Flight Services shall be deemed to be cancelled by Client.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>7. <b>Substitution or Subcharter</b></h4>\r\n<ol>\r\n<li>Flight Services are aircraft type specific. MS Aviation reserves the right to provide the Client at MS Aviation sole discretion with an equivalent or superior aircraft type from any operator at no additional cost to Client (Substitution Aircraft). Substitution or Subcharter may occur en‐route during the Flight Services.</li>\r\n<li>In the event that an equivalent or superior Substitution Aircraft is not available for the Flight Services, MS Aviation shall advise Client without delay and provide a revised quotation with revised pricing to reflect the provision for an inferior Substitution Aircraft. In the event Client does not agree to the provision of an inferior Substitution Aircraft, Client shall be entitled to terminate Flight Services at the point of substitution subject to informing MS Aviation promptly of such cancellation and MS Aviation shall refund the amount paid on a pro‐rata basis less costs for positioning the aircraft back to point of departure for the remaining part of the trip affected by the substitution event. Should Client fail to advise MS Aviation of such cancellation promptly after being informed of such planned substitution by MS Aviation (which shall be reasonably dictated by the circumstances) then MS Aviation shall be entitled to deduct all pre-positioning costs from any applicable refund.</li>\r\n<li>Where due to a substitution event a Substitution or Subcharter Aircraft is supplied, Client’s liability shall always be to pay the costs and sums set out in the quotation plus excess costs if any for the Subcharter Aircraft.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>8. <b>Compliance with entry and exit requirements; required documents</b></h4>\r\n<ol>\r\n<li>Passengers are responsible for obtaining, and presenting at check‐in/immigration, the necessary travel documents, visas and doctor\'s certificates, certificates of vaccination and the like which are required – for themselves, or for children or animals travelling with them –  under the passport, visa and health regulations of the countries in question. In particular, MS Aviation would like to draw Passengers’ attention to visa requirements for foreign nationals.</li>\r\n<li>The contracted carrier is obliged by law to refuse carriage if the entry and exit requirements for the country of departure or destination are not met, or if the required documentation / certification is not presented.</li>\r\n<li>MS Aviation and/or the actual operator take no responsibility with the regard to entry or exit requirements of Passengers. Any costs or disadvantages arising from the failure to observe these requirements shall be incurred jointly and severally by the agent and by the Passenger such as but not limited to fines and cost of repatriation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>9. <b>Safety and Security</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual operator are entitled to change the route, flight schedule, seating capacity and maximum take‐off weight if these are required under certain operational circumstances not caused by MS Aviation and/or the actual operator.</li>\r\n<li>Captain’s Decision: The pilot in command shall at all times be entitled to take all necessary measures for safety reasons. The pilot has the authority to decide with regard to Passenger\'s seating as well as baggage loading, allocation/placement and unloading. The pilot decides whether or not and how the flight is operated. The same applies if the behavior or the physical or mental condition of a Passenger requires extraordinary assistance on behalf of the actual operator’s crew. Passenger hereby accepts all such decisions. Passenger agrees that when, in the reasonable view of the actual operator or the pilot in command, safety or security may be compromised, the actual operator or the pilot in command may decide to refuse to start or commence a flight, divert a flight or take other action necessitated by such safety considerations without liability for loss, injury, damage or delay.</li>\r\n<li>Carriage of infants, children and adolescents:</li>\r\n<li>            Infants travel according to the safety regulations of the actual operator.</li>\r\n<li>Carriage of pets:</li>\r\n<li>Owing to safety reasons and because of the limited space available, Passengers are entitled to demand the transport of pets only if MS Aviation and/or the actual operator have been notified at      the time of booking and have           confirmed carriage of the pet. Passenger is responsible that the pet complies with the requirements in the        country of destination.</li>\r\n<li>Carriage of baggage:</li>\r\n<li>            (a) Excess and general baggage</li>\r\n<li>     Passenger baggage weight is limited for flight safety reasons and varies according to aircraft type. Items determined by the crew to be of excessive weight or size will not be permitted on the aircraft. Passengers are obliged to notify MS Aviation and/or the actual operator of all excess     and general baggage, stating the dimensions and weight of the items such as but not limited         to sports equipment, pushchair/buggy and child’s car seat. The carriage of excess and general baggage shall be decided on the basis of the available hold capacity and security regulations for each flight at the sole discretion of the pilot in command. Accordingly, MS Aviation and/or the actual operator reserve the right to accept only a limited quantity or refuse the carriage of excess or general baggage entirely.</li>\r\n<li>            (b) Generally prohibited baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>            (c) Prohibited items in checked baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>            (d) Prohibited items in hand baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>Electronic equipment</li>\r\n<li>For safety reasons, the use of all personal electronic devices is strictly prohibited during take‐off and landing. The use of mobile phones is not permitted throughout the entire flight. The use of other electronic devices is permitted only with the consent of the pilot in command.</li>\r\n<li> </li>\r\n<li>Smoking</li>\r\n<li>  Smoking may be prohibited on some flights depending on the individual aircraft. Additional Costs for fines, fees and/or cabin cleaning will be charged to Passenger.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>10. <b>Force Majeure</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual carrier reserve the right to at any time during the carriage to suspend or redirect the flight in question and/or provide the Passenger with another similar aircraft or cancel the flight without further liability to the Passenger in the event that the carriage cannot be completed in accordance with Passenger’s requirements due to war, warlike events, infringements of a country’s neutrality, insurrection, civil war, civil unrest, riots, sabotage, strikes, blockades, lockouts, quarantine, hijacking, terrorist actions, requisition, confiscation, expropriation, seizure, adverse weather conditions or other force majeure of any nature, technical reasons, detention or similar measures, accidents with aircraft, or due no other factors over which MS AVIATION and/or the actual operator have no control, or when the safety of the Passengers or the crew from the aircraft can reasonably be assessed to be in danger, at the discretion of the pilot in command or of the actual operator’s personnel (“Force Majeure Event”). Where MS Aviation and/or the actual operator cancel the contract having commenced but not completed the carriage due to the Force Majeure Event, Passenger shall only be charged on a pro rata basis for the portion of the carriage performed and any balance shall be refunded to Passenger.</li>\r\n<li>In the event that a Force Majeure Event occurs prior to the commencement of the carriage and no suitable solution can be found in the reasonable opinion of MS Aviation and/or the actual operator, MS Aviation and/or the actual operator reserve the right to cancel the contract of carriage without liability to Passenger. In this case, MS Aviation shall credit the Passenger with an amount corresponding to the flight in question minus all expenses already incurred.</li>\r\n<li>Unless stated otherwise in mandatory (indispensable) legislation, MS Aviation shall not be responsible for damage or loss as a result of or arising, directly or indirectly, in connection with the abovementioned circumstances.</li>\r\n<li>MS Aviation shall not be liable for any damage or loss of any nature whatsoever to Passengers arising from any delay arising as a result of a Force Majeure Event.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>11. <b>General Liability Provisions</b></h4>\r\n<ol>\r\n<li>\r\n<p>(a) Liability in connection with the carriage of Passengers, freight and baggage is subject to the individual terms and conditions of the actual operator and can be provided to Client upon request. MS Aviation makes no         representation or warranty with regard to the third party operator and will not be liable in any way whatsoever        for any loss, damage, injury or expense suffered or incurred by the Client, the Passenger(s) or any third party   howsoever.</p>\r\n<p>(b) Client shall indemnify and hold harmless MS Aviation against all liabilities, claims and expenses (including legal costs and fees) in respect of any liability of MS Aviation towards such third party operators for any loss or damage whatsoever (including costs and expenses on a full indemnity basis) arising out of any act or omission of Client, its servants or agents or any Passenger(s) carried by authority of Client.</p>\r\n<p>(c) MS Aviation and/or the actual carrier shall not be liable for damage caused in fulfilment of      government regulations or because the Passenger fails to satisfy his/her obligations pursuant to these regulations.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>12. <b>Cancellation</b></h4>\r\n<ol>\r\n<li>Flight Services shall be deemed cancelled by Client in the event of (i) cancellation of any booked flight communicated by the Client to MS Aviation in advance of the commencement of Flight Services, (ii) a delay of any passenger and/or Client in excess of 60 minutes to the scheduled time of departure unless specifically agreed by MS Aviation in advance in writing (iii) a no‐show of either the Client and/or any Passenger, or (iv) any refusal of the Client and/or any of its passengers to comply with the reasonable instructions of MS Aviation or the actual operator’s pilot‐in command for flight safety and/or security reasons leading to the pilot‐in‐command to reasonably deem it necessary to cancel or terminate a planned flight and (v) if Client fails to make the payment prior the Due Date.</li>\r\n<li>In such circumstances the Cancellation Fees set out below shall be applicable and payable by Client:</li>\r\n<li>\r\n<ul></ul>\r\n<p> <strong>Up to 15 days before the 1st scheduled departure: 10% of the charter price;</strong></p>\r\n<p><strong></strong><strong>less than 15 days but at least 7 days before the 1st scheduled departure: 20% of the charter price;</strong></p>\r\n<p><strong>less than 7 days but at least 48 hours before the 1st scheduled departure: 35% of the charter price</strong></p>\r\n</li>\r\n<li>\r\n<p><strong></strong><strong>less than 48 hours but at least 24 hours before the 1st scheduled departure or in case of a no show: 50% of the charter price.</strong></p>\r\n<p><strong></strong><strong> less than 24 hours before the 1st scheduled departure or in case of a no show: 100% of the charter price.</strong></p>\r\n</li>\r\n<li>All Cancellation Fees are subject to a minimum payment of Euro 1,000 which is a reasonable pre‐estimate of the minimum cost to MS Aviation where a booked flight is cancelled and takes account, by way of example only, costs associated with the arrangement and movement of flight crew, permissions and associated administration and the logistics involved in organizing the flight and any extra services.</li>\r\n<li>In the event a flight is cancelled not as a result of actions of MS Aviation, the costs of any additional goods and/or services arranged by MS Aviation at the Client’s request through third party supplier(s) ancillary to the actual booked flight shall also remain the responsibility of the    Client and shall be charged to the Client at the cost.</li>\r\n<li>Any reimbursement of flight charges and any other amounts paid by the Client in advance of the booked flight shall be subject to the deduction of any amounts outstanding, Cancellation Fees and the balance of the sum paid by the Client, shall be repaid to the Client within 7 days of the date of receipt of cleared funds for the original booking. In any other circumstances (for example if the funds paid by Client are not sufficient to cover the Cancellation Fees), Client undertakes to make payment of any sums to cover the Cancellation Fees within 7 days of the date of issue by MS Aviation of an invoice to the Client for such sums.</li>\r\n<li>MS Aviation will not be liable to the Client for any loss or expense incurred by the Client or any Passenger in the event of cancellation due to their failure to comply with the provisions set out in these Terms.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>13. <b>Amendments</b></h4>\r\n<ol>\r\n<li>MS Aviation reserves the right to amend these Terms at any time with effect for the future without obligation to notify Client. On continuing to use MS Aviation services after amendment of the Terms, Client declares the consent to the amendments.</li>\r\n<li>No agency, employee or any other third party is entitled to make any amendments and/or addenda to these Terms or to waive their applicability.</li>\r\n<li>These Terms contain the entire provisions of the contract between the Passenger and MS Aviation and supersede all previous agreements, regardless of whether such agreements were made verbally, by electronic means or in writing. In case of conflict between these Terms and any Special Terms, the Special Terms shall take precedence.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>14. <b>Privacy and data protection</b></h4>\r\n<ol>\r\n<li>\r\n<p>(a) Personal data (such as name, address, telephone number and credit card details) are essential for booking. The protection of Passengers’ personal data is very important to MS Aviation. Data will be stored and protected at MS Aviation files.</p>\r\n<p>(b)MS Aviation and/or the actual carrier are explicitly entitled to transmit data obtained from official photo identification documents and other personal data processed or used in connection with   the carriage to public authorities, provided that the authority\'s request for disclosure is based on mandatory legal regulations and is necessary for performance of the contract.</p>\r\n<p>(c)MS Aviation and/or actual carrier are entitled to capture, process and use personal data within the scope of performance of the contract. The data is processed or used for the following purposes: making reservations, purchase of a ticket, purchase of additional services and handling payment transactions; development and provision of services, facilitating entry and customs clearance procedures.</p>\r\n<p>(d) The Passenger authorizes MS Aviation and/or actual carrier to capture, save, modify, block, delete and use the        data for that purpose and to transmit it to their own branch offices, authorized representatives and to the        parties who provide the above services on behalf of MS Aviation and/or actual carrier.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>15. <b>Severability</b></h4>\r\n<ol>\r\n<li>\r\n<p>Should any one or more clauses of these terms be found to be illegal or unenforceable in any respect, the validity, legality and enforceability of the remaining clauses shall not in any way be affected or impaired thereby.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>16. <b>Applicable Law</b></h4>\r\n<ol>\r\n<li>\r\n<p>The contractual relationship between the Passenger and MS Aviation shall be governed by the laws of the Republic of Austria, irrespective of the Passenger’s nationality.</p>\r\n<p><strong>Please countersign this Charter Confirmation and return to us by fax or    e-mail. By countersigning and returning this Confirmation to us, you acknowledge having read, you confirm you understood and hereby agree to our General Terms and Conditions.</strong></p>\r\n<span style=\"font-weight: 400;\">.</span></li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH --></div>\r\n</div>\r\n</section>', NULL, 1, 1),
(69, 1, 37, 'Legal Notice German', '<section class=\"fleets section_padding padding-top-190\">\r\n<div class=\"container-fluid\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>lEGAL NOTICE</h2>\r\n<!-- <p>The Austrian company MS Aviation was founded by a group of industry professionals in business aviation.</p> --></div>\r\n</div>\r\n<div class=\"container\">\r\n<p class=\"x_MsoNormal\"><span>Impressum</span></p>\r\n<p class=\"x_MsoNormal\"><span>Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz</span></p>\r\n<p class=\"x_MsoNormal\"> </p>\r\n<p class=\"x_MsoNormal\"><span>MS Aviation GmbH</span></p>\r\n<p class=\"x_MsoNormal\"><span>Unternehmensgegenstand: Luftfahrtunternehmen</span></p>\r\n<p class=\"x_MsoNormal\"><span>UID-Nummer: ATU71491245</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmenbuchnummer: FN459795 w</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmenbuchgericht: Landesgericht Wr. Neustadt</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmensitz: 2355 Wiener Neudorf</span></p>\r\n<p class=\"x_MsoNormal\"><span>IZ NÖ-SÜD, Strasse 3 Objekt 1, Top 1,Austria</span></p>\r\n<p class=\"x_MsoNormal\"><span>Tel.: <a href=\"tel:06645020462\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">06645020462</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>E-Mail: <a href=\"mailto:ms@msaviation.at\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">ms@msaviation.at</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>Berufsrecht: <a href=\"http://www.ris.bka.gv.at/\" target=\"_blank\" rel=\"noopener noreferrer\">www.ris.bka.gv.at</a></span></p>\r\n<p class=\"x_MsoNormal\"> </p>\r\n<p class=\"x_MsoNormal\"><span>Aufsichtsbehörde/Gewerbebehörde: Austro Control Österreichische Gesellschaft für Zivilluftfahrt mit beschränkter Haftung Wagramer Straße 19, 1220 Vienna <a href=\"http://www.austrocontrol.co.at/\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">www.austrocontrol.co.at</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>Berufsbezeichnung: Luftfahrtunternehmen</span></p>\r\n<p class=\"x_MsoNormal\"><span>Verleihungsstaat: Österreich</span></p>\r\n<p class=\"x_MsoNormal\"><span>Angaben zur Online-Streitbeilegung: Verbraucher haben die Möglichkeit, Beschwerden an die OnlineStreitbeilegungsplattform der EU zu richten: <a href=\"http://ec.europa.eu/odr\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">http://ec.europa.eu/odr</a>. Sie können allfällige Beschwerde auch an die oben angegebene E-Mail-Adresse richten.</span></p>\r\n<!-- <p>The Austrian company MS Aviation was founded by a group of industry professionals in business aviation.</p> --></div>\r\n</div>\r\n</section>', NULL, 1, 1),
(70, 1, 38, 'Legal Notice Russian', '<section class=\"fleets section_padding padding-top-190\">\r\n<div class=\"container-fluid\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>lEGAL NOTICE</h2>\r\n<!-- <p>The Austrian company MS Aviation was founded by a group of industry professionals in business aviation.</p> --></div>\r\n</div>\r\n<div class=\"container\">\r\n<p class=\"x_MsoNormal\"><span>Impressum</span></p>\r\n<p class=\"x_MsoNormal\"><span>Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz</span></p>\r\n<p class=\"x_MsoNormal\"> </p>\r\n<p class=\"x_MsoNormal\"><span>MS Aviation GmbH</span></p>\r\n<p class=\"x_MsoNormal\"><span>Unternehmensgegenstand: Luftfahrtunternehmen</span></p>\r\n<p class=\"x_MsoNormal\"><span>UID-Nummer: ATU71491245</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmenbuchnummer: FN459795 w</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmenbuchgericht: Landesgericht Wr. Neustadt</span></p>\r\n<p class=\"x_MsoNormal\"><span>Firmensitz: 2355 Wiener Neudorf</span></p>\r\n<p class=\"x_MsoNormal\"><span>IZ NÖ-SÜD, Strasse 3 Objekt 1, Top 1,Austria</span></p>\r\n<p class=\"x_MsoNormal\"><span>Tel.: <a href=\"tel:06645020462\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">06645020462</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>E-Mail: <a href=\"mailto:ms@msaviation.at\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">ms@msaviation.at</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>Berufsrecht: <a href=\"http://www.ris.bka.gv.at/\" target=\"_blank\" rel=\"noopener noreferrer\">www.ris.bka.gv.at</a></span></p>\r\n<p class=\"x_MsoNormal\"> </p>\r\n<p class=\"x_MsoNormal\"><span>Aufsichtsbehörde/Gewerbebehörde: Austro Control Österreichische Gesellschaft für Zivilluftfahrt mit beschränkter Haftung Wagramer Straße 19, 1220 Vienna <a href=\"http://www.austrocontrol.co.at/\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">www.austrocontrol.co.at</a></span></p>\r\n<p class=\"x_MsoNormal\"><span>Berufsbezeichnung: Luftfahrtunternehmen</span></p>\r\n<p class=\"x_MsoNormal\"><span>Verleihungsstaat: Österreich</span></p>\r\n<p class=\"x_MsoNormal\"><span>Angaben zur Online-Streitbeilegung: Verbraucher haben die Möglichkeit, Beschwerden an die OnlineStreitbeilegungsplattform der EU zu richten: <a href=\"http://ec.europa.eu/odr\" target=\"_blank\" rel=\"noopener noreferrer\" dir=\"ltr\">http://ec.europa.eu/odr</a>. Sie können allfällige Beschwerde auch an die oben angegebene E-Mail-Adresse richten.</span></p>\r\n<!-- <p>The Austrian company MS Aviation was founded by a group of industry professionals in business aviation.</p> --></div>\r\n</div>\r\n</section>', NULL, 1, 1);
INSERT INTO `page_content` (`page_content_id`, `page_content_type_related_id`, `page_content_related_id`, `page_content_name`, `page_content_data`, `page_content_template`, `is_public`, `page_content_order`) VALUES
(71, 1, 39, 'TERMS RU', '<div id=\"header\" class=\"triangle header-title fleet\" style=\"background-image: url(\'{base_url()}assets/images/background/msaviation_bg_about.jpg\'); background-size: cover;\"></div>\r\n<!-- Start Supertabs -->\r\n<p></p>\r\n<!-- Start About -->\r\n<section class=\"about section_padding\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 section-header\">\r\n<h2>Условия и положения</h2>\r\n<p></p>\r\n</div>\r\n</div>\r\n<!-- End Header-Preamble -->\r\n<div class=\"row\">\r\n<p></p>\r\n<p>Your use and access of this website indicates you accept these Terms and Conditions.</p>\r\n<h4>1. <strong>General</strong></h4>\r\n<ol>\r\n<li>These General Terms and Conditions for Charter and any additional terms set out in any relevant quotation and/or confirmation (collectively hereinafter the “Terms”) form the contractual basis for the provision of Flight Services arranged by MS Aviation GmbH (“MS Aviation”).</li>\r\n<li>The Terms are applicable for commercial transport of Passengers and/or any permitted goods from an agreed point of departure to an agreed point of destination as more particularly detailed in the quotation and/or confirmation (“Flight Services”).</li>\r\n<li>The contract may‐be concluded with (i) Agents only or (ii) with Passenger directly however in any case Agents and Passengers (“Client”) are jointly bound by these Terms. It is the responsibility of the Client to ensure that each Passenger abides by the Terms.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>2. <strong>Conclusion of Contract</strong></h4>\r\n<ol>\r\n<li>The quotation issued by MS Aviation constitutes a non‐binding offer. Only the issuance of a Confirmation by MS Aviation constitutes a binding offer which requires acceptance within the acceptance period. If such acceptance period is lapsed, MS Aviation shall not be bound by its Confirmation.</li>\r\n<li>The return of the Confirmation duly signed by an Officer of Client constitutes a binding contract of carriage between Client and MS Aviation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>3. <b>Performance of Flight Services</b></h4>\r\n<ol>\r\n<li>MS Aviation shall not act as a common carrier or any other type of carrier in respect of any of its obligations under the relevant agreement with such third party and MS Aviation acts solely as agent for Client and operator. Therefore, MS Aviation subcontracts its contractual obligations in part or in whole to a third party or to third parties including any actual carrier subject to clause 12. In such cases, the terms of business of third party operators shall apply to Client, which are hereby incorporated by reference and a copy shall be furnished to Client upon request.</li>\r\n<li>The Flight Services will be performed pursuant to and in accordance with the actual Terms as applicable at the date of Flight Services and the operating procedures approved by the competent authority of the contractual or the actual carrier as the case may‐</li>\r\n<li>MS Aviation and/or the actual operator expressively reserve the right to utilize on its own account any lay‐over period or empty capacity the aircraft may have, including any empty legs related to the Flight Services, before, during or after the period in which the aircraft is available to Client.</li>\r\n<li>Flight Services are planned with a set of one (1) crew (Pilot in Command, Copilot) subject to crew duty time and rest period restrictions by applicable duty limitation regulations.</li>\r\n<li>Cabin Service by one (1) Cabin Hostess is included for all flights on Heavy Jets. Additional Cabin Hostess may be supplied upon request and subject to additional charge at the sole discretion of MS Aviation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>4. <b>Included and Excluded Costs</b></h4>\r\n<ol>\r\n<li>The Price does include aircraft costs including crew, fuel, maintenance, air navigation, airport and handling charges, standard use of a General Aviation Terminal or similar facilities at destinations by passengers (e.g. FBOs, VIP Halls, VIP areas), inflight MS Aviation standard catering (depending on flight time and time of day), Passenger and baggage insurance as provided by the actual carrier.</li>\r\n<li>The following costs are not included and shall be charged separately to Client at cost:</li>\r\n<li>\r\n<ol></ol>\r\n<p>            - De- or Anti-Icings;</p>\r\n<p>            ‐ Insurance surcharges;</p>\r\n<p>            ‐ SATCOM services;</p>\r\n<p>            ‐ Special catering requests such as but not limited to caviar and special wines or spirits;</p>\r\n<p>            - VIP lounges or meeting rooms for exclusive use at the destinations</p>\r\n<p>            ‐ special handling, helicopter and/or limousine services;</p>\r\n<p>            ‐ any other concierge services rendered by MS Aviation upon request by Client;</p>\r\n<p>‐ additional or enlarged crew or Cabin Hostess as result of a request by the Client and/or any  Passenger. In such event, Client acknowledges and agrees that if the actual carrier has to use an enlarged or second crew, this may necessitate crew being in the cabin during the flight.</p>\r\n</li>\r\n<li>Costs for schedule changes and re-routings upon Client’s request, costs generated by passenger delays, costs for flight diversions and the extension of airport operating hours are excluded and shall be charged to Client.</li>\r\n<li>\r\n<ul></ul>\r\n<p> </p>\r\n</li>\r\n<li>MS Aviation offers are net and do not include any commission, unless requested by Client.</li>\r\n<li>Taxes if applicable will be posted separately on the quotation/invoice.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>5. <b>Payment</b></h4>\r\n<ol>\r\n<li>All payments due to MS Aviation shall be made upon receipt of invoice and no later than one (1) business day prior flight date (the “Due Date”), without setoff or deduction. Time for payment shall be of the essence, if the Due Date is not a business day (bank holiday, Saturday or Sunday) the due and payable amount shall be received and credited to MS Aviation account on the last preceding business day. Payments are to be made at costs of the sender in Euro currency to the bank account stated below:</li>\r\n<li>\r\n<ol></ol>\r\n<p> </p>\r\n<p>Beneficiary MS Aviation GmbH</p>\r\n<p>Bank: Raiffeisen Regionalbank Mödling, Am Kirchanger 3A, 2353 Guntramsdorf</p>\r\n<p>BIC: RLNWATWWGTD</p>\r\n<p>IBAN: AT35 3225 0000 0101 8449</p>\r\n<p>           </p>\r\n</li>\r\n<li>Late payments shall be subject to interest at 10% of the outstanding sum per annum from the date due until MS Aviation receipt and MS Aviation shall not be in breach of contract if it suspends Flight Services or additional services until receipt of funds.</li>\r\n<li>Incoming payments shall first be offset against the oldest debt. Payment which is not sufficient to cover the entire debt will first be offset against the interest and finally against the principal debt.</li>\r\n<li>If the payment has still not been made after issuance of a reminder and the setting of a deadline for payment, MS Aviation shall be entitled to withdraw from the contract and cancel the booking, subject to cancellation charges as set out in Section 12. MS Aviation may refuse to set a payment deadline if the imminence of the departure date makes it unfeasible to stipulate a period for payment prior to departure. In such event, MS Aviation may withdraw from the contract and refuse performance of Flight Services subject to cancellation charges as set out in Section 12.</li>\r\n<li>Major credit cards will be accepted for payment subject to any surcharges that may apply and a handling charge of 5%. If a credit card institute or a bank refuses to honor the payment required under the contract, MS Aviation shall levy Client with an administration charge of EUR 500, in addition to any charges made by the credit card institute or the bank.</li>\r\n<li>Client and Passenger shall be jointly and severally liable for the payment of the Flight Services and any additional costs set out in the quotation and/or Terms as well the cost of any damages or losses caused as a result of the conduct of the Client and/or any Passenger.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>6. <b>Flight Changes and Delays</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual carrier shall endeavor to the best of their ability to ensure punctual carriage of passengers and baggage. However, the announced flight times are subject to reasonable changes owing to operational and technical circumstances beyond MS Aviation and/or the actual operator’s control.</li>\r\n<li>The Client is responsible to ensure that passengers arrive adequately in advance of the scheduled departure time. MS Aviation and/or the actual operator’s ability to satisfy any variation in the Flight Services shall always be subject to crew duty times and rest periods and the availability of additional crew.</li>\r\n<li>Client may request a departure delay of up to a maximum of 60 minutes beyond any confirmed departure time. MS Aviation and/or the actual operator shall agree to such delay if it is compatible with crew duty time restrictions, applicable aviation regulations and air traffic control requirements. If Client delays a flight in excess of 60 minutes beyond the confirmed departure time for any reason that is not the fault of MS Aviation and/or the actual operator, the Flight Services shall be deemed to be cancelled by Client.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>7. <b>Substitution or Subcharter</b></h4>\r\n<ol>\r\n<li>Flight Services are aircraft type specific. MS Aviation reserves the right to provide the Client at MS Aviation sole discretion with an equivalent or superior aircraft type from any operator at no additional cost to Client (Substitution Aircraft). Substitution or Subcharter may occur en‐route during the Flight Services.</li>\r\n<li>In the event that an equivalent or superior Substitution Aircraft is not available for the Flight Services, MS Aviation shall advise Client without delay and provide a revised quotation with revised pricing to reflect the provision for an inferior Substitution Aircraft. In the event Client does not agree to the provision of an inferior Substitution Aircraft, Client shall be entitled to terminate Flight Services at the point of substitution subject to informing MS Aviation promptly of such cancellation and MS Aviation shall refund the amount paid on a pro‐rata basis less costs for positioning the aircraft back to point of departure for the remaining part of the trip affected by the substitution event. Should Client fail to advise MS Aviation of such cancellation promptly after being informed of such planned substitution by MS Aviation (which shall be reasonably dictated by the circumstances) then MS Aviation shall be entitled to deduct all pre-positioning costs from any applicable refund.</li>\r\n<li>Where due to a substitution event a Substitution or Subcharter Aircraft is supplied, Client’s liability shall always be to pay the costs and sums set out in the quotation plus excess costs if any for the Subcharter Aircraft.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>8. <b>Compliance with entry and exit requirements; required documents</b></h4>\r\n<ol>\r\n<li>Passengers are responsible for obtaining, and presenting at check‐in/immigration, the necessary travel documents, visas and doctor\'s certificates, certificates of vaccination and the like which are required – for themselves, or for children or animals travelling with them –  under the passport, visa and health regulations of the countries in question. In particular, MS Aviation would like to draw Passengers’ attention to visa requirements for foreign nationals.</li>\r\n<li>The contracted carrier is obliged by law to refuse carriage if the entry and exit requirements for the country of departure or destination are not met, or if the required documentation / certification is not presented.</li>\r\n<li>MS Aviation and/or the actual operator take no responsibility with the regard to entry or exit requirements of Passengers. Any costs or disadvantages arising from the failure to observe these requirements shall be incurred jointly and severally by the agent and by the Passenger such as but not limited to fines and cost of repatriation.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>9. <b>Safety and Security</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual operator are entitled to change the route, flight schedule, seating capacity and maximum take‐off weight if these are required under certain operational circumstances not caused by MS Aviation and/or the actual operator.</li>\r\n<li>Captain’s Decision: The pilot in command shall at all times be entitled to take all necessary measures for safety reasons. The pilot has the authority to decide with regard to Passenger\'s seating as well as baggage loading, allocation/placement and unloading. The pilot decides whether or not and how the flight is operated. The same applies if the behavior or the physical or mental condition of a Passenger requires extraordinary assistance on behalf of the actual operator’s crew. Passenger hereby accepts all such decisions. Passenger agrees that when, in the reasonable view of the actual operator or the pilot in command, safety or security may be compromised, the actual operator or the pilot in command may decide to refuse to start or commence a flight, divert a flight or take other action necessitated by such safety considerations without liability for loss, injury, damage or delay.</li>\r\n<li>Carriage of infants, children and adolescents:</li>\r\n<li>            Infants travel according to the safety regulations of the actual operator.</li>\r\n<li>Carriage of pets:</li>\r\n<li>Owing to safety reasons and because of the limited space available, Passengers are entitled to demand the transport of pets only if MS Aviation and/or the actual operator have been notified at      the time of booking and have           confirmed carriage of the pet. Passenger is responsible that the pet complies with the requirements in the        country of destination.</li>\r\n<li>Carriage of baggage:</li>\r\n<li>            (a) Excess and general baggage</li>\r\n<li>     Passenger baggage weight is limited for flight safety reasons and varies according to aircraft type. Items determined by the crew to be of excessive weight or size will not be permitted on the aircraft. Passengers are obliged to notify MS Aviation and/or the actual operator of all excess     and general baggage, stating the dimensions and weight of the items such as but not limited         to sports equipment, pushchair/buggy and child’s car seat. The carriage of excess and general baggage shall be decided on the basis of the available hold capacity and security regulations for each flight at the sole discretion of the pilot in command. Accordingly, MS Aviation and/or the actual operator reserve the right to accept only a limited quantity or refuse the carriage of excess or general baggage entirely.</li>\r\n<li>            (b) Generally prohibited baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>            (c) Prohibited items in checked baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>            (d) Prohibited items in hand baggage</li>\r\n<li>For safety reasons, certain materials and items shall not be placed in either hold or checked baggage and will not be carried. The list of such items shall be provided to Client by MS Aviation and/or the actual operator upon Client’s request.</li>\r\n<li>Electronic equipment</li>\r\n<li>For safety reasons, the use of all personal electronic devices is strictly prohibited during take‐off and landing. The use of mobile phones is not permitted throughout the entire flight. The use of other electronic devices is permitted only with the consent of the pilot in command.</li>\r\n<li> </li>\r\n<li>Smoking</li>\r\n<li>  Smoking may be prohibited on some flights depending on the individual aircraft. Additional Costs for fines, fees and/or cabin cleaning will be charged to Passenger.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>10. <b>Force Majeure</b></h4>\r\n<ol>\r\n<li>MS Aviation and/or the actual carrier reserve the right to at any time during the carriage to suspend or redirect the flight in question and/or provide the Passenger with another similar aircraft or cancel the flight without further liability to the Passenger in the event that the carriage cannot be completed in accordance with Passenger’s requirements due to war, warlike events, infringements of a country’s neutrality, insurrection, civil war, civil unrest, riots, sabotage, strikes, blockades, lockouts, quarantine, hijacking, terrorist actions, requisition, confiscation, expropriation, seizure, adverse weather conditions or other force majeure of any nature, technical reasons, detention or similar measures, accidents with aircraft, or due no other factors over which MS AVIATION and/or the actual operator have no control, or when the safety of the Passengers or the crew from the aircraft can reasonably be assessed to be in danger, at the discretion of the pilot in command or of the actual operator’s personnel (“Force Majeure Event”). Where MS Aviation and/or the actual operator cancel the contract having commenced but not completed the carriage due to the Force Majeure Event, Passenger shall only be charged on a pro rata basis for the portion of the carriage performed and any balance shall be refunded to Passenger.</li>\r\n<li>In the event that a Force Majeure Event occurs prior to the commencement of the carriage and no suitable solution can be found in the reasonable opinion of MS Aviation and/or the actual operator, MS Aviation and/or the actual operator reserve the right to cancel the contract of carriage without liability to Passenger. In this case, MS Aviation shall credit the Passenger with an amount corresponding to the flight in question minus all expenses already incurred.</li>\r\n<li>Unless stated otherwise in mandatory (indispensable) legislation, MS Aviation shall not be responsible for damage or loss as a result of or arising, directly or indirectly, in connection with the abovementioned circumstances.</li>\r\n<li>MS Aviation shall not be liable for any damage or loss of any nature whatsoever to Passengers arising from any delay arising as a result of a Force Majeure Event.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>11. <b>General Liability Provisions</b></h4>\r\n<ol>\r\n<li>\r\n<p>(a) Liability in connection with the carriage of Passengers, freight and baggage is subject to the individual terms and conditions of the actual operator and can be provided to Client upon request. MS Aviation makes no         representation or warranty with regard to the third party operator and will not be liable in any way whatsoever        for any loss, damage, injury or expense suffered or incurred by the Client, the Passenger(s) or any third party   howsoever.</p>\r\n<p>(b) Client shall indemnify and hold harmless MS Aviation against all liabilities, claims and expenses (including legal costs and fees) in respect of any liability of MS Aviation towards such third party operators for any loss or damage whatsoever (including costs and expenses on a full indemnity basis) arising out of any act or omission of Client, its servants or agents or any Passenger(s) carried by authority of Client.</p>\r\n<p>(c) MS Aviation and/or the actual carrier shall not be liable for damage caused in fulfilment of      government regulations or because the Passenger fails to satisfy his/her obligations pursuant to these regulations.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>12. <b>Cancellation</b></h4>\r\n<ol>\r\n<li>Flight Services shall be deemed cancelled by Client in the event of (i) cancellation of any booked flight communicated by the Client to MS Aviation in advance of the commencement of Flight Services, (ii) a delay of any passenger and/or Client in excess of 60 minutes to the scheduled time of departure unless specifically agreed by MS Aviation in advance in writing (iii) a no‐show of either the Client and/or any Passenger, or (iv) any refusal of the Client and/or any of its passengers to comply with the reasonable instructions of MS Aviation or the actual operator’s pilot‐in command for flight safety and/or security reasons leading to the pilot‐in‐command to reasonably deem it necessary to cancel or terminate a planned flight and (v) if Client fails to make the payment prior the Due Date.</li>\r\n<li>In such circumstances the Cancellation Fees set out below shall be applicable and payable by Client:</li>\r\n<li>\r\n<ul></ul>\r\n<p> <strong>Up to 15 days before the 1st scheduled departure: 10% of the charter price;</strong></p>\r\n<p><strong></strong><strong>less than 15 days but at least 7 days before the 1st scheduled departure: 20% of the charter price;</strong></p>\r\n<p><strong>less than 7 days but at least 48 hours before the 1st scheduled departure: 35% of the charter price</strong></p>\r\n</li>\r\n<li>\r\n<p><strong></strong><strong>less than 48 hours but at least 24 hours before the 1st scheduled departure or in case of a no show: 50% of the charter price.</strong></p>\r\n<p><strong></strong><strong> less than 24 hours before the 1st scheduled departure or in case of a no show: 100% of the charter price.</strong></p>\r\n</li>\r\n<li>All Cancellation Fees are subject to a minimum payment of Euro 1,000 which is a reasonable pre‐estimate of the minimum cost to MS Aviation where a booked flight is cancelled and takes account, by way of example only, costs associated with the arrangement and movement of flight crew, permissions and associated administration and the logistics involved in organizing the flight and any extra services.</li>\r\n<li>In the event a flight is cancelled not as a result of actions of MS Aviation, the costs of any additional goods and/or services arranged by MS Aviation at the Client’s request through third party supplier(s) ancillary to the actual booked flight shall also remain the responsibility of the    Client and shall be charged to the Client at the cost.</li>\r\n<li>Any reimbursement of flight charges and any other amounts paid by the Client in advance of the booked flight shall be subject to the deduction of any amounts outstanding, Cancellation Fees and the balance of the sum paid by the Client, shall be repaid to the Client within 7 days of the date of receipt of cleared funds for the original booking. In any other circumstances (for example if the funds paid by Client are not sufficient to cover the Cancellation Fees), Client undertakes to make payment of any sums to cover the Cancellation Fees within 7 days of the date of issue by MS Aviation of an invoice to the Client for such sums.</li>\r\n<li>MS Aviation will not be liable to the Client for any loss or expense incurred by the Client or any Passenger in the event of cancellation due to their failure to comply with the provisions set out in these Terms.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>13. <b>Amendments</b></h4>\r\n<ol>\r\n<li>MS Aviation reserves the right to amend these Terms at any time with effect for the future without obligation to notify Client. On continuing to use MS Aviation services after amendment of the Terms, Client declares the consent to the amendments.</li>\r\n<li>No agency, employee or any other third party is entitled to make any amendments and/or addenda to these Terms or to waive their applicability.</li>\r\n<li>These Terms contain the entire provisions of the contract between the Passenger and MS Aviation and supersede all previous agreements, regardless of whether such agreements were made verbally, by electronic means or in writing. In case of conflict between these Terms and any Special Terms, the Special Terms shall take precedence.</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>14. <b>Privacy and data protection</b></h4>\r\n<ol>\r\n<li>\r\n<p>(a) Personal data (such as name, address, telephone number and credit card details) are essential for booking. The protection of Passengers’ personal data is very important to MS Aviation. Data will be stored and protected at MS Aviation files.</p>\r\n<p>(b)MS Aviation and/or the actual carrier are explicitly entitled to transmit data obtained from official photo identification documents and other personal data processed or used in connection with   the carriage to public authorities, provided that the authority\'s request for disclosure is based on mandatory legal regulations and is necessary for performance of the contract.</p>\r\n<p>(c)MS Aviation and/or actual carrier are entitled to capture, process and use personal data within the scope of performance of the contract. The data is processed or used for the following purposes: making reservations, purchase of a ticket, purchase of additional services and handling payment transactions; development and provision of services, facilitating entry and customs clearance procedures.</p>\r\n<p>(d) The Passenger authorizes MS Aviation and/or actual carrier to capture, save, modify, block, delete and use the        data for that purpose and to transmit it to their own branch offices, authorized representatives and to the        parties who provide the above services on behalf of MS Aviation and/or actual carrier.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>15. <b>Severability</b></h4>\r\n<ol>\r\n<li>\r\n<p>Should any one or more clauses of these terms be found to be illegal or unenforceable in any respect, the validity, legality and enforceability of the remaining clauses shall not in any way be affected or impaired thereby.</p>\r\n</li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH -->\r\n<h4>16. <b>Applicable Law</b></h4>\r\n<ol>\r\n<li>\r\n<p>The contractual relationship between the Passenger and MS Aviation shall be governed by the laws of the Republic of Austria, irrespective of the Passenger’s nationality.</p>\r\n<p><strong>Please countersign this Charter Confirmation and return to us by fax or    e-mail. By countersigning and returning this Confirmation to us, you acknowledge having read, you confirm you understood and hereby agree to our General Terms and Conditions.</strong></p>\r\n<span style=\"font-weight: 400;\">.</span></li>\r\n</ol>\r\n<!--ENDS TERMS PARAGRAPH --></div>\r\n</div>\r\n</section>', NULL, 1, 1),
(72, 2, 40, 'Fly now RU', NULL, '_fly_now_form', 1, 1),
(73, 2, 42, 'FLY NOW DE', NULL, '_fly_now_form', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_content_types`
--

CREATE TABLE `page_content_types` (
  `page_content_type_id` int(1) UNSIGNED NOT NULL,
  `page_content_type_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_content_type_title_template_file` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_content_types`
--

INSERT INTO `page_content_types` (`page_content_type_id`, `page_content_type_title`, `page_content_type_title_template_file`) VALUES
(1, 'html', NULL),
(2, 'template', NULL),
(3, 'multitemplate', NULL),
(4, 'single_template', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `portfolio_id` int(1) NOT NULL,
  `portfolio_title` varchar(185) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_main_image_1` varchar(355) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_main_image_2` varchar(355) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_main_image_3` varchar(355) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portfolio_location` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_client` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_date` date NOT NULL,
  `portfolio_surface_area` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`portfolio_id`, `portfolio_title`, `portfolio_description`, `portfolio_main_image_1`, `portfolio_main_image_2`, `portfolio_main_image_3`, `portfolio_location`, `portfolio_client`, `portfolio_date`, `portfolio_surface_area`) VALUES
(1, 'An ideal position for the bed', '<p>\n	portfolio</p>\n', '197f4-36541802_402118663625709_8738552202882187264_o.jpg', '', NULL, 'Cyprus', 'designyourway', '2016-04-13', '50000 m²'),
(2, 'Choosing the appropriate color scheme', '<p>\n	Colors are tightly associated with temperature, and that&rsquo;s exactly what you need to compare before making a final choice. Selecting warm shades for the bedroom walls may be over-stimulating and disturb the relaxing atmosphere of your bedroom, which doesn&rsquo;t imply you should use a completely cool palette, as such can depress you rather than relaxing you.</p>\n', 'de7f0-37750667_421806524990256_8472369315495018496_o.jpg', '', NULL, 'Cyprus', 'Colors', '2016-07-12', 'm²'),
(4, 'Use what you already have', '<p>\n	Changing the overall vibe of your bedroom interior design is possible without spending a single cent, as long as you know how to rearrange your furniture. There are many interior design bedroom ideas online, and we&rsquo;re going to focus on a few in this article. There are many DIY techniques which require nothing but a blue painter&rsquo;s tape and a proper plan. To start with, you should measure the room to get the whole picture of your possibilities, map out furniture placement, and know exactly what you want to achieve. Once you have a reasonable plan on paper, try to imagine the whole process, and think of small, off-the-shelf priced knickknacks that can complete the look.</p>\n', '8661d-thumb-large-image.png', '', NULL, 'Cyprus', 'logo', '2014-07-15', 'm²'),
(5, 'Regular maintenance', '<p>\n	The old good rule of &lsquo;It is not interesting enough&rsquo; doesn&rsquo;t stop with the living room, and you can easily find yourself less exited about your new bed in a week or too. In fact, this happens to bedrooms much more often than it does to the rest of the house, and they turn into storage rooms where we pile cloths and working materials. But is it really possible to be relaxed in a room like that? The answer is obviously no. In order to feel good inside and to calm our nerves down, we need to store belongings neatly, avoid cluttering with accessories, and most of all &ndash; clean regularly.</p>\n', '14111-35526773_388291911675051_6475762624448430080_o.jpg', '', NULL, 'Cyprus', 'accessories', '2015-07-06', 'm²');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(1) UNSIGNED NOT NULL,
  `post_slug` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_html` text COLLATE utf8mb4_unicode_ci,
  `post_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_category_id` int(1) UNSIGNED NOT NULL,
  `post_created_at` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts_categories`
--

CREATE TABLE `posts_categories` (
  `posts_category_id` int(1) UNSIGNED NOT NULL,
  `posts_category_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posts_category_description` text COLLATE utf8mb4_unicode_ci,
  `posts_category_slug` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posts_category_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(1) NOT NULL,
  `product_category_id` int(1) NOT NULL,
  `product_image` varchar(280) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image_2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image_3` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image_4` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image_5` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(220) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `product_price` float DEFAULT NULL,
  `product_created_at` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_category_id`, `product_image`, `product_image_2`, `product_image_3`, `product_image_4`, `product_image_5`, `product_name`, `product_description`, `product_price`, `product_created_at`) VALUES
(1, 1, '10789-16175516_1407341632663170_8830879023195226112_n.jpg', '9a736-16175516_1407341632663170_8830879023195226112_n.jpg', '4109b-16632844_1366831576723096_4007343652698324992_n.jpg', NULL, NULL, 'PROJECT X T-SHIRT LONG SLEEVE', '<p><span>CODE: SS17.88162243.PJX</span><br /><span>SIZE: S, M, L, XL, XXL</span><br /><span>COLOUR: BLACK &amp; WHITE</span><br /><span>Made in France</span></p>', 48, '1488936272'),
(2, 3, 'cbf14-15729632_1224554944306482_2634662982386188288_n.jpg', '9a736-16175516_1407341632663170_8830879023195226112_n.jpg', '703a5-16641311_1282086278541985_8855738182235324416_n.jpg', NULL, NULL, 'Differ Suit', '<p>\r\n	<span>CODE: SS17.88162243.PJX</span><br />\r\n	<span>SIZE: S, M, L, XL, XXL</span><br />\r\n	<span>COLOUR: BLACK &amp; WHITE</span><br />\r\n	<span>Made in France</span></p>\r\n', 32, '1490707388'),
(3, 5, '1f8c2-16632844_1366831576723096_4007343652698324992_n.jpg', '9a736-16175516_1407341632663170_8830879023195226112_n.jpg', '703a5-16641311_1282086278541985_8855738182235324416_n.jpg', NULL, NULL, 'IMPERIAL SHIRTS', '<p><span>CODE: SS17.88162243.PJX</span><br /><span>SIZE: S, M, L, XL, XXL</span><br /><span>COLOUR: BLACK &amp; WHITE</span><br /><span>Made in France</span></p>', 25, '1488936919'),
(4, 2, '6e4a2-16632844_1366831576723096_4007343652698324992_n.jpg', '9a736-16175516_1407341632663170_8830879023195226112_n.jpg', '703a5-16641311_1282086278541985_8855738182235324416_n.jpg', NULL, NULL, 'Shirt', '<p><span>CODE: SS17.88162243.PJX</span><br /><span>SIZE: S, M, L, XL, XXL</span><br /><span>COLOUR: BLACK &amp; WHITE</span><br /><span>Made in France</span></p>', 33.5, '1488936926'),
(5, 7, '7b595-16649731_1144701832318942_3573167156797374464_n.jpg', '703a5-16641311_1282086278541985_8855738182235324416_n.jpg', NULL, NULL, NULL, 'Imperial T-Shirt', '<p>\r\n	<span>CODE: SS17.88162243.PJX</span><br />\r\n	<span>SIZE: S, M, L, XL, XXL</span><br />\r\n	<span>COLOUR: BLACK &amp; WHITE</span><br />\r\n	<span>Made in France</span></p>\r\n', 39, '1490707492'),
(6, 7, '34e20-16175825_1435673173123512_965765431350525952_n.jpg', '703a5-16641311_1282086278541985_8855738182235324416_n.jpg', '703a5-16641311_1282086278541985_8855738182235324416_n.jpg', NULL, NULL, 'IMPERIAL BLAZER', NULL, 118, '1488936091');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `property_id` int(11) NOT NULL,
  `property_slug` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_title_en` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_description_en` text COLLATE utf8mb4_unicode_ci,
  `property_main_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_location_id` int(1) NOT NULL,
  `property_type_id` int(1) NOT NULL,
  `property_listing_type_id` tinyint(1) NOT NULL,
  `property_size` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `property_price` decimal(8,0) NOT NULL,
  `property_bedrooms` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `property_bathrooms` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `property_latitude` double DEFAULT NULL,
  `property_longitude` double DEFAULT NULL,
  `property_priority` tinyint(1) NOT NULL DEFAULT '1',
  `property_visible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`property_id`, `property_slug`, `property_title_en`, `property_description_en`, `property_main_image`, `property_location_id`, `property_type_id`, `property_listing_type_id`, `property_size`, `property_price`, `property_bedrooms`, `property_bathrooms`, `property_latitude`, `property_longitude`, `property_priority`, `property_visible`) VALUES
(3, 'bedroom-house-columbia', 'For Sale 4 Bedroom House Columbia', '<p><span>Mauris elementum tempus nisi, vitae ullamcorper sem ultricies vitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></p>', '62849-70930827.jpg', 1, 1, 0, '22', '614', '5+', '', 41.0473099, 29.0077019, 1, 1),
(4, 'poolside-character-home', 'Poolside Character Home', '<p><span>Vivamus quis dui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in vulputate non, tristique eu mi. Aliquam tristique dapibus tempor. Vivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis bibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum porta sit amet, iaculis in neque.</span></p>', '9e988-slider2.jpg', 1, 1, 0, '462', '2266', '0', '0', NULL, NULL, 1, 1),
(6, '2-bedroom-apartment-for-sale', 'Spacious 2 bedroom apartment for sale', '<p>\r\n	Spacious 2 bedroom apartment for sale, in a very charming and friendly neighborhood, behind Faneromenis church. Faneromeni area with its large array of shops, banks, and much much more make it a very popular and much sought after area. Situated just a 10 minutes walking to Larnaca Town centre and the beach. The apartment is situate in the first floor, is furnish and fully equipped so that you feel completely at home and at ease in your new surroundings. It has 2 big bedrooms, a separate kitchen, and a balcony in the front. There is a title deed.</p>\r\n', '956a3-coral-seas.jpg', 1, 1, 0, '55', '555', '0', '0', NULL, NULL, 1, 1),
(7, 'detached-villa', 'Detached Villa', '<p>This secluded oasis is accessed via a private driveway into the grounds of this impeccable home. Being set back from the main street on this beautiful hill top development means this house benefits from a grand view through olive groves down to the sea and is afforded additional privacy by park land to the back of the property. From the front gate and lengthy driveway it is clear that this beautiful home is well loved with a highly maintained appearance and fabulous mature gardens. Through the front door and you enter a spacious and bright open-plan living room with ful airconditioning, fans and a fabulous open fireplace providing warmth on those cooler evenings. The living area flows very well into the dining space and kitchen area. The light is provided by two large patio doors which access a spacious covered verandah perfectly suited to outdoor dining and entertaining where you sit among the mature shrubs and flowers. The verandahs are framed by quality stonework. Your Gourmet Kitchen is also a bright space with modern cabinetry and a useful breakfast bar, all finished with quality granite worktops.</p>', '34a63-kelokedara.jpg', 1, 1, 0, '555', '82500', '0', '0', NULL, NULL, 1, 1),
(9, 'Luxurious-Exclusive-Villa-Ayios-Tychonas', 'Luxurious Exclusive Villa - Ayios Tychonas', '<p>This luxurious exclusive villa is located in a prime and very prestigious elevated position in Agios Tychonas. The villa is set on a hill just off the main Limassol Highway, opposite the Four Seasons 5 Star Hotel. Overlooking Limassol town and with panoramic unobstructed sea views. It is just 10 minutes drive to the Limassol town centre and only 2 minutes to the beach.</p>\r\n<p>·         6 bedrooms</p>\r\n<p>·         Covered house area: 690 sqm</p>\r\n<p>·         Open veranda area: 300 sqm (with KARISTO stones)</p>\r\n<p>·         Additional covered verandas</p>\r\n<p>·         Attic area: 140 sqm</p>\r\n<p>·         Garage: 85 sqm</p>\r\n<p>·         Swimming pool: 104 sqm</p>\r\n<p>·         BBQ area</p>\r\n<p>·         Landscaped gardens with authomatic irrigation system</p>\r\n<p><strong><u>Ground Floor</u></strong> Hall leading to living/dinning room, TV room, kitchen, office and toilet.</p>\r\n<p><strong><u>First Floor</u></strong> Hall leading to 4 bedrooms all with en-suits (bath of shower), the master bedroom has a Jacuzzi bath.</p>\r\n<p><strong><u>Second Floor</u></strong> Leads to an Attic area which includes 2 bedrooms, 1 bathroom, and living room area and storage room.</p>\r\n<p><strong><u>Interior Fixtures and Fittings</u></strong> As expected with a villa of this vast size, all fixtures and fittings are of a very high luxurious standard. Some of the standard features are as follows: Marble and Granite floors, Italian kitchen, full air conditioning, double glazed windows, satellite systems, intern internet connection, Jacuzzi, central heating and alarm system.</p>\r\n<p>On the left and the right side of the house there is public green area in which nobody can build anything so the view remains open.</p>', '1b176-unnamed-1-1-.jpg', 1, 2, 0, '2,200', '6', '5', '5', NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `properties_gallery`
--

CREATE TABLE `properties_gallery` (
  `properties_gallery_id` int(11) NOT NULL,
  `properties_gallery_related_id` int(1) NOT NULL,
  `properties_gallery_title` varchar(250) NOT NULL,
  `properties_gallery_url` varchar(250) DEFAULT NULL,
  `properties_gallery_priority` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `properties_gallery`
--

INSERT INTO `properties_gallery` (`properties_gallery_id`, `properties_gallery_related_id`, `properties_gallery_title`, `properties_gallery_url`, `properties_gallery_priority`) VALUES
(1, 9, '', '72c53-unnamed--6-.jpg', NULL),
(2, 9, '', 'c67db-unnamed--5-.jpg', NULL),
(3, 9, '', '17607-unnamed--12-.jpg', NULL),
(4, 9, '', '5332f-unnamed--8-.jpg', NULL),
(5, 9, '', 'd52aa-unnamed--7-.jpg', NULL),
(6, 9, '', 'd717d-unnamed--4-.jpg', NULL),
(7, 9, '', '8c972-unnamed--3-.jpg', NULL),
(8, 9, '', '58a37-unnamed--11-.jpg', NULL),
(9, 9, '', 'c4074-unnamed--1-.jpg', NULL),
(10, 9, '', '8f87c-image001.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `properties_locations`
--

CREATE TABLE `properties_locations` (
  `properties_location_id` int(1) NOT NULL,
  `properties_location_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `properties_locations`
--

INSERT INTO `properties_locations` (`properties_location_id`, `properties_location_name`) VALUES
(1, 'Limassol'),
(2, 'Nicosia'),
(3, 'Paphos'),
(4, 'Ammochostos'),
(5, 'Larnaca');

-- --------------------------------------------------------

--
-- Table structure for table `properties_types`
--

CREATE TABLE `properties_types` (
  `properties_type_id` int(1) NOT NULL,
  `properties_type_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `properties_types`
--

INSERT INTO `properties_types` (`properties_type_id`, `properties_type_name`) VALUES
(1, 'Apartment'),
(2, 'House'),
(3, 'Shop'),
(4, 'Office'),
(5, 'Building'),
(6, 'Business'),
(7, 'Residential Land'),
(8, 'Agricultural Land');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(1) UNSIGNED NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_image` varchar(255) DEFAULT NULL,
  `site_description` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `fav_icon` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `language_id` int(11) UNSIGNED DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(25) NOT NULL,
  `description` text,
  `site_suffix` varchar(255) DEFAULT NULL,
  `use_smtp` int(1) NOT NULL DEFAULT '0',
  `smtp_host` varchar(255) NOT NULL,
  `smtp_port` int(11) NOT NULL,
  `smtp_username` varchar(255) NOT NULL,
  `smtp_password` varchar(255) NOT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `facebook_url` varchar(355) DEFAULT NULL,
  `google_url` varchar(355) DEFAULT NULL,
  `twitter_url` varchar(355) DEFAULT NULL,
  `instagram_url` varchar(355) DEFAULT NULL,
  `pinterest_url` varchar(355) DEFAULT NULL,
  `linkedin_url` varchar(355) DEFAULT NULL,
  `fb_api` varchar(255) DEFAULT NULL,
  `is_multilingual` tinyint(1) NOT NULL DEFAULT '0',
  `maintenance_mode` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `email`, `company`, `site_name`, `site_image`, `site_description`, `logo`, `fav_icon`, `address`, `location`, `zip_code`, `country_id`, `language_id`, `phone`, `fax`, `description`, `site_suffix`, `use_smtp`, `smtp_host`, `smtp_port`, `smtp_username`, `smtp_password`, `default_image`, `facebook_url`, `google_url`, `twitter_url`, `instagram_url`, `pinterest_url`, `linkedin_url`, `fb_api`, `is_multilingual`, `maintenance_mode`) VALUES
(1, 'info@dikkertje.nl', 'Dikkertje | Grieks restaurant in Utrecht', 'Dikkertje | Grieks restaurant in Utrecht', NULL, 'Dikkertje | Grieks restaurant in Utrecht', NULL, NULL, 'Amsterdamsestraatweg 328 3551 CV - Utrecht', '+48.137769, +16.276226', 2355, 1, 1, '030-2443359', '030-2443359', NULL, 'Lazarou', 0, '', 0, '', '', NULL, 'D.A. Christodoulou Constructions & Developments Ltd', NULL, NULL, 'Dikkertje | Grieks restaurant in Utrecht', 'Dikkertje | Grieks restaurant in Utrecht', NULL, '486992334724444', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(1) UNSIGNED NOT NULL,
  `slider_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_img` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_text_top_nl` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_text_middle_nl` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_text_nl` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_text_top_en` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_text_middle_en` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_text_en` varchar(220) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_link` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_priority` tinyint(1) NOT NULL DEFAULT '0',
  `slider_published` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_name`, `slider_img`, `slider_text_top_nl`, `slider_text_middle_nl`, `button_text_nl`, `slider_text_top_en`, `slider_text_middle_en`, `button_text_en`, `button_link`, `slider_priority`, `slider_published`) VALUES
(10, 'slide 1', '3a40b-christodoulou_slider_52.jpg', '', '', '', 'We build your dream', NULL, 'Learn more', 'about', 1, 1),
(12, 'sld 2', 'ab94c-36580422_402118493625726_8362080052891877376_o.jpg', '', '', '', 'New projects ', NULL, NULL, 'portfolio', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `team_id` int(1) UNSIGNED NOT NULL,
  `team_image` varchar(355) COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_name_en` varchar(355) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_name_de` varchar(355) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description_en` varchar(355) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description_de` varchar(355) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_email` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_phone` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_order` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`team_id`, `team_image`, `team_name_en`, `team_name_de`, `team_description_en`, `team_description_de`, `team_email`, `team_phone`, `team_order`) VALUES
(1, '', 'Lefteris Demetriades', NULL, 'CEO', NULL, 'l.demetriades@lclccy.com', '(+357) 96 538 477', '0'),
(2, '', 'Joanna Christofi ', NULL, 'Chartered Surveyor FRICS', NULL, 'l j.christofi@lclccy.com', '(+357) 99 641 415', ''),
(3, '', 'Katerina Pafiti ', NULL, 'Legal Advisor', NULL, 'k.pafiti@lclccy.com', '(+357) 99 592 228', ''),
(4, '', 'Liasos Constantinou ', NULL, 'Corporate Services Consultant', NULL, ' l.constantinou@lclccy.com', '(+357) 99 608 102', ''),
(5, '', 'Vasilis Vasiliou ', NULL, 'Financial Advisor', NULL, ' v.vasiliou@lclccy.com', '(+357) 99 415 113', ''),
(6, '', 'Matina Tzeremaki ', NULL, 'Property Consultant', NULL, 'm.tzeremaki@lclccy.com', '(+357) 99 420 840', ''),
(7, '', 'Christakis Philipou', NULL, 'Property Agent', NULL, 'c.philipou@lclccy.com', '(+357) 97 612 055', '');

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `title_id` int(1) UNSIGNED NOT NULL,
  `title_caption` varchar(255) DEFAULT NULL,
  `relation_id` int(1) UNSIGNED DEFAULT NULL,
  `data_type` varchar(255) DEFAULT NULL,
  `language_id` int(1) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `user_image` varchar(155) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `user_image`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$m18dVs0b6Clejr7GZAzjfOacVKJ5qKTsanHthSxMmS7CJrQ169iGu', NULL, 'admin@admin.com', '85f12-web_development_1.png', NULL, NULL, NULL, NULL, 1268889823, 1580316172, 1, 'JupiWeb', 'Blogger', 'ADMIN', '0'),
(2, '127.0.0.1', 'christina', '$2y$08$m18dVs0b6Clejr7GZAzjfOacVKJ5qKTsanHthSxMmS7CJrQ169iGu', NULL, 'christina@jupiweb.com', '85f12-web_development_1.png', NULL, NULL, NULL, NULL, 1268889823, 1486167744, 1, 'Christina', 'Vog', 'ADMIN', '0'),
(3, '127.0.0.1', 'panos', '$2y$08$m18dVs0b6Clejr7GZAzjfOacVKJ5qKTsanHthSxMmS7CJrQ169iGu', NULL, 'panos@jupiweb.com', '85f12-web_development_1.png', NULL, NULL, NULL, NULL, 1268889823, 1486167744, 1, 'Panos', 'Vog', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`admin_menu_id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`blog_id`),
  ADD KEY `blog_related_category_id` (`blog_related_category_id`),
  ADD KEY `blog_related_user_id` (`blog_related_user_id`);

--
-- Indexes for table `blogs_categories`
--
ALTER TABLE `blogs_categories`
  ADD PRIMARY KEY (`blogs_category_id`),
  ADD KEY `blogs_category_slug` (`blogs_category_slug`),
  ADD KEY `blogs_category_slug_2` (`blogs_category_slug`),
  ADD KEY `blogs_category_slug_3` (`blogs_category_slug`);

--
-- Indexes for table `blogs_tags`
--
ALTER TABLE `blogs_tags`
  ADD PRIMARY KEY (`blog_tag_id`),
  ADD KEY `blog_tag_blog_id` (`blog_tag_blog_id`),
  ADD KEY `blog_tag_language_id` (`blog_tag_language_id`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`collection_id`),
  ADD KEY `collection_main_category_related_id` (`collection_main_category_related_id`);

--
-- Indexes for table `collections_main_categories`
--
ALTER TABLE `collections_main_categories`
  ADD PRIMARY KEY (`collections_main_category_id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `language_settings_id` (`language_settings_id`);

--
-- Indexes for table `language_vars`
--
ALTER TABLE `language_vars`
  ADD PRIMARY KEY (`lang_var_id`),
  ADD KEY `lang_var_related_settings_id` (`lang_var_related_settings_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_gallery`
--
ALTER TABLE `main_gallery`
  ADD PRIMARY KEY (`main_gallery_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `menu_slug` (`menu_slug`),
  ADD KEY `menu_language_id` (`menu_language_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `meta_content`
--
ALTER TABLE `meta_content`
  ADD PRIMARY KEY (`meta_content_id`);

--
-- Indexes for table `newsletter_subscribers`
--
ALTER TABLE `newsletter_subscribers`
  ADD PRIMARY KEY (`subscriber_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `page_slug` (`page_slug`),
  ADD KEY `page_language_id` (`page_language_id`);

--
-- Indexes for table `page_content`
--
ALTER TABLE `page_content`
  ADD PRIMARY KEY (`page_content_id`),
  ADD KEY `page_content_related_id` (`page_content_related_id`),
  ADD KEY `page_content_type_related_id` (`page_content_type_related_id`);

--
-- Indexes for table `page_content_types`
--
ALTER TABLE `page_content_types`
  ADD PRIMARY KEY (`page_content_type_id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`portfolio_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `post_category_id` (`post_category_id`),
  ADD KEY `post_slug` (`post_slug`);

--
-- Indexes for table `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD PRIMARY KEY (`posts_category_id`),
  ADD KEY `posts_category_slug` (`posts_category_slug`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`property_id`),
  ADD KEY `property_location_id` (`property_location_id`),
  ADD KEY `property_type_id` (`property_type_id`);

--
-- Indexes for table `properties_gallery`
--
ALTER TABLE `properties_gallery`
  ADD PRIMARY KEY (`properties_gallery_id`),
  ADD KEY `brands_gallery_related_id` (`properties_gallery_related_id`);

--
-- Indexes for table `properties_locations`
--
ALTER TABLE `properties_locations`
  ADD PRIMARY KEY (`properties_location_id`);

--
-- Indexes for table `properties_types`
--
ALTER TABLE `properties_types`
  ADD PRIMARY KEY (`properties_type_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`title_id`),
  ADD KEY `relation_id` (`relation_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `admin_menu_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `banner_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `blog_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `blogs_categories`
--
ALTER TABLE `blogs_categories`
  MODIFY `blogs_category_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blogs_tags`
--
ALTER TABLE `blogs_tags`
  MODIFY `blog_tag_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `collection_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `collections_main_categories`
--
ALTER TABLE `collections_main_categories`
  MODIFY `collections_main_category_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `color_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `language_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `language_vars`
--
ALTER TABLE `language_vars`
  MODIFY `lang_var_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_gallery`
--
ALTER TABLE `main_gallery`
  MODIFY `main_gallery_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `meta_content`
--
ALTER TABLE `meta_content`
  MODIFY `meta_content_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `newsletter_subscribers`
--
ALTER TABLE `newsletter_subscribers`
  MODIFY `subscriber_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `page_content`
--
ALTER TABLE `page_content`
  MODIFY `page_content_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `page_content_types`
--
ALTER TABLE `page_content_types`
  MODIFY `page_content_type_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `portfolio_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_categories`
--
ALTER TABLE `posts_categories`
  MODIFY `posts_category_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `property_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `properties_gallery`
--
ALTER TABLE `properties_gallery`
  MODIFY `properties_gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `properties_locations`
--
ALTER TABLE `properties_locations`
  MODIFY `properties_location_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `properties_types`
--
ALTER TABLE `properties_types`
  MODIFY `properties_type_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `team_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `title_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `properties`
--
ALTER TABLE `properties`
  ADD CONSTRAINT `properties_ibfk_1` FOREIGN KEY (`property_location_id`) REFERENCES `properties_locations` (`properties_location_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `properties_ibfk_2` FOREIGN KEY (`property_type_id`) REFERENCES `properties_types` (`properties_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `properties_gallery`
--
ALTER TABLE `properties_gallery`
  ADD CONSTRAINT `properties_gallery_ibfk_1` FOREIGN KEY (`properties_gallery_related_id`) REFERENCES `properties` (`property_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
