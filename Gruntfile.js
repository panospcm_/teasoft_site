
module.exports = function(grunt) {

	//project configurations
	grunt.initConfig({
uglify: {
    my_target: {
      files: {
        'assets/js/plugins.min.js': ['assets/js/plugins.js']
      }
    }
  },
		cssmin : {
			target : {
				src : ["assets/css/plugins.css"],
				dest : "vendors/style.min.css"
			}
		},
	imagemin: {
	    jpgs: {
	        options: {
	            progressive: true
	        },
	        files: [{
	            expand: true,
	            cwd: 'assets/images/background',
	            src: ['**/*.{png,jpg,gif}'],
	            dest: 'assets/images/bg/'
	        }]
	    }
	}
	});

	//load cssmin plugin
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-imagemin');

	//create default task
	grunt.registerTask("default", ["cssmin"]);
	grunt.registerTask('default', ['uglify:my_target']);
	grunt.registerTask('default', ['imagemin:jpgs']);


};