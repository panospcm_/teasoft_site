<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$CI =& get_instance();
define('MY_LANG', $CI->lang->lang());
//1

function removeEvilStyles($tagSource) {
  // this will leave everything else, but:
  $evilStyles = array('font', 'color', 'font-family', 'mso-spacerun', 'font-face', 'font-size-adjust', 'font-stretch', 'font-variant');

  $find = array();
  $replace = array();

  foreach ($evilStyles as $v) {
    $find[]    = "/$v:.*?/";
    $replace[] = '';
  }

  return preg_replace($find, $replace, $tagSource);
}

function reading_time($mycontent){


$word = str_word_count(strip_tags($mycontent));
$m = floor($word / 200);
$s = floor($word % 200 / (200 / 60));
$est = $m . ' minute' . ($m == 1 ? '' : 's') . ', ' . $s . ' second' . ($s == 1 ? '' : 's');

return $est;
}
function show_404_view(){

	$CI =& get_instance();
	$CI->output->set_status_header('404'); 
    //loading in custom error view
    
    return $CI->load->view('err404');
}
function parse_php_tag(
    /* string */ $subject,
    array        $variables,
    /* string */ $escapeChar = '@',
    /* string */ $errPlaceholder = null
) {
    $esc = preg_quote($escapeChar);
    $expr = "/
        $esc$esc(?=$esc*+{)
      | $esc{
      | {(\w+)}
    /x";

    $callback = function($match) use($variables, $escapeChar, $errPlaceholder) {
        switch ($match[0]) {
            case $escapeChar . $escapeChar:
                return $escapeChar;

            case $escapeChar . '{':
                return '{';

            default:
                if (isset($variables[$match[1]])) {
                    return $variables[$match[1]];
                }

                return isset($errPlaceholder) ? $errPlaceholder : $match[0];
        }
    };

    return preg_replace_callback($expr, $callback, $subject);
}

 function stripHTMLtags($str)
{
    $t = preg_replace('/<[^<|>]+?>/', '', htmlspecialchars_decode($str));
    $t = htmlentities($t, ENT_QUOTES, "UTF-8");
    return $t;
}

     function myVillages()
    {

      $CI =& get_instance();
      $data  = $CI->MyModel->getVillages();
      $CONFIDENCEVALUE =  $data;
$config['confidencevalue'] = $CONFIDENCEVALUE;
        return $config['confidencevalue'];
    }


     function getmodel($model=NULL)
    {

      $CI =& get_instance();
      $CI->modeldata  = $CI->MyModel->getTeam();
        // return json_encode($data);
    }



     function getmyvar($var=NULL)
    {
      
      $CI =& get_instance();
      return json_encode($CI->modeldata[0]);
        // return json_encode($data);
    }

     function logo_main_img()
    {
        return base_url().'assets/images/tea_soft_logo.png';
    }
     function logo_main_img_white()
    {
        return base_url().'assets/images/logo/labc_white.png';
    }
     function event_main_img($feed_img_title)
    {
        return base_url().'assets/images/events/'.$feed_img_title;
    }

     function team_main_img_thumb($feed_img_title)
    {
        return base_url().'assets/images/team/thumbs/'.$feed_img_title;
    }
     function property_main_img_thumb($feed_img_title)
    {
        return base_url().'assets/images/properties/thumbs/'.$feed_img_title;
    }
     function property_main_img($feed_img_title)
    {
        return base_url().'assets/images/properties/'.$feed_img_title;
    }
     function user_main_img($feed_img_title)
    {
        return base_url().'assets/uploads/users/'.$feed_img_title;
    }

     function blog_main_img($feed_img_title)
    {
        return base_url().'assets/uploads/blogs/'.$feed_img_title;
    }
     function blog_link($blog_id,$blog_title)
    {
        return site_url(array('blogs',$blog_id,url_title(convert_accented_characters(utf8_encode(strtolower($blog_title))),'dash')));
    }

     function tour_link($tour_slug)
    {
        return site_url(array('tour',$tour_slug));
    }


    function helpdesk_article_img_category_path($article)
    {
        return base_url().'assets/uploads/files/'.$article->helpdesk_category_image;
    }


    function helpdesk_article_img_path($article)
    {
        return base_url().'assets/uploads/files/'.$article->helpdesk_article_image_1;
    }

    function helpdesk_article_file1_path($article)
    {
        return base_url().'assets/uploads/files/'.$article->helpdesk_article_file_1;
    }

    function helpdesk_article_link($tour_slug)
    {
        return site_url(array('admin/helpdesk_article',$tour_slug->helpdesk_article_id));
    }

     function book_tour_link($tour_slug)
    {
        return site_url(array('tour-booking',$tour_slug));
    }


    function admin_final_link($link){

        if($link==NULL||$link==''){

            return 'management/'.$link;

        }
        else{

            return $link;

        }

        return $link;

    }

    function baba()
    {    
    
            $CI =& get_instance();
            // $CI->lang->localized($uri);            

    // $this->site_info = $this->MyModel->getWebsiteInfo();
        $config['multilingual']= false;

        return true;
    }

      function send_simple_message($to = null,$subject = null,$message = null,$sender_email = null) {  

$url = 'https://api.elasticemail.com/v2/email/send';

try{
        $post = array('from' => $sender_email,
    'fromName' => 'K’Arte_Design',
    'apikey' => '51af9b22-c98b-49cb-b70d-ef353fd2e36c',
    'subject' => $subject,
    'to' => $to,
    'bodyHtml' => $message,
    'bodyText' => 'Text Body',
    'isTransactional' => false);
    
    $ch = curl_init();
    curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
      CURLOPT_SSL_VERIFYPEER => false
        ));
    
        $result=curl_exec ($ch);
        curl_close ($ch);
    
        // echo $result;

      $obj =  json_decode($result, true);
      // var_dump($obj['success']);
        if($obj['success']===true)
        return true;
      else 
        return false;
}
catch(Exception $ex){
  echo $ex->getMessage();
}
}
  function sendemail($to = null,$subject = null,$message = null,$sender_email = null,$attachment = null){



    $CI =& get_instance();

      // $config = array( 
      // 'protocol' => 'mail', 
      // 'smtp_host' => 'mail.maistralistalis.gr', 
      // 'smtp_port' => 587, 
      // 'charset' =>'utf-8',
      // 'mailtype'  => 'html',
      // 'wordwrap' => true,
      // 'starttls'  => true,
      // 'smtp_timeout' => 60,
      // 'smtp_user' => 'info@maistralistalis.gr', 
      // 'smtp_pass' => 'maistrali9058', ); 
    $config = Array( 

    
      'protocol' => 'mail',
      'mailtype' => 'html',
      'smtp_host' => 'mail.24tractari.ro',
      'smtp_port' => 465,
      'crlf' => "\r\n",
      'newline' => "\r\n",
      'smtp_timeout'=>60,
      'smtp_crypto'=>'tls',

      'smtp_user' => '_mainaccount@24tractari.ro', 

      'smtp_pass' => '1EmyEehR2X4oqd' ); 

      

      $CI->load->library('email', $config); 

      $CI->email->set_newline("\r\n");

      $CI->email->from($sender_email, 'New Entry Added from our Website');

      // $attachment!=NULL?$CI->email->attach($attachment):'';

      // $sender_email!=NULL?$CI->email->reply_to($sender_email):''; //User email submited in form

      $CI->email->to($to);

      $CI->email->subject($subject); 

      $CI->email->message($message);

      if (!$CI->email->send()) {

        show_error($CI->email->print_debugger()); }

      else {

        return true;

      }

    } 
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

