<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = 'my404';
$route['translate_uri_dashes'] = FALSE;


// $route['^(en|nl|de|gr)/admin/(:any)/(:any)/(:any)/(:any)'] = "admin/$1/$2/$3/$4";
$route['^(en|nl|de|gr)/admin'] = "admin/index";
// $route['^(en|nl|de|gr)/gallery'] = "images_examples/index";
// $route['^(en|nl|de|gr)/gallery/(:any)'] = "images_examples/$2";
$route['^(en|nl|de|gr)/admin/(:any)'] = "admin/$2";
$route['^(en|nl|de|gr)/admin/(:any)/(:any)'] = "admin/$2/$3";
$route['^(en|nl|de|gr)/admin/(:any)/(:any)/(:any)'] = "admin/$2/$3/$4";
$route['^(en|nl|de|gr)/admin/(:any)/(:any)/(:any)/(:any)'] = "admin/$2/$3/$4/$5";
// $route['^(en|nl|de|gr)/admin/(:any)/(:any)'] = "admin/$1/$2";
// $route['^(en|nl|de|gr)/admin/(:any)/(:any)/(:any)'] = "admin/$1/$2/$3";
$route['^(en|nl|de|gr)/auth/(:any)'] = "auth/$2";
$route['^(en|nl|de|gr)/auth/(:any)'] = "auth/$2";

$route['^(en|nl|de|gr)/404'] = "welcome/my_404_view";

$route['db'] = "welcome/db_backup";
$route['^(en|nl|de|gr)/db'] = "welcome/db_backup";
$route['^(en|nl|de|gr)/upload_file'] = "welcome/upload_file";
$route['^(en|nl|de|gr)/contactSubmit'] = "welcome/contactSubmit";
$route['^(en|nl|de|gr)/customTourSubmit'] = "welcome/customTourSubmit";
$route['^(en|nl|de|gr)/quoteSubmit'] = "welcome/quoteSubmit";
$route['^(en|nl|de|gr)/requestBooking'] = "welcome/requestBooking";
$route['^(en|nl|de|gr)/reqPayment'] = "welcome/reqPayment";
$route['^(en|nl|de|gr)/getBrainTreeToken'] = "welcome/getBrainTreeToken";
$route['^(en|nl|de|gr)/invoice/(:any)'] = "welcome/invoice/$2";

$route['^(en|nl|de|gr)/paypal'] = "paypal/index";
$route['^(en|nl|de|gr)/paypal/SetExpressCheckout'] = "paypal/SetExpressCheckout";
$route['^(en|nl|de|gr)/paypal/OrderComplete'] = "paypal/OrderComplete";
$route['^(en|nl|de|gr)/paypal/OrderCancelled'] = "paypal/OrderCancelled";
$route['^(en|nl|de|gr)/paypal/DoExpressCheckoutPayment'] = "paypal/DoExpressCheckoutPayment";
$route['^(en|nl|de|gr)/paypal/GetExpressCheckoutDetails'] = "paypal/GetExpressCheckoutDetails";


$route['^(en|nl|de|gr)/shop/(:any)'] = "welcome/shop/$2";
$route['^(en|nl|de|gr)/shop'] = "welcome/shop/$2";
$route['^(en|nl|de|gr)/shop/(:any)/(:any)'] = "welcome/shop/$2/$3";
$route['^(en|nl|de|gr)/shop/(:any)/(:any)/(:any)'] = "welcome/shop/$2/$3/$4";
$route['^(en|nl|de|gr)/product/(:any)'] = "welcome/product_detail/$2";
$route['^(en|nl|de|gr)/submit_req_form'] = "welcome/submitReqForm";
$route['^(en|nl|de|gr)/submit_newsletter_form'] = "welcome/submitNewsletter";
$route['^(en|nl|de|gr)/login'] = "welcome/login";
$route['^(en|nl|de|gr)/properties'] = "welcome/properties";
$route['^(en|nl|de|gr)/team'] = "welcome/team";
$route['^(en|nl|de|gr)/cyprus'] = "welcome/cyprus";
$route['^(en|nl|de|gr)/legal_services'] = "welcome/legal_services";
$route['^(en|nl|de|gr)/banking_services'] = "welcome/banking_services";
$route['^(en|nl|de|gr)/request_quote'] = "welcome/request_quote";
// $route['^(en|nl|de|gr)/portfolio'] = "welcome/portfolio";
$route['^(en|nl|de|gr)/portfolio/(:any)'] = "welcome/portfolio_single/$2";
// $route['^(en|nl|de|gr)/services'] = "welcome/services";
// $route['^(en|nl|de|gr)/services/(:any)'] = "welcome/services/$2";


$route['^(en|nl|de|gr)/terms'] = "welcome/terms";

$route['^(en|nl|de|gr)/(:any)'] = "welcome/index/$2";
$route['^(en|nl|de|gr)/blogs/(:any)/(:any)'] = "welcome/news_detail/$2";
// $route['^(en|nl|de|gr)/event/(:any)'] = "welcome/event_detail/$2";
$route['^(en|nl|de|gr)/tour-booking/(:any)'] = "welcome/tour_booking/$2";
// $route['^(en|nl|de|gr)/(.+)$'] = "welcome/index/$2";
 
// '/en' and '/fr' -> use default controller
$route['^fr$'] = $route['default_controller'];
$route['^nl$'] = $route['default_controller'];



// $route['/:any/(:any)'] = 'welcome/view/$1';
// $route['(:any)'] = 'welcome/view/$1';
// $route['/admin/(:any)'] = 'admin/$1';
/* Auth routes */
// $route['auth/:any'] = "auth/$1";
$route['^en/auth'] = "auth";



$route['(:any)'] = "welcome/index/$1";
// example: '/en/about' -> use controller 'about'

$route['^(en|nl|de|gr)/(.+)$']        = "welcome/index/$1/$2";

// $route['^(en|nl|de|gr)/(:any)'] = "welcome/$2";
// '/en' and '/fr' -> use default controller
// $route['^(gr|de|en|es|ru)$'] = $route['default_controller'];
