<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['post_controller_constructor'][] = array(
                                            'function' => 'redirect_ssl',
                                            'filename' => 'Ssl.php',
                                            'filepath' => 'hooks'
                                          );
$hook['pre_controller'] = array(
   'class' => 'Http_request_logger',
    'function' => 'log_all',
        'filename' => 'Tester.php',
        'filepath' => 'hooks',
        'params'   => array('beer', 'wine', 'snacks')
);

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/
// $hook['display_override'][] = array(
//     'class' => '',
//     'function' => 'CI_Minifier_Hook_Loader',
//     'filename' => '',
//     'filepath' => ''
// );