<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class General extends CI_Model
{

    function __construct ()
    {
        parent::__construct();
    }

    public function run ($params = array())
    {

        // var_dump($params['type']);
        $owners = array('link', 'Mike Schuurman', 'Piet Wittebol');
        

        $type = isset($params['type']) ? $params['type'] : '';

        $class = isset($params['class']) ? $params['class'] : 'default';
        $url = isset($params['url']) ? $params['url'] : '';
        $title = isset($params['title']) ? $params['title'] : '';

        switch ($type) {
            case 'link':
        
        $str = '<a class="' . ($class) . '" href="' . site_url($url) . '">';
        $str .= $params['title'];
        $str .= '</a>';

                break;
            case 'image':
        
        $str = '<img class="' . ($class) . '" src="' . base_url('assets/images/'.$url) . '" alt="' . $params['title'] . '" >';
        $str .= '';
                break;
            
            default:
                # code...
                break;
        }

        
        return $str;
    }
}