<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyModel extends CI_Model {


    protected $_table_name = '';
    // protected $_table_name = '';
    // protected $_table_name = '';

     public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->myLang = $this->lang->lang();
    }

    // Select from "setting" table
    function getWebsiteInfo()
    {
        $this->db->select('*');
        $this->db->from('settings');
        // $this->db->join('languages',"languages.language_settings_id=settings.id",'left');
        $query = $this->db->get();
        $return = $query->result();
        return count($return)?$return[0]:0;
    }


    function getLanguageID($lang_slug)
    {
        $this->db->select('*');
        $this->db->from('languages');
        $this->db->where('language_slug',$lang_slug);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_pages($limit = FALSE,$offset = 0){
        
        $this->db->select('*');
        $this->db->from('page');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result();


    }

    public function get_page_content($limit = FALSE,$offset = 0){
        
        $this->db->select('*');
        $this->db->from('page_content');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result();


    }


    function getPageContents($page_content_related_id=null)
    {
        $this->db->select('*');
        $this->db->from('page_content');
        // if($language_id!=null)
        $this->db->join('page_content_types',"page_content_types.page_content_type_id = page_content.page_content_type_related_id");
        $this->db->where('page_content_related_id',$page_content_related_id);
        $this->db->order_by("page_content.page_content_order", "asc");

        $query = $this->db->get()->result();
        return $query;
    }


    // Select setting's options in one language from "setting_options_per_lang" table
    function getPage($page_slug=null,$page_id=NULL)
    {
        $this->db->select('page.*,meta_content.*');
        $this->db->from('page');
        // if($language_id!=null)
            $this->db->where('page_slug',$page_slug);
        $this->db->join('languages',"languages.language_id=page.page_language_id",'left');
        $this->db->join('meta_content',"page.page_id=meta_content.meta_content_related_page_id",'left');

        // $this->db->where('languages.language_slug',$this->lang->lang());
        $this->db->where('languages.language_slug',$this->lang->lang());

        $query = $this->db->get()->result();
        return isset($query[0])?$query[0]:NULL;
    }


    // Select a list from "menu" table
    function getMenu($conditions=null)
    {
        $this->db->select('*');
        $this->db->from('menu');
        // $this->db->join('meta_content',"meta_content.meta_content_related_menu_id=menu.menu_id",'left');
        // $this->db->where('titles.data_type',"menu");
        //$this->db->where('titles.language_id',$_SESSION["language"]["language_id"]);
        // $this->db->where('public',1);
        if($conditions!=null) $this->db->where($conditions);
        $this->db->where('is_parent',1);
        $this->db->join('languages',"languages.language_id=menu.menu_language_id",'left');
        $this->db->where('languages.language_slug',$this->lang->lang());
        
        $this->db->order_by("menu_order", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    
    public function search_helpdesk_articles($limit = FALSE,$offset = 0){
        
        $this->db->select('*');
        $this->db->from('helpdesk_categories');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result();


    }


    

    public function get_helpdesk_category($category_id = NULL){
        
        $this->db->select('*');
        $this->db->from('helpdesk_categories');
        $category_id !== NULL ? $this->db->where('helpdesk_category_id', $category_id) : '';
        $query = $this->db->get();
        return $query->result();


    }
    public function get_helpdesk_categories($limit = FALSE,$offset = 0){
        
        $this->db->select('*');
        $this->db->from('helpdesk_categories');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result();


    }

    
    public function get_helpdesk_articles($article_id = NULL, $category_id = NULL, $keyword = NULL){
        
        $this->db->select('*');
        $this->db->from('helpdesk_articles');
        $article_id !== NULL ? $this->db->where('helpdesk_article_id', $article_id) : '';
        $category_id !== NULL ? $this->db->where('helpdesk_article_related_category_id', $category_id) : '';
        $keyword !== NULL ? $this->db->like('helpdesk_article_title', $keyword, 'both') : '';
        $keyword !== NULL ? $this->db->or_like('helpdesk_article_description', $keyword, 'both') : '';
        // $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result();


    }

    function getPortfolio()
    {
        $this->db->select('*');
        $this->db->from('portfolio');
        // $this->db->where('language_slug',$lang_slug);
        $query = $this->db->get();
        return $query->result();
    }

    function getSinglePortfolio($id)
    {
        $this->db->select('*');
        $this->db->from('portfolio');
        $this->db->where('portfolio_id',$id);
        $query = $this->db->get();
        return $query->result();
    }

    function updateMenu($menuID,$menuOrder)
    {
  $data = array(
               'menu_order' => $menuOrder,
            );

$this->db->where('menu_id', $menuID);
 
 if($this->db->update('menu', $data))
      {
        return true;
      }
      else
      {
        return false;
      }
    }


    function updateTeam($teamID,$menuOrder)
    {
    $data = array(
    'team_order' => $menuOrder,
    );

    $this->db->where('team_id', $teamID);

    if($this->db->update('team', $data))
    {
    return true;
    }
    else
    {
    return false;
    }
    }


    // Select a list from "menu" table
    function getSubMenu()
    {
        $this->db->select('*');
        $this->db->from('menu');
        // $this->db->join('meta_content',"meta_content.meta_content_related_menu_id=menu.menu_id",'left');
        // $this->db->where('titles.data_type',"menu");
        //$this->db->where('titles.language_id',$_SESSION["language"]["language_id"]);
        // $this->db->where('public',1);
        // $this->db->where('parent_id',$parent_id);
        $this->db->where('is_parent',0);
        $this->db->join('languages',"languages.language_id=menu.menu_language_id",'left');
        $this->db->where('languages.language_slug',$this->lang->lang());
        $this->db->order_by("menu_order", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // Select a list from "menu" table
    function getAdminMenu($conditions=null)
    {
        $this->db->select('*');
        $this->db->from('admin_menu');
        // $this->db->join('titles',"titles.relation_id=menu_id");
        // $this->db->where('titles.data_type',"menu");
        //$this->db->where('titles.language_id',$_SESSION["language"]["language_id"]);
        // $this->db->where('public',1);
        if($conditions!=null) $this->db->where($conditions);
        $this->db->order_by("admin_menu_order", "DESC");
        $query = $this->db->get();
        return $query->result();
    }


    function getTeam($page_slug=null)
    {
        $this->db->select('team.*, team.team_name_'.$this->myLang.' as team_name, team.team_description_'.$this->myLang.' as team_description');
        $this->db->from('team');

        $this->db->order_by("team_order", "asc");

        $query = $this->db->get()->result();
        return isset($query[0])?$query:NULL;
    }

    function getPropertiesLocations()
    {
        $this->db->select('*');
        $this->db->from('properties_locations');
        $query = $this->db->get();
        return $query->result();
    }

    function getPropertiesTypes()
    {
        $this->db->select('*');
        $this->db->from('properties_types');
        $query = $this->db->get();
        return $query->result();
    }

    function getSingleProperty($fleet_slug=null)
    {
        $this->db->select('properties.*,properties_locations.*,properties_types.*, properties.property_title_'.$this->myLang.' as property_title, properties.property_description_'.$this->myLang.' as property_description');
        $this->db->join('properties_locations', 'properties_locations.properties_location_id = properties.property_location_id');
        $this->db->join('properties_types', 'properties_types.properties_type_id = properties.property_type_id');
        $this->db->from('properties');

        $this->db->where('property_slug',$fleet_slug);
        $this->db->order_by("property_priority", "asc");

        $query = $this->db->get()->result();
        return isset($query[0])?$query:NULL;
    }

    function getProperties($location=NULL,$type=NULL,$bedrooms=NULL,$price=NULL,$listingType=NULL)
    {
        $this->db->select('properties.*,properties_locations.*,properties_types.*, properties.property_title_'.$this->myLang.' as property_title, properties.property_description_'.$this->myLang.' as property_description');
        $this->db->join('properties_locations', 'properties_locations.properties_location_id = properties.property_location_id');
        $this->db->join('properties_types', 'properties_types.properties_type_id = properties.property_type_id');
        $this->db->from('properties');

        $location!=''?$this->db->where('properties.property_location_id',$location):NULL;
        $listingType!=''?$this->db->where('properties.property_listing_type_id',$listingType):NULL;
        $type!=''?$this->db->where('properties_types.properties_type_id',$type):NULL;
        $bedrooms!=''?$this->db->where('properties.property_bedrooms',$bedrooms):NULL;
        if($price!=''){

            $where_price = "properties.property_price".$price."";
            $this->db->where($where_price);
        }
        

        $this->db->order_by("property_priority", "asc");

        $query = $this->db->get()->result();
        return isset($query[0])?$query:NULL;
    }


    function getSingleFleet($fleet_slug=null)
    {
        $this->db->select('fleet.*, fleet.fleet_title_'.$this->myLang.' as fleet_title, fleet.fleet_description_'.$this->myLang.' as fleet_description, fleet.fleet_properties_'.$this->myLang.' as fleet_properties');
        $this->db->from('fleet');

        $this->db->where('fleet_slug',$fleet_slug);
        $this->db->order_by("fleet_priority", "asc");

        $query = $this->db->get()->result();
        return isset($query[0])?$query:NULL;
    }


    public function getSingleEvent($tour_slug = FALSE){
        
        $this->db->select('events.*, events.event_title_'.$this->myLang.' as event_title, events.event_description_'.$this->myLang.' as event_description');
        $this->db->from('events');
        $this->db->order_by("event_priority", "ASC");
        $this->db->where('event_slug',$tour_slug);
        $query = $this->db->get();
        return $query->result();


    }

    public function getSingleTourByID($tour_id = FALSE){
        
        $this->db->select('tours.*, tours.tour_title_'.$this->myLang.' as tour_title, tours.tour_description_'.$this->myLang.' as tour_description, tours.tour_duration_'.$this->myLang.' as tour_duration, tours.tour_starting_point_'.$this->myLang.' as tour_starting_point, tours.tour_ending_point_'.$this->myLang.' as tour_ending_point, tours.tour_itinerary_'.$this->myLang.' as tour_itinerary, tours.tour_included_'.$this->myLang.' as tour_included, tours.tour_villages_'.$this->myLang.' as tour_villages');
        $this->db->from('tours');
        $this->db->order_by("tour_priority", "ASC");
        $this->db->where('tour_id',$tour_id);
        $query = $this->db->get();
        return $query->result();


    }
    public function getBookingByID($tours_booking_id = FALSE){
        
        $this->db->select('tours_bookings.*,tours_payment_types.*');
        $this->db->from('tours_bookings');
        $this->db->join('tours_payment_types', 'tours_payment_types.tours_payment_type_id = tours_bookings.tours_booking_payment_type_id');
        $this->db->where('tours_booking_id',$tours_booking_id);
        $query = $this->db->get();
        return $query->result();


    }
    public function getPricesByTourIdAndLocationName($tour_price_related_tour_id = FALSE, $tour_price_related_tour_starting_location_id = NULL){
        
        $this->db->select('tours_prices.*');
        $this->db->from('tours_prices');
        $this->db->where('tour_price_related_tour_id',$tour_price_related_tour_id);
        $this->db->where('tour_price_related_tour_starting_location_id',$tour_price_related_tour_starting_location_id);
        $query = $this->db->get();
        return $query->result();


    }
    public function getEvents($limit = FALSE,$offset = 0){
        
        $this->db->select('events.*, events.event_id as event_id, events.event_title_'.$this->myLang.' as event_title, events.event_description_'.$this->myLang.' as event_description');
        $this->db->from('events');
        $this->db->limit($limit, $offset);
        $this->db->where('event_visible',1);
        $this->db->group_by("event_id");
        $this->db->order_by("events.event_id", "ASC");
        $query = $this->db->get();
        return $query->result();


    }

    public function getTours($limit = FALSE,$offset = 0){
        
        $this->db->select('tours.*,tours_prices.*, tours.tour_id as tour_id, tours_prices.tour_price_id, tours.tour_title_'.$this->myLang.' as tour_title, tours.tour_description_'.$this->myLang.' as tour_description, tours.tour_duration_'.$this->myLang.' as tour_duration, tours.tour_starting_point_'.$this->myLang.' as tour_starting_point, tours.tour_ending_point_'.$this->myLang.' as tour_ending_point, tours.tour_itinerary_'.$this->myLang.' as tour_itinerary, tours.tour_included_'.$this->myLang.' as tour_included, tours.tour_villages_'.$this->myLang.' as tour_villages, tours.tour_b_notes, tours.tour_activities_notes');
        $this->db->from('tours');
        $this->db->join('tours_prices', 'tours_prices.tour_price_related_tour_id = tours.tour_id');
        $this->db->limit($limit, $offset);
        $this->db->where('tour_visible',1);
        $this->db->group_by("tour_id");
        $this->db->order_by("tours.tour_id", "ASC");
        $query = $this->db->get();
        return $query->result();


    }

    public function getSingleToursAvailableDate($tours_available_date_related_tour_id = NULL){
        
        $this->db->select('tours_available_dates.*');
        $this->db->from('tours_available_dates');
        $this->db->where('tours_available_date_related_tour_id',$tours_available_date_related_tour_id);
        $query = $this->db->get();
        return $query->result();


    }

    public function getTourPriceAndLocations($tour_id = NULL){
        
        $this->db->select('tours_prices.*,tours_starting_locations.*');
        $this->db->from('tours_prices');
        $this->db->join('tours_starting_locations', 'tours_prices.tour_price_related_tour_starting_location_id = tours_starting_locations.tour_starting_location_id');
        // $this->db->limit($limit, $offset);
        // $this->db->order_by("tour_gallery_priority", "ASC");
        $this->db->where('tour_price_related_tour_id',$tour_id);
        $query = $this->db->get();
        return $query->result();




    }
    public function getMainGallery($limit = FALSE,$offset = 0){
        
        $this->db->select('main_gallery.*');
        $this->db->from('main_gallery');
        $this->db->limit($limit, $offset);
        $this->db->order_by("main_gallery_priority", "ASC");
        // $this->db->where('tour_visible',1);
        $query = $this->db->get();
        return $query->result();


    }

    public function getPropertyGallery($fleet_id = NULL){
        
        $this->db->select('properties_gallery.*');
        $this->db->from('properties_gallery');
        // $this->db->limit($limit, $offset);
        $this->db->order_by("properties_gallery_priority", "ASC");
        $this->db->where('properties_gallery_related_id',$fleet_id);
        $query = $this->db->get();
        return $query->result();




    }

    public function getVillages($tour_id = NULL){
        
        $this->db->select('villages.*');
        $this->db->from('villages');
        // $this->db->limit($limit, $offset);
        // $this->db->order_by("tour_gallery_priority", "ASC");
        // $this->db->where('tour_gallery_related_id',$tour_id);
        $query = $this->db->get();
        return $query->result();




    }

    function insert_tours_booking($datanew) 
    {   

      $this->db->insert('tours_bookings', $datanew);
    return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function getProductByID($id=NULL){
        
        $this->db->select('products.*');
        $this->db->from('products');
        $this->db->limit(1, 0);
        // $this->db->order_by("brand_id", "ASC");
        $this->db->where('products.product_id',$id);
        // $this->db->join('products_sizes', 'products_sizes.products_size_related_product_id = products.product_id','right');
        // $this->db->join('sizes', 'products_sizes.products_size_related_size_id = sizes.size_id');
        // $this->db->join('products_colors', 'products_colors.products_color_related_product_id = products.product_id','left');
        // $this->db->join('colors', 'products_colors.products_color_related_color_id = colors.color_id');
        // $this->db->group_by("products.product_id");
        $query = $this->db->get();
        return $query->result();


    }

    public function getMainCollectionsSeasons($limit = FALSE,$offset = 0){
        
        $this->db->select('*');
        $this->db->from('collections_main_categories');
        $this->db->limit($limit, $offset);
        $this->db->order_by("collections_main_category_is_current", "DESC");
        $this->db->where('collections_main_category_is_visible',1);
        $query = $this->db->get();
        return $query->result();


    }
    public function getCollections($limit = FALSE,$offset = 0,$main_collection=NULL){
        
        $this->db->select('*');
        $this->db->from('collections');
        $this->db->limit($limit, $offset);
        $this->db->join('collections_main_categories', 'collections.collection_main_category_related_id = collections_main_categories.collections_main_category_id');
        $this->db->order_by("collection_order", "ASC");
        $this->db->where('collection_visible',1);
        $main_collection!=NULL?$this->db->where('collections_main_categories.collections_main_category_slug',$main_collection):$this->db->where('collections_main_categories.collections_main_category_is_current',1);
        $query = $this->db->get();
        return $query->result();


    }
  public function getSliders($limit = FALSE,$offset = 0){
        
         $this->db->select('slider.*, 
            slider.slider_text_top_'.$this->myLang.' as slider_text_top, 
            slider.slider_text_middle_'.$this->myLang.' as slider_text_middle,
            slider.button_text_'.$this->myLang.' as button_text');
        $this->db->from('slider');
        $this->db->limit($limit, $offset);
        $this->db->order_by("slider_priority", "ASC");
        $this->db->where('slider_published',1);
        $query = $this->db->get();
        return $query->result();


    }
    // Select a list from "languages" table
    function getWorks($limit = FALSE,$offset = 0)
    {
        $this->db->select('*');
        $this->db->from('works');
        $this->db->join('works_categories', 'works.workCategoryID = works_categories.categoryID');
        $this->db->order_by("workDateCreated", "DESC");
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    // Select a list from "languages" table
    function getLanguages()
    {
        $this->db->select('*');
        $this->db->from('languages');
        $this->db->where('is_active',1);
        $query = $this->db->get();
        return $query->result_array();
    }



    var $client_service = "frontend-client";
    var $auth_key       = "simplerestapi";

    public function check_auth_client(){
        $client_service = $this->input->get_request_header('Client-Service', TRUE);
        $auth_key  = $this->input->get_request_header('Auth-Key', TRUE);
        if($client_service == $this->client_service && $auth_key == $this->auth_key){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized.'));
        }
    }

    public function get_count_of($tablename = null){
        
        $this->db->from($tablename);

        return $this->db->count_all_results();


    }

    public function get_blogs_categories($limit = FALSE,$offset = 0){
        
        $this->db->select('*');
        $this->db->from('blogs_categories');
        // $this->db->join('users',"blogs.blog_related_user_id = users.id");
        $this->db->limit($limit, $offset);
        $this->db->order_by("blogs_category_id", "ASC");
        $query = $this->db->get();
        return $query->result();


    }
    public function get_blogs($limit = FALSE,$offset = 0,$blog_category_slug=NULL){
        
        $this->db->select('*');
        $this->db->from('blogs');
        $this->db->join('users',"blogs.blog_related_user_id = users.id");
        $this->db->join('blogs_categories', 'blogs.blog_related_category_id = blogs_categories.blogs_category_id');
        $blog_category_slug!=NULL?$this->db->where('blogs_categories.blogs_category_slug',$blog_category_slug):'';
        $this->db->limit($limit, $offset);
        $this->db->order_by("blog_created_at", "DESC");
        $query = $this->db->get();
        return $query->result();


    }
    public function get_blog_next($blog_id){
        
        $this->db->select('*');
        $this->db->from('blogs');
        $this->db->where('blog_id >',$blog_id);
        $this->db->limit(1);
        $this->db->order_by("blog_id", "ASC");
        $query = $this->db->get();
        return count($query->result())?$query->result():0;


    }
    public function get_blog_previous($blog_id){
        
        $this->db->select('*');
        $this->db->from('blogs');
        $this->db->where('blog_id <',$blog_id);
        $this->db->limit(1);
        $this->db->order_by("blog_id", "DESC");
        $query = $this->db->get();
        return count($query->result())?$query->result():0;


    }
    public function get_single_blog($blog_id){
        
        $this->db->select('*');
        $this->db->from('blogs');
        $this->db->join('users',"blogs.blog_related_user_id = users.id");
        $this->db->where('blog_id',$blog_id);
        $query = $this->db->get();
        return $query->result();


    }
function newsletter_mail_exists($key)
{
    $this->db->where('subscriber_email',$key);
    $query = $this->db->get('newsletter_subscribers');
    if ($query->num_rows() > 0){
        return true;
    }
    else{
        return false;
    }
}
    function insert_newsletter_subscriber($datanew) 
{   

  $this->db->insert('newsletter_subscribers', $datanew);
return ($this->db->affected_rows() != 1) ? false : true;
}

    function insert_products_requests($datanew) 
{   

  $this->db->insert('products_requests', $datanew);
return ($this->db->affected_rows() != 1) ? false : true;
}

    // public function login($username,$password)
    // {
    //     $q  = $this->db->select('password,id')->from('users')->where('username',$username)->get()->row();
    //     if($q == ""){
    //         return array('status' => 204,'message' => 'Username not found.');
    //     } else {
    //         $hashed_password = $q->password;
    //         $id              = $q->id;
    //         if (hash_equals($hashed_password, crypt($password, $hashed_password))) {
    //            $last_login = date('Y-m-d H:i:s');
    //            $token = crypt(substr( md5(rand()), 0, 7));
    //            $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
    //            $this->db->trans_start();
    //            $this->db->where('id',$id)->update('users',array('last_login' => $last_login));
    //            $this->db->insert('users_authentication',array('users_id' => $id,'token' => $token,'expired_at' => $expired_at));
    //            if ($this->db->trans_status() === FALSE){
    //               $this->db->trans_rollback();
    //               return array('status' => 500,'message' => 'Internal server error.');
    //            } else {
    //               $this->db->trans_commit();
    //               return array('status' => 200,'message' => 'Successfully login.','id' => $id, 'token' => $token);
    //            }
    //         } else {
    //            return array('status' => 204,'message' => 'Wrong password.');
    //         }
    //     }
    // }

    public function login($username,$password)
    {
        $q  = $this->db->select('*')->from('users')->where('username',$username)->get()->row();

        if(!$q){
            return array('status' => 201,'message' => 'Username not found.');
        } else {


            $hashed_password = $q->password;

            if (password_verify($password, $hashed_password)) {


                  $id = $q->id;
                  return array('status' => 200,'message' => 'Successfully login.','data' => $q, 'token' => NULL);

            } else {
               return array('status' => 201,'message' => 'Wrong password.');
            }
        }
    }
    public function logout()
    {
        $users_id  = $this->input->get_request_header('User-ID', TRUE);
        $token     = $this->input->get_request_header('Authorization', TRUE);
        $this->db->where('users_id',$users_id)->where('token',$token)->delete('users_authentication');
        return array('status' => 200,'message' => 'Successfully logout.');
    }


    public function get_works($data = null)
    {

              $this->db->select('*');
        $this->db->from('works');

        $this->db->join('categories', 'works.workCategoryID = categories.categoryID');

        $query = $this->db->get()->result_array();
        return $query;

    }

    public function getWorksCategories($data = null)
    {

              $this->db->select('*');
        $this->db->from('works_categories');

        $query = $this->db->get()->result_array();
        return $query;

    }


   function DuplicateMySQLRecord($table, $primary_key_field, $primary_key_val) 
{
   /* generate the select query */
   $this->db->where($primary_key_field, $primary_key_val); 
   $query = $this->db->get($table);
  
    foreach ($query->result() as $row){   
       foreach($row as $key=>$val){        
          if($key != $primary_key_field){ 
          /* $this->db->set can be used instead of passing a data array directly to the insert or update functions */
          $this->db->set($key, $val);               
          }//endif              
       }//endforeach
    }//endforeach

    /* insert the new record into table*/
    return $this->db->insert($table); 
}


}
