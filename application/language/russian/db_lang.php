<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - English
*
* Author: Ben Edmunds
*         ben.edmunds@gmail.com
*         @benedmunds
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.14.2010
*
* Description:  English language file for Ion Auth messages and errors
*
*/

// Account Creation
$lang['about_text']            = 'JupiWeb is a web agency located in Greece &amp; Cyprus which offers a
                    wide range of web development, web design and ecommerce website development
                    services. We create beautiful responsive &amp; SEO Friendly websites
                    <strong>using custom-made web tools and themes</strong>.';
$lang['header_social']            = 'Stay Tuned';
$lang['header_about']            = 'About Us';