<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - English
*
* Author: Ben Edmunds
*         ben.edmunds@gmail.com
*         @benedmunds
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.14.2010
*
* Description:  English language file for Ion Auth messages and errors
*
*/

// Account Creation

$lang['header_main_promo']            = 'I believe a house is more a home by being a work of art.';
$lang['header_about']            = 'О нас';
$lang['header_inter_design']            = 'Интерьер Дизайн';

$lang['header_about_head']            = 'О нас K’Arte Design';

$lang['header_about_p1']            = 'K’Arte Design основана Катериной Чевычаловой и специализируется на частных и коммерческих проектах. До того как основать свою компанию, Катерина работала в нескольких лидирующих дизайн компаниях Лондона, включая 5 лет в Kelly Hoppen Interiors. С образованием в сфере интерьер дизайна, искусства и финансов на глобальном рынке и знанием 5 языков, K’Arte Design стремится сделать все для достижения идеального результата и комфорта каждого заказчика.';
$lang['header_about_p2']            = 'Благодаря почти 10 летнему опыту работы в сфере индивидуального интерьер дизайна по всему миру от Лондона до Гонг Конга и Саудовской Аравии до острова Маврикий, мы найдем уникальное и оптимальное предложение для каждого Клиента.';
$lang['header_about_p3']            = 'K’Arte Design черпает вдохновение для своего творчества в каждом уголке мира и благодаря налаженным прямым контактам с лучшими международными поставщиками, каждая часть проекта продумывается до мельчайших деталей';
$lang['header_about_p4']            = 'K’Arte Design находится в Лондоне и на Кипре с возможностью работать по всему миру.';


$lang['header_int_text_1']            = 'Создание концепции дизайна';
$lang['header_int_text_2']            = 'Подбор отделочных материалов и сантехники';
$lang['header_int_text_3']            = 'Подбор мебели и составление сметы';
$lang['header_int_text_4']            = 'Дизайн встроенной мебели на заказ';
$lang['header_int_text_5']            = 'Полный пакет информации для подрядчиков и субподрядчиков';
$lang['header_int_text_6']            = 'Закупка мебели';
$lang['header_int_text_7']            = 'Стайлинг интерьера';
$lang['header_int_text_8']            = 'Авторский надзор ';
$lang['header_int_text_9']            = 'Инсталляция под ключ ';

$lang['header_art_cons']            = 'Арт Кансалтинг';
$lang['header_portfolio']            = 'Портфолио';
$lang['header_p_avail']            = 'Предоставляется по запросу';
$lang['header_contact']            = 'Контакты';
$lang['header_contact_sm']            = 'Контакты';


$lang['header_art_cons_text_1']            = 'Подборка исскуства и арт объектов для частных коллекций';
$lang['header_art_cons_text_2']            = 'Подборка искусства и арт обьектов для коммерческих проектов на основе бюджета и требований Клиента';
$lang['header_art_cons_text_3']            = 'Искусство на заказ по желанию Клиента';
$lang['header_art_cons_text_4']            = 'Организация визитов на арт-ярмарки, в галлереи и мастерские художников';
$lang['header_art_cons_text_5']            = 'Покупка предметов искусства от имени Клиента';
$lang['header_art_cons_text_6']            = 'Координация доставки и инсталляции ';


$lang['txt_your_info']            = 'Контактная информация';
$lang['txt_your_name']            = 'Ваше имя';
$lang['txt_your_email']            = 'Адрес электронной почты';
$lang['txt_your_phone']            = 'Ваш телефон';
$lang['txt_your_msg']            = 'Сообщение';
$lang['txt_your_btn']            = 'Представить форму';
$lang['txt_contact_phone_uk']            = 'Английский номер';
$lang['txt_contact_phone_cy']            = 'Кипрский номер';
$lang['txt_contact_email']            = 'Email';
