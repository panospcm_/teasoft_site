<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - English
*
* Author: Ben Edmunds
*         ben.edmunds@gmail.com
*         @benedmunds
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.14.2010
*
* Description:  English language file for Ion Auth messages and errors
*
*/

// Account Creation

$lang['header_main_promo']            = 'I believe a house is more a home by being a work of art.';
$lang['header_about']            = 'About';
$lang['header_inter_design']            = 'Interior Design';

$lang['header_about_head']            = 'About K’Arte Design';

$lang['header_about_p1']            = 'K’Arte Design is a luxury interior design and art consultancy specialising in high-end residential and commercial projects founded by Katerina Tchevytchalova. Before launching her practice, Katerina worked in a number of design companies in London including 5 years at Kelly Hoppen Interiors. With an academic background in Interior Design, Art & Finance in the Global Market as well as knowledge of 5 languages, Katerina makes every project approachable and enjoyable for her Clients, with a seamless result being the ultimate goal.';
$lang['header_about_p2']            = 'After almost a decade of working in the luxury interiors market and a wealth of experience in projects ranging from London to Hong Kong and Saudi Arabia to Mauritius, we take pride in our ability to provide a tailored and unique proposal to each Client.';
$lang['header_about_p3']            = 'Our inspiration is sourced globally and our rich network of international suppliers ensures that no detail is ever left to chance.';
$lang['header_about_p4']            = 'K’Arte Design is based in London and Cyprus with ability to work worldwide.';

$lang['header_int_text_1']            = 'Concept Design';
$lang['header_int_text_2']            = 'Finishes and sanitaryware selection';
$lang['header_int_text_3']            = 'Selection and costing of all Furniture, Fixtures and Equipment';
$lang['header_int_text_4']            = 'Bespoke joinery design';
$lang['header_int_text_5']            = 'Full scope of information for contractors';
$lang['header_int_text_6']            = 'Full project procurement';
$lang['header_int_text_7']            = 'Project coordination';
$lang['header_int_text_8']            = 'Interior styling';
$lang['header_int_text_9']            = 'Turn key white glove installation';

$lang['header_art_cons']            = 'Art Consulting';
$lang['header_portfolio']            = 'Portfolio';
$lang['header_p_avail']            = 'Available on request';
$lang['header_contact']            = 'CONTACT US';
$lang['header_contact_sm']            = 'Contact';


$lang['header_art_cons_text_1']            = 'Art sourcing for private collections';
$lang['header_art_cons_text_2']            = 'Art sourcing and curated proposals for commercial projects based on budget, brand alignment and Client requirements';
$lang['header_art_cons_text_3']            = 'Commissioning bespoke artwork for Clients';
$lang['header_art_cons_text_4']            = 'Organising visits to art fairs, galleries and artists’ studios';
$lang['header_art_cons_text_5']            = 'Procurement and purchase of artwork on the Client’s behalf';
$lang['header_art_cons_text_6']            = 'Shipping and installation coordination';



$lang['txt_your_info']            = 'Contact Info';
$lang['txt_your_name']            = 'Your Name';
$lang['txt_your_email']            = 'Your Email';
$lang['txt_your_phone']            = 'Your Phone';
$lang['txt_your_msg']            = 'Your Message';
$lang['txt_your_btn']            = 'Submit Form';
$lang['txt_contact_phone_uk']            = 'UK Phone';
$lang['txt_contact_phone_cy']            = 'Cyprus Phone';
$lang['txt_contact_email']            = 'Email';


