<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - English
*
* Author: Ben Edmunds
*         ben.edmunds@gmail.com
*         @benedmunds
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.14.2010
*
* Description:  English language file for Ion Auth messages and errors
*
*/

// Account Creation
$lang['about_text']            = 'Η JupiWeb προσφέρει ένα ευρή φάσμα υπηρεσιών δημιουργίας ιστοσελίδων, σχεδιασμό ιστοσελίδων και διαδικτυακού μάρκετινγκ στο Ηράκλειο Κρήτης, Ελλάδα και Κύπρο.';
$lang['header_social']            = 'Βρείτε μας';
$lang['header_about']            = 'Για Εμάς';
