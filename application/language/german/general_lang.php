<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - English
*
* Author: Ben Edmunds
*         ben.edmunds@gmail.com
*         @benedmunds
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.14.2010
*
* Description:  English language file for Ion Auth messages and errors
*
*/

// Account Creation
$lang['about_text']            = 'JupiWeb is a web agency located in Greece &amp; Cyprus which offers a
                    wide range of web development, web design and ecommerce website development
                    services. We create beautiful responsive &amp; SEO Friendly websites
                    <strong>using custom-made web tools and themes</strong>.';
$lang['header_main_promo']            = 'Jederzeit...';
$lang['header_about']            = 'Über uns';
$lang['footer_about_head']            = 'Über MS Aviation';
$lang['footer_ql_head']            = 'Schnelle Links';
$lang['footer_about_sub_head']            = 'Das Unternehmen bietet eine umfassende Palette von Dienstleistungen für das Management der Luftflotten, die Aufrechterhaltung der Lufttüchtigkeit, die Beschäftigung von Luftfahrtpersonal und die Organisation von Charterflügen.';

$lang['header_links_head']            = 'Links';
$lang['header_contact_head']            = 'Kontakt';
$lang['header_fleet_head']            = 'Flotte';
$lang['header_services_head']            = 'Unsere Dienstleistungen';


$lang['header_copyright_head']            = 'Alle Rechte vorbehalten. Unterstützt von';


$lang['main_about_head_1']            = 'Über MS Aviation';
$lang['main_about_head_1_sub']            = 'Das österreichische Unternehmen MS Aviation wurde von einer Gruppe von Branchenexperten in der Geschäftsluftfahrt gegründet.';


$lang['main_about_head_1_right_head']            = 'Wir Sind Eine Gruppe Von Branchenexperten In Der Geschäftsluftfahrt';
$lang['main_about_head_1_right_sub_1']            = 'Das österreichische Unternehmen MS Aviation wurde von einer Gruppe von Branchenexperten in der Geschäftsluftfahrt gegründet. Jedes Mitglied unseres Teams erkennt, dass die Standards für unseren Arbeitsbereich sehr hoch sind. Und wir sind bereit, alle Qualitätserwartungen unserer individuellen und korporativen Kunden zu übertreffen.';
$lang['main_about_head_1_right_sub_2']            = 'MS Aviation konzentriert sich auf das Asset Management der Luftfahrt - vom technischen Support und der Lösung betrieblicher Probleme bis hin zur finanziellen Verbesserung des Geschäftsjet-Eigentums. Unsere Firma erbringt Services, die den gesamten Umfang des Flugzeugmanagements abdecken.';
$lang['main_about_head_1_left_head']            = 'Wir halten uns an alle europäischen Sicherheitsstandards';
$lang['main_about_head_1_left_sub_1']            = 'Unser Team arbeitet, um die finanziellen Interessen von Flugzeugbesitzern zu schützen und zu sichern. Deshalb ist es unser Hauptanliegen, alle Prozesse des Flugzeugmanagements transparent und transparent zu machen. MS Aviation optimiert die Kosten unserer Kunden und hält alle europäischen Sicherheitsstandards ein.';

// WHY CHOOSE US

$lang['main_why_us_head']            = 'Warum wir';
$lang['main_why_us_sub']            = 'Vorteile';
$lang['main_why_us_sub_1']            ='Erfahren Sie mehr über die Vorteile von MS Aviation.';

$lang['main_why_us_serv_1']            = 'Hohe Ansprüche';
$lang['main_why_us_serv_2']            = 'Optimierung';
$lang['main_why_us_serv_3']            = 'Regelmäßige Berichte';
$lang['main_why_us_serv_4']            = 'Spezialisten';
$lang['main_why_us_serv_5']            = 'Logistik';
$lang['main_why_us_serv_6']            = 'Planung';


$lang['main_why_us_serv_1_sub']            = 'Hoher Arbeitsstandard nach österreichischem Recht';
$lang['main_why_us_serv_2_sub']            = 'Strategische und finanzielle Optimierung für Privatjet-Besitzer';
$lang['main_why_us_serv_3_sub']            = 'Regelmäßige Berichte einschließlich Fakten- / Kostenanalyse';
$lang['main_why_us_serv_4_sub']            = 'High-Class-Luftfahrt-Spezialist einstellen';
$lang['main_why_us_serv_5_sub']            = 'Verbesserung der technischen Logistik';
$lang['main_why_us_serv_6_sub']            = 'Verbesserung der Flugroutenplanung';


$lang['office_text']            = 'Büro';
$lang['office_text_1_s']            = 'IZ NÖ-Süd, Strasse 3, Objekt 1, Top 1, Wiener Neudorf, Austria 2355';
$lang['office_text_2_s']            = 'ul. Pirogovskogo, 18, 3 floor, Kiev, Ukraine 03110';
$lang['office_text_3_s']            = 'Glinischevsky pereulok house 3, office 203-204, Moscow, Russia 125009';

$lang['terms_text'] = 'Geschäftsbedingungen';
$lang['legal_notice_text'] = 'Impressum';
$lang['read_more_btn_text'] = 'Erfahren Sie mehr';
$lang['properties_text'] = 'Eigenschaften';
$lang['fly_now_btn_text'] = 'Fliege jetzt';


$lang['fly_now_fname_text']='DeinvollständigerName';
$lang['fly_now_email_text']='E-MailAdresse';
$lang['fly_now_dep_date_text']='Abflugdatum';
$lang['fly_now_dep_time_text']='Abflugzeit';
$lang['fly_now_dep_from_text']='Abfahrtvon';
$lang['fly_now_select_text']='Auswählen';
$lang['fly_now_sel_o_way_text']='EinWeg';
$lang['fly_now_sel_multi_city_text']='MultiCity';
$lang['fly_now_sel_round_trip_text']='RoundTrip';
$lang['fly_now_phone_text']='Telefon';
$lang['fly_now_num_passengers_text']='AnzahlderPassagiere';
$lang['fly_now_return_date_text']='Rückkehrdatum';
$lang['fly_now_arriving_in_text']='Ankommend';
$lang['fly_now_requests_text']='SpezielleAnfragen';
$lang['fly_now_submit_text']='Submit';
$lang['fly_now_plc_city_text']='StadtundFlughafeneingeben';