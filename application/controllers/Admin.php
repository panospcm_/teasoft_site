<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');


    $this->site_info = $this->MyModel->getWebsiteInfo();


		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			
			redirect('login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
		}

		$this->user = $this->ion_auth->user()->row();
		$this->admin_menu = $this->MyModel->getAdminMenu();

		
		$this->site_languages = $this->MyModel->getLanguageID($this->lang->lang());

	}

	public function helpdesk($cat_id = NULL) {

		
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$crud->set_table('blogs');
$output = $crud->render();          
		$data['counts_users'] = $this->MyModel->get_count_of('users');
		// $this->_example_output((object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data),'templates/admin/_helpdesk_main.php');

		$output = (object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data);
		$this->load->view('templates/admin/header',$output);
		$this->load->view('templates/admin/_helpdesk_main',$output);
		$this->load->view('templates/admin/footer',$output);
	}

	public function helpdesk_article($article_id = NULL) {

		
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('blogs');
		$output = $crud->render();          
		$data['article'] = $this->MyModel->get_helpdesk_articles($article_id);
		// $this->_example_output((object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data),'templates/admin/_helpdesk_main.php');

		$output = (object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data);
		$this->load->view('templates/admin/header',$output);
		$this->load->view('templates/admin/_helpdesk_article',$output);
		$this->load->view('templates/admin/footer',$output);
	}

	public function helpdesk_category($cat_id = NULL) {

		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('blogs');
		$output = $crud->render();          
		$data['article'] = $this->MyModel->get_helpdesk_articles(NULL, $cat_id);
		// $this->_example_output((object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data),'templates/admin/_helpdesk_main.php');

		$output = (object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data);
		$this->load->view('templates/admin/header',$output);
		$this->load->view('templates/admin/_helpdesk_category',$output);
		$this->load->view('templates/admin/footer',$output);
	}


	public function helpdesk_search() {

		
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('blogs');
		$output = $crud->render();          
		$data['articles'] = $this->MyModel->get_helpdesk_articles(NULL, NULL, $this->input->get('query'));
		// $this->_example_output((object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data),'templates/admin/_helpdesk_main.php');

		$output = (object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data);
		$this->load->view('templates/admin/header',$output);
		$this->load->view('templates/admin/_helpdesk_search',$output);
		$this->load->view('templates/admin/footer',$output);
	}


	public function _example_output($output = null, $template = 'admin.php')
	{
		$this->load->view('templates/admin/header',$output);
		$this->load->view($template,$output);
		$this->load->view('templates/admin/footer',$output);
	}

	
    public function db_backup()
  {
         $this->load->dbutil();   

     $prefs = array(
            'tables' => array(),  
            'ignore' => array(),
            'format' => 'txt',
            'filename' => $this->db->database . '_' . date('d-m-Y_H-i-s') . '_backup.sql',
            'add_drop' => TRUE,
            'add_insert' => TRUE,
            'newline' => "\n" 
        );

$backup = $this->dbutil->backup($prefs);

         // $backup =$this->dbutil->backup();  
         $this->load->helper('file');
         write_file('backup/'.$prefs['filename'], $backup); 
  }

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{

		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$crud->set_table('blogs');
$output = $crud->render();          

		// var_dump($output->js_files);
array_push($output->js_files, base_url("assets/my_js_folder/myjs.file.js")); 
array_push($output->js_files, base_url("assets/another_js/config.module.js")); 

		$data['counts_users'] = $this->MyModel->get_count_of('users');
		$data['counts_pages'] = $this->MyModel->get_count_of('page');
		$data['counts_menu_pages'] = $this->MyModel->get_count_of('menu');
		$data['counts_blogs'] = $this->MyModel->get_count_of('blogs');
		$data['counts_page_contents'] = $this->MyModel->get_count_of('page_content');
		$data['latest_blogs'] = $this->MyModel->get_blogs(5,0);
		$data['latest_pages'] = $this->MyModel->get_pages(5,0);
		$data['latest_page_content'] = $this->MyModel->get_page_content(5,0);
		$this->_example_output((object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data),'templates/admin/index.php');
	}


	public function management($tableName)
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table($tableName);
			$crud->set_subject($tableName);

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function helpdesk_categories_management()
	{
		$crud = new grocery_CRUD();
			$crud->set_subject('Our helpdesk helpdesk_category');
			$crud->set_theme('bootstrap');
			// $crud->set_theme('datatables');
			$crud->set_table('helpdesk_categories');

			$crud->set_field_upload('helpdesk_category_image','assets/uploads/files');
			// $crud->field_type('brand_order','dropdown',array('1' => '1', '2' => '2','3' => '3' , '4' => '4' , '5' => '5' , '6' => '6' , '7' => '7' , '8' => '8' , '9' => '9'));

			try{
$output = $crud->render();
} catch(Exception $e) {
    show_error($e->getMessage());
}

$this->_example_output($output);

	}

	function helpdesk_articles_management()
	{
		$crud = new grocery_CRUD();
			$crud->set_subject('Our helpdesk articles');
			$crud->set_theme('bootstrap');
			// $crud->set_theme('datatables');
			$crud->set_table('helpdesk_articles');
			$crud->set_relation('helpdesk_article_related_category_id','helpdesk_categories','helpdesk_category_title');


			// $crud->set_relation('collection_main_category_related_id','collections_main_categories','collections_main_category_title');
			// $crud->required_fields('folder_name');
			$crud->set_field_upload('helpdesk_article_image_1','assets/uploads/files');
			$crud->set_field_upload('helpdesk_article_file_1','assets/uploads/files');

			$crud->field_type('helpdesk_article_date_created', 'hidden', time());
			// $crud->field_type('brand_order','dropdown',array('1' => '1', '2' => '2','3' => '3' , '4' => '4' , '5' => '5' , '6' => '6' , '7' => '7' , '8' => '8' , '9' => '9'));

			try{
$output = $crud->render();
} catch(Exception $e) {
    show_error($e->getMessage());
}

$this->_example_output($output);

	}
	
	function site_gallery_management()
	{
		$this->load->library('Image_CRUD');
		$Image_CRUD = new Image_CRUD();
	
		$Image_CRUD->set_primary_key_field('main_gallery_id');
		$Image_CRUD->set_url_field('main_gallery_url');
		$Image_CRUD->set_title_field('main_gallery_title');
		$Image_CRUD->set_table('main_gallery')
		->set_ordering_field('main_gallery_priority')
		->set_image_path('assets/images/gallery');
			
		$output = $Image_CRUD->render();
	
		$this->_example_output($output,'admin_brands_images.php');
	}

	function property_gallery_upload_image_management()
	{
		$this->load->library('Image_CRUD');
		$Image_CRUD = new Image_CRUD();
	
		$Image_CRUD->set_primary_key_field('properties_gallery_id');
		$Image_CRUD->set_url_field('properties_gallery_url');
		$Image_CRUD->set_title_field('properties_gallery_title');
		$Image_CRUD->set_table('properties_gallery')
		->set_relation_field('properties_gallery_related_id')
		->set_ordering_field('properties_gallery_priority')
		->set_image_path('assets/images/properties_gallery');
			
		$output = $Image_CRUD->render();
	
		$this->_example_output($output,'admin_brands_images.php');
	}
	

	function collections_management(){
			
			$crud = new grocery_CRUD();
			$crud->set_subject('Collections');
			$crud->set_theme('bootstrap');
			$crud->set_table('collections');
			 
			// $crud->unset_columns('collection_gallery_image','collection_link','collection_description');

			$crud->set_relation('collection_main_category_related_id','collections_main_categories','collections_main_category_title');

			// $crud->required_fields('folder_name');
			$crud->set_field_upload('collection_gallery_image','assets/images/brands');
			$crud->set_field_upload('collection_image','assets/images/brands');

			$crud->field_type('collection_order','dropdown',array('1' => '1', '2' => '2','3' => '3' , '4' => '4' , '5' => '5' , '6' => '6' , '7' => '7' , '8' => '8' , '9' => '9'));
			$output = $crud->render();

			$this->_example_output($output);
	}

	function static_brands_management(){
			
			$crud = new grocery_CRUD();
			$crud->set_subject('Our Brands');
			$crud->set_theme('bootstrap');
			// $crud->set_theme('datatables');
			$crud->set_table('static_brands');
			 
			// $crud->required_fields('folder_name');
			$crud->set_field_upload('brand_image','assets/images/brands');

			$crud->field_type('brand_order','dropdown',array('1' => '1', '2' => '2','3' => '3' , '4' => '4' , '5' => '5' , '6' => '6' , '7' => '7' , '8' => '8' , '9' => '9' , '10' => '10'));
			$output = $crud->render();

			$this->_example_output($output);
	}

	function villages_management(){
			
			$crud = new grocery_CRUD();
			$crud->set_subject('Our villages');
			$crud->set_theme('bootstrap');
			// $crud->set_theme('datatables');
			$crud->set_table('villages');
			$crud->set_field_upload('village_image','assets/images/villages');
			 
			// $crud->required_fields('folder_name');
			// $crud->set_field_upload('banner_large_image','assets/images/brands');

			// $crud->field_type('brand_order','dropdown',array('1' => '1', '2' => '2','3' => '3' , '4' => '4' , '5' => '5' , '6' => '6'));
			$output = $crud->render();

			$this->_example_output($output);
	}

	public function sub_single_tours_date_management($tour_id)
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$crud->set_table('blogs');
$output = $crud->render();          

		// var_dump($output->js_files);
array_push($output->js_files, base_url("assets/my_js_folder/myjs.file.js")); 
array_push($output->js_files, base_url("assets/another_js/config.module.js")); 

		$data['counts_users'] = $this->MyModel->get_count_of('users');
		$data['counts_pages'] = $this->MyModel->get_count_of('page');
		$data['counts_menu_pages'] = $this->MyModel->get_count_of('menu');
		$data['counts_blogs'] = $this->MyModel->get_count_of('blogs');
		$data['counts_page_contents'] = $this->MyModel->get_count_of('page_content');
		$data['latest_blogs'] = $this->MyModel->get_blogs(5,0);
		$data['latest_pages'] = $this->MyModel->get_pages(5,0);
		$data['latest_page_content'] = $this->MyModel->get_page_content(5,0);
		$this->_example_output((object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data),'templates/admin/index.php');

	}

	public function single_tours_dates_management($tour_id)
	{
			$crud = new grocery_CRUD();

			$crud->set_subject('Tour date');
			$crud->set_theme('bootstrap');
			$crud->set_table('tours_available_dates');
$crud->where('tours_available_date_related_tour_id',$tour_id);
			// $crud->set_relation('tour_price_related_tour_id','tours','tour_title_en');
			// $crud->set_relation('tour_price_related_tour_starting_location_id','tours_starting_locations','tour_starting_location_name_en');

			$crud->set_relation('tours_available_date_related_tour_id','tours','tour_title_en');
// $crud->add_action('Edit Dates', 'https://maxcdn.icons8.com/Android/PNG/48/Very_Basic/picture-48.png', site_url('admin/single_tours_date_management/'));
			$output = $crud->render();



			$this->_example_output($output);
	}
	public function tours_dates_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_subject('Tour date');
			$crud->set_theme('bootstrap');
			$crud->set_table('tours_available_dates');
			// $crud->set_relation('tour_price_related_tour_id','tours','tour_title_en');
			// $crud->set_relation('tour_price_related_tour_starting_location_id','tours_starting_locations','tour_starting_location_name_en');

$crud->unset_add();
$crud->unset_edit();
$crud->unset_read();
$crud->unset_delete();
			$crud->set_relation('tours_available_date_related_tour_id','tours','tour_title_en');
$crud->add_action('Edit Dates', '', '','ui-icon-image',array($this,'just_a_testt'));
// $crud->add_action('Edit Dates', 'https://maxcdn.icons8.com/Android/PNG/48/Very_Basic/picture-48.png', site_url('admin/single_tours_dates_management/',array($this,'just_a_testt')));
			$output = $crud->render();



			$this->_example_output($output);
	}
function just_a_testt($primary_key , $row)
{
return site_url('admin/single_tours_dates_management').'/'.$row->tours_available_date_related_tour_id;
}
	public function tours_prices_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('tours_prices');
			$crud->set_relation('tour_price_related_tour_id','tours','tour_title_en');
			$crud->set_relation('tour_price_related_tour_starting_location_id','tours_starting_locations','tour_starting_location_name_en');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function tours_bookings_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('tours_bookings');
			
			$crud->set_relation('tours_booking_payment_type_id','tours_payment_types','tours_payment_type_title');
			$crud->set_relation('tours_booking_related_id','tours','tour_title_en');
			$crud->field_type('tours_booking_status','dropdown',array('Pending' => 'Pending', 'Success' => 'Success'));
			$crud->callback_field('tours_booking_date_created',array($this,'tours_booking_date_created_callback'));

			$output = $crud->render();

			$this->_example_output($output);
	}

	function tours_booking_date_created_callback($value)
	{
	return '<span>'.date("d-M-Y H:i:s",strtotime(trim($value))).'</span>';
	}


	public function team_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('team');
			// $crud->unset_columns('productDescription');
			// $crud->set_relation('blog_related_user_id','users','{first_name} {last_name}');
			// $crud->set_relation('blog_related_category_id','blogs_categories','blogs_category_name');
			$crud->set_field_upload('team_image','assets/images/team');
			
			$crud->callback_after_upload(array($this,'team_resize_callback'));

			$output = $crud->render();

			$this->_example_output($output);
	}


	function team_resize_callback($uploader_response,$field_info, $files_to_upload){
			$this->load->library('image_moo');
			$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
			$thumb = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;
			$pic = $this->image_moo->load($file_uploaded);
			$pic->resize(550,650)->save($file_uploaded,true);
			$pic->resize(140,220)->save($thumb,true);
			return true;
	}
	public function portfolio_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('portfolio');
			// $crud->unset_columns('productDescription');
			// $crud->set_relation('blog_related_user_id','users','{first_name} {last_name}');
			// $crud->set_relation('blog_related_category_id','blogs_categories','blogs_category_name');
			// $crud->set_field_upload('event_main_image','assets/images/events');
			$crud->set_field_upload('portfolio_main_image_1','assets/images/portfolio');
			$crud->set_field_upload('portfolio_main_image_2','assets/images/portfolio');
			$crud->set_field_upload('portfolio_main_image_3','assets/images/portfolio');
			$crud->add_action('Edit Gallery', 'https://maxcdn.icons8.com/Android/PNG/48/Very_Basic/picture-48.png', site_url('admin/property_gallery_upload_image_management/'));
			
			$crud->callback_after_upload(array($this,'property_resize_callback'));
			// $crud->field_type('product_created_at', 'hidden', time());
			// $crud->set_relation_n_n('Sizes', 'products_sizes', 'sizes', 'products_size_related_product_id', 'products_size_related_size_id', 'size_title');
			// $crud->set_relation_n_n('Colors', 'products_colors', 'colors', 'products_color_related_product_id', 'products_color_related_color_id', 'color_title');
			// $crud->callback_field('event_description_en',array($this,'tour_description_callback'));
			// $crud->callback_edit_field('portfolio_description',array($this,'property_description_en_callback'));

			// $crud->set_relation('property_location_id','properties_locations','properties_location_name');
			// $crud->set_relation('property_type_id','properties_types','properties_type_name');


			// $crud->field_type('property_listing_type_id','dropdown',array('1' => 'For Sale', '2' => 'For Rent'));
			// $crud->field_type('property_bedrooms','dropdown',array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5+' => '5+',));
			// $crud->field_type('property_bathrooms','dropdown',array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5+' => '5+',));
			// $crud->display_as('property_listing_type_id','For Sale/For Rent')->display_as('property_type_id','Type')->display_as('property_size','Property size (Sq m)')->display_as('property_location_id','Property Location');
			// $crud->field_type('property_slug', 'hidden');
			// $crud->field_type('property_features_en', 'hidden');

			// $crud->callback_before_insert(array($this,'encrypt_password_and_insert_callback'));


			// $crud->callback_edit_field('fleet_properties_ru',array($this,'fleet_properties_ru_callback'));
			// $crud->callback_edit_field('fleet_properties_de',array($this,'fleet_properties_de_callback'));
			// $crud->callback_field('tour_itinerary_en',array($this,'tour_itinerary_callback'));
			// $crud->callback_column('blog_description',array($this,'blog_description_callback'));

			$output = $crud->render();

			$this->_example_output($output);
	}

function encrypt_password_and_insert_callback($post_array) {

$post_array['property_slug'] = url_title($post_array['property_title_en']);
 
// return $this->db->insert('properties',$post_array);

return $post_array;
}

	function property_description_en_callback($value)
	{
	return '<textarea  id="field-portfolio_description" name="portfolio_description" class="form-control body-tinymce">'.trim($value).'</textarea>';
	}


	function tour_description_callback($value)
	{
	return '<textarea  id="field-event_description_en" name="event_description_en" class="form-control body-tinymce">'.trim($value).'</textarea>';
	}

	function tour_itinerary_callback($value)
	{
	return '<textarea  id="field-tour_itinerary_en" name="tour_itinerary_en" class="form-control body-tinymce">'.trim($value).'</textarea>';
	}



	function property_resize_callback($uploader_response,$field_info, $files_to_upload){
			$this->load->library('image_moo');
			$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
			$thumb = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;
			$pic = $this->image_moo->load($file_uploaded);
			$pic->resize(1200,750)->save($file_uploaded,true);
			$pic->resize_crop(540,320)->save($thumb,true);
			return true;
	}
	function product_resize_callback($uploader_response,$field_info, $files_to_upload){
			$this->load->library('image_moo');
			$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
			$thumb = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;
			$pic = $this->image_moo->load($file_uploaded);
			$pic->resize(800,450)->save($file_uploaded,true);
			$pic->resize_crop(340,180)->save($thumb,true);
			return true;
	}

	function product_description_callback($value)
	{
	return '<textarea  id="field-product_description" name="product_description" class="form-control body-tinymce">'.trim($value).'</textarea>';
	}


	function slider_management(){
			
			$crud = new grocery_CRUD();
			$crud->set_theme('bootstrap');
			$crud->set_subject('Slider');
			// $crud->set_theme('datatables');
			$crud->set_table('slider');
			 
			// $crud->required_fields('folder_name');
			$crud->set_field_upload('slider_img','assets/images/slider');

			$crud->field_type('slider_priority','dropdown',array('1' => '1', '2' => '2','3' => '3' , '4' => '4' , '5' => '5' , '6' => '6'));
			$output = $crud->render();

			$this->_example_output($output);
	}
	public function admin_menu_management()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('admin_menu');
			$crud->set_subject('admin menu');

			$crud->field_type('admin_menu_importance', 'dropdown',
			array('1' => 'Very Important', '2' => 'Kind of Important'));

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function sort_menu(){


	foreach($_POST['listItem'] as $position=>$listItem):
	$this->MyModel->updateMenu($listItem,$position);
	endforeach;


	}

	function sort_team(){


	foreach($_POST['teamItem'] as $position=>$teamItem):
	var_dump($this->MyModel->updateTeam($teamItem,$position));
	endforeach;


	}

	public function menu_management()
	{


		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('menu');
			$crud->set_subject('menu');
			$crud->field_type('is_parent', 'dropdown',
			array('0' => 'No', '1' => 'Yes'));
			$crud->field_type('public', 'dropdown',
			array('0' => 'No', '1' => 'Yes'));

			$crud->set_relation('menu_language_id','languages','language_name');

			$crud->where('menu_language_id',$this->site_languages[0]['language_id']);
			$crud->columns(array('menu_name','menu_slug','menu_language_id','menu_template','is_parent','public'));
			$crud->add_action('ss', '', '','ui-icon-image',array($this,'just_a_test'));

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
 
	public function page_content_management()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('page_content');
			$crud->set_subject('Page Contents');
			$crud->field_type('is_parent', 'dropdown',
			array('0' => 'No', '1' => 'Yes'));
			$crud->field_type('is_public', 'dropdown',
			array('0' => 'No', '1' => 'Yes'));

			$crud->set_relation('page_content_type_related_id','page_content_types','page_content_type_title');
			$crud->set_relation('page_content_related_id','page','{page_name} - {page_slug} - {page_language_id}');

			// $crud->callback_edit_field('page_content_data',array($this,'page_content_data_callback'));
			//$crud->columns(array('menu_name','menu_slug','menu_language_id','menu_template','is_parent','public'));
			//$crud->add_action('ss', '', '','ui-icon-image',array($this,'just_a_test'));

			$crud->callback_field('page_content_data',array($this,'page_content_data_callback'));
			$output = $crud->render();


  			$output->data = $this->MyModel->getMenu();

			// var_dump($output->data);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}


function page_content_data_callback($value)
{

	$data = htmlspecialchars(file_get_contents(APPPATH.'views/templates/blogs.php'));

	 // $data = "<pre contenteditable='true'>";
  //   $data .= htmlspecialchars(file_get_contents(APPPATH.'views/templates/blogs.php'));
  //   $data .= "</pre>";

return '<textarea  id="field-page_content_data" name="page_content_data" class="form-control body-tinymce">'.htmlspecialchars(trim($value)).'</textarea>';
 
// return '<textarea  id="field-page_content_data" name="page_content_data" class="form-control body-tinymce">'.trim($value).'</textarea>';
}

	function just_a_test($primary_key , $row)
	{
	return site_url('demo/action/action_photos').'?country='.$row->menu_slug;
	}

	public function settings_management()
	{
			$crud = new grocery_CRUD();


			$crud->set_theme('bootstrap');
			$crud->set_table('settings');
			$crud->set_subject('settings');
			$crud->columns('email','company');
			$crud->field_type('description', 'string');


			$crud->set_field_upload('site_image','assets/images');
			$crud->set_field_upload('logo','assets/images');
			$crud->set_field_upload('default_image','assets/images');

			$crud->unset_delete();
			$output = $crud->render();

			$this->_example_output($output);
	}


	public function custom_management()
	{
			$crud = new grocery_CRUD();


			// $crud->set_theme('bootstrap');
			// $crud->set_table('meta_content');
			// $crud->set_subject('Meta Content - Menu');
			// $crud->set_relation('meta_content_related_menu_id','menu','menu_name');
			$output = $crud->render();

	$this->_example_output((object)array('output' =>'' , 'js_files' => $output->js_files , 'css_files' =>$output->css_files , 'data' => $data),'templates/admin/index.php');
	}


	public function meta_content_management()
	{
			$crud = new grocery_CRUD();


			$crud->set_theme('bootstrap');
			$crud->set_table('meta_content');
			$crud->set_subject('Meta Content - Menu');
			$crud->set_relation('meta_content_related_page_id','page','page_name');

			
			$output = $crud->render();

			$this->_example_output($output);
	}


	public function pages_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('page');

			$crud->field_type('page_template', 'string');
			$crud->field_type('page_content', 'string');
			$crud->set_subject('page');
			$crud->set_relation('page_language_id','languages','language_name');


			$crud->where('page_language_id',$this->site_languages[0]['language_id']);
$crud->callback_field('page_content',array($this,'decrypt_password_callback'));

			$output = $crud->render();

			$this->_example_output($output);
	}

function decrypt_password_callback($value)
{

 
return '<textarea  id="field-page_content" name="page_content" class="form-control body-tinymce">'.trim($value).'</textarea>';
}
	public function customers_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_table('customers');
			$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
			$crud->display_as('salesRepEmployeeNumber','from Employeer')
				 ->display_as('customerName','Name')
				 ->display_as('contactLastName','Last Name');
			$crud->set_subject('Customer');
			$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function works_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('works');
			$crud->set_subject('works');
			// $crud->unset_columns('productDescription');
			// $crud->set_relation('blog_related_user_id','users','{first_name} {last_name}');
			// $crud->set_relation('blog_related_category_id','blogs_categories','blogs_category_name');
			$crud->set_field_upload('workImage','assets/uploads/files');
			

			$crud->set_relation('workCategoryID','works_categories','categoryTitle');
			// $crud->field_type('blog_created_at', 'hidden', time());

			// $crud->callback_field('blog_description',array($this,'blog_description_callback'));
			// $crud->callback_column('blog_description',array($this,'blog_description_callback'));

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function users_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('users');
			$crud->set_subject('users');
			// $crud->unset_columns('productDescription');
			// $crud->set_relation('blog_related_user_id','users','{first_name} {last_name}');
			// $crud->set_relation('blog_related_category_id','blogs_categories','blogs_category_name');
			$crud->set_field_upload('user_image','assets/uploads/users');
			
			// $crud->field_type('blog_created_at', 'hidden', time());

			// $crud->callback_field('blog_description',array($this,'blog_description_callback'));
			// $crud->callback_column('blog_description',array($this,'blog_description_callback'));

			$output = $crud->render();

			$this->_example_output($output);
	}


	public function blogs_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('blogs');
			$crud->set_subject('Blogs');
			// $crud->unset_columns('productDescription');
			$crud->set_relation('blog_related_user_id','users','{first_name} {last_name}');
			$crud->set_relation('blog_related_category_id','blogs_categories','blogs_category_name');
			$crud->set_field_upload('blog_image','assets/uploads/blogs');
			$crud->callback_after_upload(array($this,'blog_resize_callback'));
			
			$crud->field_type('blog_created_at', 'hidden', time());

			// $crud->callback_field('blog_description',array($this,'blog_description_callback'));
			// $crud->callback_column('blog_description',array($this,'blog_description_callback'));

			$output = $crud->render();

			$this->_example_output($output);
	}


	function blog_resize_callback($uploader_response,$field_info, $files_to_upload){
			$this->load->library('image_moo');
			$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
			$thumb = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;
			$pic = $this->image_moo->load($file_uploaded);
			$pic->resize(9999,650)->save($file_uploaded,true);
			$pic->resize_crop(440,220)->save($thumb,true);
			return true;
	}

	function blog_description_callback($value)
	{
	return '<textarea  id="field-blog_description" name="blog_description" class="form-control body-tinymce">'.trim($value).'</textarea>';
	}

	public function blogs_categories_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('bootstrap');
			$crud->set_table('blogs_categories');
			$crud->set_subject('Blogs');
			// $crud->unset_columns('productDescription');
			$crud->set_field_upload('blogs_category_image','assets/uploads/blogs');
			// $crud->callback_column('buyPrice',array($this,'valueToEuro'));

			$output = $crud->render();

			$this->_example_output($output);
	}



	function multigrids()
	{
		$this->config->load('grocery_crud');
		$this->config->set_item('grocery_crud_dialog_forms',true);
		$this->config->set_item('grocery_crud_default_per_page',10);

		$output1 = $this->offices_management2();

		$output2 = $this->employees_management2();

		$output3 = $this->customers_management2();

		$js_files = $output1->js_files + $output2->js_files + $output3->js_files;
		$css_files = $output1->css_files + $output2->css_files + $output3->css_files;
		$output = "<h1>List 1</h1>".$output1->output."<h1>List 2</h1>".$output2->output."<h1>List 3</h1>".$output3->output;

		$this->_example_output((object)array(
				'js_files' => $js_files,
				'css_files' => $css_files,
				'output'	=> $output
		));
	}

	public function offices_management2()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('offices');
		$crud->set_subject('Office');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}




}