<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
// public function index()
//  {
//   $data['title'] = "Index";


//   $this->load->view('welcome_message', $data);
//  }

  public function __construct()
  {
    parent::__construct();
    
    baba();
    $this->site_info = $this->MyModel->getWebsiteInfo();

    //var_dump($this->site_info);
    // if($this->site_info->is_multilingual!=0)
    // $this->site_languages = $this->MyModel->getLanguages();


    $this->nav = $this->MyModel->getMenu();
    // $this->sub_nav = $this->MyModel->getSubMenu();

    // $this->shop_cats = $this->MyModel->getProductsCategories();
    // $this->subNav = $this->MyModel->getMenu('is_parent',0);
    // var_dump($this->config->item('agent_username_ary'));

    // var_dump($this->shop_cats);
    $this->lang->load('general');

              //$username = $params['username'];
              //$password = $params['password'];
          // var_dump($_POST['name']);


// Please specify an SMTP Number 25 and 8889 are valid SMTP Ports.

   
  }


  function terms(){
    
    $data['content'] = (object)[]; 
    $data['content']->meta_content_meta_title = $this->site_info->site_name;
    $data['content']->meta_content_meta_description = $this->site_info->site_description; 

    $this->parser->parse('templates/header', $data);
    $this->parser->parse('templates/nav', $data);
    $this->parser->parse('templates/_terms_data_protection', $data);
    $this->parser->parse('templates/footer', $data);

    }

  private function printJSON($var){
        echo json_encode($var);
    }


  //   public function portfolio($id=NULL)
  // {
  //      $data['content'] = (object)[];
         
  //        $params = json_decode(file_get_contents('php://input'), TRUE);


  // $data['content']->page_meta_title = 'About Cyprus |  ';
  // $data['content']->page_meta_description = 'About Cyprus '; 
  // $this->parser->parse('templates/header', $data);
  // $this->parser->parse('templates/nav', $data);
  // $this->parser->parse('templates/_portfolio', $data);
  // $this->parser->parse('templates/footer', $data);
  //       // echo json_encode(array('success'=>true, 'msg' => $this->load->view('templates/_product_info', $data)));
  // }

 public function index($page=NULL,$page_slug=FALSE)
 {
 // $this->load->library("braintree_lib");
        // $token = $this->braintree_lib->create_client_token();
        // $this->printJSON($token);
        // exit;
$this->load->library('parser');

  // var_dump($this->uri->segment(1));
 // var_dump($this->site_languages);
 $page = $this->uri->segment(2)!=NULL?$this->uri->segment(2):'welcome';

 $data['single_page_id'] = $this->uri->segment(3)!=NULL?$this->uri->segment(3):NULL;


  // var_dump($page);
  //$page = lcfirst(convert_accented_characters(urldecode($page)));

  // if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
  // {
  //     redirect('auth', 'login');
  // }
  // $hData['meta_content'] = 
  $data['title'] = ucfirst($page); // Capitalize the first letter
  
  $data['content'] = $this->MyModel->getPage($page);
  
  // var_dump($data['content']);
  // $data['content']==NULL?show_404_view():'';

  if($data['content']==NULL){


     $this->output->set_status_header('404'); 
    $data['content'] = 'error_404'; 
  $this->parser->parse('templates/header', $data);
  $this->parser->parse('templates/nav', $data);
    $this->load->view('err404',$data); 

  $this->parser->parse('templates/footer', $data);
    // exit;
  }
 else if($this->site_info->maintenance_mode==1){


  $data['site_info'] = $this->site_info;
  // $this->parser->parse('templates/header', $data);
  // $this->parser->parse('templates/nav', $data);
    $this->load->view('maintenance',$data); 
    
  // $this->parser->parse('templates/footer', $data);
    }
  else{

  $data['site_info'] = $this->site_info;


  $this->parser->parse('templates/header', $data);
  $this->parser->parse('templates/nav', $data);

  if($data['content']->page_template!=NULL)
{

$this->load->view('templates/'.$data['content']->page_template, $data);

}
else{


}


  $data['mydata'] = $this->MyModel->getPageContents($data['content']->page_id);
// var_dump(count($data['mydata']));
  $template=  $data['content']->page_template==NULL?'templates/default':$data['content']->page_template;
  // if(count($data['content']>=0)
  // $current_template = 
  // exit;



  
  // $this->load->view($template, $data);



 foreach ($data['mydata'] as $key => $mydata) {
                switch ($mydata->page_content_type_title) {
                    case 'html':
                        // $template = '{mydata}{page_content_data}{/mydata}';
                        $template = 'Hello, {firstname} {lastname}';
                        $data = array('title' => 'Mr', 'firstname' => 'John', 'lastname' => 'Doe');
                        $this->parser->parse_string($mydata->page_content_data, $data);
                        // echo $this->shortcodes->parse( ($mydata->page_content_data));
                        // echo html_entity_decode($data->page_content_data);
                        
                    break;
                    case 'template':
                        // $this->load->view('templates/' . $mydata->page_content_template, $mydata);
                    break;
                    case 'multitemplate':
                        $data = array('title' => 'Mr', 'firstname' => 'John', 'lastname' => 'Doe');
                        $this->parser->parse($template, $data);
                    break;
                    case 'single_template':
                        $this->parser->parse_string($mydata->page_content_data, $data);
                    break;
                    default:
                        # code...
                        
                    break;
                }
            }



  $this->parser->parse('templates/footer', $data);
  }


  // $this->parser->parse($template, $data);


  // if($data['single_page_id']!=NULL){

 
  //     $this->load->view('templates/blog_single',$data);

  // }
  // $this->load->view('pages/'.$page, $data);
  // $this->load->view('templates/footer', $data);
 }



  public function upload_file()
{
    $status = "";
    $msg = "";
    $file_element_name = 'myFile';
    $application_form_file_element_name = 'myFile';


       $params = json_decode(file_get_contents('php://input'), TRUE);
          // var_dump($_POST['name']);
// var_dump(file_get_contents("php://input")); 

                  // var_dump($params);
                  $results = json_decode(json_encode($_POST), true);
// var_dump($_POST);

     // var_dump($this->input->post('first_name'));

     // exit;
    // if (empty($_POST['first-name']))
    // {
    //     $status = "error";
    //     $msg = "Please enter a title";
    // }
     


    if ($status != "error")
    {
        $config['upload_path'] = realpath(APPPATH . '../assets/uploads/');
        $config['allowed_types'] = 'gif|jpg|png|doc|txt|xls|docx|xlsx';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
 
        $this->load->library('upload', $config);
 
        if (!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
          echo json_encode(array('status' => $status, 'msg' => $msg));
        }
        else
        {
            $data = $this->upload->data();
// var_dump($data);
                        $status = "success";
                $msg = "File successfully uploaded";

            $this->upload->do_upload($file_element_name);
            $app_file_data = $this->upload->data();
// $path_to_the_file = realpath(FCPATH.'assets/uploads/'.$data['file_name']);
 // $path = set_realpath('uploads/pdf/');
 // $this->email->attach($path . 'yourfile.pdf');

 // var_dump($path_to_the_file);

 $email_message = '';

        foreach ($_POST as $key => $value)
    $email_message.= "".htmlspecialchars($key).": ".htmlspecialchars($value)." \r\n <br/>";
    $email_message.= "Αρχειο Βιογραφικού: ".base_url()."assets/uploads/".$data['file_name']."  \r\n";
    // $email_message.= "Application Form file: ".base_url()."assets/uploads/".$app_file_data['file_name']."  \r\n";
    // $email_message.= "Date sent: ".date('d-M-Y H:i:s')." \r\n";

                  // $email_message = 'Name: '.$name.' '\r\n'E-Mail: '.$email.'<br />Phone: '.$phone.'<br />Subject: '.$msg_subject.'<br />Message: '.$message.'<br /><br /><br /><br />';
    if($status=="success"){

    if(send_simple_message($this->config->item('admin_email'),'New message from our Website - Career Form',$email_message,$_POST['email']))
    echo json_encode(array('success'=>true, 'msg' => "Η φόρμα εστάλη επιτυχώς. Θα σας απαντήσουμε το συντομότερο δυνατόν. Ευχαριστούμε!"));
    }
    else
    echo json_encode(array('status' => $status, 'msg' => $msg));
            // $file_id = $this->files_model->insert_file($data['file_name'], $_POST['title']);
        //     if($file_id)
        //     {
    


        //     }
        //     else
        //     {
        //         unlink($data['full_path']);
        //         $status = "error";
        //         $msg = "Something went wrong when saving the file, please try again.";
        //     }
        }
        // @unlink($_FILES[$file_element_name]);
    }
}



    public function my_404_view()
  {

    // echo '5';
        // $CI =& get_instance();
        // $CI->output->set_status_header('404'); 
    //loading in custom error view
    

        // $data['content'] = $this->MyModel->getPage($page);
        // $CI->load->view('templates/header',$data);
        // $CI->load->view('err404');
        // $CI->load->view('templates/footer',$data);
  }
    public function db_backup()
  {
         $this->load->dbutil();   
         $backup =$this->dbutil->backup();  
         $this->load->helper('file');
         write_file('backup/your_DB'.date('Y-M-d').'.zip', $backup); 
  }


  //   public function services($type=NULL)
  // {
  //      $data['content'] = (object)[];
         
  //        $params = json_decode(file_get_contents('php://input'), TRUE);


  // $this->parser->parse('templates/header', $data);
  // $this->parser->parse('templates/nav', $data);
  // $this->parser->parse('templates/_services', $data);
  // $this->parser->parse('templates/footer', $data);
  //       // echo json_encode(array('success'=>true, 'msg' => $this->load->view('templates/_product_info', $data)));
  // }

 
    public function portfolio_single($id=NULL)
  {
       $data['content'] = (object)[];
         
  $data['portfolio'] = $this->MyModel->getSinglePortfolio($id);

         $params = json_decode(file_get_contents('php://input'), TRUE);

  $data['content']->meta_content_meta_title =  $data['portfolio'][0]->portfolio_title;
  $data['content']->meta_content_meta_description = character_limiter(strip_tags($data['portfolio'][0]->portfolio_description), 140);
  $this->parser->parse('templates/header', $data);
  $this->parser->parse('templates/nav', $data);
  $this->parser->parse('templates/portfolio_single', $data);
  $this->parser->parse('templates/footer', $data);
        // echo json_encode(array('success'=>true, 'msg' => $this->load->view('templates/_product_info', $data)));
  }

    public function property_valuations($id=NULL)
  {
       $data['content'] = (object)[];
         
         $params = json_decode(file_get_contents('php://input'), TRUE);


  $data['content']->page_meta_title = 'Property Valuations ';
  $data['content']->page_meta_description = 'Property Valuations '; 
  $this->parser->parse('templates/header', $data);
  $this->parser->parse('templates/nav', $data);
  $this->parser->parse('templates/_property_valuations', $data);
  $this->parser->parse('templates/footer', $data);
        // echo json_encode(array('success'=>true, 'msg' => $this->load->view('templates/_product_info', $data)));
  }

    public function property_management($id=NULL)
  {
       $data['content'] = (object)[];
         
         $params = json_decode(file_get_contents('php://input'), TRUE);


  $data['content']->page_meta_title = 'Property property_management  ';
  $data['content']->page_meta_description = 'About Cyprus '; 
  $this->parser->parse('templates/header', $data);
  $this->parser->parse('templates/nav', $data);
  $this->parser->parse('templates/property_management', $data);
  $this->parser->parse('templates/footer', $data);
        // echo json_encode(array('success'=>true, 'msg' => $this->load->view('templates/_product_info', $data)));
  }




    public function request_quote()
  {
       $data['content'] = (object)[];
         
         $params = json_decode(file_get_contents('php://input'), TRUE);


  $data['content']->page_meta_title = 'request_quote ';
  $data['content']->page_meta_description = 'request_quote ';
  $this->parser->parse('templates/header', $data);
  $this->parser->parse('templates/nav', $data);
  $this->parser->parse('templates/_request_quote', $data);
  $this->parser->parse('templates/footer', $data);
        // echo json_encode(array('success'=>true, 'msg' => $this->load->view('templates/_product_info', $data)));
  }

    public function properties($id=NULL)
  {
       $data['content'] = (object)[];
         
         $params = json_decode(file_get_contents('php://input'), TRUE);


  $data['content']->page_meta_title = 'Properties ';
  $data['content']->page_meta_description = 'Properties';
  $this->parser->parse('templates/header', $data);
  $this->parser->parse('templates/nav', $data);
  $this->parser->parse('templates/all_properties', $data);
  $this->parser->parse('templates/footer', $data);
        // echo json_encode(array('success'=>true, 'msg' => $this->load->view('templates/_product_info', $data)));
  }

    public function product_detail($slug)
  {
    $data['content'] = (object)[];

  // $data['content']->page_meta_title =  $data['property'][0]->property_title;
  // $data['content']->page_meta_description = character_limiter(strip_tags($data['property'][0]->property_description), 140).' -1 '.$this->site_info->site_name; 

    switch ($slug) {
      case 'pos-horeca-module':
        # code...
        $data['content']->meta_content_meta_title =  'Horeca Module';
        $data['content']->meta_content_meta_description =  'Horeca Module';
        $templ = '_horeca_module';
        break;
        case 'pos-shop-module':
          # code...
        $data['content']->meta_content_meta_title =  'Winkel Module';
        $data['content']->meta_content_meta_description =  'Horeca Module';
          $templ = '_shop_module';
          break;

          
          
          case 'webshop-apps':
            # code...
        $data['content']->meta_content_meta_title =  'Webshop Module';
        $data['content']->meta_content_meta_description =  'Webshop Module';
            $templ = '_webshop_module';
            break;

          case 'pos-supermarket-module':
            # code...
        $data['content']->meta_content_meta_title =  'Supermarkt Module';
        $data['content']->meta_content_meta_description =  'Horeca Module';
            $templ = '_supermarket_module';
            break;
            case 'meerdere-filialen-module':
              # code...
        $data['content']->meta_content_meta_title =  'Meerdere Filialen Module';
        $data['content']->meta_content_meta_description =  'Horeca Module';
              $templ = '_mehr';
              break;

      default:
        # code...
        break;
    }

    $this->parser->parse('templates/header', $data);
    $this->parser->parse('templates/nav', $data);
    $this->parser->parse('templates/'.$templ,$data);
    $this->parser->parse('templates/footer', $data);

  }



	
	// log the user in
	public function login()
	{

    $this->data['content'] = (object)[];
    $this->data['content']->meta_content_meta_title =  'Klanten Portaal';
    $this->data['content']->meta_content_meta_description =  'Klanten Portaal - Login';
		$this->data['title'] = $this->lang->line('login_heading');

		//validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() == true)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('admin/helpdesk', 'refresh');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id'   => 'password',
				'type' => 'password',
      );
      
      
  $this->load->view('templates/header', $this->data);
  $this->load->view('templates/nav', $this->data);
  $this->load->view('auth/login',$this->data);
  // $this->load->view('pages/'.$page, $data);
  $this->load->view('templates/footer', $this->data);

		}
	}

    public function submitReqForm($data=NULL)
  {
       

         $params = json_decode(file_get_contents('php://input'), TRUE);

        
        $data['product'] = $this->MyModel->getProductByID($_POST['products_request_related_id']);
        $email_message = "";
        foreach ($_POST as $key => $value)
        $email_message .= "".htmlspecialchars($key)." is ".htmlspecialchars($value)." \r\n ";
        $email_message .= "Date sent: ".date('d-M-Y H:i:s')." \r\n";
        // $email_message = 'Name: '.$name.' '\r\n'E-Mail: '.$email.'<br />Phone: '.$phone.'<br />Subject: '.$msg_subject.'<br />Message: '.$message.'<br /><br /><br /><br />';

        // if(sendemail('panospcm@hotmail.com','New email from our contact form',$email_message))
        // echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));



        echo json_encode(array('success'=>$this->MyModel->insert_products_requests($_POST), 'msg' => 'Form was submitted successfully. Thank you.'));
  }


    public function submitNewsletter($data=NULL)
  {
       

         $params = json_decode(file_get_contents('php://input'), TRUE);

          // var_dump($this->MyModel->newsletter_mail_exists($_POST['subscriber_email']));
        // $data['product'] = $this->MyModel->getProductByID($_POST['products_request_related_id']);
        // $email_message = "";
        // foreach ($_POST as $key => $value)
        // $email_message .= "".htmlspecialchars($key)." is ".htmlspecialchars($value)." \r\n ";
        // $email_message .= "Date sent: ".date('d-M-Y H:i:s')." \r\n";
        // $email_message = 'Name: '.$name.' '\r\n'E-Mail: '.$email.'<br />Phone: '.$phone.'<br />Subject: '.$msg_subject.'<br />Message: '.$message.'<br /><br /><br /><br />';

        // if(sendemail('panospcm@hotmail.com','New email from our contact form',$email_message))
        // echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));

         $success = ($this->MyModel->newsletter_mail_exists($_POST['subscriber_email'])===false&&$this->MyModel->insert_newsletter_subscriber($_POST))?true:false;
         $successMsg = $success===true?'Subscribed successfully!':'E-Mail already exists!';
        echo json_encode(array('success'=>$success, 'msg' => $successMsg));
  }


  public function invoice($tour_id){

  $data['booking'] = $this->MyModel->getBookingByID($tour_id);

  $data['tour'] = $this->MyModel->getSingleTourByID($data['booking'][0]->tours_booking_related_id);


 $this->load->view('templates/tour_booking_invoice',$data);
  // $message = $this->load->view('templates/tour_booking_invoice',$data,TRUE);
  // if(sendemail('panospcm@hotmail.com','New email from our contact form',$message))
    // echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));

  // var_dump($data['blog_previous']);
  // $this->load->view('templates/header', $data);
  // $this->load->view('templates/nav', $data);
  // $this->load->view('templates/tour_booking_invoice',$data);
  // $this->load->view('pages/'.$page, $data);
  // $this->load->view('templates/footer', $data);

  }

public function reqPayment(){

  $this->load->library("braintree_lib");
  $token = $this->braintree_lib->create_client_token();
  $create_sale = $this->braintree_lib->create_sale($_POST['price'],$_POST['nonce']);

  $price = $_POST['price'];
  $nonce = $_POST['nonce'];


  $data['tour'] = $this->MyModel->getSingleTour($_POST['tours_booking_related_tour_slug']);

  //Starting calculating booking price
  $data['tour_prices'] = $this->MyModel->getPricesByTourIdAndLocationName($data['tour'][0]->tour_id, $_POST['tours_booking_starting_location']);

  $data['number_of_adults'] = $_POST['tours_booking_people_num'];
  $data['total_adults_price'] = ($_POST['tours_booking_people_num'] * $data['tour_prices'][0]->tour_price_adult_price);
  $data['total_children_price'] = ($_POST['tours_booking_children_num'] * $data['tour_prices'][0]->tour_price_child_price);

  //ENDs calculating booking price


  //Adding TOTAL PRICE to POST
  $_POST['tours_booking_total_price'] = ($_POST['tours_booking_people_num'] * $data['tour_prices'][0]->tour_price_adult_price) + ($_POST['tours_booking_children_num'] * $data['tour_prices'][0]->tour_price_child_price);

  unset($_POST['tours_booking_related_tour_slug']);
  unset($_POST['price']);
  unset($_POST['nonce']);


  // var_dump($create_sale->transaction->status);
  // var_dump($create_sale->message);

    if($create_sale->success==true){

    $_POST['braintree_id']                    = $create_sale->transaction->id;
    $_POST['braintree_paymentInstrumentType'] = $create_sale->transaction->paymentInstrumentType;
    $_POST['braintree_status']                = $create_sale->transaction->status;
    $_POST['tours_booking_status']            = $create_sale->transaction->status;


    if($this->MyModel->insert_tours_booking($_POST)){

    $invoiceID = $this->db->insert_id();

    // $data['tour_prices'] = $this->MyModel->getPricesByTourIdAndLocationName($data['tour'][0]->tour_id, $_POST['tour_starting_location']);

    $data['booking'] = $this->MyModel->getBookingByID($invoiceID);

    // $email_message = $this->load->view('templates/tour_booking_invoice',$data,TRUE);
    $message = $this->load->view('templates/tour_booking_invoice',$data,TRUE);
    //   foreach ($_POST as $key => $value)
    // $email_message .= "".htmlspecialchars($key)." is ".htmlspecialchars($value)." \r\n ";
    // $email_message .= "Date sent: ".date('d-M-Y H:i:s')." \r\n";

    // $email_message = 'Name: '.$name.' '\r\n'E-Mail: '.$email.'<br />Phone: '.$phone.'<br />Subject: '.$msg_subject.'<br />Message: '.$message.'<br /><br /><br /><br />';

    if(sendemail($this->config->item('admin_email'),'Tour Booking!',$message)){
      //
    // echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));
      
  // $this->load->view('templates/header', $data);
  // $this->load->view('templates/nav', $data);
  // $this->load->view('templates/completed_booking',$data);
  // // $this->load->view('pages/'.$page, $data);
  // $this->load->view('templates/footer', $data);


    echo json_encode(array('success'=>$create_sale->success, 'msg' => 'Transaction status: '.$create_sale->transaction));

    }
    }

    }
    else
    {
      echo json_encode(array('success'=>$create_sale->success, 'msg' => 'There was an error in the payment. Contact the administrator to resolve the issue'));
    // echo json_encode(array('success'=>false, 'msg' => "There was an error saving your booking. Contact the administrator to resolve the issue."));
    }
    
}


  public function requestBooking(){



    $this->session->set_userdata('booking_vars', $_POST);



    // var_dump($_POST);
    $data['tour'] = $this->MyModel->getSingleTour($_POST['tours_booking_related_tour_slug']);

    //Starting calculating booking price
    $data['tour_prices'] = $this->MyModel->getPricesByTourIdAndLocationName($data['tour'][0]->tour_id, $_POST['tours_booking_starting_location']);

    $data['number_of_adults'] = $_POST['tours_booking_people_num'];
    $data['total_adults_price'] = ($_POST['tours_booking_people_num'] * $data['tour_prices'][0]->tour_price_adult_price);
    $data['total_children_price'] = ($_POST['tours_booking_children_num'] * $data['tour_prices'][0]->tour_price_child_price);

    //ENDs calculating booking price


    //Adding TOTAL PRICE to POST
    $_POST['tours_booking_total_price'] = ($_POST['tours_booking_people_num'] * $data['tour_prices'][0]->tour_price_adult_price) + ($_POST['tours_booking_children_num'] * $data['tour_prices'][0]->tour_price_child_price);

    unset($_POST['tours_booking_related_tour_slug']);
    // $related_tour_id = 
    // $related_tour_price = 
    // $tour_booking_first_name =
    // $tour_booking_last_name =
    // $tour_booking_last_name =
    // $tours_booking_people_num =
    // $tours_booking_email =
    // $tours_booking_phone =
    // $tour_booking_date = 
    // $tour_booking_price = $_POST['tours_booking_single_price'];
    // $tour_booking_quantity = $_POST['tours_booking_people_num'];


    // var_dump($tour_booking_quantity*$tour_booking_price);
    // exit;
    if(isset($_POST['paypal_request'])){

    unset($_POST['paypal_request']);

    $_POST['tours_booking_payment_type_id'] = 2;
    // Clear PayPalResult from session userdata
    $this->session->unset_userdata('PayPalResult');

    // Clear cart from session userdata
    $this->session->unset_userdata('shopping_cart');


    //************ ***********   ***********   ***********   ***********   
    //***********   ADD POST TO SESSION ARRAY !!!!!  *********** ***********     
    //************ ***********   ***********   ***********   ***********   
    $this->session->set_userdata('booking_vars', $_POST);
    // For demo purpose, we create example shopping cart data for display on sample cart review pages

    // Example Data - cart item
    $cart['items'][0] = array(
    'id' => 'TOUR-'.$data['tour'][0]->tour_id,
    'name' => 'Booking '.$data['tour'][0]->tour_title.' - Date: '.$_POST['tours_booking_date'],
    'qty' => 1,
    'price' => $_POST['tours_booking_total_price'],
    );

    // Example Data - cart variable with items included
    $cart['shopping_cart'] = array(
    'items' => $cart['items'],
    'subtotal' => $_POST['tours_booking_total_price'],
    'shipping' => 0,
    'handling' => 0,
    'tax' => 0,
    );

    // Example Data - grand total
    $cart['shopping_cart']['grand_total'] = number_format($cart['shopping_cart']['subtotal'] + $cart['shopping_cart']['handling'] + $cart['shopping_cart']['tax'], 2);

    // Load example cart data to variable
    $this->load->vars('cart', $cart);

    // Set example cart data into session
    $this->session->set_userdata('shopping_cart', $cart);
    redirect(site_url('paypal/SetExpressCheckout'), 'refresh');
  }
  else{


    unset($_POST['email_request']);

    $params = json_decode(file_get_contents('php://input'), TRUE);
    // var_dump($_POST['name']);
    $email_message = "";

    if($this->MyModel->insert_tours_booking($_POST)){

    $invoiceID = $this->db->insert_id();




    // $data['tour_prices'] = $this->MyModel->getPricesByTourIdAndLocationName($data['tour'][0]->tour_id, $_POST['tour_starting_location']);

    $data['booking'] = $this->MyModel->getBookingByID($invoiceID);

    // $email_message = $this->load->view('templates/tour_booking_invoice',$data,TRUE);
    $message = $this->load->view('templates/tour_booking_invoice',$data,TRUE);
    //   foreach ($_POST as $key => $value)
    // $email_message .= "".htmlspecialchars($key)." is ".htmlspecialchars($value)." \r\n ";
    // $email_message .= "Date sent: ".date('d-M-Y H:i:s')." \r\n";

    // $email_message = 'Name: '.$name.' '\r\n'E-Mail: '.$email.'<br />Phone: '.$phone.'<br />Subject: '.$msg_subject.'<br />Message: '.$message.'<br /><br /><br /><br />';

    if(sendemail($this->config->item('admin_email'),'Tour Booking!',$message)){
      //
    // echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));
      
  $this->load->view('templates/header', $data);
  $this->load->view('templates/nav', $data);
  $this->load->view('templates/completed_booking',$data);
  // $this->load->view('pages/'.$page, $data);
  $this->load->view('templates/footer', $data);

    }
    }
    else
    {

    echo json_encode(array('success'=>false, 'msg' => "There was an error saving your booking. Contact the administrator to resolve the issue."));
    }
    

    ob_end_flush(); 
  }


   

//var_dump($params);

  }




  public function customTourSubmit(){

    // exit;

          $params = json_decode(file_get_contents('php://input'), TRUE);
          // var_dump($_POST['name']);


              //$username = $params['username'];
              //$password = $params['password'];
              $email = $_POST['email_contact'];
              $name = $_POST['name_contact'].' '.$_POST['lastname_contact'];
              $phone = $_POST['phone_contact'];
              $message = $_POST['message_contact'];
              $msg_subject = $_POST['message_contact'];
    $email_message = "";

    foreach ($_POST as $key => $value)
    $email_message .= "".htmlspecialchars($key)." is ".htmlspecialchars($value)." \r\n  <br>";
    $email_message .= "Date sent: ".date('d-M-Y H:i:s')." \r\n <br>";

                  // $email_message = 'Name: '.$name.' '\r\n'E-Mail: '.$email.'<br />Phone: '.$phone.'<br />Subject: '.$msg_subject.'<br />Message: '.$message.'<br /><br /><br /><br />';
    
    if(sendemail($this->config->item('admin_email'),'New Custom Tour',$email_message))
    echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));

ob_end_flush();

//var_dump($params);

  }


  public function contactSubmit(){

    $params = json_decode(file_get_contents('php://input'), TRUE);


  $email_message = "";

  foreach ($_POST as $key => $value):

  if(is_array($value)){

  $dd = json_encode($value);
  $array = array($value);

  foreach($array as $keey=>$vv){
  $email_message .= "".($keey).": ".(json_encode($vv))." \r\n";
  }
  }
  else {

  $email_message .= "".($key).": ".($value)." \r\n";
  }
  endforeach;

  // foreach ($_POST as $key => $value)
  // $email_message .= "".htmlspecialchars($key).": ".htmlspecialchars($value)." \r\n";
  // $email_message .= "Date sent: ".date('d-M-Y H:i:s')." \r\n <br>";

                // $email_message = 'Name: '.$name.' '\r\n'E-Mail: '.$email.'<br />Phone: '.$phone.'<br />Subject: '.$msg_subject.'<br />Message: '.$message.'<br /><br /><br /><br />';
  
   if(sendemail($this->config->item('admin_email'),'New email from our contact form',$email_message,$_POST['email'])) {

    $this->session->set_flashdata('email_success', 'Message Submitted successfully. We will get back to you soon.Thank you!');
    redirect($_SERVER['HTTP_REFERER']);

   }
  // echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));

  // if(sendemail($this->config->item('admin_email'),'New email from our contact form',$email_message))
  // echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));

ob_end_flush();

//var_dump($params);

}

  public function quoteSubmit(){


              $email = $_POST['email'];
    $message = "";

    foreach ($_POST as $key => $value)
    $message .= "".htmlspecialchars($key)." is ".htmlspecialchars($value)." \r\n<br/> ";

  if(send_simple_message($this->config->item('admin_email'),'New fly now quote from our contact form',$message,$email))
    echo json_encode(array('success'=>true, 'msg' => "Message Submitted successfully. We will get back to you soon.Thank you!"));

  ob_end_flush();
  }
  // public function contactSubmit()
  // {

  //   if(isset($_POST['submitContact'])){

  //       $message =
  //       "Email message: \r\n ".
  //       "FullName: ".$this->input->post('fullName')." \r\n".
        
  //       "Contact Email : ".$this->input->post('email')." \r\n ".
  //       "date sent: ".date('d-M-Y H:i:s')." \r\n".
  //       "subject: ".$this->input->post('subject')." \r\n ".
  //       "Message: ".$this->input->post('message')." \r\n ";
  //       if(sendemail('panospcm@hotmail.com','New Message from our website! (acgc-group.com)',$message)){

  //         $this->session->set_flashdata('email_success', 'Success message.');
  //         redirect("welcome/contact");
  //       }
  //       //echo json_encode(true);

  //       }

  // }

    public function news_detail($blog_id)
  {
    // var_dump($blog_id);


  // $data['data'] = $this->MyModel->getPageContents($data['content']->page_id);
  $data['blog'] = $this->MyModel->get_single_blog($blog_id);

  $data['content'] = (object)[];

  $data['content']->page_meta_title =  $data['blog'][0]->blog_title.' News - Gastronomy Cyprus';
  $data['content']->page_meta_description = character_limiter(strip_tags($data['blog'][0]->blog_description), 180).' News - Gastronomy Cyprus'; 


  $data['blog_next'] = $this->MyModel->get_blog_next($blog_id);
  $data['blog_previous'] = $this->MyModel->get_blog_previous($blog_id);


  // var_dump($data['blog_previous']);
  $this->parser->parse('templates/header', $data);
  $this->parser->parse('templates/nav', $data);
  $this->parser->parse('templates/blog_single',$data);
  // $this->load->view('pages/'.$page, $data);
  $this->parser->parse('templates/footer', $data);

  }
}
