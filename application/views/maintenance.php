
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Gastronomy Cyprus">
        <meta name="author" content="<?php echo isset($page_meta_description)?$page_meta_description:$this->site_info->site_name; ?>">
        <title>Gastronomy Cyprus</title>
        

            <meta property="og:url"                content="<?php echo current_url(); ?>" />
            <meta property="og:type"               content="article" />
            <meta property="og:title"              content="<?php echo isset($content->page_meta_title)?$content->page_meta_title:$this->site_info->site_name; ?>" />
            <meta property="og:description"        content="<?php echo isset($page_meta_description)?$page_meta_description:$this->site_info->site_name; ?>" />
            <meta property="og:image:url"              content="<?php echo isset($page_meta_image)?$page_meta_image:logo_main_img(); ?>" />
            <meta property="og:image:type" content="image/png" />


        <!-- Stylesheets -->
        <link href="http://choco-pixel.com/SeventyTwo-1.6/Slideshow/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/maint/bootstrapValidator.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/maint/ladda-themeless.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/owl.carousel.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/maint/app.css" rel="stylesheet">
        

        
        <!-- Font icons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css"/><!-- Fontawesome icons css -->
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="http://choco-pixel.com/SeventyTwo-1.6/Slideshow/js/html5shiv.js"></script>
            <script src="http://choco-pixel.com/SeventyTwo-1.6/Slideshow/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Preloader -->
        <div id="preloader">
            <div id="status" class="text-center">
                <div class="spinner">
                  <div class="rect1"></div>
                  <div class="rect2"></div>
                  <div class="rect3"></div>
                  <div class="rect4"></div>
                  <div class="rect5"></div>
                </div>
            </div>
        </div>
      


        <!-- Main -->
        <section class="main text-center" id="home">
            <div class="page">
                <div class="wrapper">
                    <div class="container">
                        <!-- <img src="<?php echo base_url(); ?>assets/images/logo/gastronomyLogo.png" height="150" alt="gastronomy Cyprus Logo"> -->
                        <h1 class="heading">Gastronomy Cyprus</h1>
                        <h3 class="">Coming Soon</h3>
                        <p>
                         We are launching our new website very soon. <br/> Please stay up-to-date by subscribing to our newsletter below.
                        </p>
                        <div class="row">
                            <div id="countdown"></div>
                        </div>
                        <!-- Subscription form -->
                        <form class="form-inline signup" method="POST" action="<?php echo site_url('submit_newsletter_form'); ?>" role="form" id="signupForm">
                            <div class="form-group">
                                <input type="email" name="subscriber_email" id="subscriber_email" class="form-control" placeholder="Email address">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-theme ladda-button" data-style="expand-left"><span class="ladda-label">Get notified!</span></button>
                            </div>
                        </form>
                        <footer class="text-center">
                            <div class="social">
                                <a href="#" class="btn btn-theme"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="btn btn-theme"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="btn btn-theme"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="btn btn-theme"><i class="fa fa-dribbble"></i></a>
                                <a href="#" class="btn btn-theme"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </section>
 
 <script type="text/javascript">

var base_url = '<?php echo base_url(); ?>';
var site_url = '<?php echo site_url(); ?>';
</script>
<!-- jQuery js -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<!-- bootstrap js -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- owl carousel js -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
        
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/maint/jquery.backstretch.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/maint/bootstrapValidator.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/maint/emailAddress.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/maint/spin.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/maint/ladda.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/wow.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/maint/init.js"></script>
        

    </body>
</html>