
<?php
// var_dump($mydata);



    // $this->parser->parse('main_view', $main);


$data_villages = $this->MyModel->getVillages();

// var_dump($data_villages);
?>


<?php foreach($data_villages as $key=>$iterVillage): ?>


	<div class="listing-item hidden" data-marker-id="<?php echo $key; ?>"  data-marker-img="<?php echo base_url().'assets/images/villages/'.$iterVillage->village_image; ?>" data-marker-title="<?php echo $iterVillage->village_title_en; ?>" data-marker-lat="<?php echo $iterVillage->village_latitude; ?>" data-marker-lng="<?php echo $iterVillage->village_longitude; ?>" data-marker-descr="<?php echo $iterVillage->village_description_en; ?>">
  <div class="map-infowindow-html">
    <?php echo $iterVillage->village_title_en; ?> <a href="#/some-uri">Go</a>
  </div>
</div>


<?php endforeach; ?>
<section class="about-section ">
    <div class="container-fluid">
        <div class="">
            <div>
                <div id="villages-map" class="map">
                </div>
            </div>
        </div>
    </div>
</section>

<section>
	
	<div class="container padding_60">
<div class="row">
<div class="col-md-8 col-sm-8">
<div class="form_title">
<h3><strong><i class="fa fa-envelope"></i></strong>Contact us for a tailormade Tour</h3>
<p>Please fill the form below for a tailormade Tour request. We will get back to you as soon as possible to arrange further details.</p>
</div>
<div class="step">
<div id="message-contact"></div>
<form method="POST" action="<?php echo site_url('customTourSubmit'); ?>" id="customTourForm">
<div class="row">
<div class="col-md-6 col-sm-6">
<div class="form-group"><label>First Name</label> <input type="text" required="" class="form-control" id="name_contact" name="name_contact" placeholder="Enter Name" /></div>
</div>
<div class="col-md-6 col-sm-6">
<div class="form-group"><label>Last Name</label> <input type="text" required="" class="form-control" id="lastname_contact" name="lastname_contact" placeholder="Enter Last Name" /></div>
</div>
</div>
<!-- End row -->
<div class="row">
<div class="col-md-6 col-sm-6">
<div class="form-group"><label>Email</label> <input type="email" required="" id="email_contact" name="email_contact" class="form-control" placeholder="Enter Email" /></div>
</div>
<div class="col-md-6 col-sm-6">
<div class="form-group"><label>Phone</label> <input type="text" required="" id="phone_contact" name="phone_contact" class="form-control" placeholder="Enter Phone number" /></div>
</div>
</div>


<div class="row">
<div class="col-md-12">


<div class="form-group"><label>Choose Villages you would like in your Custom Tour</label> 


<select data-placeholder="Select Cyprus Villages" class="chosen-select form-control" name="villages[]" multiple tabindex="6">
<?php foreach($data_villages as $key=>$iterVillage): ?>

<option value="<?php echo $iterVillage->village_title_en; ?>"><?php echo $iterVillage->village_title_en; ?> </option>

<?php endforeach; ?>
</select>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">


<div class="form-group"><label>Comments</label> <textarea rows="5" required="" id="message_contact" name="message_contact" class="form-control" placeholder="Write your message" style="height: 200px;"></textarea></div>
</div>
</div>
<div class="row">
<div class="col-md-6"><input type="submit" value="Submit" class="btn_1" id="submit-contact" /></div>
</div>
</form></div>
</div>
<!-- End col-md-8 -->
<div class="col-md-4 col-sm-4">
<div class="box_style_1"><span class="tape"></span>
<h4>Address <span><i class="icon-pin pull-right"></i></span></h4>
<p>Ellados Street 40, 3041 Limassol Cyprus</p>
<hr />
<h4>Help center <span><i class="icon-help pull-right"></i></span></h4>
<p>Contact us for any tailor-made tour requests or any questions you might have.</p>
<ul id="contact-info">
<li>+357 97653604</li>
<li>info@gastronomycyprus.com</li>
</ul>
</div>
<div class="box_style_4"><i class="icon_set_1_icon-57"></i>
<h4>Need <span>Help?</span></h4>
<a href="tel://+357 97653604" class="phone">+357 97653604</a> <small>Monday to Friday 08:00 - 13:00, 16:00 - 19:00</small></div>
</div>
<!-- End col-md-4 --></div>
<!-- End row --></div>
<!-- End container -->


</section>