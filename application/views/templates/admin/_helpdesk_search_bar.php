
<section class="p-t-80 p-b-40 back-gradient">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-1">
					<div class="input-buttons success mb-4">
					<form action="<?php echo site_url('admin/helpdesk_search'); ?>" method="GET">
						<input type="text" placeholder="Search for help"  name="query">
						<button><i class="fa fa-search"></i></button>
                  </form>
					</div>
				</div>
				<div class="col-lg-10 offset-1">
					<ol class="breadcrumb center-center text-left">
						<li class="breadcrumb-item"><a href="<?php echo site_url(array('admin', 'helpdesk')); ?>">Help Center</a></li>
						<li class="breadcrumb-item active" aria-current="page">Search</li>
					</ol>
				</div>
			</div>
		</div>
	</section>

