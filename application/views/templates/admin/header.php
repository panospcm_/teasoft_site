<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <title><?php echo $this->site_info->site_name; ?></title>
    <?php
    foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <!-- CSS Global -->
    <!-- <link href="http://simpleqode.com/preview/kite/assets/css/styles.css" rel="stylesheet"> -->
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
    <!-- CSS Plugins -->
    <!-- <link href="http://coreui.io/demo/Static_Demo/assets/css/style.css" rel="stylesheet"> -->

        <!-- Font icons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css"/><!-- Fontawesome icons css -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/admin/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/admin/responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="js/html5shiv.min.js"></script>
          <script src="js/respond.min.js"></script>
    <![endif]-->
    <?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

       <?php

    if($this->router->fetch_method()=='single_tours_date_management'): ?>

        
        <link type="text/css" rel="stylesheet" href="http://localhost/jw_gastronomy/assets/grocery_crud/css/ui/simple/jquery-ui-1.10.1.custom.min.css" />
        <script src="<?php echo base_url(); ?>assets/grocery_crud/js/jquery_plugins/ui/jquery-ui-1.10.3.custom.min.js"></script>
        <!-- <script src="<?php echo base_url(); ?>assets/grocery_crud/js/jquery_plugins/config/jquery.datepicker.config.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/grocery_crud/themes/bootstrap/build/js/global-libs.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/grocery_crud/themes/bootstrap/js/form/edit.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/grocery_crud/themes/bootstrap/js/jquery-plugins/tinymce/tinymce.min.js"></script>
  
      <script type="text/javascript">
        
        var site_url = '<?php echo site_url(); ?>';
$(function(){

  $( document ).ready(function( $ ) {

    function load_dates(){


      // alert(product_id);
   $.ajax({
      url: site_url+'admin/',
      dataType: 'html',
      // type: 'post',
      data: {
  start: $('input#tour_id').val(),
    end: endDateTime

      },
      beforeSend: function() {
       
         // $('.loading').html('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
         // $('#myModal .modal-dialog').html('gewge');
      },    
      complete: function() {
         $('.wait').remove();
      },
      success: function(json) {

      //   if(json['success']===true)
      //   alert(json['msg']);
      // else
      //   alert(json['msg']);

        console.log(json);
        
      }
   });
    }

  $('.datepicker-inputt').datepicker({
      // dateFormat: js_date_format,
      showButtonPanel: true,
      changeMonth: true,
      changeYear: true,
       onSelect: function(selectedDate) {
    // custom callback logic here
    load_dates();
  }
  });
  
  $('.datepicker-input-clear').button();
  
  $('.datepicker-input-clear').click(function(){
    $(this).parent().find('.datepicker-inputt').val("");
    return false;
  });
  
});
});

      </script>
    <?php

    endif;
        
    ?>
  </head>
  <body class="sticky-header <?php echo $this->ion_auth->is_admin() ? 'no-side' : 'no-side'; ?>">


   <!--Start left side Menu-->
    <div class="  sticky-left-side <?php echo $this->ion_auth->is_admin() ? 'hidden' : 'hidden'; ?>">

        <!--logo-->
        <div class="logo">
            <a href="<?php echo site_url(array('admin','index')); ?>">Klanten Portaal</a>
        </div>

        <div class="logo-icon text-center">
            <a href="<?php echo current_url(); ?>"><?php echo $this->site_info->site_name; ?></a>
        </div>
        <!--logo-->

        <div class="left-side-inner">
            <!--Sidebar nav-->
<style type="text/css">
  .editable{
    position: relative;
  }

</style>
            <ul class="nav nav-pills nav-stacked custom-nav">
            <li>
                            <a href="<?php echo site_url(array('admin','helpdesk')); ?>" class="waves-effect editable">
                            <i class="fa fa-home"></i><span> Home </span></a>
                            </li>

             <?php 
             
             $helpdesk_categories = $this->MyModel->get_helpdesk_categories();

             if ($this->ion_auth->is_admin())
             {
            
             foreach ($this->admin_menu as $iterAdminMenu):
             if($iterAdminMenu->admin_menu_importance==1):
              ?>
                            <li>
                            <a href="<?php echo site_url(array('admin',admin_final_link($iterAdminMenu->admin_menu_link))); ?>" class="waves-effect editable"><i class="<?php echo $iterAdminMenu->admin_menu_icon; ?>"></i><span> <?php echo $iterAdminMenu->admin_menu_title; ?> </span></a>
                            </li>

                        <?php 
                        endif;
                        endforeach; 
                      }
                        
                        ?>

<?php foreach($helpdesk_categories as $key=>$iterCategory): ?>
<li>
<a href="<?php echo site_url(array('admin','helpdesk_category', $iterCategory->helpdesk_category_id)); ?>" 
class="waves-effect"><i class="<?php echo $iterCategory->helpdesk_category_icon; ?>"></i><span>
<?php echo $iterCategory->helpdesk_category_title; ?> </span></a>
</li>

<?php 
endforeach; ?>

                <li class="menu-list"><a href="#"><i class="icon-home"></i> <span>Menu</span></a>
                    <ul class="sub-menu-list">
                 
             <?php foreach ($this->admin_menu as $iterAdminMenu):
             if($iterAdminMenu->admin_menu_importance==0):
              ?>
                            <li>
                            <a href="<?php echo site_url(array('admin',admin_final_link($iterAdminMenu->admin_menu_link))); ?>" class="waves-effect"><i class="<?php echo $iterAdminMenu->admin_menu_icon; ?>"></i><span> <?php echo $iterAdminMenu->admin_menu_title; ?> </span></a>
                            </li>

                        <?php 
                        endif;
                        endforeach; ?>
                    </ul>
                </li>

        
             

            </ul>
            <!--End sidebar nav-->

        </div>
    </div>
    <!--End left side menu-->


    <!-- Main content -->

    <!-- main content start-->
    <div class="main-content" >


        <!-- header section start-->
        <div class="header-section ">

        <div class="container">
        <div class="col-md-6 col-xs-3">
<a href="<?php echo site_url(); ?>" class=" " target="_blank">
        <img src="<?php echo logo_main_img(); ?>" class="img-fluid img-responsive"
style="max-height:50px;"
alt="<?php echo $this->site_info->site_name; ?> main logo" title=""   />
                      </a>
            <a class="toggle-btn <?php echo $this->ion_auth->is_admin() ? 'hidden' : 'hidden'; ?>"><i class="fa fa-bars"></i></a>


              
 </div>
        
            <!--notification menu start -->
            <div class="col-md-6 col-xs-9">
                <ul class="notification-menu pull-right">

<li>



<a href="<?php echo site_url(); ?>" class="btn  navbar-btn navbar-left btn " target="_blank">
                    <i class="fa fa-link"></i> Visit website
                  </a>

</li>


                      <!-- <li class="dropdown">
 <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url(); ?>assets/images/<?php echo ($this->lang->lang()); ?>_flag.png" alt="" />
                         <?php echo $this->lang->lang_full_text(); ?>
                           
                        </a>
      <ul class="dropdown-menu" role="menu">
        <li><?php echo anchor($this->lang->switch_uri('en'),'english'); ?></li>
        <li><?php echo anchor($this->lang->switch_uri('nl'),'Dutch'); ?></li>
        <li><?php echo anchor($this->lang->switch_uri('de'),'german '); ?></li>
        <li><?php echo anchor($this->lang->switch_uri('ru'),'russian '); ?></li>
        <li class="divider"></li>
      </ul>
    </li> -->
                    <li>
                        <a href="#" class=" navbar-btn dropdown-toggle btn btn-default " data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                         <?php echo $this->user->username; ?>
                           
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">


               <?php         
             if ($this->ion_auth->is_admin())
             {
               ?>
<li>
<a href="<?php echo site_url(array('admin','helpdesk_articles_management')); ?>"><i class="fa fa-edit"></i> Edit Helpdesk Articles</a>
</li>
<li>
<a href="<?php echo site_url(array('admin','helpdesk_categories_management')); ?>"><i class="fa fa-edit"></i>Edit Helpdesk Categories</a>
</li>

<?php         
             }
?>

<li>
<a href="<?php echo site_url(array('admin','helpdesk')); ?>"><i class="fa fa-info-circle"></i> Helpdesk</a>
</li>

<li>
<a href="<?php echo site_url(array('auth','change_password')); ?>"><i class="fa fa-user"></i> Change User Password</a>
</li>
<li>
<a href="<?php echo site_url(array('admin','users_management','edit', $this->user->id )); ?>"><i class="fa fa-edit"></i> Edit profile</a>
</li>
<li>
<a href="<?php echo site_url(array("auth","logout")); ?>"><i class="fa fa-sign-out"></i> Sign out</a>
</li>
</ul>
</li>

                </ul>
            </div>
            <!--notification menu end -->
            </div>
        </div>
        <!-- header section end-->


<div class="wrapper">