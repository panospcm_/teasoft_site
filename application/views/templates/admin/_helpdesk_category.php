<?php 
      $helpdesk_category = $this->MyModel->get_helpdesk_category($this->uri->segment(4));
      $helpdesk_category = $helpdesk_category[0];
      $helpdesk_categories = $this->MyModel->get_helpdesk_articles(NULL, $this->uri->segment(4), NULL);
    


      $this->load->view('templates/admin/_helpdesk_search_bar');

 ?>




<section class="article-style-5 clearfix com-pad post-details">
        
          
        <div class="container">
     

        <div class="topic-page topic-list blog-list">
                            
                            

                        </div>


          <div class="row row-eq-hight justify-content-center">
          
            <div class="col-sm-10">

            <div class="inner-page-title col-sm-12 ">
              <h4>Category : <?php echo $helpdesk_category->helpdesk_category_title; ?></h4>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">
                  Help Center
                </li>
                </ol>
              </nav>
            </div>

            <div class="clearfix">

</div>

              <div class="article-style-5-inner">
                <!-- <h5><a href="#">Account Settings</a></h5> -->
            
						<?php foreach($helpdesk_categories as $key=>$article): ?>    



              

                            
                            <!-- end article well -->

                            <article class="well clearfix">
                                <div class="topic-desc row-fluid clearfix">
                                    <div class="col-sm-4">
                                    <a href="<?php echo helpdesk_article_link($article); ?>" title="">
                                        <img src="<?php echo helpdesk_article_img_path($article); ?>" alt="" class="img-responsive img-thumbnail">
            </a>
                                    </div>
                                    <div class="col-sm-8">
                                        <h4><a href="<?php echo helpdesk_article_link($article); ?>" title=""><?php echo $article->helpdesk_article_title; ?> </a></h4>
                                        <!-- <div class="blog-meta clearfix">
                                            <small>September 12, 2016</small>
                                            <small><a href="#">31 Comments</a></small>
                                            <small>in <a href="#"> Tips &amp; Tricks</a></small>
                                            <small>by <a href="#"> Jenny Doe</a></small>
                                        </div> -->
                                        <p> <?php echo word_limiter(strip_tags($article->helpdesk_article_description), 24); ?></p>
                                        <a href="<?php echo helpdesk_article_link($article); ?>" class="readmore" title="">Continue reading →</a>
                                    </div>
                                </div>
                                <!-- end tpic-desc -->

                            </article>
                            <!-- end article well -->

    


            <?php endforeach; ?>
            
               
              </div>
            </div>
            

          </div>
        </div>


    </section>