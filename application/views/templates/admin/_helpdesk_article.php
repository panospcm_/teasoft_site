<?php 
      $helpdesk_categories = $this->MyModel->get_helpdesk_categories();
      $article = $data['article'][0];

 ?>

<section class="p-t-80 p-b-40 back-gradient">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-2">
					<div class="input-buttons success mb-4">
               <form action="<?php echo site_url('admin/helpdesk_search'); ?>" method="GET">
						<input type="text" placeholder="Search for help"  name="query">
						<button><i class="fa fa-search"></i></button>
                  </form>
					</div>
				</div>
				<div class="col-lg-10 offset-1">
					<ol class="breadcrumb center-center">
						<li class="breadcrumb-item"><a href="<?php echo site_url(array('admin', 'helpdesk')); ?>">Help Center</a></li>
						<li class="breadcrumb-item active" aria-current="page">Article</li>
					</ol>
				</div>
			</div>
		</div>
   </section>
   
<section class="p-t-60 p-b-120">
		<div class="container">
			<div class="row">
				<div class="offset-lg-1 col-lg-10">
					<div class="box rounded p-5">
						<h1 class="m-b-10">
            <?php echo $article->helpdesk_article_title; ?></h1>
					

						<!-- profile card start -->
						<div class="divider m-b-20"></div>
					
						<div class="divider m-b-20"></div>
						<!-- profile card end -->


<div class="attached_file_section padding-top-bottom-10">
<h5>Attached Files</h5>
<span class="separator small"></span>
<div class="wrapbox">
<table class="table table-hover"> 
<thead> 
<tr> 
<th><h6>#</h6></th> 
<th><h6>File</h6></th> 
<th><h6>Download</h6></th> 
</tr> 
</thead>	
<tbody> 
<tr> 
<th scope="row">1</th> 
<td><a href="<?php echo helpdesk_article_file1_path($article); ?>" target="_blank"><?php echo $article->helpdesk_article_file_1; ?></a></td> 
<th scope="row">
<a href="<?php echo helpdesk_article_file1_path($article); ?>" class="btn btn-link" download="<?php echo ($article->helpdesk_article_file_1); ?>">
<i class="fa fa-link"></i> Download</a></th> 
</tr> 
</tbody><tbody> 

</tbody></table>
</div>
</div>



						<div class="text-content">
                     
                     
<img src="<?php echo helpdesk_article_img_path($article); ?>" />

<p><?php echo $article->helpdesk_article_description; ?>


						</div>

						<div class="divider m-b-40"></div>
                  
                  
                  
					</div>
				</div>
			</div>
		</div>
	</section>

         <div class="container content-wrapper body-content">
    <div class="row margin-top-60 margin-bottom-60">
    <div class="left-widget-sidebar"></div>
        <div class="col-md-9 col-sm-12  kbpg card"> 
        
		
<!--SEARCH-->
<!--EOF SEARCH-->

<
              
	           <div class="clearfix"></div>
                	
        </div>

           </div>
</div>

