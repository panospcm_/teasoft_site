<?php 
      $helpdesk_categories = $this->MyModel->get_helpdesk_categories();

 ?>




<section class="p-t-80 p-b-40 back-gradient">
		<div class="container">
			<div class="row">
         <div class="col-lg-8 offset-2">

                
                <h1 class="text-white text-center m-b-30"><b>Welcome! </b>how can we help?</h1>
					<div class="input-buttons success mb-4">
                  <form action="<?php echo site_url('admin/helpdesk_search'); ?>" method="GET">
						<input type="text" placeholder="Search for help"  name="query">
						<button><i class="fa fa-search"></i></button>
                  </form>
					</div>
				</div>
            
                
			</div>
		</div>
	</section>





         
         <div class="product">


         <div class="product_inner mt-5">
            <div class="container">
               <div class="title">
                  <h2>Which category do you need help with?</h2>
                  <p>Please select a category to view related articles. </p>
               </div>
               <div class="row row-eq-height product_row">

               <?php foreach($helpdesk_categories as $key=>$iterCategory): ?>


                  <?php if ( !$this->ion_auth->is_admin() && $iterCategory->helpdesk_category_admin_only == 1):
                     
                     continue;

                     ?>


                     
                  <?php else: ?>
                  <div class="topic-desc row-fluid clearfix text-left">
                    <div class="col-sm-3">
                        <a data-gall="myGallery" class="venobox vbox-item" 
                        href="<?php echo site_url(array('admin','helpdesk_category', $iterCategory->helpdesk_category_id)); ?>">
                        <img src="<?php echo helpdesk_article_img_category_path($iterCategory); ?>" alt="" class="img-responsive img-thumbnail" style="width:100%;"></a>
                    </div>
                    <div class="col-sm-9">
                        <h4>
                        <a data-gall="myGallery" class="venobox vbox-item" 
                        href="<?php echo site_url(array('admin','helpdesk_category', $iterCategory->helpdesk_category_id)); ?>">
                        <?php echo $iterCategory->helpdesk_category_title; ?>
               </a>
                     </h4>
                     <p><?php echo $iterCategory->helpdesk_category_description; ?></p>
                        <p> <a href="<?php echo site_url(array('admin','helpdesk_category', $iterCategory->helpdesk_category_id)); ?>" class="btn btn-primary">View More</a> </p>
                    </div>
                </div>
                  <?php endif; ?>

                  <!-- <div class="col-sm-3 product_box_outer">
                     <div class="product_box  ">
                        <a href="<?php echo site_url(array('admin','helpdesk_category', $iterCategory->helpdesk_category_id)); ?>">
                           <i class="<?php echo $iterCategory->helpdesk_category_icon; ?> color-change"></i>
                           <div class="product_tittle">
                              <h4 class="hover-color"><?php echo $iterCategory->helpdesk_category_title; ?></h4>
                           </div>
                        </a>
                     </div>
                  </div> -->
               <?php endforeach; ?>
                 
                
                
               </div>
               
            
            </div>
         </div>



         </div>
