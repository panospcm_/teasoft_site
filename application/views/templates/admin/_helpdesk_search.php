<?php 
      $helpdesk_categories = $this->MyModel->get_helpdesk_categories();
      $articles = $data['articles'];

 ?>

<section class="p-t-80 p-b-40 back-gradient">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-1">
					<div class="input-buttons success mb-4">
					<form action="<?php echo site_url('admin/helpdesk_search'); ?>" method="GET">
						<input type="text" placeholder="Search for help"  name="query">
						<button><i class="fa fa-search"></i></button>
                  </form>
					</div>
				</div>
				<div class="col-lg-10 offset-1">
					<ol class="breadcrumb center-center text-left">
						<li class="breadcrumb-item"><a href="<?php echo site_url(array('admin', 'helpdesk')); ?>">Help Center</a></li>
						<li class="breadcrumb-item active" aria-current="page">Search</li>
					</ol>
				</div>
			</div>
		</div>
	</section>


	

	
<section class="p-t-120 p-b-120">
		<div class="container">
			<div class="row">
				<div class="offset-lg-2 col-lg-8">
					<div class="box rounded p-5">
						<h3 class="m-b-10">
						<?php echo count($articles); ?>  Results for <?php echo $this->input->get('query'); ?></h3>
					

						<!-- profile card start -->
						<div class="divider m-b-20"></div>
						
						<div class="divider m-b-20"></div>
						<!-- profile card end -->





						<div class="text-content">
                     
							<div class="article-style-5-inner">
                  <!-- <h5><a href="#">Account Settings</a></h5> -->
				  <ul>
						<?php foreach($articles as $key=>$article): ?>       

            
                  <li><a href="<?php echo helpdesk_article_link($article); ?>"><?php echo $article->helpdesk_article_title; ?> </a></li> 

						<?php endforeach; ?>
						</ul>
               
			   </div>

						</div>

						<div class="divider m-b-40"></div>
                  
                  
                  
					</div>
				</div>
			</div>
		</div>
	</section>
