<div id="content" class="site-content" role="main">

<section class="page-title-section bg-img"  style="background-image: url(https://images.pexels.com/photos/3773835/pexels-photo-3773835.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940);">
<div class="container">
<div class="row text-center">
   <div class="col-md-12">
      <!-- <h1>We houden van allerlei soorten communicatie. Neem vrijblijvend contact met ons op.</h1> -->
   </div>
   
</div>
</div>
</section>

   <article id="post-6591" class="post-6591 page type-page status-publish hentry">
      <div class="entry-content">
         <div id="dslc-main">
            <div class="dslc-modules-section  dslc-full  dslc-no-columns-spacing " style="padding-bottom:0px;padding-top:0px;background-color:transparent;border-right-style: hidden; border-left-style: hidden; " data-section-id="1508f48843b">

      <div class="dslc-modules-section  dslc-no-columns-spacing " style="padding-bottom:0px;padding-top:0px;padding-left:7%;padding-right:7%;border-color:rgb(244, 244, 244);border-top-style: hidden; border-right-style: hidden; border-left-style: hidden; " data-section-id="0740a356f5e">
				
				
				<div class="dslc-modules-section-wrapper dslc-clearfix"><div class="dslc-modules-area dslc-col dslc-12-col dslc-last-col" data-size="12" data-valign="">
		<div id="dslc-module-5571" class="dslc-module-front dslc-module-DSLC_Separator dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular  dslc-in-viewport" data-module-id="5571" data-module="DSLC_Separator" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none" style="animation: 0.65s ease 0s 1 normal none running forwards;">

			
			
					<div class="dslc-separator-wrapper">
				<div class="dslc-separator dslc-separator-style-invisible">
									</div>
				<div></div>
			</div><!-- .dslc-separator-wrapper -->
			
				</div><!-- .dslc-module -->
		</div><div class="dslc-modules-area" data-size="8" data-valign="">
		<div id="dslc-module-1359" class="dslc-module-front container " data-module-id="1359" data-module="DSLC_Text_Simple" 
      data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" 
      data-dslc-anim-easing="ease" data-dslc-preset="none" 
      >

			
			
		
		<div class="">
		Wacht dan niet langer en vul ons contactformulier! Waar kunnen we je een plezier mee doen?
	</div>

		
				</div><!-- .dslc-module -->
		</div><div class="dslc-modules-area dslc-col dslc-4-col dslc-last-col" data-size="4" data-valign="">
		<div id="dslc-module-1360" class="dslc-module-front dslc-module-DSLC_Button dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular  dslc-in-viewport" data-module-id="1360" data-module="DSLC_Button" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none" style="animation: 0.65s ease 0s 1 normal none running forwards;">

			
			

						
				</div><!-- .dslc-module -->
		</div><div class="dslc-modules-area dslc-col dslc-12-col dslc-last-col" data-size="12" data-valign="">
		<div id="dslc-module-5577" class="dslc-module-front dslc-module-DSLC_Separator dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular  dslc-in-viewport" data-module-id="5577" data-module="DSLC_Separator" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none" style="animation: 0.65s ease 0s 1 normal none running forwards;">

			
			
					<div class="dslc-separator-wrapper">
				<div class="dslc-separator dslc-separator-style-invisible">
									</div>
				<div></div>
			</div><!-- .dslc-separator-wrapper -->
			
				</div><!-- .dslc-module -->
		</div></div></div>

               <section class="contact-section-s3">
                  <div class="container">
                     <div class="row">
					 
                        
                        <div class=" col-md-8 col-xs-12">
                          
                           <div class="contact-form">

<?php if( isset($_GET['success'])): ?>

   
   <div class="clearfix error-handling-messages alert alert-success">

   <h2><em>Dank u!</em></h2>
                                    <div id="success">Uw bericht is succesvol verzonden. Wij nemen spoedig contact met u op!</div>
</div>

<?php endif; ?>

<!-- www.123formbuilder.com script begins here --><iframe allowTransparency="true" style="min-height:1200px; height:inherit; overflow:auto;" 
width="100%" id="contactform123" name="contactform123" marginwidth="0" marginheight="0" frameborder="0" src="//www.123formbuilder.com/my-contact-form-5538778.html">
<p>Your browser does not support iframes. The contact form cannot be displayed. Please use another contact method (phone, fax etc)</p></iframe><!-- www.123formbuilder.com script ends here -->

<!-- <form action="<?php echo site_url('contactSubmit'); ?>" method="post" class="form-horizontal default-form">
    <input type="hidden" name="_after" value="<?php echo site_url('contact?success=true'); ?>">
                                 <div> <input type="text" class="form-control" name="name" id="name" placeholder="Name*"> </div>
                                 <div> <input type="email" class="form-control" name="email" id="email" placeholder="Email*"> </div>
                                 <div> <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone*"> </div>
                                 <div> <input type="text" class="form-control" name="business" id="business" placeholder="Business name*"> </div>
                                 <div class="fullwidth">
                                 <textarea class="form-control" name="message" placeholder="Type your message here..."></textarea>
                                 </div>
                                 <div class="submit-area">
                                    <button type="submit" class="default-btn btn-block">Submit</button> 
                                    <div id="loader"> <i class="ti-reload"></i> </div>
                                 </div>
                              </form> -->
                           </div>
                        </div>

						<div class="col col-md-4">


						<div class="contact-info">
                            <ul>
                                <li>
                                    <i class="fi dslc-icon dslc-icon-ext-map"></i>
                                    <h4>Hoofdkwartier</h4>
                                    <p>Utrecht Netherlands</p>
                                </li>
                                <li>
                                    <i class="fi dslc-icon dslc-icon-ext-mail"></i>
                                    <h4>E-mailadres</h4>
                                    <p><a href="mailto:<?php echo $this->site_info->email; ?>"><?php echo $this->site_info->email; ?></a></p>
                                  
                                </li>
                                <li>
                                    <i class="fi dslc-icon dslc-icon-ext-phone4"></i>
                                    <h4>Telefoon</h4>
                                    <p><a href="tel:<?php echo $this->site_info->phone; ?>"><?php echo $this->site_info->phone; ?></a></p>
                                </li>
                            </ul>
                        </div>


                        </div>


						
                     </div>
                  </div>
                  <!-- end container --> 
               </section>
            </div>
           
		   
         </div>
      </div>
      <!-- .entry-content --> 
   </article>
   <!-- #post-## --> 
</div>
<!-- #content --> 



<!-- Custom Footer Styles -->
<style type="text/css">

/*! CSS Used from: https://3i98kg2c0esw237td22u9yvf-wpengine.netdna-ssl.com/wp-content/plugins/live-composer-page-builder/css/dist/frontend.min.css?ver=1.5.5 ; media=all */
@media all{
.dslc-modules-section{border:0px solid transparent;position:relative;}
.dslc-modules-section-wrapper{position:relative;max-width:100%;margin:0 auto;}
.dslc-module-front{position:relative;}
.dslc-col{display:block;float:left;margin-right:2.12766%;min-height:1px;}
.dslc-4-col{width:31.91489%;}
.dslc-8-col{width:65.95744%;}
.dslc-12-col{width:100%;}
.dslc-last-col{margin-right:0;}
.dslc-first-col{clear:both;}
.dslc-no-columns-spacing .dslc-col{margin:0;}
.dslc-no-columns-spacing .dslc-4-col{width:33.33333%;}
.dslc-no-columns-spacing .dslc-8-col{width:66.66666%;}
.dslc-no-columns-spacing .dslc-12-col{width:100%;}
.dslc-clearfix:before,.dslc-clearfix:after{content:" ";display:table;}
.dslc-clearfix:after{clear:both;}
.dslc-clearfix{*zoom:1;}
@media only screen and (max-width: 767px){
.dslc-col,.dslc-no-columns-spacing .dslc-col{width:100%;margin:0;margin-bottom:30px;min-width:0px;}
.dslc-module-front.dslc-col{margin-bottom:0;}
}
@media all and (-ms-high-contrast: none){
.dslc-in-viewport-check{transform:none!important;opacity:1!important;}
}
.dslc-module-front{font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;}
.dslc-separator-wrapper{padding-bottom:1px;}
.dslc-separator{clear:both;height:1px;padding-bottom:25px;margin-bottom:25px;width:100%;}
.dslc-module-front.dslc-module-DSLC_Separator{min-height:auto;}
.dslc-text-module-content:before{content:"";top:0;left:0;bottom:0;right:0;position:absolute;z-index:-1;}
}
/*! CSS Used from: https://3i98kg2c0esw237td22u9yvf-wpengine.netdna-ssl.com/wp-content/themes/seowp/style.css?ver=1565899147 ; media=all */
@media all{
h1{font-size:2em;margin:0.67em 0;}
*,*:before,*:after{box-sizing:border-box;}
div,h1{margin:0;padding:0;}
@media only screen and (min-width: 1024px) and (max-width: 1280px){
.dslc-modules-section-wrapper{width:940px!important;}
}
@media only screen and (min-width: 768px) and (max-width: 1023px){
.dslc-modules-section-wrapper{width:688px!important;}
}
@media only screen and (min-width: 480px) and (max-width: 767px){
.dslc-modules-section-wrapper{width:420px!important;}
}
@media only screen and (max-width: 479px){
.dslc-modules-section-wrapper{width:280px!important;}
}
.dslc-modules-section-wrapper{width:1200px;}
}
/*! CSS Used from: Embedded */
body .dslc-module-front{font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;line-height:27px;font-weight:300;color:rgb(65, 72, 77);}
h1{font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-size:42px;line-height:48px;font-weight:200;margin-bottom:25px;color:rgb(70, 72, 75);}
/*! CSS Used from: Embedded */
.dslc-modules-section:not(.dslc-full){padding-left:4%;padding-right:4%;}
.dslc-modules-section{margin-left:0%;margin-right:0%;margin-bottom:0px;padding-bottom:80px;padding-top:80px;padding-left:0%;padding-right:0%;background-image:none;background-repeat:repeat;background-position:left top;background-attachment:scroll;background-size:auto;border-width:0px;border-style:solid;}
#dslc-module-6906 .dslc-separator{margin-bottom:30px;padding-bottom:30px;border-color:#ededed;border-width:1px;}
#dslc-module-6906 .dslc-separator-wrapper{background-repeat:repeat;background-attachment:scroll;background-position:top left;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-left-radius:0px;border-bottom-right-radius:0px;}
@media only screen and (min-width : 768px) and (max-width : 1024px){
#dslc-module-6906 .dslc-separator{margin-bottom:34px;padding-bottom:34px;}
}
@media only screen and ( max-width: 767px ){
#dslc-module-6906 .dslc-separator{margin-bottom:22px;padding-bottom:22px;}
}
#dslc-module-1357 .dslc-text-module-content{min-height:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;padding-top:0px;padding-bottom:0px;padding-left:16px;padding-right:16px;background-repeat:repeat;background-attachment:scroll;background-position:top left;background-size:auto;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-left-radius:0px;border-bottom-right-radius:0px;}
#dslc-module-1357 .dslc-text-module-content{font-size:20px;font-weight:300;font-style:normal;line-height:30px;letter-spacing:0px;text-align:right;}
#dslc-module-1357 .dslc-text-module-content h1{font-size:66px;font-weight:200;font-style:normal;line-height:70px;letter-spacing:0px;margin-bottom:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;text-align:left;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-left-radius:0px;border-bottom-right-radius:0px;}
@media only screen and (min-width : 768px) and (max-width : 1024px){
#dslc-module-1357 .dslc-text-module-content{margin-bottom:0px;padding-top:0px;padding-bottom:0px;padding-left:20px;padding-right:20px;}
#dslc-module-1357 .dslc-text-module-content{font-size:16px;line-height:26px;text-align:left;}
#dslc-module-1357 .dslc-text-module-content h1{font-size:55px;line-height:0px;margin-bottom:15px;text-align:left;}
}
@media only screen and ( max-width: 767px ){
#dslc-module-1357 .dslc-text-module-content{margin-bottom:0px;padding-top:0px;padding-bottom:0px;padding-left:52px;padding-right:52px;}
#dslc-module-1357 .dslc-text-module-content{font-size:20px;line-height:21px;text-align:left;}
#dslc-module-1357 .dslc-text-module-content h1{font-size:33px;line-height:40px;margin-bottom:0px;text-align:left;}
}
#dslc-module-1358 .dslc-text-module-content{min-height:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;padding-top:18px;padding-bottom:18px;padding-left:0px;padding-right:0px;background-repeat:repeat;background-attachment:scroll;background-position:top left;background-size:auto;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-left-radius:0px;border-bottom-right-radius:0px;}
#dslc-module-1358 .dslc-text-module-content{color:rgb(115, 126, 137);font-size:20px;font-weight:300;font-style:normal;line-height:30px;letter-spacing:0px;text-align:right;}
@media only screen and (min-width : 768px) and (max-width : 1024px){
#dslc-module-1358 .dslc-text-module-content{margin-bottom:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;}
#dslc-module-1358 .dslc-text-module-content{font-size:16px;line-height:26px;text-align:left;}
}
@media only screen and ( max-width: 767px ){
#dslc-module-1358 .dslc-text-module-content{margin-bottom:0px;padding-top:9px;padding-bottom:9px;padding-left:62px;padding-right:62px;}
#dslc-module-1358 .dslc-text-module-content{font-size:14px;line-height:21px;text-align:left;}
}
#dslc-module-33 .dslc-separator{margin-bottom:30px;padding-bottom:30px;border-color:#ededed;border-width:1px;}
#dslc-module-33 .dslc-separator-wrapper{background-repeat:repeat;background-attachment:scroll;background-position:top left;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-left-radius:0px;border-bottom-right-radius:0px;}
@media only screen and (min-width : 768px) and (max-width : 1024px){
#dslc-module-33 .dslc-separator{margin-bottom:34px;padding-bottom:34px;}
}
@media only screen and ( max-width: 767px ){
#dslc-module-33 .dslc-separator{margin-bottom:22px;padding-bottom:22px;}
}



 </style>