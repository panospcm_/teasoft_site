<section class="oppi_restaurent_action_area padding">
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div class="oppi_restaurent_action_img">
                <img class="wow fadeInLeft rounded-2" data-wow-delay="0.6s" src="<?php echo base_url(); ?>assets/images/background/supermarket-tea-software-pos-software-image.jpg" 
                alt="">
                
            </div>
        </div>
        <div class="col-lg-5 offset-lg-1 d-flex align-items-center">
            <div class="oppi_restaurent_action_content">
                <h2 class="wow fadeInUp" data-wow-delay="0.2s">Supermarkt Module</h2>
                <p class="wow fadeInUp" data-wow-delay="0.4s">

                TEA Software is zodanig ontwikkeld dat met minimale tijd alles in een goedwerking laat gaan, dat betekend dat  
                  maximaal kunt bezighouden met klanten, advies en verkoop
</p>

    
            </div>
        </div>
    </div>
</div>
</section>



<section class="pos-section padding">
            <div class="container">
                <div class="row flex-row-reverse align-items-center">
                    <div class="col-lg-6 col-12">
                        <div class="pos-thmub wow fadeInRight" data-wow-delay="0.2s">
                            <img src="<?php echo base_url(); ?>assets/images/background/teasoftware_retail_supermarket_software_infographic1.jpg" class=" rounded-2" 
                            alt="tea software pos supermarkt software">
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="pos-content-area">
                            <div class="section-header style-3">
                                <h2  class="wow fadeInUp" data-wow-delay="0.4s">Supermarkt Module</h2>

                            </div>
                            <div class="section-wrapper">
                                <ul>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Assortimentsbeheer</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Voorraadbeheer</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Relatiebeheer - Loyaliteit en sparen</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Verkooporders - Facturatie</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Uitgebreide overzichten </li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Meerdere kassa's koppelbaar</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Inkoopbeheer</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Gemengde en gesplitste betalingen</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>		


		
<!-- Start Main Banner Area -->

<div class="container wow fadeInUp" data-wow-delay="0.1s">
<section class="parallax overlay-gradient padding m-t-30 m-b-30 " data-image-src="../assets/images/photos/parallax/3.jpg">
<div class="content">
<div class=" text-center">
<div class="">
<div class="header-badge-white m-b-15">Wij komen met plezier naar u toe om een persoonlijk advies te geven voor uw Supermarkt</div>

<div class=" text-center  m-t-20" data-size="6" data-valign="">
    <div id="dslc-module-2194" class="dslc-module-handle-like-regular " data-module-id="2194" data-module="DSLC_Button" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

        <div class="text-center">
            <a href="<?php echo site_url('contact'); ?>" target="_self" onClick="" class=" food_btn">
                <span class="dslc-icon-ext-envelope"></span>
                <span>Neem dan contact met ons op</span>
            </a>
        </div>
        <!-- .dslc-button -->

    </div>
    <!-- .dslc-module -->
</div>

</div>
</div>
</div>
</section>
</div>
		<!-- <section class="pos-section bgc-3 padding">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-12">
                        <div class="pos-thmub">
                            <img src="http://demos.codexcoder.com/labartisan/html/smartsass/assets/images/pos/02.png" alt="pos-=thumb">
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="pos-content-area">
                            <div class="section-header style-3">
                                <h2>All-in-one Retail Platform</h2>
                                <p>Sell in-store, at retail events, pop-up stores and even online. With all sales channels in one POS software Hike manages all aspects of your retail business.</p>
                            </div>
                            <div class="section-wrapper">
                                <ul>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Sell in-store</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Loyalty</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Sell on-the-go</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Inventory counts</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Sell online integrated eCommerce</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Customer profiles</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Sell at Amazon marketplace</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Multi-store</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>And Mach More...</li>
                                    <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Reporting</li>
                                </ul>
<a href="#" class="default-btn">View Details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
 -->




 <section class="services-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="service-grids">

                        <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-wallet2"></i>
                                <h3>Loyaliteit en sparen</h3>
                                <p>Spaarpunten instelbaar, Opmerkingen bij klanten plaatsen</p>
                            </div>


                            
                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-mobile"></i>
                                <h3>Assortimentsbeheer</h3>
                                <p>Meerdere leveranciers per artikel koppelbaar </p>
                            </div>

                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-lock"></i>
                                <h3>Meerdere kassa’s koppelbaar</h3>
                                <p>Meerdere kassasystemen en voorraadlocaties in een filiaal </p>
                            </div>

                        </div>
                        
                        <div class="clearfix">
</div>
                        <div class="service-grids">
                            <div class="grid">
                                <i class="fi dslc-icon dslc-icon-ext-chat"></i>
                                <h3>Verkoop – facturatie</h3>
                                <p>Kassasoftware, zelf in te stellen autorisatieniveaus, facturen of kassabonnen printen, 
                                kassaknoppen zelf configureren, meerdere kassa's koppelbaar  </p>
                            </div>

                            
                            <div class="grid">
                                <i class="fi dslc-icon dslc-icon-ext-software_layout_header_3columns"></i>
                                <h3>Voorraadbeheer </h3>
                                <p>Artikelrapport met o.a. verkoop percentage, totale verkoop, marge, inkoopwaarde en verkoopwaarde Uitgebreid artikelbeheer,
                                 indeling artikelen in hoofdgroep, subgroep. </p>
                            </div>
                            
                   

                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-ecommerce_creditcard"></i>
                                <h3>Inkoopbeheer</h3>
                                <p>Bestellen met min en max voorraadwaarde, meerdere leveranciers per product, bestellen met mobile terminal.
                                Bestellen met barcodescanner
</p>
                            </div>

                        </div>
                        
                        <div class="clearfix">
</div>
                        <div class="service-grids">
                            <div class="grid">
                                <i class="fi dslc-icon dslc-icon-ext-cloud3"></i>
                                <h3>Relatiebeheer</h3>
                                <p>Vastleggen klanten, leveranciers en fabrikanten, vastleggen gegevens, Verkopen op klantniveau</p>
                            </div>
             
                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-basic_link"></i>
                                <h3>Uitgebreide overzichten</h3>
                                <p>Dashboard met overzichtelijke rapporten
Alle rapporten  te filteren op meerdere criteria, genereren grafieken, klantgedrag, verkooprapportage
 </p>
                            </div>

                            
                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-screen"></i>
                                <h3>Realtime Data Voorraad – Verkoop </h3>
                                <p>Omnichannel platform, nieuwe technologie, geen gedoe met bestanden inlezen</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>