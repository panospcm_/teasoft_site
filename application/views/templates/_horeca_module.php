<!-- oppi_restaurent_action_area -->
<section class="oppi_restaurent_action_area padding">
   <div class="container-fluid">
      <div class="col-xl-8 offset-xl-2 col-md-10 offset-md-1">
         <div class="row">
            <div class="col-lg-8">
               <div class="oppi_restaurent_action_img">
                  <div class="row">
                     <div class="col-md-12">
                        <img class="wow fadeInLeft rounded-2" data-wow-delay="0.6s" src="<?php echo base_url(); ?>assets/images/background/1.png" 
                           alt="tablet-pos-horeca-main-left-image-software-module">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 offset-lg-1 d-flex align-items-center">
               <div class="oppi_restaurent_action_content">
                  <h2 class="wow fadeInUp" data-wow-delay="0.2s">Horeca Module</h2>
                  <p class="wow fadeInUp" data-wow-delay="0.4s">
                     Module met tafel beheer gebaseerd op uw eigen tafel en ruimte situatie. <br><br>
                     Mobile Order app, bestellingen kunnen aan tafel opgenomen worden met een smartphone of tablet en worden automatisch opgeslagen en  gesplitst geprint bij de bar en keuken.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="pos-section padding">
   <div class="container">
      <div class="row flex-row-reverse align-items-center">
         <div class="col-lg-6 col-12">
            <div class="pos-thmub">
               <div class="row">
                  <div class="col-md-12">
                     <img class="wow fadeInLeft rounded-2" data-wow-delay="0.6s" src="<?php echo base_url(); ?>assets/images/background/6b8e487d-b480-42a7-a512-2e2c665ec493.jfif" 
                        alt="tea retail pos software ">
                  </div>
                  <!-- <div class="col-md-4 col-5">
                     <img class="wow fadeInLeft" data-wow-delay="0.3s" src="<?php echo base_url(); ?>assets/images/background/smartmockups_k81qqwxr.png"  
                     style="max-height:350px;" alt="">
                     
                     </div> -->
               </div>
               <!-- <img src="<?php echo base_url(); ?>assets/images/background/tea_retail_pos_software_mockup_smartphone2.png" alt="pos-=thumb"> -->
            </div>
         </div>
         <div class="col-lg-6 col-12">
            <div class="pos-content-area">
               <div class="section-header style-3">
                  <h2>Features</h2>
                  <!-- <h4>Restaurant / Fast-Food / Snack-Bar</h4>
                     <p>
                     Module met tafel beheer gebaseerd op uw eigen tafel en ruimte situatie.
                     Mobile Order app , bestellingen kunnen aan tafel opgenomen worden met een smartphone of tablet en worden automatisch opgeslagen en  gesplitst geprint bij de bar en keuken.
                     </p> -->
               </div>
               <div class="section-wrapper">
                  <ul>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Module  met tafel beheer</li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Mobile Order app </li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>In- en uitloggen met personeelspas</li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Totale communicatie met de keuken </li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Touch knoppen </li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Gemengde en gesplitste betalingen</li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Direct Real time voorraad</li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Speciale module voor het samenstellen van recepten</li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Module thuisbezorgd</li>
                     <li><i class="dslc-icon-ext-arrows_circle_check"></i>Diverse koppelingen met derde partijen</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Start Main Banner Area -->
<div class="container">
   <section class="parallax overlay-gradient padding m-t-30 m-b-30 ">
      <div class="content">
         <div class=" text-center">
            <div class="">
               <div class="header-badge-white m-b-15">Wij komen met plezier naar u toe om een persoonlijk advies te geven voor uw horeca bedrijf</div>
               <div class=" text-center  m-t-20" data-size="6" data-valign="">
                  <div id="dslc-module-2194" class="dslc-module-handle-like-regular " data-module-id="2194" data-module="DSLC_Button" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">
                     <div class="text-center">
                        <a href="<?php echo site_url('contact'); ?>" target="_self" onClick="" class=" food_btn">
                        <span class="dslc-icon-ext-envelope"></span>
                        <span>Neem dan contact met ons op</span>
                        </a>
                     </div>
                     <!-- .dslc-button -->
                  </div>
                  <!-- .dslc-module -->
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<!-- oppi_restaurent_action_area -->
<section class="oppi_restaurent_action_area padding">
   <div class="container-fluid">
      <div class="col-md-8 offset-md-2">
         <div class="row">
            <div class="col-lg-6">
               <div class="oppi_restaurent_action_img">
                  <div class="row">
                     <div class="col-md-12">
                        <img class="wow fadeInLeft rounded-2" data-wow-delay="0.6s" 
                           src="<?php echo base_url(); ?>assets/images/background/teasoftware_retail_software_infographic1.jpg" 
                           alt="tea software retail software pos horeca module">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-5 offset-lg-1 d-flex align-items-center">
               <div class="oppi_restaurent_action_content">
                  <h3 class="wow fadeInUp" data-wow-delay="0.2s">Zowel qua prijs als qua software 
                     <br>enorm toegankelijk
                  </h3>
                  <p class="wow fadeInUp" data-wow-delay="0.4s">
                  Met een snel en efficiënt kassasysteem houd je grip op de zaak. 
                  Dankzij de overzichtelijke indeling van het scherm is het afrekenen heel makkelijk..
                     <br>
                     <br>
                     Eigen programmeurs,  eigen softwarearchitecten en designers.
                     Non-stop innovatie!
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- 
   <section class="pos-section padding">
       <div class="container">
           <div class="row flex-row-reverse align-items-center">
               <div class="col-lg-6 col-12">
                   <div class="pos-thmub">
                       <img src="<?php echo base_url(); ?>assets/images/background/teasoftware_retail_software_infographic1.jpg" alt="pos-=thumb">
                   </div>
               </div>
               <div class="col-lg-6 col-12">
                   <div class="pos-content-area">
                       <div class="section-header style-3">
                           <h2>Horeca Module</h2>
   
                       </div>
                       <div class="section-wrapper">
                           <ul>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>100% Online POS </li>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>Live BackOffice </li>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>Omni channel Technologie</li>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>100% Offline </li>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>Overzichten, Analyses</li>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>Realtime voorraad bekijken</li>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>Instore kiosk </li>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>Gedetailleerde rapporten</li>
                               <li><i class="dslc-icon-ext-arrows_circle_check"></i>Module thuisbezorgd</li>
                           </ul>
   <a href="#" class="default-btn">Neem dan contact met ons op</a>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>	 -->
<!-- <section class="pos-section bgc-3 padding">
   <div class="container">
       <div class="row align-items-center">
           <div class="col-lg-6 col-12">
               <div class="pos-thmub">
                   <img src="http://demos.codexcoder.com/labartisan/html/smartsass/assets/images/pos/02.png" alt="pos-=thumb">
               </div>
           </div>
           <div class="col-lg-6 col-12">
               <div class="pos-content-area">
                   <div class="section-header style-3">
                       <h2>All-in-one Retail Platform</h2>
                       <p>Sell in-store, at retail events, pop-up stores and even online. With all sales channels in one POS software Hike manages all aspects of your retail business.</p>
                   </div>
                   <div class="section-wrapper">
                       <ul>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Sell in-store</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Loyalty</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Sell on-the-go</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Inventory counts</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Sell online integrated eCommerce</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Customer profiles</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Sell at Amazon marketplace</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Multi-store</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>And Mach More...</li>
                           <li><i class="dslc-icon-ext-arrows_circle_check"></i>Reporting</li>
                       </ul>
   <a href="#" class="default-btn">View Details</a>
                   </div>
               </div>
           </div>
       </div>
   </div>
   </section>
   -->
<section class="services-section section-padding">
   <div class="container">
      <div class="row">
         <div class="col col-xs-12">
            <div class="service-grids">
               <div class="grid">
                  <i class="fi dslc-icon dslc-icon-ext-megaphone"></i>
                  <h3>Module met tafel beheer</h3>
                  <p>Module  met tafel beheer gebaseerd op uw eigen tafel en ruimte situatie </p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-mobile"></i>
                  <h3>Mobile Order app</h3>
                  <p>Bestellingen kunnen aan tafel opgenomen worden met een smartphone  
                     of tablet en worden zo automatisch opgeslagen en gesplitst geprint bij de bar en keuken.
                  </p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-lock"></i>
                  <h3>In- en uitloggen met personeelspas</h3>
                  <p>In- en uitloggen met personeelspas, pincode of RFID tag, antipersoneel fraude logging </p>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="service-grids">
               <div class="grid">
                  <i class="fi dslc-icon dslc-icon-ext-chat"></i>
                  <h3>Totale communicatie</h3>
                  <p>Totale communicatie met de keuken, opmerkingen, klant wensen  </p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-screen"></i>
                  <h3>Touch knoppen</h3>
                  <p>Touch knoppen kassa’s instelbaar qua gemak en positie</p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-ecommerce_creditcard"></i>
                  <h3>Betalingen</h3>
                  <p>Gemengde en gesplitste betalingen, pin, contant, op rekening, credit card</p>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="service-grids">
               <div class="grid">
                  <i class="fi dslc-icon dslc-icon-ext-cloud4"></i>
                  <h3>Module met tafel beheer</h3>
                  <p>Direct Real time voorraad, no need for backoffice</p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-wallet2"></i>
                  <h3>Samenstellen van recepten</h3>
                  <p>Speciale module voor het samenstellen van recepten, inclusief  bijwerken van de voorraad</p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-basic_link"></i>
                  <h3>Link Building</h3>
                  <p>Diverse koppelingen met derde partijen </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- end container -->
</section>