<section class="oppi_restaurent_action_area padding">
   <div class="container">
      <div class="row">
         <div class="col-lg-6">
            <div class="oppi_restaurent_action_img">
               <img class="wow fadeInLeft rounded-2" data-wow-delay="0.6s" src="<?php echo base_url(); ?>assets/images/background/shop.png" 
                  alt="">
            </div>
         </div>
         <div class="col-lg-5 offset-lg-1 d-flex align-items-center">
            <div class="oppi_restaurent_action_content">
               <h2 class="wow fadeInUp" data-wow-delay="0.2s">Winkel Module</h2>
               <p class="wow fadeInUp" data-wow-delay="0.4s">
               TEA Software is zodanig ontwikkeld dat met minimale tijd u het maximum resultaat kunt halen uit het beheer van uw bedrijf. Uw volledige aandacht is gericht op uw klanten, advies aan uw klanten en verkoop.
               </p>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="pos-section padding">
   <div class="container">
      <div class="row flex-row-reverse align-items-center">
         <div class="col-lg-6 col-12">
            <div class="pos-thmub">
               <img src="<?php echo base_url(); ?>assets/images/background/teasoftware_retail_shop_module_pos_software_infographic1.jpg" 
               alt="tea software retail pos shop module">
            </div>
         </div>
         <div class="col-lg-6 col-12">
            <div class="pos-content-area">
               <div class="section-header style-3">
                  <h2>Winkel Module</h2>
               </div>
               <div class="section-wrapper">
                  <ul>
                     <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Intuïtieve menustructuren </li>
                     <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Superslim, razendsnelle kassa software</li>
                     <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Aanpassingen eenvoudig en snel geregeld </li>
                     <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Eenvoudig artikelbeheer </li>
                     <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Kwaliteit en snelheid</li>
                     <li  class="wow fadeInUp" data-wow-delay="0.1s"><i class="dslc-icon-ext-arrows_circle_check"></i>Modulair, dus jij bepaalt welke functionaliteiten</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Start Main Banner Area -->
<div class="container wow fadeInUp" data-wow-delay="0.1s">
   <section class="parallax overlay-gradient padding m-t-30 m-b-30 " data-image-src="../assets/images/photos/parallax/3.jpg">
      <div class="content">
         <div class=" text-center">
            <div class="">
               <div class="header-badge-white m-b-15">Wij komen met plezier naar u toe om een persoonlijk advies te geven voor uw Winkel</div>
               <div class=" text-center  m-t-20" data-size="6" data-valign="">
                  <div id="dslc-module-2194" class="dslc-module-handle-like-regular " data-module-id="2194" data-module="DSLC_Button" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">
                     <div class="text-center">
                        <a href="<?php echo site_url('contact'); ?>" target="_self" onClick="" class=" food_btn">
                        <span class="dslc-icon-ext-envelope"></span>
                        <span>Neem dan contact met ons op</span>
                        </a>
                     </div>
                     <!-- .dslc-button -->
                  </div>
                  <!-- .dslc-module -->
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<section class="services-section section-padding">
   <div class="container">
      <div class="row">
         <div class="col col-xs-12">
            <div class="service-grids">
               <div class="grid">
                  <i class="fi dslc-icon dslc-icon-ext-megaphone"></i>
                  <h3>Oplossingen meerdere filialen</h3>
                  <p>Oplossingen meerdere filialen dankzij de Software Architectuur  en  Cloud Computing  </p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-phone"></i>
                  <h3>100% Online POS</h3>
                  <p>100% Online POS , alles en overal wordt in realtime bewerkt </p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-statistics"></i>
                  <h3>Live BackOffice</h3>
                  <p>Live BackOffice , altijd en overal totale controle dankzij internet, ook thuis en op vakantie</p>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="service-grids">
               <div class="grid">
                  <i class="fi dslc-icon dslc-icon-ext-chat"></i>
                  <h3>Omni channel Technologie</h3>
                  <p>Omni channel Technologie, live afrekenen, retouren, kortingen, spaarpunten, voorraden</p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-screen"></i>
                  <h3>100% Offline</h3>
                  <p>Kassa kan ten alle tijde 100% Offline werken, alles wordt bijgewerkt zodra internet hersteld is</p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-ecommerce_creditcard"></i>
                  <h3>Intuïtieve BackOffice</h3>
                  <p>Intuïtieve BackOffice, makkelijk in gebruik, Besteladviezen, Overzichten, Analyses</p>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="service-grids">
               <div class="grid">
                  <i class="fi dslc-icon dslc-icon-ext-cloud4"></i>
                  <h3>Realtime</h3>
                  <p>Realtime voorraad bekijken, ook van alle andere winkels</p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-wallet2"></i>
                  <h3>Instore kiosk</h3>
                  <p>Instore kiosk communiceren met  Mobileapp in Realtime</p>
               </div>
               <div class="grid">
                  <i class="dslc-icon dslc-icon-ext-basic_link"></i>
                  <h3>Gedetailleerde rapporten</h3>
                  <p>Gedetailleerde rapporten, (X, Z) producten, gebruikers, magazijnen etc</p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- end container -->
</section>