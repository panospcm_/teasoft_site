<?php 

$this->load->model('MyModel');
// $brand_gallery = $brand_gallery[0];
// $data_brands = $this->MyModel->getBrandsGallery(NULL,0);
// $categories = $this->MyModel->getWorksCategories();
 // $brand_gallery_ = count($brand_gallery)>0?$brand_gallery[0]:NULL;
 
$data_gallery = $this->MyModel->getMainGallery(NULL,0);
// var_dump($brand_gallery[0]);
?>



<section class="header header-bg-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="header-content">
                                <div class="header-content-inner">
                                    <h1>Our Gallery</h1>
                                    <p>View photos from our gastronomy tours in Cyprus </p>
                                    <div class="ui breadcrumb">
                                        <a href="<?php echo site_url(); ?>" class="section">Home</a>
                                        <div class="divider"> / </div>
                                        <div class="active section">Gallery</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

 

<section class="gray">

    <div class="container">
        <div class="row">
            <div class="gallery padding_60 mfp-gallery" id="trip-gallery-2">

                  <?php foreach($data_gallery as $iterImage): ?>



                <div class="col-sm-3">
                    <a href="<?php echo base_url(); ?>assets/images/gallery/<?php echo $iterImage->main_gallery_url; ?>" class="gallery-item mfp-link" data-lightbox="trip-detail-gallery" data-title="Lorem ipsum dolor.">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/thumb__<?php echo $iterImage->main_gallery_url; ?>"   alt="<?php echo base_url(); ?>assets/images/gallery/<?php echo $iterImage->main_gallery_url; ?>" class="img-responsive" >
                        <div class="hover-overlay">
                            <span class="fa fa-search"></span>
                        </div>
                    </a>
                </div>

      
      
      
      <?php endforeach; ?>


            </div>
        </div>
    </div>
</section>
