
        <!-- .page-title start -->
        <div class="page-title-style01 page-title-negative-top pt-bkg01">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Skylink - News</h1>

                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-title-style01.page-title-negative-top end -->

  <div class="page-content">
            <div class="container">



<div class="main-container wishlist list-blogs">
    <div class="container">
        <h3 class="page-title">Our Blog</h3>
        <div class="blog-grid butique-masonry">
            <div class="masonry-grid" data-layoutmode="masonry" data-cols="3">
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="images/blogs/17-Blog1.png"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Nulla laoreet ipsum dignissim magna maximus, vitae euis mod turpis iaculis. Sed pharetra lacus sit amet dui conse quat dignissim bibendum ullamcorper sem.</p> 
                        </div>
                        <a class="readmore" href="#" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="images/blogs/17-Blog2.png"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Sed a sapien in tellus fringilla vestibulum. Sed elementum nisl eget turpis pharetra, vel posuere felis volutpat. Sed elementum enim nulla, ac molestie orci sollicitudin ac. Cras vitae purus lacus. Pellentesque vel urna id nibh vehicula sagittis eget in turpis.</p> 
                        </div>
                        <a class="readmore" href="#" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a  class="banner-opacity"  href="#"><img alt="17_blog" src="images/blogs/17-Blog3.png"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Nulla laoreet ipsum dignissim magna maximus, vitae euis mod turpis iaculis. Sed pharetra lacus sit amet dui conse quat dignissim bibendum ullamcorper sem.</p> 
                        </div>
                        <a class="readmore" href="#" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="images/blogs/17-Blog4.png"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Nulla laoreet ipsum dignissim magna maximus, vitae euis mod turpis iaculis. Sed pharetra lacus sit amet dui conse quat dignissim bibendum ullamcorper sem.</p> 
                        </div>
                        <a class="readmore" href="#" >Readmore</a>
                    </div>                                                            
                </div>
                


            </div>
        </div>



                <div class="row">
                    <ul class="col-md-8 col-md-offset-2 blog-posts post-list">


        <?php 

$this->load->model('MyModel');
$blogs = $this->MyModel->get_blogs(NULL,0);
// var_dump($blogs);
        foreach ($blogs as $key => $iterBlog): ?>
     


                        <li class="blog-post clearfix">
                            <div class="post-media">
                                <a href="news-single.html" class="post-img">
                                    <img src="<?php echo blog_main_img($iterBlog->blog_image); ?>" alt="Trucking Transportation and Logistics HTML template"/>
                                </a>
                            </div><!-- .post-media end -->

                            <div class="post-date">
                                <p class="day"><?php echo date('d', $iterBlog->blog_created_at); ?></p>
                                <p class="month"><?php echo date('M', $iterBlog->blog_created_at); ?></p>
                            </div><!-- .post-date end -->

                            <div class="post-body">
                                <a href="<?php echo blog_link($iterBlog->blog_id,$iterBlog->blog_title) ; ?>">
                                    <h3><?php echo $iterBlog->blog_title; ?></h3>
                                </a>

                                <p>
                              <?php echo character_limiter(strip_tags($iterBlog->blog_description), 110); ?>
                                </p>

                                <a href="<?php echo blog_link($iterBlog->blog_id,$iterBlog->blog_title) ; ?>" class="read-more">
                                    <span>
                                        Read more
                                        <i class="fa fa-chevron-right"></i>
                                    </span>                              
                                </a>
                            </div><!-- .post-body end -->
                        </li><!-- .blog-post end -->

<?php endforeach; ?>

                    </ul><!-- .col-md-9.blog-posts.post-list end -->

                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->





