<!DOCTYPE html>
<html lang="<?php echo $this->lang->lang(); ?>" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from flat.seowptheme.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 03 Feb 2020 21:42:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="xmlrpc.php" />
   <title><?php echo $content->meta_content_meta_title; ?> | <?php echo $this->site_info->site_name; ?></title>
<?php
$meta_content_meta_title = isset($content->meta_content_meta_title)?$content->meta_content_meta_title:$this->site_info->site_name;
$meta_content_meta_description = isset($content->meta_content_meta_description)?$content->meta_content_meta_description:$this->site_info->site_description;
$page_meta_keywords = isset($content->page_meta_keywords)?$content->page_meta_keywords:$this->site_info->site_name;
$page_meta_image = isset($content->page_meta_image)?$content->page_meta_image:logo_main_img();
?>
<meta name="keywords" content="<?php echo isset($content->meta_content_meta_keywords)?$content->meta_content_meta_keywords:$this->site_info->site_name; ?>" />
<meta name="description" content="<?php echo $meta_content_meta_description; ?>" />

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/images/favicc/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/images/favicc/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/images/favicc/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/favicc/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicc/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/images/favicc/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/images/favicc/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $this->site_info->site_name; ?>" />
<meta property="og:description" content="<?php echo $meta_content_meta_description; ?>" />
<meta property="og:url" content="<?php echo site_url(); ?>" />
<meta property="og:site_name" content="<?php echo $this->site_info->site_name; ?>" />
<link rel='dns-prefetch' href='https://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />


<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>

<meta name="facebook-domain-verification" content="fqt9piwhmw3ep16qkekod3028xn9jz" />

<link rel="alternate" type="application/rss+xml" title="<?php echo $this->site_info->site_name; ?> &raquo; Feed" href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="<?php echo $this->site_info->site_name; ?> &raquo; Comments Feed" href="comments/feed/index.html" />
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.9.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<!-- Note: MonsterInsights is not currently configured on this site. The site owner needs to authenticate with Google Analytics in the MonsterInsights settings panel. -->
<!-- No UA code set -->
<!-- / Google Analytics by MonsterInsights -->
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/flat.seowptheme.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.2"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>

<link href="<?php echo base_url(); ?>assets/css/font-awesome/css/all.min.css" rel="stylesheet" type="text/css">

<link rel='stylesheet' id='dslc-ext-css'  href='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/themes/seowp/iconfont/styled5f7.css?ver=2.0' type='text/css' media='all' />
<link rel='stylesheet' id='dslc-plugins-css-css'  href='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/plugins/live-composer-page-builder/css/dist/frontend.plugins.mine225.css?ver=1.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='dslc-frontend-css-css'  href='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/plugins/live-composer-page-builder/css/dist/frontend.mine225.css?ver=1.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='lcmenupro-css-css'  href='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/plugins/lc-extensions/extensions/menu/css/main9dff.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='ms-custom-css'  href='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/uploads/sites/2/masterslider/custom4e44.css?ver=1.3' type='text/css' media='all' />
<link rel='stylesheet' id='lbmn-style-css'  href='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/themes/seowp/style6a56.css?ver=1565899155' type='text/css' media='all' />

<link rel='stylesheet' id='bootstrap-css'  href='<?php echo base_url(); ?>assets/css/bootstrap.min.css' type='text/css' media='all' />

<link rel='stylesheet' id='style-css'  href='<?php echo base_url(); ?>assets/css/style.css' type='text/css' media='all' />

<link rel='stylesheet' id='owl-css33'  href='<?php echo base_url(); ?>assets/css/swiper.min.css' type='text/css' media='all' />


<link rel='stylesheet' id='magnificpopup-css33'  href='<?php echo base_url(); ?>assets/css/magnificpopup.css' type='text/css' media='all' />


<style id='lbmn-style-inline-css' type='text/css'>

			.pseudo-preloader .global-container { z-index: 100; position: relative; }
			.pseudo-preloader .global-wrapper:before {
				position: absolute; content: ""; left: 0; top: 0; width: 100%; height: 100%;
				position: fixed; height: 100vh;
				-webkit-transition: all 0.3s;
				-webkit-transition-delay: 0.2s;
				-moz-transition: all 0.3s 0.2s;
				-o-transition: all 0.3s 0.2s;
				transition: all 0.3s 0.2s;
				z-index: 999999; background: #fff; }

			.pseudo-preloader .global-wrapper:after {
				width: 80px;
				height: 80px;
				content: "";		background: transparent url("<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/themes/seowp/images/preloader.gif") no-repeat;		background-size: 80px 80px;
				position: fixed; display: block; left: 50%; top: 50vh; margin-left: -40px; z-index: 1000000;

				-webkit-transition: all 0.4s;
				-webkit-transition-delay: 0.4s;

				-moz-transition: all 0.4s 0.4s;
				-o-transition: all 0.4s 0.4s;
				transition: all 0.4s 0.4s;
			}

			html.content-loaded .global-wrapper:before,
			html.content-loaded .global-wrapper:after {
				opacity: 0; z-index: -1; color: rgba(0, 0, 0, 0);
				-webkit-transition: all 0.2s;
				-moz-transition: all 0.2s;
				-o-transition: all 0.2s;
				transition: all 0.2s; }
		
body, .global-wrapper {background-color:#FFF;}body.boxed-page-layout {background-color:RGB(102, 130, 144);}body.boxed-page-layout:before {opacity:;}.notification-panel {background-color:RGB(24, 101, 160);}.notification-panel, .notification-panel * { color:RGB(189, 227, 252);}.notification-panel:before {min-height:50px;}.notification-panel:hover {background-color:RGB(15, 119, 200);}.notification-panel:hover, .notification-panel:hover * {color:RGB(255, 255, 255);}a {color:rgb(42, 160, 239);}a:hover {color:rgb(93, 144, 226);}div.editor-styles-wrapper .wp-block-button:not(.is-style-outline) .wp-block-button__link:not(.has-background),.entry-content .wp-block-button:not(.is-style-outline) .wp-block-button__link:not(.has-background) {background-color:rgb(42, 160, 239);}.has-primary-background-color {background-color:rgb(42, 160, 239);}.wp-block-button.is-style-outline .wp-block-button__link:not(.has-text-color) {color:rgb(42, 160, 239);}body, body .dslc-module-front {font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;line-height:27px;font-weight:300;color:RGB(65, 72, 77);}.site {font-size:17px;}p {margin-bottom:20px;}h1 {font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-size:42px;line-height:48px;font-weight:200;margin-bottom:25px;color:RGB(70, 72, 75);}h2 {font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-size:31px;line-height:38px;font-weight:300;margin-bottom:20px;color:RGB(39, 40, 43);}h3 {font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-size:24px;line-height:33px;font-weight:300;margin-bottom:20px;color:RGB(16, 16, 17);}h4 {font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-size:21px;line-height:29px;font-weight:300;margin-bottom:18px;color:RGB(53, 54, 57);}h5 {font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-size:17px;line-height:27px;font-weight:500;margin-bottom:25px;color:RGB(16, 16, 17);}h6 {font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-size:17px;line-height:27px;font-weight:400;margin-bottom:25px;color:RGB(70, 72, 75);}.calltoaction-area {background-color:rgb(54, 61, 65);height:160px;line-height:160px;}.calltoaction-area, .calltoaction-area * {color:RGB(255, 255, 255);}.calltoaction-area:hover {background-color:rgb(86, 174, 227);}.calltoaction-area:hover, .calltoaction-area:hover * { color:RGB(255, 255, 255); }.calltoaction-area__content {font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-weight:300;font-size:35px;}input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea {background:#FFF;}
</style>
<link rel='stylesheet' id='lbmn-megamainmenu-alternative-style-css'  href='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/themes/seowp/design/nopluginscss/nomegamenuactive6a56.css?ver=1565899147' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/plugins/estimation-form/assets/js/lfb_frontend.min5733.js?ver=90.671'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/plugins/live-composer-page-builder/js/dist/client_plugins.mine225.js?ver=1.5.3'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/vendors/s6lbj1uz4ng2sore443wvx84-wpengine.netdna-ssl.com/wp-content/plugins/lc-extensions/extensions/openstreetmap/js/leaflet9dff.js?ver=5.3.2'></script>
<link rel='https://api.w.org/' href='wp-json/index.html' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />

<link rel="canonical" href="<?php echo current_url(); ?>" />
<link rel='shortlink' href='<?php echo current_url(); ?>' />

<script type="text/javascript">var essb_settings = {"ajax_url":"https:\/\/flat.seowptheme.com\/wp-admin\/admin-ajax.php","essb3_nonce":"aaba539ea6","essb3_plugin_url":"https:\/\/flat.seowptheme.com\/wp-content\/plugins\/easy-social-share-buttons3","essb3_stats":false,"essb3_ga":false,"essb3_ga_mode":"simple","blog_url":"https:\/\/flat.seowptheme.com\/","essb3_postfloat_stay":false,"post_id":5499};</script>	<script type="text/javascript">
		function lbmn_passedFiveSeconds() {
			var el_html = document.getElementsByTagName( 'html' );
			if ( (typeof el_html.className !== 'undefined') && (el_html.className.indexOf( 'content-loaded' ) === -1) ) {
				el_html.className += ' content-loaded';
			}
		}
		setTimeout( lbmn_passedFiveSeconds, 5000 );
	</script>

	<!-- Messenger Chat Plugin Code -->
    <div id="fb-root"></div>

    <!-- Your Chat Plugin code -->
    <!-- <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", "100113655715024");
      chatbox.setAttribute("attribution", "biz_inbox");

      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v12.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/nl_NL/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script> -->
	
   </head>
   

   
<body class="home page-template-default page page-id-5499 wp-custom-logo wp-embed-responsive dslc-page dslc-page-has-content dslc-page-has-hf _masterslider _msp_version_30.2.11 pseudo-preloader">
<div class="off-canvas-wrap">
	<div class="site global-container inner-wrap" id="global-container">
		<div class="global-wrapper">
			<div id="dslc-content" class="dslc-content dslc-clearfix">
            
         
   <div id="dslc-header" class="dslc-header-pos-relative " data-hf >
	
		<div  class="dslc-modules-section area-mobile-margin-bottom-none " style="padding-bottom:30px;padding-top:30px;background-color:#ffffff;" data-section-id="c6d2298528b">
				
				
				<div class="dslc-modules-section-wrapper dslc-clearfix"><div class="dslc-modules-area dslc-col dslc-12-col 
				dslc-last-col" data-size="12" data-valign="">
		<div id="dslc-module-59fbc29c913" class="dslc-module-front dslc-module-DSLC_Logo dslc-in-viewport-check 
		dslc-in-viewport-anim-none  dslc-col dslc-2-col  dslc-module-handle-like-regular phone-width-half" data-module-id="59fbc29c913" data-module="DSLC_Logo" data-dslc-module-size="3" data-dslc-anim="none" data-dslc-anim-delay="" data-dslc-anim-duration="650"  data-dslc-anim-easing="ease" data-dslc-preset="none" >

			
			
		
		
		<div class="dslc-logo">

			
<a href="<?php echo site_url(); ?>" target="_self">
<img src="<?php echo logo_main_img(); ?>"
style="height:70px;"
alt="<?php echo $this->site_info->site_name; ?> main logo" title=""   />
</a>

			
		</div>

		
				</div><!-- .dslc-module -->
		
		<div id="dslc-module-d9298a99edb" class="dslc-module-front dslc-module-DSLC_Menu_Pro dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-8-col  dslc-module-handle-like-regular phone-width-half" data-module-id="d9298a99edb" data-module="DSLC_Menu_Pro" data-dslc-module-size="6" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650"  data-dslc-anim-easing="default" data-dslc-preset="none" >

			
			
					<!-- <div class="lcmenu-pro"> -->
				<div class="lcmenupro-navigation lcmenupro-sub-position-center">
					<div class="lcmenupro-inner">
					<!-- $full_menu_classes -->
						<div class="menu-mega-main-menu-container">
							<ul id="menu-mega-main-menu" class="menu dslc-hide-on-tablet dslc-hide-on-phone ">
							
						<li id="menu-item-1455" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1455"><a href="<?php echo site_url(); ?>">Home</a></li>
						<li id="menu-item-1455"
						class="<?php echo $this->uri->segment(2) === 'product' ? 'current-menu-ancestor' : ''; ?> menu-item menu-item-type-custom menu-item-object-custom  menu-item-has-children menu-item-1445">
						<a href="<?php echo site_url('products'); ?>">Producten</a>
						<ul class="sub-menu">
						<li class=" menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('product/pos-horeca-module'); ?>/">Horeca Module</a></li>
						<li class="menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('product/pos-shop-module'); ?>/">Shop Module</a></li>
						<li class=" menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('product/pos-supermarket-module'); ?>/">Supermarkt Module</a></li>
						<li class=" menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('product/webshop-apps'); ?>/">Apps and Webshops</a></li>
	
</ul>
						
						</li>
						<li id="menu-item-1468" 
						class="<?php echo $this->uri->segment(2) === 'advice' ? 'current-menu-ancestor' : ''; ?> menu-type-columns menu-width-2l menu-item menu-item-type-custom menu-item-object-custom
						 menu-item-has-children menu-item-1468"><a href="<?php echo site_url('advice'); ?>">Advies</a>
</li>


<li id="menu-item-1479" class="<?php echo $this->uri->segment(2) === 'support' ? 'current-menu-ancestor' : ''; ?> menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1479"><a href="<?php echo site_url('support'); ?>">Support</a>

</li>

<li id="menu-item-1445" class="<?php echo $this->uri->segment(2) === 'login' ? 'current-menu-ancestor' : ''; ?> menu-type-columns menu-width-full menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1445"><a href="<?php echo site_url('login'); ?>">Klanten Portaal</a>

</li>

<li id="menu-item-1461" class="<?php echo $this->uri->segment(2) === 'contact' ? 'current-menu-ancestor' : ''; ?> menu-type-columns menu-width-2s menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1461"><a href="<?php echo site_url('contact'); ?>">Contact</a>

</li>
</ul></div>
											</div>
					<svg class="lcmenupro-icon lcmenu-mobile-hook dslc-hide-on-desktop "><use xlink:href="#icon-menu"></use></svg>				</div>
			<!-- </div> -->

			<div class="lcmenupro-site-overlay"></div>

			<div class="lcmenupro-mobile-navigation">
				<div class="lcmenupro-mobile-inner">
					<div class="lcmenu-mobile-close-hook">
						<svg class="lcmenupro-icon"><use xlink:href="#icon-x"></use></svg>
					</div>
																	<div class="lcmenu-mobile-logo">
							<img src="<?php echo logo_main_img(); ?>" alt="<?php echo $this->site_info->site_name; ?> logo mobile" 
							title="<?php echo $this->site_info->site_name; ?>" />
						</div>
										<div class="menu-mega-main-menu-container"><ul id="menu-mega-main-menu-1" class="lcmenupro-mobile-menu">
										

										<li id="menu-item-1455" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1455">
										<a href="#" onclick="return false;">Producten</a>

										<ul class="sub-menu">
						<li class=" menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('product/pos-horeca-module'); ?>/">Horeca Module</a></li>
						<li class="menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('product/pos-shop-module'); ?>/">Shop Module</a></li>
						<li class=" menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('product/pos-supermarket-module'); ?>/">Supermarkt Module</a></li>
						<li class=" menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('product/webshop-apps'); ?>/">Apps and Webshops</a></li>
						<li class=" menu-item menu-
						item-type-custom menu-item-object-custom">
						<a href="<?php echo site_url('products'); ?>/">Alle Producten</a></li>
	
</ul>
										</li>
						<li id="menu-item-1468" class="menu-type-columns menu-width-2l menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1468"><a href="<?php echo site_url('advice'); ?>">Advies</a>
</li>


<li id="menu-item-1479" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1479"><a href="<?php echo site_url('support'); ?>">Support</a>

</li>

<li id="menu-item-1445" class="menu-type-columns menu-width-full menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1445"><a href="<?php echo site_url('login'); ?>">Klanten Portaal</a>

</li>

<li id="menu-item-1461" class="menu-type-columns menu-width-2s menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1461"><a href="<?php echo site_url('contact'); ?>">Contact</a>

</li>										


</ul></div>
														</div>
			</div>
			
				</div><!-- .dslc-module -->
		
		<div id="dslc-module-f6fd23ed62f" class="dslc-module-front dslc-module-DSLC_Button dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-2-col dslc-last-col dslc-hide-on-phone  dslc-module-handle-like-regular " data-module-id="f6fd23ed62f" data-module="DSLC_Button" data-dslc-module-size="3" data-dslc-anim="none" data-dslc-anim-delay="" data-dslc-anim-duration="650"  data-dslc-anim-easing="ease" data-dslc-preset="none" >

			
			
		
			<div class="dslc-button">
									<a href="<?php echo site_url('contact'); ?>" target="_self"  onClick="" class="">
																				
																										<span>Request a Demo</span>
																	</a>
							</div><!-- .dslc-button -->


						
				</div><!-- .dslc-module -->
		</div></div></div></div>