<div class="bg-gray-light p-tb20">
                <div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-1">
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li>Services</li>
                    </ul>
                </div>
            </div>

<section class="services">
        <div class="container">
           <div class="section-head">
                            <h2 class="text-uppercase">Terms & Conditions</h2>
                            <div class="wt-separator-outer">
                               <div class="wt-separator style-square">
                                   <span class="separator-left bg-primary"></span>
                                   <span class="separator-right bg-primary"></span>
                               </div>
                           </div>
<p>The Terms and Conditions apply to all visitors and users of the Website. Please thus read these Terms and Conditions carefully before accessing, browsing and/or using the Website.</p>
<p>By accessing, browsing and/or using the Website, you acknowledge that you have read, understood and agree to be bound by and comply with the Terms and Conditions and declare that all information you provide is true and accurate. If you do not agree with the Terms and Conditions contained herein, please refrain from accessing, using and/or contributing to the Website.</p>
<p>The information available on the Website is intended as a guide and every reasonable effort shall be made to keep it update, complete and accurate. The Website may contain typographical errors or certain inaccuracies and D.A Christodoulou reserves the right to correct any such errors, inaccuracies or omissions and to update the information on the Website.</p>
<p>Intellectual Property Rights</p>
<p>Unless expressly stated to the contrary, any copyright and other intellectual property rights (such as design rights, trademarks, patents etc.) in any material displayed on the Website is and/or remains the property of D.A Christodoulou (or of any other rightful owner, as the case may be). The material on the website including text and images, may not be printed, copied, reproduced, republished, posted, displayed, modified, reused, broadcasted or transmitted in any way, except solely for the user's own personal non-commercial use. Prior written permission for any other type of use must be obtained by the user from D.A Christodoulou.</p>
<p>Collection of Personal Data</p>
<p>We collect information from you when you complete the property enquiry section for receiving information on a property registered on our Website.</p>
<p>The use of any personal information of users (&ldquo;Personal Data&rdquo;) collected by us is subject to a) the lawful collection of the Personal Data in accordance with the Privacy Policy of D.A Christodoulou Website and b) lawful processing of these data in accordance with and pursuant to Law N.133 (I) 2001 on Processing of Personal Data (Protection of the Individual) Law as amended and supplemented from time to time by any additional legislation and/or regulation.</p>
<p>D.A Christodoulou undertakes to protect the Personal Data of the users and take all reasonable precautions to protect security of the same from any loss or unauthorised use, access or disclosure. D.A Christodoulou further undertakes not to disclose any Personal Data of the users unless such disclosure is required by any relevant applicable law or if the user provides its express consent.</p>
                        </div>
            
            
        </div>
    </section>