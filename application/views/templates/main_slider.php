<div class="site-main">

    <div id="content" class="site-content" role="main">
        <article id="post-5499" class="post-5499 page type-page status-publish hentry">

            <!-- start of hero -->
            <section class="hero-slider hero-style-1">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="slide-inner slide-bg-image" data-background="<?php echo base_url(); ?>assets/images/cashier-482142019-58a4bc453df78c345b7001f3.jpg">
                                <div class="container">
                                    <div data-swiper-parallax="300" class="slide-title">
                                        <h2>Bij ons staat de klant  <br> centraal</h2>
                                    </div>
                                    <div data-swiper-parallax="400" class="slide-text">

                                        <p>Wilt u ook centraal staan en verschil maken voor u en uw bedrijf</p>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div data-swiper-parallax="500" class="slide-btns">
                                        <a href="<?php echo site_url('contact'); ?>" class="default-btn">Neem dan contact met ons op</a>
                                        <a href="https://www.youtube.com/watch?v=ArjaAUknGSk?autoplay=1" class="hero-video-btn popup-youtube video-btn" data-type="iframe" tabindex="0"><i class="fi fa fa-play"></i>Watch The Video</a>
                                    </div>
                                </div>
                            </div>
                            <!-- end slide-inner -->
                        </div>
                        <!-- end swiper-slide -->

                        <div class="swiper-slide">
                            <div class="slide-inner slide-bg-image" 
                            data-background="<?php echo base_url(); ?>assets/images/background/pos-software-slider-home.jpg">
                                <div class="container">
                                    <div data-swiper-parallax="300" class="slide-title">
                                        <h2>Totale transparantie, weet waar je aan toe bent. </h2>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div data-swiper-parallax="500" class="slide-btns">
                                        <a href="<?php echo site_url('contact'); ?>" class="default-btn">Neem dan contact met ons op</a>
                                        <a href="https://www.youtube.com/watch?v=ArjaAUknGSk?autoplay=1" class="hero-video-btn video-btn popup-youtube" data-type="iframe" tabindex="0"><i class="fi fa fa-play"></i>Watch The Video</a>
                                    </div>
                                </div>
                            </div>
                            <!-- end slide-inner -->
                        </div>
                        <!-- end swiper-slide -->

                        <div class="swiper-slide">
                            <div class="slide-inner slide-bg-image" data-background="<?php echo base_url(); ?>assets/images/background/pos-system-features.jpg" style="background-position: center;">
                                <div class="container">
                                    <div data-swiper-parallax="300" class="slide-title">
                                        <h2>Prijs / Kwaliteit verhouding </h2>
                                    </div>
                                    <div data-swiper-parallax="400" class="slide-text">
                                        <p>Snel en gebruiksvriendelijk</p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div data-swiper-parallax="500" class="slide-btns">
                                        <a href="<?php echo site_url('contact'); ?>" class="default-btn">Neem dan contact met ons op</a>
                                        <a href="https://www.youtube.com/watch?v=ArjaAUknGSk?autoplay=1" class="hero-video-btn video-btn popup-youtube" data-type="iframe" tabindex="0"><i class="fi fa fa-play"></i>Watch The Video</a>
                                    </div>
                                </div>
                            </div>
                            <!-- end slide-inner -->
                        </div>
                        <!-- end swiper-slide -->
                    </div>
                    <!-- end swiper-wrapper -->

                    <!-- swipper controls -->
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </section>
            <!-- end of hero slider -->

            <!-- start features-section -->
            <section class="features-section">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <div class="feature-grids clearfix">
                                <div class="grid">
                                    <div class="header">
                                        <a href="<?php echo site_url('product/pos-horeca-module'); ?>">
                                            <h4>Horeca Module</h4>
                                        </a>

                                    </div>
                                    <div class="details">
                                        <p>TEA Software module met tafel beheer gebaseerd op uw eigen tafel en ruimte situatie.
                                            <br>
                                            <br> Mobile Order app, bestellingen kunnen aan tafel opgenomen worden met een mobile device
                                        </p>
                                        <a href="<?php echo site_url('product/pos-horeca-module'); ?>" class="read-more">
                                                                                    Meer over</a>
                                    </div>
                                </div>
                                <div class="grid">
                                    <div class="header">
                                        <a href="<?php echo site_url('product/pos-shop-module'); ?>">
                                            <h4>
                        Winkel Module
</h4>
                                        </a>
                                    </div>
                                    <div class="details">
                                        <p>TEA Software is een totaal oplossing voor uw winkelketen
                                            <br>
                                            <br> Realtime voorraad bekijken, ook van alle andere winkels
                                            <br>
                                            Omni-channel Technologie
                                        </p>
                                        <a href="<?php echo site_url('product/pos-shop-module'); ?>" class="read-more">Meer over</a>
                                    </div>
                                </div>
                                <div class="grid">
                                    <div class="header">
                                        <a href="<?php echo site_url('product/pos-supermarket-module'); ?>">
                                            <h4>Supermarkt Module</h4>
                                        </a>
                                    </div>
                                    <div class="details">
                                        <p>TEA Software is een totaal oplossing voor uw winkelketen
                                            <br>
                                            <br> Omni-channel Technologie, live afrekenen, retouren, kortingen, spaarpunten, voorraden
                                        </p> <a href="<?php echo site_url('product/pos-supermarket-module'); ?>" class="read-more">Meer over</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end container -->
            </section>
            <!-- end features-section -->

            <!-- Start Main Banner Area -->

            <div class="container">
                <section class="parallax overlay-gradient padding m-t-50 m-b-50">
                    <div class="content">
                        <div class="row text-center">
                            <div class="col-lg-12">
                                <div class="header-badge-white m-b-15">Klanten vertrouwen op ons, iedere dag</div>

                                <div class=" text-center  m-t-20" data-size="6" data-valign="">
                                    <div id="dslc-module-2194" class="dslc-module-handle-like-regular " data-module-id="2194" data-module="DSLC_Button" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                        <div class="text-center">
                                            <a href="<?php echo site_url('contact'); ?>" target="_self" onClick="" class=" food_btn">
                                                <span class="dslc-icon-ext-envelope"></span>
                                                <span>Neem dan contact met ons op</span>
                                            </a>
                                        </div>
                                        <!-- .dslc-button -->

                                    </div>
                                    <!-- .dslc-module -->
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>



<section class="services-section section-padding">
<div class="container">
<div class="row">
<div class="col col-xs-12">
<div class="service-grids">
<a class="grid" href="<?php echo site_url('product/pos-horeca-module'); ?>">
<i class="fi fas fa-utensils"></i>
<h3>Restaurant</h3>

</a>

<a class="grid" href="<?php echo site_url('product/pos-horeca-module'); ?>">
<i class="fas fa-hamburger"></i>
<h3>Fast food</h3>
</a>

<a class="grid" href="<?php echo site_url('product/pos-shop-module'); ?>">
<i class="dslc-icon fas fa-tshirt"></i>
<h3>Fashion store</h3>
</a>

</div>

<div class="clearfix">
</div>
<div class="service-grids">
<a class="grid" href="<?php echo site_url('product/pos-shop-module'); ?>">
<i class="fi dslc-icon fas fa-store-alt"></i>
<h3>Convenience store</h3>
</a>

<a class="grid" href="<?php echo site_url('product/pos-shop-module'); ?>">
<i class="dslc-icon fas fa-running"></i>
<h3>Sports & Leisure</h3>
</a>

<a class="grid" href="<?php echo site_url('product/pos-supermarket-module'); ?>">
<i class="fi dslc-icon fas fa-shopping-cart"></i>
<h3>Supermarkt</h3>
</a>

</div>

<div class="clearfix">
</div>
</div>
</section>

            <div class="entry-content">
                <div id="dslc-main">

                    <div class="dslc-modules-section " style="padding-bottom:0px;padding-top:0px;border-color:rgb(241, 241, 241);border-width:1px;border-top-style: hidden; border-right-style: hidden; border-left-style: hidden; " data-section-id="6c4e26ceb48">

                    </div>
                    <div class="dslc-modules-section " style="padding-bottom:0px;padding-top:0px;border-color:rgb(241, 241, 241);border-width:1px;border-top-style: hidden; border-right-style: hidden; border-left-style: hidden; " data-section-id="1063aa08aa6">

                        <div class="dslc-modules-section-wrapper dslc-clearfix">
                            <div class="dslc-modules-area dslc-col dslc-6-col dslc-first-col" data-size="5" data-valign="">
                                <div id="dslc-module-2079" class="dslc-module-front dslc-module-DSLC_Image dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2079" data-module="DSLC_Image" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-image-container">

                                        <div class="dslc-separator-wrapper">
                                            <div class="dslc-separator dslc-separator-style-invisible">
                                            </div>
                                            <div></div>
                                        </div>
                                        <div class="dslc-separator-wrapper">
                                            <div class="dslc-separator dslc-separator-style-invisible">
                                            </div>
                                            <div></div>
                                        </div>

                                        <div class="dslc-image">

                                            <img class="rounded" src="<?php echo base_url(); ?>assets/images/background/image_fashion_store.jpg" 
                                            alt="image fashion store pos software" title="seo_specialist_workplace-optimized" srcset="<?php echo base_url(); ?>assets/images/background/image_fashion_store.png 212w" sizes="(max-width: 225px) 100vw, 225px" />

                                        </div>
                                        <!-- .dslc-image -->
                                    </div>

                                </div>
                                <!-- .dslc-module -->
                            </div>
                            <div class="dslc-modules-area dslc-col dslc-6-col dslc-last-col" data-size="7" data-valign="">
                                <div id="dslc-module-2080" class="dslc-module-front dslc-module-DSLC_Separator dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2080" data-module="DSLC_Separator" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-separator-wrapper">
                                        <div class="dslc-separator dslc-separator-style-invisible">
                                        </div>
                                        <div></div>
                                    </div>
                                    <!-- .dslc-separator-wrapper -->

                                </div>
                                <!-- .dslc-module -->

                                <div id="dslc-module-2081" class="dslc-module-front dslc-module-DSLC_Text_Simple dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2081" data-module="DSLC_Text_Simple" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-text-module-content">
                                        <h2 style="color:#23a4c2;">Met minimale tijd, een maximum resultaat behalen!</h2>
                                        <p>TEA Software is zodanig ontwikkeld dat met minimale 
                                        tijd u het maximum resultaat kunt halen uit het beheer van uw bedrijf. Uw volledige aandacht is gericht op uw klanten, advies aan uw klanten en verkoop.
                                        </p>
                                    </div>

                                </div>
                                <!-- .dslc-module -->

                                <div id="dslc-module-2082" class="dslc-module-front dslc-module-DSLC_Info_Box dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2082" data-module="DSLC_Info_Box" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-info-box dslc-info-box-icon-pos-aside dslc-info-box-image-pos-above">

                                        <div class="dslc-info-box-wrapper">

                                            <div class="dslc-info-box-main-wrap dslc-clearfix">

                                                <div class="dslc-info-box-image">
                                                    <div class="dslc-info-box-image-inner">
                                                        <span class="dslc-icon dslc-icon-ext-profile-male"></span>
                                                    </div>
                                                    <!-- .dslc-info-box-image-inner -->
                                                </div>
                                                <!-- .dslc-info-box-image -->

                                                <div class="dslc-info-box-main">

                                                    <div class="dslc-info-box-title">
                                                        <h4>Prijs / Kwaliteit verhouding </h4>
                                                    </div>
                                                    <!-- .dslc-info-box-title -->

                                                </div>
                                                <!-- .dslc-info-box-main -->

                                            </div>
                                            <!-- .dslc-info-box-main-wrap -->

                                        </div>
                                        <!-- .dslc-info-box-wrapper -->

                                    </div>
                                    <!-- .dslc-info-box -->

                                </div>
                                <!-- .dslc-module -->

                                <div id="dslc-module-2083" class="dslc-module-front dslc-module-DSLC_Info_Box dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2083" data-module="DSLC_Info_Box" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-info-box dslc-info-box-icon-pos-aside dslc-info-box-image-pos-above">


                                            <div class="dslc-info-box-wrapper">

                                                <div class="dslc-info-box-main-wrap dslc-clearfix">

                                                    <div class="dslc-info-box-image">
                                                        <div class="dslc-info-box-image-inner">
                                                            <span class="dslc-icon dslc-icon-ext-trophy"></span>
                                                        </div>
                                                        <!-- .dslc-info-box-image-inner -->
                                                    </div>
                                                    <!-- .dslc-info-box-image -->

                                                    <div class="dslc-info-box-main">

                                                        <div class="dslc-info-box-title">
                                                            <h4>Totale transparantie, weet waar je aan toe bent</h4>
                                                        </div>
                                                        <!-- .dslc-info-box-title -->

                                                    </div>
                                                    <!-- .dslc-info-box-main -->

                                                </div>
                                                <!-- .dslc-info-box-main-wrap -->

                                            </div>
                                            <!-- .dslc-info-box-wrapper -->
                                    

                                    </div>
                                    <!-- .dslc-info-box -->

                                </div>
                                <!-- .dslc-module -->

                                <div id="dslc-module-2084" class="dslc-module-front dslc-module-DSLC_Info_Box dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2084" data-module="DSLC_Info_Box" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-info-box dslc-info-box-icon-pos-aside dslc-info-box-image-pos-above">

                                        <div class="dslc-info-box-wrapper">

                                            <div class="dslc-info-box-main-wrap dslc-clearfix">

                                                <div class="dslc-info-box-image">
                                                    <a href="<?php echo site_url('products'); ?>" target="_self" class="">
                                                        <div class="dslc-info-box-image-inner">
                                                            <span class="dslc-icon dslc-icon-ext-clock"></span>
                                                        </div>
                                                    </a>
                                                    <!-- .dslc-info-box-image-inner -->
                                                </div>
                                                <!-- .dslc-info-box-image -->

                                                <div class="dslc-info-box-main">

                                                    <div class="dslc-info-box-title">
                                                        <h4>Bespaar tijd en geld</h4>
                                                    </div>
                                                    <!-- .dslc-info-box-title -->

                                                </div>
                                                <!-- .dslc-info-box-main -->

                                            </div>
                                            <!-- .dslc-info-box-main-wrap -->

                                        </div>
                                        <!-- .dslc-info-box-wrapper -->

                                    </div>
                                    <!-- .dslc-info-box -->

                                </div>
                                <!-- .dslc-module -->

                                <!-- .dslc-module -->

                                <!-- .dslc-module -->
                            </div>
                        </div>
                    </div>

                    <div class="dslc-separator-wrapper">
                        <div class="dslc-separator dslc-separator-style-invisible">
                        </div>
                        <div></div>
                    </div>

                    <div class="dslc-modules-section " style="padding-bottom:0px;padding-top:0px;border-color:rgb(241, 241, 241);border-width:1px;border-top-style: hidden; border-right-style: hidden; border-left-style: hidden; " data-section-id="301b17c356d">

                        <div class="dslc-modules-section-wrapper dslc-clearfix">
                            <div class="dslc-modules-area dslc-col dslc-6-col dslc-first-col" data-size="6" data-valign="">
                                <div id="dslc-module-2183" class="dslc-module-front dslc-module-DSLC_Button dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2183" data-module="DSLC_Button" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-button">
                                        <a href="<?php echo site_url('advice'); ?>" target="_self" onClick="" class="">
                                            <span class="dslc-icon dslc-icon-ext-bubble"></span>
                                            <span>Free Advice Consultation</span>
                                        </a>
                                    </div>
                                    <!-- .dslc-button -->

                                </div>
                                <!-- .dslc-module -->
                            </div>
                            <div class="dslc-modules-area dslc-col dslc-6-col dslc-last-col" data-size="6" data-valign="">
                                <div id="dslc-module-2194" class="dslc-module-front dslc-module-DSLC_Button dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2194" data-module="DSLC_Button" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-button">
                                        <a href="<?php echo site_url('contact'); ?>" target="_self" onClick="" class="">
                                            <span class="dslc-icon dslc-icon-ext-chat"></span>
                                            <span>Request a Free Demo</span>
                                        </a>
                                    </div>
                                    <!-- .dslc-button -->

                                </div>
                                <!-- .dslc-module -->
                            </div>
                            <div class="dslc-modules-area dslc-col dslc-12-col dslc-last-col" data-size="12" data-valign="">
                                <div id="dslc-module-2195" class="dslc-module-front dslc-module-DSLC_Separator dslc-in-viewport-check dslc-in-viewport-anim-none  dslc-col dslc-12-col dslc-last-col  dslc-module-handle-like-regular " data-module-id="2195" data-module="DSLC_Separator" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

                                    <div class="dslc-separator-wrapper">
                                        <div class="dslc-separator dslc-separator-style-invisible">
                                        </div>
                                        <div></div>
                                    </div>
                                    <!-- .dslc-separator-wrapper -->

                                </div>
                                <!-- .dslc-module -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- .entry-content -->
        </article>
        <!-- #post-## -->
    </div>
    <!-- #content -->
</div>
<!-- .site-main -->


