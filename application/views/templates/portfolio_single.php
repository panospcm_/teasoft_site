<?php
$this->load->model('MyModel');
// $brand_gallery = $brand_gallery[0];
// $data_brands = $this->MyModel->getBrandsGallery(NULL,0);
// $categories = $this->MyModel->getWorksCategories();
// $brand_gallery_ = count($brand_gallery)>0?$brand_gallery[0]:NULL;

$portfolio = $portfolio[0];
// var_dump($brand_gallery[0]);
?>

<div class="bg-gray-light p-tb20">
              <div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-1">
                        <li><a href="javascript:void(0);">Home</a></li>
                        <li>Portfolio-detail</li>
                    </ul>
                </div>
            </div>


<div class="container">
          <div class="row ">
            <div class="col-sm-12">
              <div class="heading-box pb-30 ">
                <h2> <?php echo $portfolio->portfolio_title; ?></h2>
                <span class="b-line l-left"></span>
              </div>

            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="project-details">
                <figure>
                  <img src="<?php echo base_url(); ?>assets/images/portfolio/<?php echo $portfolio->portfolio_main_image_1; ?>" alt="<?php echo $portfolio->portfolio_main_image_1; ?>" style="width: 100%;">
                </figure>
                <div class="project-info col-sm-12 col-lg-4 ">
                  <h3>Project Description</h3>
                  <ul>
                  
                    <li>
                      <strong>Location:</strong> <?php echo $portfolio->portfolio_location; ?>
                    </li>
                    <li>
                      <strong>Surface Area:</strong> <?php echo $portfolio->portfolio_surface_area; ?>
                    </li>
                    <li>
                      <strong>Year Completed:</strong> <?php echo date("M Y", strtotime($portfolio->portfolio_date)); ?>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-lg-7 mt-30">
              <div class="box-title mb-20">
                <h3><?php echo $portfolio->portfolio_title; ?></h3>
              </div>
              <div class="text-content">
                <p>
              <?php echo $portfolio->portfolio_description; ?>
                </p>

              </div>
            </div>
            
         
            
          </div>
        </div>