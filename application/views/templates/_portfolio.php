 <?php 

$this->load->model('MyModel');
$portfolio = $this->MyModel->getPortfolio();
// $sliders = $this->MyModel->getSliders();



// var_dump($productsCategories);
?>

        <!-- CONTENT START -->
        <div class="page-content">
        
            <!-- INNER PAGE BANNER -->
       <!--      <div class="wt-bnr-inr overlay-wraper" style="background-image:url(http://thewebmax.com/constrot/images/banner/Portfolio.jpg);">
              <div class="overlay-main bg-black" style="opacity:0.5;"></div>
                <div class="container">
                    <div class="wt-bnr-inr-entry">
                        <h1 class="text-white">Portfolio</h1>
                    </div>
                </div>
            </div> -->
            <!-- INNER PAGE BANNER END -->

<!-- BREADCRUMB ROW -->                            
<div class="bg-gray-light p-tb20">
<div class="container">
<ul class="wt-breadcrumb breadcrumb-style-1">
<li><a href="<?php echo site_url(); ?>">Home</a></li>
<li>Portfolio</li>
</ul>
</div>
</div>
            <!-- BREADCRUMB ROW END -->
            
            <!-- SECTION CONTENT START -->
<div class="section-full p-b50">



<div class="" style="overflow: auto;">

<?php foreach($portfolio as $key=>$iterWork): ?>


<!-- COLUMNS 1 -->
<div class="house col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xs-100pc">
<div class="wt-box p-a20">
<div class="wt-thum-bx wt-img-effect zoom">
  <img src="<?php echo base_url(); ?>assets/images/portfolio/thumbs/<?php echo $iterWork->portfolio_main_image_1; ?>" alt="<?php echo $iterWork->portfolio_main_image_1; ?> portfolio image">
</div>
<div class="wt-info">
  <h4 class="wt-title m-t20"><a href="<?php echo site_url(array('portfolio',$iterWork->portfolio_id)); ?>"><?php echo $iterWork->portfolio_title; ?></a></h4>
  
  <a href="<?php echo site_url(array('portfolio',$iterWork->portfolio_id)); ?>" class="site-button skew-icon-btn">Read More <i class="fa fa-angle-double-right"></i></a>
</div>
</div>
</div>

<?php endforeach; ?>

<!-- COLUMNS 2 -->

</div>

    <!-- GALLERY CONTENT END -->

</div>
<!-- SECTION CONTENT END  -->

</div>
<!-- CONTENT END -->
