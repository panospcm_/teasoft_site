 <?php
    $this->load->model('MyModel');
    $data_blogs = $this->MyModel->get_blogs(0,0,$this->uri->segment(3));
    $data_blogs_categories = $this->MyModel->get_blogs_categories();


    
    ?>

<section class="header header-bg-3">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="header-content">
          <div class="header-content-inner">
            <h1>Our News</h1>
            <p>View our gastronomy tours in Cyprus </p>
            <div class="ui breadcrumb">
              <a href="<?php echo site_url(); ?>" class="section">Home</a>
              <div class="divider"> / </div>
              <div class="active section">News</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="container margin_60">
    <div class="row">
         
     <div class="col-md-8 col-md-offset-2">
           <div class="box_style_1">

             <?php
                            foreach ($data_blogs as $key => $iterBlog):
                            $dateParts = explode("/", date('d/MM/Y', $iterBlog->blog_created_at));
                            // var_dump($iterBlog);
                            ?>
            <div class="post">
                    <a href="<?php echo blog_link($iterBlog->blog_id,$iterBlog->blog_title) ; ?>" ><img src="<?php echo blog_main_img($iterBlog->blog_image) ; ?>" alt="Image" class="img-responsive"></a>
                    <div class="post_info clearfix">
                        <div class="post-left">
                            <ul>
                                <li><i class="icon-calendar-empty"></i> On <span><?php echo date('d/M/Y', $iterBlog->blog_created_at); ?></span></li>
                            </ul>
                        </div>
                       <!--  <div class="post-right"><i class="icon-comment"></i><a href="#">25 </a></div> -->
                    </div>
                    <h2><?php echo $iterBlog->blog_title; ?></h2>
                    <p><?php echo character_limiter(strip_tags($iterBlog->blog_description),110); ?></p>
                    <a href="<?php echo blog_link($iterBlog->blog_id,$iterBlog->blog_title) ; ?>" class="btn_1" >Read more</a>
                </div><!-- end post -->
                
                

                  <?php
                            endforeach;
                            ?>
                <hr>
                
                </div>
                <hr>
                
     </div><!-- End col-md-8-->   
     
    
  </div><!-- End row-->         
</div><!-- End container -->