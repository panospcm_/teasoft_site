
<section class="oppi_restaurent_action_area padding">
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div class="oppi_restaurent_action_img">
                <img class="wow fadeInLeft rounded-2" data-wow-delay="0.6s" src="<?php echo base_url(); ?>assets/images/background/tea_retail_pos_software_mockup_smartphone.png" 
                alt="tea retail pos software mockup smartphone">
                
            </div>
        </div>
        <div class="col-lg-5 offset-lg-1 d-flex align-items-center">
            <div class="oppi_restaurent_action_content">
                <h2 class="wow fadeInUp" data-wow-delay="0.2s">Meerdere filialen Module</h2>
                <p class="wow fadeInUp" data-wow-delay="0.4s">
                De kassa is 100% online en realtime, transacties werken rechtstreeks op de centrale database.
                                Dankzij het uitvalsconcept kan tijdens een internet storing lokaal doorgewerkt worden. 
                                Via internet heeft u altijd en overal de actuele gegevens bij de hand, ook thuis en op vakantie!
</p>

    
            </div>
        </div>
    </div>
</div>
</section>


		
		<section class="pos-section padding">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-12">
                        <div class="pos-content-area">
                            <div class="section-header style-3">
                                <h2>Meerdere filialen Module</h2>
                            
                                <p>

                                Informatie in de BackOffice is op een intuïtieve manier toegankelijk voor ondernemers, regiomanagers,
                                 formule marketeers, assortiment managers en directies

                                </p>

                                
                                <p>
                                TEA Software is zodanig ontwikkeld dat met minimale tijd u het maximum resultaat kunt halen uit het beheer van uw bedrijf. Uw volledige aandacht is gericht op uw klanten, advies aan uw klanten en verkoop.
                                </p>
                                <div class="section-wrapper">
                                <ul>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Intuïtieve menustructuren </li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Superslim, razendsnelle kassa software</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Aanpassingen eenvoudig en snel geregeld </li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Eenvoudig artikelbeheer </li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Kwaliteit en snelheid</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Modulair, dus jij bepaalt welke functionaliteiten</li>
                                </ul>
                            </div>

                                
                            </div>
                          
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="pos-thmub">
                            <img src="<?php echo base_url(); ?>assets/images/background/tea_retail_pos_meerdere-filialen-module_mockup.png" a
                            lt="pos tea retail  meerdere-filialen-module ">
                        </div>
                    </div>
                </div>
            </div>
        </section>


