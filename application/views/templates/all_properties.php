<?php 
// var_dump($this->input->post());


$this->load->model('MyModel');
$properties = $this->MyModel->getProperties($this->input->post('search-location'),$this->input->post('search-type'),$this->input->post('search-bedrooms'),$this->input->post('search-minprice'),$this->input->post('search-listing-type'));
$properties_locations = $this->MyModel->getPropertiesLocations();
$properties_types = $this->MyModel->getPropertiesTypes();
// var_dump($properties);

// var_dump($properties_locations);
?>

    <!-- Begin Main -->
    <div role="main" class="main pgl-bg-grey">
      <!-- Begin page top -->
      <section class="page-top">
        <div class="container">
          <div class="page-top-in">
            <h2><span>Our Properties</span></h2>
          </div>
        </div>
      </section>
      <!-- End page top -->
      
      <!-- Begin Advanced Search -->
      <section class="pgl-advanced-search pgl-bg-light">
        <div class="container">
          <form name="advancedsearch" method="POST" action="<?php echo site_url('properties'); ?>">
            <div class="row">
              
            </div>
            
            <div class="row">
              <div class="col-xs-6 col-sm-2">
                <div class="form-group">
                  <label class="sr-only" for="search-bathrooms">Location</label>
                  <select id="search-location" name="search-location" data-placeholder="Location" class="chosen-select">
                    <option selected="selected" value="">Location</option>
                    <?php foreach($properties_locations as $key=>$iterPropertyLocation): ?>
                    <option value="<?php echo $iterPropertyLocation->properties_location_id; ?>"><?php echo $iterPropertyLocation->properties_location_name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>  
              <div class="col-xs-6 col-sm-2">
                 <div class="form-group">
                  <label class="sr-only" for="area-from">Property Type</label>
                  <select id="area-from" name="search-type" data-placeholder="Area From" class="chosen-select">
                    <option selected="selected" value="<?php echo $this->input->post('search-type')!=''?$this->input->post('search-type'):''; ?>">Property Type</option>
                    <?php foreach($properties_types as $key=>$iterPropertyType): ?>
                    <option value="<?php echo $iterPropertyType->properties_type_id; ?>"><?php echo $iterPropertyType->properties_type_name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-xs-6 col-sm-2">
                 <div class="form-group">
                  <label class="sr-only" for="area-from">Bedrooms</label>
                  <select id="search-bedrooms" name="search-bedrooms" data-placeholder="Area From" class="chosen-select">
                    <option selected="selected" value="<?php echo $this->input->post('search-bedrooms')!=''?$this->input->post('search-bedrooms'):''; ?>"><?php echo $this->input->post('search-bedrooms')!=''?$this->input->post('search-bedrooms'):''; ?> Bedrooms</option>
                    <option value="1">1 Bedrooms</option>
                    <option value="2">2 Bedrooms</option>
                    <option value="3">3 Bedrooms</option>
                    <option value="4">4 Bedrooms</option>
                    <option value="5+">5+ Bedrooms</option>
                  </select>
                </div>
              </div>  
              <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                  <div class="row pgl-narrow-row">

                    <div class="col-xs-6">
                      <label class="sr-only" for="search-maxprice">listing</label>
                       <select id="search-listing" name="search-listing-type" data-placeholder="listing" class="chosen-select-ltype">
                    <option selected="selected" value="<?php echo $this->input->post('search-listing-type')!=''?$this->input->post('search-listing-type'):''; ?>">Listing Type</option>
                    <option value="1">For Sale</option>
                    <option value="2">For Rent</option>
                  </select>
                    </div>

                    <div class="col-xs-6">
                      <label class="sr-only" for="search-minprice">Price</label>
                      <select id="search-minprice" name="search-minprice" data-placeholder="Price" class="chosen-select">
                        <option selected="selected" value="<?php echo $this->input->post('search-minprice')!=''?$this->input->post('search-minprice'):''; ?>"><?php echo $this->input->post('search-minprice')!=''?$this->input->post('search-minprice'):''; ?> Price</option>
                        <option class="masses" value="<=500"><€500</option>
                        <option class="masses" value="=600">€600</option>
                        <option class="masses" value="=700">€700</option>
                        <option class="masses" value="=800">€800</option>
                        <option class="masses" value="=900">€900</option>
                        <option class="masses" value=">1000">>€1000</option>
                        <option class="colors" value=" BETWEEN 20000 AND 50000">€20,000 -  €50,000</option>
                        <option class="colors" value=" BETWEEN 50000 AND 100000">€50,000 -  €100,000</option>
                        <option class="colors" value=" BETWEEN 100000 AND 200000">€100,000 -  €200,000</option>
                        <option class="colors" value=" BETWEEN 200000 AND 200000">€200,000 -  €500,000</option>
                        <option class="colors" value=">500000">>€500,000</option>



                        <option value="">All</option>
                      </select>
                    </div>
                    
                  </div>
                </div>
              </div>  
              <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                  <button type="submit" class="btn btn-block btn-primary">Find your home</button>
                </div>
              </div>  
            </div>
            
          </form>
        </div>
      </section>
      <!-- End Advanced Search -->
      
      <!-- Begin Featured -->
    
      <!-- End Featured -->
      
      <!-- Begin Properties -->
      <section class="pgl-properties pgl-bg-grey">
        <div class="container">

          <div class="properties-full properties-listing properties-listfull">
           

   <!--          <div class="listing-header clearfix">
              <ul class="list-inline list-icons pull-left">
                <li><a href="grid-fullwidth-4-column.html"><i class="fa fa-th"></i></a></li>
                <li class="active"><a href="list-fullwidth.html"><i class="fa fa-th-list"></i></a></li>
                <li><a href="list-map.html"><i class="fa fa-map-marker"></i></a></li>
              </ul>
              <ul class="list-inline list-sort pull-right">
                <li><label for="order-status">Order</label></li>
                <li>
                  <select id="order-status" name="order-status" data-placeholder="Order" class="chosen-select">
                    <option value="Descending">Descending</option>
                    <option value="Ascending">Ascending</option>
                  </select>
                </li>
                <li><label for="sortby-status">Sort by</label></li>
                <li>
                  <select id="sortby-status" name="sortby-status" data-placeholder="Sort by" class="chosen-select">
                    <option value="Name">Name</option>
                    <option value="Area">Area</option>
                    <option value="Date">Date</option>
                  </select>
                </li>
              </ul>
            </div> -->

            <?php 
            if(count($properties)>0):
            foreach($properties as $key=>$iterProperty): ?>
            <div class="pgl-property animation">
              <div class="row">
                <div class="col-sm-6 col-md-4">
                  <div class="property-thumb-info-image">
                    <img alt="<?php echo $iterProperty->property_title; ?>" class="img-responsive" src="<?php echo property_main_img_thumb($iterProperty->property_main_image); ?>">
                    <span class="property-thumb-info-label">
                      <span class="label price">€<?php echo number_format($iterProperty->property_price , 0, ',', '.'); ?></span>
                    </span>
                  </div>
                </div>
                <div class="col-sm-6 col-md-8">
                  <div class="property-thumb-info">
                      
                    <div class="property-thumb-info-content">
                      <h3><a href="<?php echo site_url(array('property',$iterProperty->property_slug)); ?>"><?php echo $iterProperty->property_title; ?></a>   <span class="badge label-info"> <?php echo $iterProperty->property_listing_type_id==1?' for sale':' for rent'; ?></span> </h3>
                      <address><?php echo $iterProperty->properties_location_name; ?> Cyprus</address> 
                      <p><?php echo $iterProperty->property_description; ?></p>
                    </div>
                    <div class="amenities clearfix">
                      <ul class="pull-left">
                        <li><strong>Type:</strong><?php echo $iterProperty->properties_type_name; ?>&nbsp;&nbsp;<strong>Area:</strong> <?php echo $iterProperty->property_size; ?><sup>m2</sup></li>
                      </ul>
                      <ul class="pull-right">
                        <li><i class="fa fa-bed" title="bedrooms"></i> <?php echo $iterProperty->property_bedrooms; ?></li>
                        <li><i class="fa fa-bath" title="bathrooms"></i> <?php echo $iterProperty->property_bathrooms; ?></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach;
        else:
          echo '<div class="alert alert-info">No properties found. Please refine your filters.</div>';
        endif;
           ?>
           
           
          </div>
        </div>
      </section>
      <!-- End Properties -->
      
    </div>
    <!-- End Main -->