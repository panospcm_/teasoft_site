<div class="site-main">

<div id="content" class="site-content" role="main" style="min-height: 160px;">
			

            <!-- start of hero -->
            <section class="hero-slider hero-style-1 hero-style-2">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="slide-inner slide-bg-image" 
                            data-background="<?php echo base_url(); ?>assets/images/background/tea-soft-ware-products-horeca-shop-supermarket.webp"
                            style="background-position: bottom;">
                                <div class="container">
                                    <div data-swiper-parallax="300" class="slide-title">
                                        <h2 class="text-center">The sky is the limit</h2>
                                    </div>
                                   
                                </div>
                            </div>
                            <!-- end slide-inner -->
                        </div>
                        <!-- end swiper-slide -->


                        <div class="swiper-slide">
                            <div class="slide-inner slide-bg-image"
                             data-background="<?php echo base_url(); ?>assets/images/background/img_2.jpg" style="background-position: center;">
                                <div class="container">
                                    
                                <div data-swiper-parallax="300" class="slide-title">
                                        <h3 class="text-left">Onze mogelijkheden zijn eindeloos</h3>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <!-- end slide-inner -->
                        </div>
                        <!-- end swiper-slide -->
                    </div>
                    <!-- end swiper-wrapper -->

                    <!-- swipper controls -->
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </section>
            <!-- end of hero slider -->

			
<article id="post-430" class="post-430 page type-page status-publish hentry">
		<div class="entry-content">
		<div id="dslc-content" class="dslc-content dslc-clearfix"><div id="dslc-main">
	
	
		<div class="dslc-modules-section " style="background-image:disabled;background-repeat:repeat;background-position:left top;background-attachment:scroll;background-size:auto;border-color:rgb(244, 244, 244);border-width:1px;border-style:solid;border-top-style: hidden; border-right-style: hidden; border-bottom-style: hidden; border-left-style: hidden; margin-left:0%;margin-right:0%;margin-bottom:0px;padding-bottom:0px;padding-top:0px;padding-left:0%;padding-right:0%;">





<section class="content-section bd-bottom padding">
<div class="container">
<div class="row d-flex align-items-center">
<div class="col-lg-6 sm-padding wow fadeInRight" data-wow-delay="300ms">
<a href="<?php echo site_url('product/pos-horeca-module'); ?>">
<img src="<?php echo base_url(); ?>assets/images/background/products/horeca-module-pos-software.jpg" 
alt="tea software retail pos horeca module"
class="p-3" alt="img horeca-module-pos-software"
style="border-radius: 50px;">
</a>
</div>
<div class="col-lg-6 sm-padding wow fadeInLeft" data-wow-delay="200ms">
<div class="content-info">
<h2>Horeca Module</h2>
<p>Module met tafel beheer gebaseerd op uw eigen tafel en ruimte situatie.
Mobile Orders app: bestellingen kunnen aan tafel opgenomen worden met een smartphone of tablet en worden automatisch opgeslagen en gesplitst geprint bij de bar en keuken.</p>
<a href="<?php echo site_url('product/pos-horeca-module'); ?>" class="default-btn">Meer over</a>
</div>
</div>
</div>
</div>
</section>


<section class="content-section bd-bottom padding">
<div class="container">
<div class="row d-flex align-items-center">
<div class="col-lg-6 sm-padding wow fadeInRight" data-wow-delay="300ms">
<a href="<?php echo site_url('product/pos-shop-module'); ?>">
<img src="<?php echo base_url(); ?>assets/images/background/products/shop-module-pos-software.jpg" class="p-3" 
alt="img shop module pos software"
style="border-radius: 50px;">
</a>
</div>
<div class="col-lg-6 sm-padding wow fadeInLeft" data-wow-delay="200ms">
<div class="content-info">
<h2>Winkel Module</h2>
<p>TEA Software is een totaal oplossing voor uw winkelketen. De kassa is 100% online en realtime, transacties werken rechtstreeks op de centrale database. Dankzij het uitvalsconcept kan tijdens een internet storing lokaal doorgewerkt worden. Via internet heeft u altijd en overal de actuele gegevens bij de hand, ook thuis en op vakantie!</p>
<a href="<?php echo site_url('product/pos-shop-module'); ?>" class="default-btn">Meer over</a>
</div>
</div>
</div>
</div>
</section>



<section class="content-section bd-bottom padding">
<div class="container">
<div class="row d-flex align-items-center">
<div class="col-lg-6 sm-padding wow fadeInRight" data-wow-delay="300ms">
<img src="<?php echo base_url(); ?>assets/images/background/products/supermarket-module-pos-software.jpg" class="p-3" 
alt="img supermarkt module pos software"
style="border-radius: 50px;">
</div>
<div class="col-lg-6 sm-padding wow fadeInLeft" data-wow-delay="200ms">
<div class="content-info">
<h2>Supermarkt Module</h2>
<p>		Standaard bieden wij gratis ondersteuning bij storingen in het gebruik van de Software
Wij doen dit remotely  of op locatie
Support / Field engineers met vrijwel 10 - 15 jaar retail ervaring 


Klanten portaal met duidelijke handleiding and ‘’ how to ‘’ videos.</p>
<a href="<?php echo site_url('product/pos-supermarket-module'); ?>" class="default-btn">Meer over</a>
</div>
</div>
</div>
</div>
</section>




<section class="content-section bd-bottom padding">
<div class="container">
<div class="row d-flex align-items-center">
<div class="col-lg-6 sm-padding wow fadeInRight" data-wow-delay="300ms">
<img src="<?php echo base_url(); ?>assets/images/background/products/meer-filiale-module-pos-software.jpg" class="p-3" 
alt="img meer filiale module pos software"
style="border-radius: 50px;">
</div>
<div class="col-lg-6 sm-padding wow fadeInLeft" data-wow-delay="200ms">
<div class="content-info">
<h2>Meerdere filialen Module</h2>
<p>Wij hebben een dedicated team met veel ervaring op software, hardware en netwerk gebied. Uw appèl staat bij ons voorop, u hoeft alleen maar te bellen of mailen. 
Wij gaan direct te werk en lossen alle problemen op. <br> Het beantwoorden van korte ‘hoe-moet-dit’ vragen over de Software staat voor ons voorop.</p>
<a href="<?php echo site_url('product/meerdere-filialen-module'); ?>" class="default-btn">Meer over</a>
</div>
</div>
</div>
</div>
</section>
		



				




				</div> 
		
		
		 
		</div></div>	</div><!-- .entry-content -->
</article><!-- #post-## -->	</div><!-- #content -->
</div>



<!-- Custom Footer Styles -->
<style type="text/css">


/*! CSS Used from: https://3i98kg2c0esw237td22u9yvf-wpengine.netdna-ssl.com/wp-content/plugins/live-composer-page-builder/css/font-awesome.css?ver=1.5.5 ; media=all */
@media all{
[class*=" dslc-icon-"]{font-family:DSLCFontAwesome;font-weight:normal;font-style:normal;text-decoration:inherit;-webkit-font-smoothing:antialiased;*margin-right:.3em;}
[class*=" dslc-icon-"]:before{text-decoration:inherit;display:inline-block;speak:none;}
a [class*=" dslc-icon-"]{display:inline;}
[class*=" dslc-icon-"]{display:inline;width:auto;height:auto;line-height:normal;vertical-align:baseline;background-image:none;background-position:0% 0%;background-repeat:repeat;margin-top:0;}
}
/*! CSS Used from: https://3i98kg2c0esw237td22u9yvf-wpengine.netdna-ssl.com/wp-content/plugins/live-composer-page-builder/css/dist/frontend.min.css?ver=1.5.5 ; media=all */
@media all{
.dslc-module-front{position:relative;}
.dslc-icon{text-align:center;vertical-align:middle;}
.dslc-col{display:block;float:left;margin-right:2.12766%;min-height:1px;}
.dslc-6-col{width:48.93617%;}
.dslc-12-col{width:100%;}
.dslc-last-col{margin-right:0;}
.dslc-first-col{clear:both;}
.dslc-clearfix:before,.dslc-clearfix:after{content:" ";display:table;}
.dslc-clearfix:after{clear:both;}
.dslc-clearfix{*zoom:1;}
.dslc-post-separator{clear:both;height:1px;margin-bottom:15px;padding-bottom:15px;border-bottom:1px solid #ededed;}
@media only screen and (max-width: 767px){
.dslc-col{width:100%;margin:0;margin-bottom:30px;min-width:0px;}
.dslc-module-front.dslc-col{margin-bottom:0;}
}
@media all and (-ms-high-contrast: none){
.dslc-in-viewport-check{transform:none!important;opacity:1!important;}
}
.dslc-module-front a{text-decoration:none;}
#dslc-content .dslc-post{overflow:hidden;}
.dslc-module-front{font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;}
#dslc-content .dslc-cpt-post-thumb{margin-bottom:20px;overflow:hidden;position:relative;line-height:0;}
#dslc-content .dslc-cpt-post-thumb-inner{border:0 solid transparent;overflow:hidden;}
#dslc-content .dslc-cpt-post-thumb a{display:inline-block;max-width:100%;}
#dslc-content .dslc-cpt-post-thumb img{border-radius:0;-moz-box-shadow:none;-webkit-box-shadow:none;box-shadow:none;display:block;max-width:100%;height:auto;}
#dslc-content .dslc-cpt-post-title{margin-bottom:13px;}
#dslc-content .dslc-cpt-post-title h2{font-weight:600;font-size:15px;line-height:26px;margin:0;padding:0;}
#dslc-content .dslc-cpt-post-title h2 a{display:block;color:inherit;text-decoration:none;}
#dslc-content .dslc-cpt-post-excerpt{margin-bottom:22px;font-size:13px;line-height:22px;}
#dslc-content .dslc-cpt-post-read-more a{display:inline-block;line-height:1;text-decoration:none;border:0 solid transparent;}
.dslc-post-thumb{overflow:hidden;}
.dslc-posts-orientation-horizontal .dslc-post-thumb{float:left;margin-right:20px;width:200px;}
#dslc-content .dslc-posts-orientation-horizontal .dslc-post-main{overflow:hidden;}
@media only screen and (max-width: 767px){
#dslc-content .dslc-posts-orientation-horizontal .dslc-post-main{overflow:visible;}
}
@media only screen and (max-width: 767px){
#dslc-content div.dslc-post-thumb{width:100%;}
}
}
/*! CSS Used from: https://3i98kg2c0esw237td22u9yvf-wpengine.netdna-ssl.com/wp-content/themes/seowp/style.css?ver=1565899147 ; media=all */
@media all{
a{background:transparent;}
a:active,a:hover{outline:0;}
img{border:0;}
*,*:before,*:after{box-sizing:border-box;}
div,h2{margin:0;padding:0;}
a{text-decoration:none;line-height:inherit;}
a img{border:none;}
img{max-width:100%;height:auto;}
.dslc-post-separator{border-right-width:0;border-left-width:0;border-top-width:0;}
body #dslc-content .dslc-cpt-post-title h2{font-family:inherit;}
@media print{
img{max-width:500px;}
}
}
/*! CSS Used from: Embedded */
a{color:rgb(42, 160, 239);}
a:hover{color:rgb(93, 144, 226);}
body .dslc-module-front{font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;line-height:27px;font-weight:300;color:rgb(65, 72, 77);}
h2{font-family:'Roboto', Helvetica,Arial,'DejaVu Sans','Liberation Sans',Freesans,sans-serif;font-size:31px;line-height:38px;font-weight:300;margin-bottom:20px;color:rgb(39, 40, 43);}
/*! CSS Used from: Embedded */
#dslc-module-4426 .dslc-posts{background-color:transparent;}
#dslc-module-4426 .dslc-post-separator{margin-bottom:25px;padding-bottom:25px;border-color:rgba(0,0,0,0);border-bottom-width:1px;border-style:none;}
#dslc-module-4426 .dslc-cpt-post-thumb{text-align:left;background-color:transparent;width:40%;}
#dslc-module-4426 .dslc-cpt-post-thumb-inner{padding-left:40px;padding-right:40px;}
#dslc-module-4426 .dslc-cpt-post-thumb,#dslc-module-4426 .dslc-cpt-post-thumb-inner{border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-left-radius:4px;border-bottom-right-radius:4px;}
#dslc-module-4426 .dslc-cpt-post-main{background-color:rgb(255, 255, 255);padding-top:20px;padding-bottom:20px;text-align:left;}
#dslc-module-4426 .dslc-post{border-bottom-left-radius:4px;border-bottom-right-radius:4px;}
#dslc-module-4426 .dslc-cpt-post-title h2,#dslc-module-4426 .dslc-cpt-post-title h2 a{color:rgb(27, 26, 26);font-size:21px;font-weight:300;line-height:29px;}
#dslc-module-4426 .dslc-cpt-post-title h2:hover,#dslc-module-4426 .dslc-cpt-post-title h2 a:hover{color:rgb(88, 144, 229);}
#dslc-module-4426 .dslc-cpt-post-title{margin-bottom:10px;}
#dslc-module-4426 .dslc-cpt-post-title h2{text-transform:none;}
#dslc-module-4426 .dslc-cpt-post-excerpt{color:rgb(97, 103, 108);font-size:15px;font-weight:300;margin-bottom:22px;}
#dslc-module-4426 .dslc-cpt-post-excerpt{line-height:22px;}
#dslc-module-4426 .dslc-cpt-post-read-more a{background-color:transparent;border-radius:3px;color:rgb(42, 160, 239);font-size:14px;font-weight:300;padding-top:12px;padding-bottom:12px;padding-left:12px;padding-right:12px;border-width:1px;border-style:solid solid solid solid;border-color:rgba(136, 136, 136, 0.26);}
#dslc-module-4426 .dslc-cpt-post-read-more a:hover{background-color:#4b7bc2;color:#ffffff;border-color:rgb(75, 123, 194);}
#dslc-module-4426 .dslc-cpt-post-read-more a .dslc-icon{margin-right:5px;}
@media only screen and (min-width : 768px) and (max-width : 1024px){
#dslc-module-4426 .dslc-post-separator{margin-bottom:8px;padding-bottom:8px;}
#dslc-module-4426 .dslc-cpt-post-thumb-inner{padding-left:20px;padding-right:20px;}
#dslc-module-4426 .dslc-cpt-post-main{padding-top:19px;padding-bottom:19px;padding-left:10px;padding-right:10px;}
#dslc-module-4426 .dslc-cpt-post-title h2,#dslc-module-4426 .dslc-cpt-post-title h2 a{font-size:16px;}
#dslc-module-4426 .dslc-cpt-post-title h2,#dslc-module-4426 .dslc-cpt-post-title h2 a{line-height:22px;}
#dslc-module-4426 .dslc-cpt-post-title{margin-bottom:7px;}
#dslc-module-4426 .dslc-cpt-post-excerpt{font-size:14px;margin-bottom:22px;}
#dslc-module-4426 .dslc-cpt-post-excerpt{line-height:22px;}
#dslc-module-4426 .dslc-cpt-post-read-more a{font-size:12px;padding-top:12px;padding-bottom:12px;padding-left:12px;padding-right:12px;}
#dslc-module-4426 .dslc-cpt-post-read-more a .dslc-icon{margin-right:5px;}
}
@media only screen and ( max-width: 767px ){
#dslc-module-4426 .dslc-cpt-post-main{margin-bottom:20px;padding-top:8px;padding-bottom:8px;padding-left:5px;padding-right:5px;}
#dslc-module-4426 .dslc-post-separator{margin-bottom:1px;padding-bottom:1px;}
#dslc-module-4426 .dslc-cpt-post-thumb-inner{padding-left:10px;padding-right:10px;}
#dslc-module-4426 .dslc-cpt-post-title h2,#dslc-module-4426 .dslc-cpt-post-title h2 a{font-size:16px;line-height:23px;}
#dslc-module-4426 .dslc-cpt-post-title{margin-bottom:8px;}
#dslc-module-4426 .dslc-cpt-post-excerpt{font-size:14px;margin-bottom:16px;}
#dslc-module-4426 .dslc-cpt-post-excerpt{line-height:23px;}
#dslc-module-4426 .dslc-cpt-post-read-more a{font-size:12px;padding-top:12px;padding-bottom:12px;padding-left:12px;padding-right:12px;}
#dslc-module-4426 .dslc-cpt-post-read-more a .dslc-icon{margin-right:5px;}
}
 </style>