<?php
// var_dump($blog);
$blog = $blog[0];
$blog_next = $blog_next[0];
$blog_previous = $blog_previous[0];
$dateParts = explode("/", date('d/M/Y', $blog->blog_created_at));
// var_dump($blog_previous);
?>

<section class="header header-bg-1" style="background-image:url('<?php echo blog_main_img($blog->blog_image); ?>');">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="header-content">
          <div class="header-content-inner">
            <h1><?php echo $blog->blog_title; ?></h1>
            <div class="ui breadcrumb">
              <a href="<?php echo site_url(); ?>" class="section">Home</a>
              <div class="divider"> / </div>
              <div class="active section">Blog post</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="latest-news blog blog-single padding-top-100 mt50">
  <div class="main-container">
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="blog-detail">
          <article class="blog-item">
            <h3 class="blog-title"><?php echo $blog->blog_title; ?></h3>
            <div class="entry-meta">
              <span class="post-date"><?php echo date('d M Y', $blog->blog_created_at); ?></span>
            </div>
            <div class="blog-short-desc">
              <?php echo ($blog->blog_description); ?>
            </div>
          </article>
        </div>
        <div class="comment-social">
          
          <div class="social">
            <span>Share</span>
            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>"><i class="fa fa-facebook"></i></a>
            <a  target="_blank" href="http://twitter.com/share?text=<?php echo $blog->blog_title; ?>&url=<?php echo current_url(); ?>"><i class="fa fa-twitter"></i></a>
            <a target="_blank" href="https://plus.google.com/share?url=<?php echo current_url(); ?>" class="ion-social-googleplus"><i class="fa fa-google-plus"></i></a>
          </div>
        </div>
        <div class="col-sm-12">
              <nav>
                <ul class="pager">
                  <?php
                  if($blog_previous!=NULL): ?>
                  <li class="previous"><a href="<?php echo blog_link($blog_previous->blog_id,$blog_previous->blog_title) ; ?>"><span aria-hidden="true">←</span> Previous</a></li>
                  <?php endif; ?>
                  <?php
                  if($blog_next!=NULL): ?>
                  <li class="next"><a href="<?php echo blog_link($blog_next->blog_id,$blog_next->blog_title) ; ?>">Next <span aria-hidden="true">→</span></a></li>
                  <?php endif; ?>
                </ul>
              </nav>
            </div>
      </div>
    </div>
  </div>
</section>