
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta name="author" content="JupiWeb" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
    <title><?php echo isset($title)?$title:'JupiWeb - Web Agency, Web Development, Mobile Development'; ?></title>
	

	<meta name="keywords" content="<?php echo isset($meta_keywords)?$meta_keywords:''; ?>" />
	<meta name="description" content="<?php echo isset($meta_description)?$meta_description:'We are JupiWeb. A Web Agency from Greece focusing on Web Development, SEO Marketing, Mobile Development'; ?>" />
	<meta name="resource-type" content="document" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="robots" content="all, index, follow" />
	<meta name="googlebot" content="all, index, follow" />

	<!-- 		   
			   <?php if(isset($og_title)): ?>
			   <meta property="og:title" content="<?php echo $og_title; ?>">
			   <?php endif; ?>
			   <?php if(isset($og_descr)): ?>
			   <meta property="og:description" content="<?php echo $og_descr; ?>">
			   <?php endif; ?>
			   <?php if(isset($og_image)): ?>
			   <meta property="og:image" content="<?php echo $og_image; ?>">
			   <?php endif; ?>
			   <meta property="og:type" content="article">
				<?php if(isset($og_url)): ?>
			   <meta property="og:url" content="<?php echo $og_url; ?>">
			   <?php endif; ?> -->

	<meta property="og:url"                content="<?php echo site_url(); ?>" />
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="JupiWeb - Web Agency, Web Development, Web Marketing, Mobile Development" />
	<meta property="og:description"        content="Web Development, E-commerce, SEO, Mobile Apps" />
	<meta property="og:image"              content="<?php echo base_url(); ?>assets/images/jupiweb_logo.png" />
	<meta name="og:site_name" content="" />
	<meta name="og:locale" content="en" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:url" content="<?php echo site_url(); ?>" />
	<meta name="twitter:title" content="JupiWeb - Web Agency, Web Development, Web Marketing, Mobile Development" />
	<meta name="twitter:description" content="We are JupiWeb. A Web Agency from Greece focusing on Web Development, SEO Marketing, Mobile Development" />
	<meta name="twitter:image" content="<?php echo base_url(); ?>assets/images/jupiweb_logo.png" />

	<!-- Stylesheets -->

	<!-- <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet"> -->
	<!-- <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" title="main-css"> -->
	<!-- <link href="<?php echo base_url(); ?>assets/css/login.css" rel="stylesheet" title="main-css"> -->
 <!-- Font icons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css"/><!-- Fontawesome icons css -->
	
  <!-- CSS Global -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/admin/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/admin/responsive.css" rel="stylesheet">

	
	
</head>
<body class="royal_preloader" data-spy="scroll" data-target=".navbar" data-offset="70">



	<!-- Begin Header -->
	<header>

	</header>
	<!-- End Header -->