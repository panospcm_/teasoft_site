
<section class="page-title-section bg-img" data-overlay-dark="6" style="background-image: url(<?php echo base_url(); ?>assets/images/background/3593819.jpg);">
   <div id="particles-js"></div><!-- /.particles div -->
<div class="container">
<div class="row text-center">
   <div class="col-md-12">
      <h1>Innovation</h1>
   </div>
   
</div>
</div>
</section>


<section class="pos-section padding">
            <div class="container">


            
<div class="increase-area">
<div class="container">
<div class="row justify-content-center">
<div class="col-lg-10 text-center wow fadeInUp " data-wow-delay="0.2s">
<div class="title">
<h2>Wij zijn gespecialiseerd in op maat gemaakt e-commerce webdesign op maat, corporate webdesign en marketingoplossingen voor <span>organisaties.</span></h2>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-4 col-md-6 wow fadeInUp m-b-20" data-wow-delay="0.2s">
<div class="single-increase">
<div class="increase-text">
<h3>web design & development</h3>
<p>Onze projecten worden dagelijks door duizenden gebruikers gebruikt, daarom hebben we zorgvuldig de beste platforms, tools, ontwerp- en implementatiepraktijken van de wereld geselecteerd</p>
</div>
<img src="<?php echo base_url(); ?>assets/images/services/increase-9-1.png" alt="web design and web development">
</div>
</div>
<div class="col-lg-4 col-md-6 wow fadeInUp m-b-20" data-wow-delay="0.4s">
<div class="single-increase">
<div class="increase-text">
<h3>E-commerce & marketing solutions</h3>
<p>Bouw van online winkels op maat met complexe functies, B2B, B2C of met een autonoom magazijn, XML-verbindingen en nog veel meer!.</p>
</div>
<img src="<?php echo base_url(); ?>assets/images/services/increase-9-2.png" alt="e-commerce and marketing solutions">
</div>
</div>
<div class="col-lg-4 col-md-6 wow fadeInUp hidden-sm m-b-20" data-wow-delay="0.6s">
<div class="single-increase">
<div class="increase-text">
<h3>Custom software solutions</h3>
<p>Bouw van aangepaste websites exclusief ontworpen voor uw behoeften met de nadruk op UX, UI Web Design voor de maximale prestaties van uw online aanwezigheid!</p>
</div>
<img src="<?php echo base_url(); ?>assets/images/services/increase-9-3.png" alt="custom software solutions">
</div>
</div>
</div>
</div>
</div>


   
            </div>
        </section>		


		
<!-- Start Main Banner Area -->

<div class="container">
<section class="parallax overlay-gradient padding m-t-30 m-b-30 " data-image-src="../assets/images/photos/parallax/3.jpg">
<div class="content">
<div class=" text-center">
<div class="">
<div class="header-badge-white m-b-15">Wij komen met plezier naar u toe om een persoonlijk advies te geven voor uw Supermarkt</div>

<div class=" text-center  m-t-20" data-size="6" data-valign="">
    <div id="dslc-module-2194" class="dslc-module-handle-like-regular " data-module-id="2194" data-module="DSLC_Button" data-dslc-module-size="12" data-dslc-anim="none" data-dslc-anim-delay="0" data-dslc-anim-duration="650" data-dslc-anim-easing="ease" data-dslc-preset="none">

        <div class="text-center">
            <a href="<?php echo site_url('contact'); ?>" target="_self" onClick="" class=" food_btn">
                <span class="dslc-icon-ext-envelope"></span>
                <span>Neem dan contact met ons op</span>
            </a>
        </div>
        <!-- .dslc-button -->

    </div>
    <!-- .dslc-module -->
</div>

</div>
</div>
</div>
</section>
</div>
		<!-- <section class="pos-section bgc-3 padding">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-12">
                        <div class="pos-thmub">
                            <img src="http://demos.codexcoder.com/labartisan/html/smartsass/assets/images/pos/02.png" alt="pos-=thumb">
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="pos-content-area">
                            <div class="section-header style-3">
                                <h2>All-in-one Retail Platform</h2>
                                <p>Sell in-store, at retail events, pop-up stores and even online. With all sales channels in one POS software Hike manages all aspects of your retail business.</p>
                            </div>
                            <div class="section-wrapper">
                                <ul>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Sell in-store</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Loyalty</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Sell on-the-go</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Inventory counts</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Sell online integrated eCommerce</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Customer profiles</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Sell at Amazon marketplace</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Multi-store</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>And Mach More...</li>
                                    <li><i class="dslc-icon-ext-arrows_circle_check"></i>Reporting</li>
                                </ul>
<a href="#" class="default-btn">View Details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
 -->




 <section class="services-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="service-grids">
                            <div class="grid">
                                <i class="fi dslc-icon dslc-icon-ext-software_layout_header_3columns"></i>
                                <h3>Software & Apps</h3>
                                <p>We dagen marketing uit en verbinden deze met technologie om innovatie te stimuleren, te reageren op en zelfs uw vereisten te overtreffen. </p>
                            </div>
                            
                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-mobile"></i>
                                <h3>Mobile Apps</h3>
                                <p>Een stap verder gaan dan multi-screening en responsieve sites; gerichte inhoud voor een specifiek platform is wat we aanbieden.</p>
                            </div>

                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-desktop"></i>
                                <h3>Web Design</h3>
                                <p>Met behulp van state-of-the-art gebruikersinterfaceontwerp verbinden we de afstand tussen het menselijk brein en het digitale product. </p>
                            </div>

                        </div>
                        
                        <div class="clearfix">
</div>
                        <div class="service-grids">
                            <div class="grid">
                                <i class="fi dslc-icon dslc-icon-ext-ecommerce_cart"></i>
                                <h3>E-commerce</h3>
                                <p>Met Opencart & Woocommerce ontwikkelen we intuïtieve en inspirerende sites die een unieke winkelervaring opdoen die de verkoop escaleert.  </p>
                            </div>
                            
                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-basic_paperplane"></i>
                                <h3>Social Media Campaigns</h3>
                                <p>We vermarkten en vertellen uw verhaal op een manier die actie inspireert, leidt tot klantbetrokkenheid om onthouden en gedeeld te worden.
</p>
                            </div>

                            <div class="grid">
                                <i class="dslc-icon  dslc-icon-ext-chat"></i>
                                <h3>Support & Maintenance</h3>
                                <p>We bieden onze klanten een toegewijde, 24/7 ondersteunende service. De volledige end-to-end-oplossing omvat hosting, onderhoud of verdere ontwikkelingen.</p>
                            </div>

                        </div>
                        
                        <div class="clearfix">
</div>
                        <div class="service-grids">
                            <div class="grid">
                                <i class="fi dslc-icon dslc-icon-ext-basic_elaboration_message_check"></i>
                                <h3>SEO</h3>
                                <p>We combineren zowel technische als creatieve processen om relevante bezoekers naar uw website te trekken dankzij on-site en off-site activiteiten.
</p>
                            </div>
                            
                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-wallet2"></i>
                                <h3>Resultaatgericht</h3>
                                <p>We zullen u helpen bij het definiëren van de echte doelen van uw website - merkopbouw, leadgeneratie, online verkoop - en ervoor zorgen dat ze worden bereikt</p>
                            </div>

                            <div class="grid">
                                <i class="dslc-icon dslc-icon-ext-basic_sheet_pen"></i>
                                <h3>Customization</h3>
                                <p>Diverse koppelingen met derde partijen </p>
                            </div>

                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>