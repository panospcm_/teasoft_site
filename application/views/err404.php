<div class="section-full p-t80 p-b50">  
                <div class="container">
                    <div class="section-content">
                        <div class="row">
                        
                            <div class="col-lg-12">
                                <div class="page-notfound text-center">
                                    <form method="post">
                                        <strong class="text-uppercase">Error</strong>
                                        <strong>4<i class="fa fa-frown-o text-primary"></i>4</strong>
                                        <span>Page not found</span>
                                        <a href="<?php echo site_url(); ?>" class="site-button skew-icon-btn">GO TO HOME <i class="fa fa-angle-double-right"></i></a>
                                    </form>
                                </div>
                            </div>
                          
                            
                        </div>
                    </div>
                </div>
            </div>

