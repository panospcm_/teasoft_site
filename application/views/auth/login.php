<section class="login-outer">
   <div class="content-area">
      <div class="login-form-holder">
         <div class="inner">
            <div class="login-form">
               <h3>Klanten Portaal Login</h3>
               <?php echo isset($message)?'<div id="infoMessage" class="alert alert-info">'.$message.'</div>':'';?>
               <form action="<?php echo site_url(array("auth","login")); ?>" method="post" name="Login_Form">
                  <div class="form-group">
                     <label for="sign-in__email">Your e-mail</label>
                     <input type="text" class="form-control" name="identity" placeholder="type your e-mail..." required="" autofocus="" />
                  </div>
                  <div class="form-group">
                     <label for="sign-in__password">Your password</label>
                     <input type="password" class="form-control" name="password" placeholder="Password" required=""/>  
                  </div>
                  <div class="container-fluid">
                     <div class=" text-right">
                        <a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
                     </div>
                  </div>
                  <div class="form-group">
                     <button type="submit" class="btn btn-primary btn-block">
                     Sign In
                     </button>
                  </div>
               </form>
            </div>
         </div>
         <div class="box-hav-accnt">
            <p>Heb je geen account? <a href="<?php echo site_url('contact'); ?>">Neem contact op</a></p>
         </div>
      </div>
   </div>
</section>