<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'third_party/Braintree/Braintree.php';

/*
 *  Braintree_lib
 *	Braintree PHP SDK v3.*
 *  For Codeigniter 3.*
 */

class Braintree_lib{

		function __construct() {
			$CI = &get_instance();
			$CI->config->load('braintree', TRUE);
			$braintree = $CI->config->item('braintree');

			if($CI->config->item('is_online')==true)
				$prefix = 'production';
			else
				$prefix = 'sandbox';
			
			Braintree_Configuration::environment($braintree[$prefix.'_braintree_environment']);
			Braintree_Configuration::merchantId($braintree[$prefix.'_braintree_merchant_id']);
			Braintree_Configuration::publicKey($braintree[$prefix.'_braintree_public_key']);
			Braintree_Configuration::privateKey($braintree[$prefix.'_braintree_private_key']);
		}

    function create_client_token(){
    	$clientToken = Braintree_ClientToken::generate();

//     	    	$clientToken = Braintree_ClientToken::generate();

//     	$result = Braintree_Transaction::sale([
//   'amount' => '10.00',
//   'paymentMethodNonce' => 'fake-valid-nonce',
//   'options' => [
//     'submitForSettlement' => True
//   ]
// ]);

// $result = Braintree_Transaction::find('aq6hzesg');
    	// var_dump($clientToken);
    	// exit;
    	return $clientToken;
    }


    function get_tokenization_key(){


		$CI = &get_instance();

		if($CI->config->item('is_online')==true)
		$key = 'production_kqzq48nv_hk6z8qkmwh4szknq';
		else
		$key = "production_j3h7hvzh_hk6z8qkmwh4szknq";

		return $key;
    		
    }

    function create_sale($amount,$nonce,$POST){
    	$clientToken = Braintree_ClientToken::generate();


		$result = Braintree_Transaction::sale([
		'amount' => $amount,
		'paymentMethodNonce' => $nonce,
		'customFields' => [
		'tour_name' => $POST['tours_booking_related_tour_slug'],
		'tour_agent_coupon_code' => $POST['tours_booking_coupon_code'],
		'tour_date' => $POST['tours_booking_date'],
		'tour_people' => 'Adults:'.$POST['tours_booking_people_num'].'-Children:'.$POST['tours_booking_children_num'],
  		],
  		'billing' =>
                [
                    'firstName' => $POST['tours_booking_first_name'],
                    'lastName' => $POST['tours_booking_last_name'],
                    'company' => $POST['tours_booking_email'],

            ],
		'descriptor' => [
	    'phone' => $POST['tours_booking_phone']
  		],
		// 'paymentMethodNonce' => 'fake-valid-nonce',
		'options' => [
		'submitForSettlement' => True
		]
		]);

// $result = Braintree_Transaction::find('aq6hzesg');
    	// var_dump($result);
    	// exit;
    	return $result;
    }
}
